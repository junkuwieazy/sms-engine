<?
Class SMS_System {
	 var $url = "";
	 var $user = "";
	 var $pwd = "";
	 var $sid = "";
	 var $poststring = "";
	 var $isPost = 1;
	 var $isReturnHeader = 0;
	 var $auth = 0;
	 var $isCustom = 0;
	 var $header = '';

	function curl(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_POST, $this->isPost);
		if($this->isPost) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->poststring);
		}

		curl_setopt($ch, CURLOPT_HEADER, $this->isReturnHeader);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // drop connection after 2 seconds
		if($this->auth) {
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->pwd);
		}
		if($this->isCustom) {
			 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->header);
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		$returned = curl_exec($ch);
		$error    = curl_error($ch);
		curl_close ($ch);

		return $returned;
	}
}
?>