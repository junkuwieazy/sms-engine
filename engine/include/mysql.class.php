<?php
Class DB {
	
	/*
	Ini adalah constructor dari kelas Db, fungsi ini otomatis akan di jalankan
	Semua setting username dan password untuk ke database diletakkan di sini
	*/
	function DB($def_server = "", $def_username = "" , $def_password = "", $def_dbName = "") {

		// Jangan edit code dibawah ini ^^
		$this->userName = $def_username;
		$this->password = $def_password;
		$this->server = $def_server;
		$this->dbName = $def_dbName;
		$this->connected = false;
		$this->counter = 0;
		$this->connectDb();
	}
	
	/*
	Fungsi ini dipanggil dari constructor, jadi ketika class di buat
	langsung di hubungkan dengan database, fungsi ini sebenernya tidak perlu
	dipanggil
	Contoh pembuatan Class :
	$db = new Db();
	*/
	function connectDb() {
		if (!$this->handle = mysql_connect($this->server , $this->userName , $this->password)) {
			$this->errMsg("Connecting failed.");
			return false;
		}
		$connected = true;
		return true;
	}
	
	/*
	Fungsi untuk menutup koneksi ke database, biasanya di letakkan di bagian
	bawah dari halaman, namun fungsi ini tidak harus dipanggil, karena
	php otomatis akan menutup koneksi ketika tidak diperlukan lagi
	*/
	function closeDb() {
		mysql_close($this->handle);
		$connected = false;
	}
	
	function start_transaction() {
		if (!$result = mysql_query("start transaction" , $this->handle)) {
			$error = mysql_error ($this->handle);
			$this->errMsg("Query '<i>$sql</i>' failed transaction <i>$this->dbName</i>. <br><b>Message:</b> <i>$error</i>.");
		}
	}
	
	function commit() {
		if (!$result = mysql_query("commit" , $this->handle)) {
			$error = mysql_error ($this->handle);
			$this->errMsg("Query '<i>$sql</i>' failed on commit <i>$this->dbName</i>. <br><b>Message:</b> <i>$error</i>.");
		}
	}
	
	function rollback() {
		if (!$result = mysql_query("rollback" , $this->handle)) {
			$error = mysql_error ($this->handle);
			$this->errMsg("Query '<i>$sql</i>' failed on rollback <i>$this->dbName</i>. <br><b>Message:</b> <i>$error</i>.");
		}
	}
	
	/*
	Fungsi ini digunakan untuk memasukan query seperti UPDATE, INSERT, DELETE
	dan semua fungsi-fungsi SQL lain nya.
	Parameter :
	$sql : diisi dengan perintah SQL nya
	Contoh :
	$result = $db->query("SELECT * FROM phonebook");
	*/
	function query($sql) {
		if (!mysql_select_db($this->dbName, $this->handle)) {
			$this->errMsg("Selecting DB failed.");
			return NULL;
		}
		if (!$result = mysql_query($sql , $this->handle)) {
			$error = mysql_error ($this->handle);
			$this->errMsg("Query '<i>$sql</i>' failed on database <i>$this->dbName</i>. <br><b>Message:</b> <i>$error</i>.");
			return $result;
		}
		$this->counter++;
		return $result;
	}
	
	/*
	Fungsi ini digunakan untuk mendapatkan nilai per baris dari query yang
	sudah di lakukan dari fungsi di atas. Fungsi ini akan mengembalikan
	result set jika berhasil dan tidak mengembalikkan pesan ERR jika $result = NULL
	Parameter :
	$result : di isi dengan handler yang di dapat dari query($sql)
	Contoh :
	//Untuk mengambil semua data
	while ($row = $db->fetch($result)) {
	//lakukan sesuatu di sini
	};
	//Untuk mengambil data pertama
	$row = $db->fetch($result);
	*/
	function fetch($result) {
		if ($result == NULL) {
			$this->errMsg("Fetching failed, empty result or incorrect result identifier.");
			return NULL;
		}
		return mysql_fetch_array($result);
	}
	
	/*
	Fungsi ini digunakan untuk mengambil 1 cell paling kiri atas dari hasil
	query
	*/
	function getCell($sql) {
		$result = $this->query($sql);
		if ($this->recordCount($result) > 0)
		return mysql_result($result , 0 , 0);
		else
		return NULL;
	}
	
	/*
	Fungsi ini digunakan untuk mengambil 1 record data dari hasil query ke dalam
	array
	*/
	function getRow($sql) {
		$result = $this->query($sql);
		if ($this->recordCount($result) > 0)
		return $this->fetch($result);
		else
		return NULL;
	}
	
	function errMsg($msg) {
		die("<br><b>DBERR:</b> $msg<br>");
	}
	
	// Alias for mysql_num_rows()
	function recordCount($result) {
		return ($result == NULL) ? 0 : mysql_num_rows($result);
	}
	// Alias for mysql_affected_rows()
	function affectedRows() {
		return mysql_affected_rows($this->handle);
	}
	// Alias for mysql_insert_id()
	function insertId() {
		return mysql_insert_id($this->handle);
	}
	function ping() {
		return mysql_ping($this->handle);
	}
}
?>