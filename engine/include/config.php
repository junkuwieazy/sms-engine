<?php

$config['dbHost']		= 'localhost';
$config['dbPort']		= '3306';
$config['dbUser']		= 'smsgw';
$config['dbPass']		= 'DBt3rb41k';
$config['dbName']		= 'mp_system';
$config['dbType']		= 'mysql';

$config['dbHostlocal']		= 'localhost';
$config['dbPortlocal']		= '3306';
$config['dbUserlocal']		= 'smsgw';
$config['dbPasslocal']		= 'DBt3rb41k';
$config['dbNamelocal']		= 'mp_system';
$config['dbTypelocal']		= 'mysql';

$config_app['DBHost']			= 'localhost';
$config_app['DBPort']			= '3306';
$config_app['DBUser']			= 'smsgw';
$config_app['DBPassword']		= 'DBt3rb41k';
$config_app['DBType']			= 'mysql';
$config_app['DBName']			= 'app_sms';

$config_cd['DBHost']			= 'localhost';
$config_cd['DBPort']			= '3306';
$config_cd['DBUser']			= 'smsgw';
$config_cd['DBPassword']		= 'DBt3rb41k';
$config_cd['DBType']			= 'mysql';
$config_cd['DBName']			= 'content_download';

$config['base_path']	= "/var/www/sms/engine";

$config['allow_ip'] = array(
						"localhost", "127.0.0.1", "::1" , "202.43.73.202", "202.43.73.203", "202.43.73.204", "202.43.73.205", "202.43.73.206",
						"202.43.73.250", "202.43.73.251", "202.43.73.252", "202.43.73.253", "202.43.73.254","103.29.214.133","103.29.214.195","103.29.214.183",
						"103.29.214.114","103.29.214.115","103.29.214.116","103.29.214.117","103.29.214.118","103.29.214.133"
						);

$config['shortcode'] = array('95799' => '95799', '95899' => '95899');
$config['telco'] = array('xl' => 'XL', 'isat' => 'Indosat');

$config['share'] = array('tsel' => 0.6 , 'isat' => 0.5 , 'xl' => 0.4 , 'flexi' => 0.5 , 'esia' => 0.5 , 'mob8' => 0.5 , 'hutch' => 0.5 , 'axis' => 0.5 , 'sakti' => 0.45);

$config['hari'] = array('01' => '01' , '02' => '02' , '03' => '03' , '04' => '04' , '05' => '05' , '06' => '06' , '07' => '07' , '08' => '08' , '09' => '09' , '10' => '10' , '11' => '11' , '12' => '12' , '13' => '13' , '14' => '14' , '15' => '15' , '16' => '16' , '17' => '17' , '18' => '18' , '19' => '19' , '20' => '20' , '21' => '21' , '22' => '22' , '23' => '23' , '24' => '24' , '25' => '25' , '26' => '26' , '27' => '27' , '28' => '28' , '29' => '29' , '30' => '30' , '31'=> '31'); 

$config['bulan'] = array('01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12');
$config['tahun'] = array('2016' => '2016', '2017' => '2017', '2018' => '2018');

$config['month'] = array('01' => 'Januari' , '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember');
$config['year'] = array('2016' => '2016','2017' => '2017','2018' => '2018');

$config['is_push'] = array('1' => 'Yes' , '0' => 'No');
$config['push_time'] = array('01' => '01' , '02' => '02' , '03' => '03' , '04' => '04' , '05' => '05' , '06' => '06' , '07' => '07' , '08' => '08' , '09' => '09' , '10' => '10' , '11' => '11' , '12' => '12' , '13' => '13' , '14' => '14' , '15' => '15' , '16' => '16' , '17' => '17' , '18' => '18' , '19' => '19' , '20' => '20' , '21' => '21' , '22' => '22' , '23' => '23');

$config['is_retry'] = array('1' => 'Yes' , '0' => 'No');
$config['retry_time'] = array('01' => '01' , '02' => '02' , '03' => '03' , '04' => '04' , '05' => '05' , '06' => '06' , '07' => '07' , '08' => '08' , '09' => '09' , '10' => '10' , '11' => '11' , '12' => '12' , '13' => '13' , '14' => '14' , '15' => '15' , '16' => '16' , '17' => '17' , '18' => '18' , '19' => '19' , '20' => '20' , '21' => '21' , '22' => '22' , '23' => '23');
$config['retry_type'] = array('today' => 'today' , 'yesterday' => 'yesterday');

$config['subject'] = array('reg' => 'Reg','unreg' => 'Unreg','pull' => 'Pull','push' => 'Push');
$config['mt_type'] = array('1' => 'Text','2' => 'Wappush','3' => 'Smart Messaging');

$config['status'] = array('1' => 'Aktif','-1' => 'Non Aktif');

$config['spool_mo_tsel']	= array ( 1=>"/spool/mo/tsel/1",2=>"/spool/mo/tsel/2",3=>"/spool/mo/tsel/3",4=>"/spool/mo/tsel/4",5=>"/spool/mo/tsel/5",6=>"/spool/mo/tsel/6",7=>"/spool/mo/tsel/7",8=>"/spool/mo/tsel/8",9=>"/spool/mo/tsel/9",10=>"/spool/mo/tsel/10" );
$config['spool_mo_isat']	= array ( 1=>"/spool/mo/isat/1",2=>"/spool/mo/isat/2",3=>"/spool/mo/isat/3",4=>"/spool/mo/isat/4",5=>"/spool/mo/isat/5",6=>"/spool/mo/isat/6",7=>"/spool/mo/isat/7",8=>"/spool/mo/isat/8",9=>"/spool/mo/isat/9",10=>"/spool/mo/isat/10" );
$config['spool_mo_xl']		= array ( 1=>"/spool/mo/xl/1",2=>"/spool/mo/xl/2",3=>"/spool/mo/xl/3",4=>"/spool/mo/xl/4",5=>"/spool/mo/xl/5" );
$config['spool_mo_hutch']	= array ( 1=>"/spool/mo/hutch/1",2=>"/spool/mo/hutch/2" );

$config['spool_mt_tsel']	= array ( 1=>"/spool/mt/tsel/1",2=>"/spool/mt/tsel/2",3=>"/spool/mt/tsel/3",4=>"/spool/mt/tsel/4",5=>"/spool/mt/tsel/5",6=>"/spool/mt/tsel/6",7=>"/spool/mt/tsel/7",8=>"/spool/mt/tsel/8",9=>"/spool/mt/tsel/9",10=>"/spool/mt/tsel/10",11=>"/spool/mt/tsel/11",12=>"/spool/mt/tsel/12",13=>"/spool/mt/tsel/13",14=>"/spool/mt/tsel/14",15=>"/spool/mt/tsel/15",16=>"/spool/mt/tsel/16",17=>"/spool/mt/tsel/17",18=>"/spool/mt/tsel/18",19=>"/spool/mt/tsel/19",20=>"/spool/mt/tsel/20",21=>"/spool/mt/tsel/21",22=>"/spool/mt/tsel/22",23=>"/spool/mt/tsel/23",24=>"/spool/mt/tsel/24",25=>"/spool/mt/tsel/25",26=>"/spool/mt/tsel/26",27=>"/spool/mt/tsel/27",28=>"/spool/mt/tsel/28",29=>"/spool/mt/tsel/29",30=>"/spool/mt/tsel/30",31=>"/spool/mt/tsel/31",32=>"/spool/mt/tsel/32",33=>"/spool/mt/tsel/33",34=>"/spool/mt/tsel/34",35=>"/spool/mt/tsel/35",36=>"/spool/mt/tsel/36",37=>"/spool/mt/tsel/37",38=>"/spool/mt/tsel/38",39=>"/spool/mt/tsel/39",40=>"/spool/mt/tsel/40",41=>"/spool/mt/tsel/41",42=>"/spool/mt/tsel/42",43=>"/spool/mt/tsel/43",44=>"/spool/mt/tsel/44",45=>"/spool/mt/tsel/45",46=>"/spool/mt/tsel/46",47=>"/spool/mt/tsel/47",48=>"/spool/mt/tsel/48",49=>"/spool/mt/tsel/49",50=>"/spool/mt/tsel/50" );
$config['spool_mt_isat']	= array ( 1=>"/spool/mt/isat/1",2=>"/spool/mt/isat/2",3=>"/spool/mt/isat/3",4=>"/spool/mt/isat/4",5=>"/spool/mt/isat/5",6=>"/spool/mt/isat/6",7=>"/spool/mt/isat/7",8=>"/spool/mt/isat/8",9=>"/spool/mt/isat/9",10=>"/spool/mt/isat/10",11=>"/spool/mt/isat/11",12=>"/spool/mt/isat/12",13=>"/spool/mt/isat/13",14=>"/spool/mt/isat/14",15=>"/spool/mt/isat/15",16=>"/spool/mt/isat/16",17=>"/spool/mt/isat/17",18=>"/spool/mt/isat/18",19=>"/spool/mt/isat/19",20=>"/spool/mt/isat/20",21=>"/spool/mt/isat/21",22=>"/spool/mt/isat/22",23=>"/spool/mt/isat/23",24=>"/spool/mt/isat/24",25=>"/spool/mt/isat/25",26=>"/spool/mt/isat/26",27=>"/spool/mt/isat/27",28=>"/spool/mt/isat/28",29=>"/spool/mt/isat/29",30=>"/spool/mt/isat/30",31=>"/spool/mt/isat/31",32=>"/spool/mt/isat/32",33=>"/spool/mt/isat/33",34=>"/spool/mt/isat/34",35=>"/spool/mt/isat/35",36=>"/spool/mt/isat/36",37=>"/spool/mt/isat/37",38=>"/spool/mt/isat/38",39=>"/spool/mt/isat/39",40=>"/spool/mt/isat/40",41=>"/spool/mt/isat/41",42=>"/spool/mt/isat/42",43=>"/spool/mt/isat/43",44=>"/spool/mt/isat/44",45=>"/spool/mt/isat/45",46=>"/spool/mt/isat/46",47=>"/spool/mt/isat/47",48=>"/spool/mt/isat/48",49=>"/spool/mt/isat/49",50=>"/spool/mt/isat/50",51=>"/spool/mt/isat/51",52=>"/spool/mt/isat/52",53=>"/spool/mt/isat/53",54=>"/spool/mt/isat/54",55=>"/spool/mt/isat/55",56=>"/spool/mt/isat/56",57=>"/spool/mt/isat/57",58=>"/spool/mt/isat/58",59=>"/spool/mt/isat/59",60=>"/spool/mt/isat/60",61=>"/spool/mt/isat/61",62=>"/spool/mt/isat/62",63=>"/spool/mt/isat/63",64=>"/spool/mt/isat/64",65=>"/spool/mt/isat/65",66=>"/spool/mt/isat/66",67=>"/spool/mt/isat/67",68=>"/spool/mt/isat/68",69=>"/spool/mt/isat/69",70=>"/spool/mt/isat/70",71=>"/spool/mt/isat/71",72=>"/spool/mt/isat/72",73=>"/spool/mt/isat/73",74=>"/spool/mt/isat/74",75=>"/spool/mt/isat/75",76=>"/spool/mt/isat/76",77=>"/spool/mt/isat/77",78=>"/spool/mt/isat/78",79=>"/spool/mt/isat/79",80=>"/spool/mt/isat/80",81=>"/spool/mt/isat/81",82=>"/spool/mt/isat/82",83=>"/spool/mt/isat/83",84=>"/spool/mt/isat/84",85=>"/spool/mt/isat/85",86=>"/spool/mt/isat/86",87=>"/spool/mt/isat/87",88=>"/spool/mt/isat/88",89=>"/spool/mt/isat/89",90=>"/spool/mt/isat/90",91=>"/spool/mt/isat/91",92=>"/spool/mt/isat/92",93=>"/spool/mt/isat/93",94=>"/spool/mt/isat/94",95=>"/spool/mt/isat/95",96=>"/spool/mt/isat/96",97=>"/spool/mt/isat/97",98=>"/spool/mt/isat/98",99=>"/spool/mt/isat/99",100=>"/spool/mt/isat/100",101=>"/spool/mt/isat/101",102=>"/spool/mt/isat/102",103=>"/spool/mt/isat/103",104=>"/spool/mt/isat/104",105=>"/spool/mt/isat/105",106=>"/spool/mt/isat/106",107=>"/spool/mt/isat/107",108=>"/spool/mt/isat/108",109=>"/spool/mt/isat/109",110=>"/spool/mt/isat/110",111=>"/spool/mt/isat/111",112=>"/spool/mt/isat/112",113=>"/spool/mt/isat/113",114=>"/spool/mt/isat/114",115=>"/spool/mt/isat/115",116=>"/spool/mt/isat/116",117=>"/spool/mt/isat/117",118=>"/spool/mt/isat/118",119=>"/spool/mt/isat/119",120=>"/spool/mt/isat/120",121=>"/spool/mt/isat/121",122=>"/spool/mt/isat/122",123=>"/spool/mt/isat/123",124=>"/spool/mt/isat/124",125=>"/spool/mt/isat/125",126=>"/spool/mt/isat/126",127=>"/spool/mt/isat/127",128=>"/spool/mt/isat/128",129=>"/spool/mt/isat/129",130=>"/spool/mt/isat/130",131=>"/spool/mt/isat/131",132=>"/spool/mt/isat/132",133=>"/spool/mt/isat/133",134=>"/spool/mt/isat/134",135=>"/spool/mt/isat/135",136=>"/spool/mt/isat/136",137=>"/spool/mt/isat/137",138=>"/spool/mt/isat/138",139=>"/spool/mt/isat/139",140=>"/spool/mt/isat/140",141=>"/spool/mt/isat/141",142=>"/spool/mt/isat/142",143=>"/spool/mt/isat/143",144=>"/spool/mt/isat/144",145=>"/spool/mt/isat/145",146=>"/spool/mt/isat/146",147=>"/spool/mt/isat/147",148=>"/spool/mt/isat/148",149=>"/spool/mt/isat/149",150=>"/spool/mt/isat/150" );
$config['spool_mt_xl']		= array ( 1=>"/spool/mt/xl/1",2=>"/spool/mt/xl/2",3=>"/spool/mt/xl/3",4=>"/spool/mt/xl/4",5=>"/spool/mt/xl/5",6=>"/spool/mt/xl/6",7=>"/spool/mt/xl/7",8=>"/spool/mt/xl/8",9=>"/spool/mt/xl/9",10=>"/spool/mt/xl/10",11=>"/spool/mt/xl/11",12=>"/spool/mt/xl/12",13=>"/spool/mt/xl/13",14=>"/spool/mt/xl/14",15=>"/spool/mt/xl/15",16=>"/spool/mt/xl/16",17=>"/spool/mt/xl/17",18=>"/spool/mt/xl/18",19=>"/spool/mt/xl/19",20=>"/spool/mt/xl/20",21=>"/spool/mt/xl/21",22=>"/spool/mt/xl/22",23=>"/spool/mt/xl/23",24=>"/spool/mt/xl/24",25=>"/spool/mt/xl/25",26=>"/spool/mt/xl/26",27=>"/spool/mt/xl/27",28=>"/spool/mt/xl/28",29=>"/spool/mt/xl/29",30=>"/spool/mt/xl/30",31=>"/spool/mt/xl/31",32=>"/spool/mt/xl/32",33=>"/spool/mt/xl/33",34=>"/spool/mt/xl/34",35=>"/spool/mt/xl/35",36=>"/spool/mt/xl/36",37=>"/spool/mt/xl/37",38=>"/spool/mt/xl/38",39=>"/spool/mt/xl/39",40=>"/spool/mt/xl/40",41=>"/spool/mt/xl/41",42=>"/spool/mt/xl/42",43=>"/spool/mt/xl/43",44=>"/spool/mt/xl/44",45=>"/spool/mt/xl/45",46=>"/spool/mt/xl/46",47=>"/spool/mt/xl/47",48=>"/spool/mt/xl/48",49=>"/spool/mt/xl/49",50=>"/spool/mt/xl/50" );
$config['spool_mt_hutch']	= array ( 1=>"/spool/mt/hutch/1",2=>"/spool/mt/hutch/2" );

$config['spool_retry_tsel']	= array ( 1=>"/spool/mt/tsel/retry/1",2=>"/spool/mt/tsel/retry/2",3=>"/spool/mt/tsel/retry/3" );
$config['spool_retry_isat']	= array ( 1=>"/spool/mt/isat/retry/1",2=>"/spool/mt/isat/retry/2",3=>"/spool/mt/isat/retry/3",4=>"/spool/mt/isat/retry/4",5=>"/spool/mt/isat/retry/5",6=>"/spool/mt/isat/retry/6",7=>"/spool/mt/isat/retry/7",8=>"/spool/mt/isat/retry/8",9=>"/spool/mt/isat/retry/9",10=>"/spool/mt/isat/retry/10",11=>"/spool/mt/isat/retry/11",12=>"/spool/mt/isat/retry/12",13=>"/spool/mt/isat/retry/13",14=>"/spool/mt/isat/retry/14",15=>"/spool/mt/isat/retry/15" );
$config['spool_retry_xl']	= array ( 1=>"/spool/mt/xl/retry/1",2=>"/spool/mt/xl/retry/2",3=>"/spool/mt/xl/retry/3",4=>"/spool/mt/xl/retry/4",5=>"/spool/mt/xl/retry/5" );
$config['spool_retry_hutch']= array ( 1=>"/spool/mt/hutch/retry/1",2=>"/spool/mt/hutch/retry/2" );

$config['spool_live_traffic']		= array ( 1=>"/spool/live_traffic/mo/1",2=>"/spool/live_traffic/mo/2",3=>"/spool/live_traffic/mo/3",4=>"/spool/live_traffic/mo/4",5=>"/spool/live_traffic/mo/5",6=>"/spool/live_traffic/mo/6",7=>"/spool/live_traffic/mo/7",8=>"/spool/live_traffic/mo/8",9=>"/spool/live_traffic/mo/9",10=>"/spool/live_traffic/mo/10" );
$config['spool_live_traffic_mt']	= array ( 1=>"/spool/live_traffic/mt/1",2=>"/spool/live_traffic/mt/2",3=>"/spool/live_traffic/mt/3",4=>"/spool/live_traffic/mt/4",5=>"/spool/live_traffic/mt/5",6=>"/spool/live_traffic/mt/6",7=>"/spool/live_traffic/mt/7",8=>"/spool/live_traffic/mt/8",9=>"/spool/live_traffic/mt/9",10=>"/spool/live_traffic/mt/10",11=>"/spool/live_traffic/mt/11",12=>"/spool/live_traffic/mt/12",13=>"/spool/live_traffic/mt/13",14=>"/spool/live_traffic/mt/14",15=>"/spool/live_traffic/mt/15",16=>"/spool/live_traffic/mt/16",17=>"/spool/live_traffic/mt/17",18=>"/spool/live_traffic/mt/18",19=>"/spool/live_traffic/mt/19",20=>"/spool/live_traffic/mt/20" );
$config['spool_live_traffic_dr']	= array ( 1=>"/spool/live_traffic/dr/1",2=>"/spool/live_traffic/dr/2",3=>"/spool/live_traffic/dr/3",4=>"/spool/live_traffic/dr/4",5=>"/spool/live_traffic/dr/5",6=>"/spool/live_traffic/dr/6",7=>"/spool/live_traffic/dr/7",8=>"/spool/live_traffic/dr/8",9=>"/spool/live_traffic/dr/9",10=>"/spool/live_traffic/dr/10" );

$config['read_mo_tsel_path']	= $config['base_path']."/spool/mo/tsel/";
$config['read_mo_isat_path']	= $config['base_path']."/spool/mo/isat/";
$config['read_mo_xl_path']		= $config['base_path']."/spool/mo/xl/";
$config['read_mo_hutch_path']	= $config['base_path']."/spool/mo/hutch/";

$config['read_mt_tsel_path']	= $config['base_path']."/spool/mt/tsel/";
$config['read_mt_isat_path']	= $config['base_path']."/spool/mt/isat/";
$config['read_mt_xl_path']		= $config['base_path']."/spool/mt/xl/";
$config['read_mt_hutch_path']	= $config['base_path']."/spool/mt/hutch/";

$config['read_retry_tsel_path']	= $config['base_path']."/spool/mt/tsel/retry/";
$config['read_retry_isat_path']	= $config['base_path']."/spool/mt/isat/retry/";
$config['read_retry_xl_path']	= $config['base_path']."/spool/mt/xl/retry/";
$config['read_retry_hutch_path']= $config['base_path']."/spool/mt/hutch/retry/";

$config['read_live_traffic_path']		= $config['base_path']."/spool/live_traffic/mo/";
$config['read_live_traffic_mt_path']	= $config['base_path']."/spool/live_traffic/mt/";
$config['read_live_traffic_dr_path']	= $config['base_path']."/spool/live_traffic/dr/";

$config['ack_picker_tsel_path']			= $config['base_path']."/spool/ack_process/tsel/";
$config['ack_picker_isat_path']			= $config['base_path']."/spool/ack_process/isat/";
$config['ack_picker_xl_path']			= $config['base_path']."/spool/ack_process/xl/";
$config['ack_picker_hutch_path']		= $config['base_path']."/spool/ack_process/hutch/";

$config['dr_picker_tsel_path']			= $config['base_path']."/spool/dr_process/tsel/";
$config['dr_picker_isat_path']			= $config['base_path']."/spool/dr_process/isat/";
$config['dr_picker_xl_path']			= $config['base_path']."/spool/dr_process/xl/";
$config['dr_picker_hutch_path']			= $config['base_path']."/spool/dr_process/hutch/";

$config['spool_ack_process_tsel']		= array ( 1=>"/spool/ack_process/tsel/1",2=>"/spool/ack_process/tsel/2",3=>"/spool/ack_process/tsel/3",4=>"/spool/ack_process/tsel/4",5=>"/spool/ack_process/tsel/5",6=>"/spool/ack_process/tsel/6",7=>"/spool/ack_process/tsel/7",8=>"/spool/ack_process/tsel/8",9=>"/spool/ack_process/tsel/9",10=>"/spool/ack_process/tsel/10" );

$config['spool_dr_process_isat']		= array ( 1=>"/spool/dr_process/isat/1",2=>"/spool/dr_process/isat/2",3=>"/spool/dr_process/isat/3",4=>"/spool/dr_process/isat/4",5=>"/spool/dr_process/isat/5",6=>"/spool/dr_process/isat/6",7=>"/spool/dr_process/isat/7",8=>"/spool/dr_process/isat/8",9=>"/spool/dr_process/isat/9",10=>"/spool/dr_process/isat/10",11=>"/spool/dr_process/isat/11",12=>"/spool/dr_process/isat/12",13=>"/spool/dr_process/isat/13",14=>"/spool/dr_process/isat/14",15=>"/spool/dr_process/isat/15",16=>"/spool/dr_process/isat/16",17=>"/spool/dr_process/isat/17",18=>"/spool/dr_process/isat/18",19=>"/spool/dr_process/isat/19",20=>"/spool/dr_process/isat/20" );
$config['spool_dr_process_xl']			= array ( 1=>"/spool/dr_process/xl/1",2=>"/spool/dr_process/xl/2",3=>"/spool/dr_process/xl/3",4=>"/spool/dr_process/xl/4",5=>"/spool/dr_process/xl/5",6=>"/spool/dr_process/xl/6",7=>"/spool/dr_process/xl/7",8=>"/spool/dr_process/xl/8",9=>"/spool/dr_process/xl/9",10=>"/spool/dr_process/xl/10",11=>"/spool/dr_process/xl/11",12=>"/spool/dr_process/xl/12",13=>"/spool/dr_process/xl/13",14=>"/spool/dr_process/xl/14",15=>"/spool/dr_process/xl/15",16=>"/spool/dr_process/xl/16",17=>"/spool/dr_process/xl/17",18=>"/spool/dr_process/xl/18",19=>"/spool/dr_process/xl/19",20=>"/spool/dr_process/xl/20" );
$config['spool_dr_process_hutch']		= array ( 1=>"/spool/dr_process/hutch/1",2=>"/spool/dr_process/hutch/2" );

$config['url_app']	= "http://localhost:8319/sms/app/app_incoming/app_incoming.php";

$config['read_mo_tsel_monitoring_path']		= $config['base_path']."/monitoring/read_mo_tsel/";
$config['read_mo_isat_monitoring_path']		= $config['base_path']."/monitoring/read_mo_isat/";
$config['read_mo_xl_monitoring_path']		= $config['base_path']."/monitoring/read_mo_xl/";
$config['read_mo_hutch_monitoring_path']	= $config['base_path']."/monitoring/read_mo_hutch/";

$config['live_traffic_monitoring_path']			= $config['base_path']."/monitoring/live_traffic/mo/";
$config['live_traffic_mt_monitoring_path']		= $config['base_path']."/monitoring/live_traffic/mt/";
$config['live_traffic_dr_monitoring_path']		= $config['base_path']."/monitoring/live_traffic/dr/";

$config['mt_send_tsel_monitoring_path']			= $config['base_path']."/monitoring/mt_send_tsel/";
$config['mt_send_isat_monitoring_path']			= $config['base_path']."/monitoring/mt_send_isat/";
$config['mt_send_xl_monitoring_path']			= $config['base_path']."/monitoring/mt_send_xl/";
$config['mt_send_hutch_monitoring_path']		= $config['base_path']."/monitoring/mt_send_hutch/";

$config['ack_process_tsel_monitoring_path']			= $config['base_path']."/monitoring/ack_process/tsel/";
$config['ack_process_isat_monitoring_path']			= $config['base_path']."/monitoring/ack_process/isat/";
$config['ack_process_xl_monitoring_path']			= $config['base_path']."/monitoring/ack_process/xl/";
$config['ack_process_hutch_monitoring_path']		= $config['base_path']."/monitoring/ack_process/hutch/";

$config['dr_process_tsel_monitoring_path']			= $config['base_path']."/monitoring/dr_process/tsel/";
$config['dr_process_isat_monitoring_path']			= $config['base_path']."/monitoring/dr_process/isat/";
$config['dr_process_xl_monitoring_path']			= $config['base_path']."/monitoring/dr_process/xl/";
$config['dr_process_hutch_monitoring_path']			= $config['base_path']."/monitoring/dr_process/hutch/";

$path_tsel 	= "http://localhost/sms/engine/monitoring/";
$path_isat 	= "http://localhost/sms/engine/monitoring/";
$path_xl 	= "http://localhost/sms/engine/monitoring/";
$path_hutch 	= "http://localhost/sms/engine/monitoring/";

?>
