<?php

class libs_xml2 {

    private $domObj = NULL;
    private $xpObj  = NULL;
    private $xml    = FALSE;

    private static $instance = NULL;

    private function __construct() {}

    public static function getInstance($xmlVersion = '1.0', $encoding='ISO-8859-1') {

        if(self::$instance == NULL) {

            self::$instance         = new self();
            self::$instance->domObj = new DomDocument($xmlVersion, $encoding);
        }

        return self::$instance;
    }

	public function readXML($string) {

        $this->xml = $this->domObj->loadXML($string);

        if($this->xml == TRUE) $this->xpObj = new DOMXPath(self::$instance->domObj);

        return $this->xml;
    }


	public function getSingleNodeValue($string, $index = 0) {

        if($this->xml == FALSE) return FALSE;

        $node = $this->xpObj->query('//' . $string);

        if($node->length == 0) return FALSE;

        return $node->item((int) $index)->nodeValue;

    }
}

?>