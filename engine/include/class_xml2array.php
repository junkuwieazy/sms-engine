<?
class xml2Array {
   
   var $arrOutput = array();
   var $resParser;
   var $strXmlData;
   
   function parse($strInputXML) {
   
           $this->resParser = xml_parser_create ();
           xml_set_object($this->resParser,$this);
           xml_set_element_handler($this->resParser, "tagOpen", "tagClosed");
           
           xml_set_character_data_handler($this->resParser, "tagData");
       
           $this->strXmlData = xml_parse($this->resParser,$strInputXML );
           if(!$this->strXmlData) {
               die(sprintf("XML error: %s at line %d",
           xml_error_string(xml_get_error_code($this->resParser)),
           xml_get_current_line_number($this->resParser)));
           }
                           
           xml_parser_free($this->resParser);
           
           return $this->arrOutput;
   }
   function tagOpen($parser, $name, $attrs) {
		$this->arrOutput[$name] = "";
		$this->name = $name;
   }
   
   function tagData($parser, $tagData) {
       if(trim($tagData)) {
			$this->arrOutput[$this->name] = $tagData;
       }
   }
   
   function tagClosed($parser, $name) {

   }
}
?>