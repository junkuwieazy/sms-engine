<?php

#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: dr_picker.php
#			FUNCTION			: Read SMS from File, process to application
#			MODIFIED			: October, 22 2008
#
#################################################################################

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
require($file_path."include/mysql.class.php");
include($file_path."include/class_aiom.php");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$i = 1;

$path = $config['dr_picker_isat_path']."unsent/";

while(true){

	if (!$db->ping()) {
		$db->closeDb();
		$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);
	}

	$cekFile = opendir($path);
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}

	closedir($cekFile);
	sleep(1);
}
$db->close();
exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $db, $path, $config;

	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);

		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		//628561305071@#@isat@#@95899@#@11130732170009002e3b@#@20151113143222@#@6285613050712015111314412643384826@#@95899331015007@#@2@#@20151113144126
		$msisdn = $mText[1];
		$telco = $mText[2];
		$shortcode = $mText[3];
		$trx_id = $mText[4];
		$sid = $mText[7];
		$status_telco = $mText[8];
		$charge_success = $mText[9];
		$trx_date = $mText[$Brs];

		$status_dr = get_status($status_telco);

		if( $status_telco == "2" && !empty( $sid ) ) {
			$sqlsqll = ",charge='".$charge_success."',sid='".$sid."'";
		}
		else {
			$sqlsqll = "";
		}

		$sql = "SELECT url_dr,session_id FROM mt_outgoing_log_".date("Ymd")." WHERE msisdn = '$msisdn' AND trx_id='$trx_id'";
		$result = $db->query($sql);
		if ($row = $db->fetch($result)){
			$session_id = $row['session_id'];

			if( $status_telco == "96" ) {
				$session_id_unreg = $msisdn.rand(0,10000);
				$sqluuu = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 96, unreg_reason = '96' WHERE msisdn = '".$msisdn."'";
				$db->query($sqluuu);

				//$sqluuu1 = "UPDATE app_sms.msisdn_subscriber_new SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 96, unreg_reason = '96' WHERE msisdn = '".$msisdn."'";
				//$db->query($sqluuu1);
			}

			if( $status_telco == "6" ) {
				$session_id_unreg = $msisdn.rand(0,10000);
				$sqluuu = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 6, unreg_reason = '6' WHERE msisdn = '".$msisdn."'";
				$db->query($sqluuu);

				//$sqluuu2 = "UPDATE app_sms.msisdn_subscriber_new SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 6, unreg_reason = '6' WHERE msisdn = '".$msisdn."'";
				//$db->query($sqluuu2);
			}

			if($row['url_dr'] <> "") {

				$url_dr = $row['url_dr'];

				$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr;
				$url = $url_dr.$param;

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 5);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

				$data = curl_exec($ch);

				curl_close($ch);

				$sql = "UPDATE mt_outgoing_log_".date("Ymd")." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
				$sql_result = $db->query($sql);
				if($sql_result) {

					$sqli = "INSERT IGNORE INTO dr_incoming_log".date("Ym")." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
								VALUES ('$telco','$shortcode','$msisdn','".mysql_escape_string($trx_id)."','$sid','$status_telco',NOW());";
					$result = $db->query($sqli);
					if($result) {
						return true;
					}
					else {
						return true;
					}

				}
				else {
					return true;
				}

			}
			else {

				$sql = "UPDATE mt_outgoing_log_".date("Ymd")." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
				$sql_result = $db->query($sql);
				if($sql_result) {

					$sqli = "INSERT IGNORE INTO dr_incoming_log".date("Ym")." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
								VALUES ('$telco','$shortcode','$msisdn','".mysql_escape_string($trx_id)."','$sid','$status_telco',NOW());";
					$result = $db->query($sqli);
					if($result) {
						return true;
					}
					else {
						return true;
					}

				}
				else {
					return true;
				}

			}

		}
		else {

			$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
			$yesterday	= date("Ymd",$date);
			$bln = date('Ym', $date);

			$sql = "SELECT url_dr,session_id FROM mt_outgoing_log_".$yesterday." WHERE msisdn = '$msisdn' AND trx_id='$trx_id'";
			$result = $db->query($sql);
			if ($row = $db->fetch($result)){
				$session_id = $row['session_id'];

				if( $status_telco == "96" ) {
					$session_id_unreg = $msisdn.rand(0,10000);
					$sqluuu = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 96, unreg_reason = '96' WHERE msisdn = '".$msisdn."'";
					$db->query($sqluuu);

					//$sqluuu3 = "UPDATE app_sms.msisdn_subscriber_new SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 96, unreg_reason = '96' WHERE msisdn = '".$msisdn."'";
					//$db->query($sqluuu3);
				}

				if( $status_telco == "6" ) {
					$session_id_unreg = $msisdn.rand(0,10000);
					$sqluuu = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 6, unreg_reason = '6' WHERE msisdn = '".$msisdn."'";
					$db->query($sqluuu);

					//$sqluuu4 = "UPDATE app_sms.msisdn_subscriber_new SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 6, unreg_reason = '6' WHERE msisdn = '".$msisdn."'";
					//$db->query($sqluuu4);
				}

				if($row['url_dr'] <> "") {

					$url_dr = $row['url_dr'];

					$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr;
					$url = $url_dr.$param;

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 5);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

					$data = curl_exec($ch);

					curl_close($ch);

					$sql = "UPDATE mt_outgoing_log_".$yesterday." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
					$sql_result = $db->query($sql);
					if($sql_result) {

						$sqli = "INSERT IGNORE INTO dr_incoming_log_".$bln." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
									VALUES ('$telco','$shortcode','$msisdn','".mysql_escape_string($trx_id)."','$sid','$status_telco',NOW());";
						$result = $db->query($sqli);
						if($result) {
							return true;
						}
						else {
							return true;
						}

					}
					else {
						return true;
					}

				}
				else {

					$sql = "UPDATE mt_outgoing_log_".$yesterday." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
					$sql_result = $db->query($sql);
					if($sql_result) {

						$sqli = "INSERT IGNORE INTO dr_incoming_log_".$bln." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
									VALUES ('$telco','$shortcode','$msisdn','".mysql_escape_string($trx_id)."','$sid','$status_telco',NOW());";
						$result = $db->query($sqli);
						if($result) {
							return true;
						}
						else {
							return true;
						}

					}
					else {
						return true;
					}

				}

			}
			else {
				if( substr($trx_date,0,8) == date("Ymd") ) {
					return false;
				}
				else {
					return true;
				}

			}
		}

	}
	else {
		return true;
	}
}

function get_status($status_telco) {
	switch($status_telco) {
		case "2" : $status_dr = "DELIVERED"; break;
		default : $status_dr = "REJECTED"; break;
	}
	return $status_dr;
}

?>
