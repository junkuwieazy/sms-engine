<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mt_send_pull_xl.php
#			FUNCTION			: Read SMS from File, send to telco
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for mt send pull xl!\n");

require("config.xl.php");

$file_path = "/var/www/sms/engine/";
require($file_path."include/config.php");
require($file_path."include/mysql.class.php");
include($file_path."include/class_aiom.php");
require($file_path."include/class_sms_system.php");  // sms system class
require_once($file_path."include/xml.class.php");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$folder = $argv[1];

$sms = new SMS_System();

$i = 1;

$path = $config['read_mt_xl_path'].$folder."/";

$file_monitoring = $config['mt_send_xl_monitoring_path']."mt_send_xl_".$folder.".txt";

while(true){

	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);

	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
}

exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $folder;
	global $config_xl, $sms, $file_path, $db;

	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);

		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
		$param['mo_origin'] = $mText[1];
		$param['msisdn'] = $mText[2];
		$param['prepaid'] = $mText[3];
		$param['sms'] = $mText[4];
		$param['telco'] = $mText[5];
		$param['shortcode'] = $mText[6];
		$param['trx_id'] = $mText[7];
		$param['trx_date'] = $mText[8];
		$param['session_id'] = $mText[9];

		$param['charge'] = $mText[10];
		$param['media_id'] = $mText[11];
		$param['partner_id'] = $mText[12];
		$param['appid'] = $mText[13];
		$param['subject'] = $mText[14];
		$param['priority'] = $mText[15];
		$param['url'] = $mText[16];
		$param['mt_type'] = $mText[17];
		$param['url_dr'] = $mText[18];
		$param['shortname'] = $mText[19];
		$param['reply_sms'] = $mText[20];

		$arrctype = explode(";",$param['subject']);
		if( strtolower($arrctype[1]) == "quiz" ) $cont_type = "QUIZ";
		else $cont_type = "INFO";

		$sid = $config_xl['sid'][$param['telco']][$param['shortcode']][$param['charge']];

		if( eregi("^PUSH",strtoupper($param['charge'])) ) $isPushMessage = 1;
		else $isPushMessage = 0;

		switch($param['mt_type']) {
			case 2 : // SMS WAPPUSH

				$sms->url = $config_xl['url']['xl']['sms']."?APP_ID=".$config_xl['user']['sms']."&APP_PWD=".$config_xl['pwd']['sms']."&DEST_ADDR=".$param['msisdn']."&SOURCE_ADDR=".$sid."&SHORT_NAME=".$param['shortname']."&PROTOCOL_ID=0&alphabet_id=0&MSG_CLASS=1&REGISTERED=yes&TEXT=".urlencode($param['reply_sms']);

				$sms->isPost = 0;
				$result = trim($sms->curl());

				$data = "==============================\n";
				$data .= "DATE:".date("Y-m-d H:i:s")."\n";
				$data .= "MSISDN:".$param['msisdn']."\n";
				$data .= "SESSION ID:".$param['session_id']."\n";
				$data .= "URL:".$sms->url."\n";
				$data .= "RESULT:".$result."\n";
				$data .= "==============================\n";

				break;
			case 3 : // SMS BINARY

				$sms->url = $config_xl['url']['xl']['sms']."?APP_ID=".$config_xl['user']['sms']."&APP_PWD=".$config_xl['pwd']['sms']."&DEST_ADDR=".$param['msisdn']."&SOURCE_ADDR=".$sid."&SHORT_NAME=".$param['shortname']."&PROTOCOL_ID=0&alphabet_id=0&MSG_CLASS=1&REGISTERED=yes&TEXT=".urlencode($param['reply_sms']);

				$sms->isPost = 0;
				$result = trim($sms->curl());

				$data = "==============================\n";
				$data .= "DATE:".date("Y-m-d H:i:s")."\n";
				$data .= "MSISDN:".$param['msisdn']."\n";
				$data .= "SESSION ID:".$param['session_id']."\n";
				$data .= "URL:".$sms->url."\n";
				$data .= "RESULT:".$result."\n";
				$data .= "==============================\n";

				break;
			default : // SMS TEXT

				if( eregi("^UNREG",strtoupper($param['subject'])) ) {

					$dntelco = "102@#@";
					$dnstatus = "DELIVERED";

					$result = '<?xml version="1.0" encoding="utf-8" ?><push-response><tid>'.$param['trx_id'].'</tid><status-id>0</status-id></push-response>';

					$data = "==============================\n";
					$data .= "DATE:".date("Y-m-d H:i:s")."\n";
					$data .= "MSISDN:".$param['msisdn']."\n";
					$data .= "SESSION ID:".$param['session_id']."\n";
					$data .= "URL:UNREG\n";
					$data .= "RESULT:".$result."\n";
					$data .= "==============================\n";
				}
				elseif( eregi("^REG",strtoupper($param['subject'])) ) {

					$dntelco = "102@#@";
					$dnstatus = "DELIVERED";

					$result = '<?xml version="1.0" encoding="utf-8" ?><push-response><tid>'.$param['trx_id'].'</tid><status-id>0</status-id></push-response>';

					$data = "==============================\n";
					$data .= "DATE:".date("Y-m-d H:i:s")."\n";
					$data .= "MSISDN:".$param['msisdn']."\n";
					$data .= "SESSION ID:".$param['session_id']."\n";
					$data .= "URL:UNREG\n";
					$data .= "RESULT:".$result."\n";
					$data .= "==============================\n";
				}
				else {

					$sql = "SELECT msisdn FROM app_sms.msisdn_blacklist WHERE msisdn = '".$param['msisdn']."'";
					$rs = $db->query($sql);
					if ($row = $db->fetch($rs)){
						$result = '<?xml version="1.0" encoding="utf-8" ?><push-response><tid>'.$param['trx_id'].'</tid><status-id>0</status-id></push-response>';
						$dntelco = "BLACKLIST";
						$dnstatus = "REJECTED";
					}
					else {
						$dntelco = "1111";
						$dnstatus = "REJECTED";

						if( $isPushMessage ) $sms->url = $config_xl['url']['xl']['sms']."?APP_ID=".$config_xl['user']['sms']."&APP_PWD=".$config_xl['pwd']['sms']."&DEST_ADDR=".$param['msisdn']."&SOURCE_ADDR=".$sid."&SHORT_NAME=".$param['shortname']."&PROTOCOL_ID=0&alphabet_id=0&MSG_CLASS=1&REGISTERED=yes&tx_id=".$param['trx_id']."&TEXT=".urlencode($param['reply_sms']);
						else $sms->url = $config_xl['url']['xl']['sms']."?APP_ID=".$config_xl['user']['sms']."&APP_PWD=".$config_xl['pwd']['sms']."&DEST_ADDR=".$param['msisdn']."&SOURCE_ADDR=".$sid."&SHORT_NAME=".$param['shortname']."&PROTOCOL_ID=0&alphabet_id=0&MSG_CLASS=1&REGISTERED=yes&tx_id=".$param['trx_id']."&TEXT=".urlencode($param['reply_sms']);

						$sms->isPost = 0;
						$result = trim($sms->curl());
					}

					$data = "==============================\n";
					$data .= "DATE:".date("Y-m-d H:i:s")."\n";
					$data .= "MSISDN:".$param['msisdn']."\n";
					$data .= "SESSION ID:".$param['session_id']."\n";
					$data .= "URL:".$sms->url."\n";
					$data .= "RESULT:".$result."\n";
					$data .= "==============================\n";
				}

				break;
		}

		// log sms in file
		$pathLOG = $config['base_path']."/logs/mt/xl/";

		$m_FileLOG = fopen($pathLOG."log_".$folder."_".date("Ymd").".txt","a");

		fwrite($m_FileLOG, $data);
		fclose($m_FileLOG);

		$mt_ok = true;

		if( $result == "" ) {
			return false;
		}
		else {

			$xml_response = $result;

			$parserObj_response 	= libs_xml2::getInstance();
			$parse_response		= $parserObj_response->readXML($xml_response);

			$moParams_response = array('status-id','tid');
			foreach ($moParams_response as $name_response) {
			   $_GET[$name_response] = $parserObj_response->getSingleNodeValue($name_response);
			}

			$ack_status = $_GET['status-id'];
			if( !empty( $_GET['tid'] ) ) $param['trx_id'] 	= $_GET['tid'];

			$date_now = date("Y-m-d H:i:s");
			// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
			$sql = "INSERT IGNORE INTO mt_outgoing_log_".date("Ymd")." (appid, mo_origin, msisdn, prepaid, sms, subject, message, url, telco, shortcode, charge, mt_type, trx_id, trx_date, session_id, media_id, partner_id, sid, ack_status, dn_telco, dn_status, url_dr, priority, shortname, datetime)
						VALUES ('".$param['appid']."', '".$param['mo_origin']."', '".$param['msisdn']."', '".$param['prepaid']."', '".mysql_escape_string($param['sms'])."', '".$param['subject']."', '".mysql_escape_string($param['reply_sms'])."', '".$param['url']."', '".$param['telco']."', '".$param['shortcode']."', '".$param['charge']."', '".$param['mt_type']."', '".$param['trx_id']."', '".$param['trx_date']."', '".$param['session_id']."', '".$param['media_id']."', '".$param['partner_id']."', '$sid', '".mysql_escape_string($ack_status)."', '$dntelco' ,'$dnstatus', '".$param['url_dr']."', '".$param['priority']."', '".$param['shortname']."', '$date_now');";

			$spool2 = range (1,SizeOf($config['spool_live_traffic_mt']));
			srand ((double)microtime()*1000000);
			shuffle ($spool2);

			$fp2 = fopen ($config['base_path'].$config['spool_live_traffic_mt'][$spool2[0]]."/sql-".$param['session_id'].".sql", "w+");
			fputs($fp2, $sql );
			fclose($fp2);

			return true;

		}

	}
	else {
		return false;
	}

}

?>
