<?php
#################################################################################
#
#			PROJECT				: Messaging Platform
#			OPERATOR			: All
#			AUTHOR				: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mt_send_pull_isat.php
#			FUNCTION			: Read SMS from File, send to telco
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for mt send pull isat retry!\n");

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
require("config.isat.php");
require($file_path."include/mysql.class.php");
include($file_path."include/class_aiom.php");
require($file_path."include/class_sms_system.php");  // sms system class

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$folder = $argv[1];

$sms = new SMS_System();

$path = $config['read_retry_isat_path'].$folder."/";

$i = 1;

$file_monitoring = $config['mt_send_isat_monitoring_path']."mt_send_isat_retry_".$folder.".txt";

while(true){

	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);

	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);

}

exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $folder;
	global $config_isat, $sms, $file_path, $db;

	if(!in_array( $folder , array("1","2","3","4","5") )) {
		if(in_array( date("H") , array("00","01","02","03","04","05") )) {
			$diproses = false;
		}
		else {
			$diproses = true;
		}
	}
	else {
		$diproses = true;
	}

	if( $diproses ) {
		//
		$SMSRow = "";
		$fp = fopen($path."/".$mFile, "r");
		if($fp) {
			while (!feof($fp)) {
				$SMSRow .= fread($fp, 1024);
			}
			fclose($fp);

			$SMSExploded = explode("@#@",$SMSRow);
			$Brs = 0;
			Foreach ($SMSExploded as $mField)
			{
				$Brs = $Brs + 1;
				$mText[$Brs] = $mField;
			}
			// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
			$param['mo_origin'] = $mText[1];
			$param['msisdn'] = $mText[2];
			$param['prepaid'] = $mText[3];
			$param['sms'] = $mText[4];
			$param['telco'] = $mText[5];
			$param['shortcode'] = $mText[6];
			$param['trx_id'] = $mText[7];
			$param['trx_date'] = $mText[8];
			$param['session_id'] = $mText[9];

			$param['charge'] = $mText[10];
			$param['media_id'] = $mText[11];
			$param['partner_id'] = $mText[12];
			$param['appid'] = $mText[13];
			$param['subject'] = $mText[14];
			$param['priority'] = $mText[15];
			$param['url'] = $mText[16];
			$param['mt_type'] = $mText[17];
			$param['url_dr'] = $mText[18];
			$param['shortname'] = $mText[19];
			$param['reply_sms'] = $mText[20];

			$sid = $config_isat['sid'][$param['telco']][$param['charge']];

			if( $param['subject'] == "PUSH;IOD;COOLssssssss;DAILYPUSHdddddd" ) {
				return true;
			}
			else {
				switch($param['mt_type']) {
					case 2 : // SMS WAPPUSH

						$udh = "0605040B8423F0";
						$udh .= "430601AE02056A0045C60C03";
						$url = str_replace("http://", "", $param["url"]);
						$url = $udh.Convert($url)."000103".Convert($param["reply_sms"])."000101";

						$xmldata = "<? xml version=\"1.0\" ?>";
						$xmldata .= "<PUSH>";
						$xmldata .= "<UID>".$config_isat['user']['isat']."</UID>";
						$xmldata .= "<PWD>".$config_isat['pwd']['isat']."</PWD>";
						$xmldata .= "<SID>".$sid."</SID>";
						$xmldata .= "<TID>".$param['trx_id']."</TID>";
						$xmldata .= "<MSISDN>".$param['msisdn']."</MSISDN>";
						$xmldata .= "<CONTENT TYPE='binary'>";
						$xmldata .= "<LINE NUMBER='1'>".$url."</LINE>";
						$xmldata .= "</CONTENT>";
						$xmldata .= "<DCS>245</DCS>";
						$xmldata .= "<UDHL>7</UDHL>";
						$xmldata .= "<SDMCODE>persib1933</SDMCODE>";
						$xmldata .= "</PUSH>";

						$sms->url = $config_isat['url']['isat'];
						$sms->isPost = 1;
						$sms->poststring = "xmldata=".urlencode($xmldata);
						$result = $sms->curl();

						$data = "==============================\n";
						$data .= "DATE:".date("Y-m-d H:i:s")."\n";
						$data .= "MSISDN:".$param['msisdn']."\n";
						$data .= "SESSION ID:".$param['session_id']."\n";
						$data .= "XML:\n";
						$data .= $xmldata."\n";
						$data .= "URL:".$sms->url."\n";
						$data .= "PARAM:".$sms->poststring."\n";
						$data .= "RESULT:".$result."\n";
						$data .= "==============================\n";

						break;
					case 3 : // SMS BINARY

						$param["reply_sms"] = trim($param["reply_sms"]);

						$lines = "";
						$line = ceil(strlen($param["reply_sms"]) / 280);
						$j = 0;
						for($i=1; $i <= $line; $i++) {
						   $lines .= "<LINE NUMBER='$i'>".substr($param["reply_sms"], $j, 280)."</LINE>";
						   $j = 280 * $i;
						}

						$xmldata = "<? xml version=\"1.0\" ?>";
						$xmldata .= "<PUSH>";
						$xmldata .= "<UID>".$config_isat['user']['isat']."</UID>";
						$xmldata .= "<PWD>".$config_isat['pwd']['isat']."</PWD>";
						$xmldata .= "<SID>".$sid."</SID>";
						if($param['trx_id'] <> "") {
							$xmldata .= "<TID>".$param['trx_id']."</TID>";
						}
						$xmldata .= "<MSISDN>".$param['msisdn']."</MSISDN>";
						$xmldata .= "<CONTENT TYPE=\"binary\">";
						$xmldata .= $lines;
						$xmldata .= "</CONTENT>";
						$xmldata .= "<DCS>245</DCS>";
						$xmldata .= "<UDHL>7</UDHL>";
						$xmldata .= "<SDMCODE>persib1933</SDMCODE>";
						$xmldata .= "</PUSH>";

						$sms->url = $config_isat['url']['isat'];
						$sms->isPost = 1;
						$sms->poststring = "xmldata=".urlencode($xmldata);
						$result = $sms->curl();

						$data = "==============================\n";
						$data .= "DATE:".date("Y-m-d H:i:s")."\n";
						$data .= "MSISDN:".$param['msisdn']."\n";
						$data .= "SESSION ID:".$param['session_id']."\n";
						$data .= "XML:\n";
						$data .= $xmldata."\n";
						$data .= "URL:".$sms->url."\n";
						$data .= "PARAM:".$sms->poststring."\n";
						$data .= "RESULT:".$result."\n";
						$data .= "==============================\n";

						break;
					default : // SMS TEXT

						$sql = "SELECT msisdn FROM app_sms.msisdn_blacklist WHERE msisdn = '".$param['msisdn']."'";
						$rs = $db->query($sql);
						if ($row = $db->fetch($rs)){
							$result = "BLACKLIST";
						}
						else {
							if( eregi("^PUSH",strtoupper($param['charge'])) ) $isPushMessage = 1;
							else $isPushMessage = 0;

							if($isPushMessage){
								$sms->url = $config_isat['url']['isat']."?uid=".$config_isat['user']['isat']."&pwd=".$config_isat['pwd']['isat']."&serviceid=".$sid."&smstype=0&msisdn=".$param['msisdn']."&sdmcode=".$param['shortname']."&sms=".urlencode($param['reply_sms']);
							}
							else {
								$sms->url = $config_isat['url']['isat']."?uid=".$config_isat['user']['isat']."&pwd=".$config_isat['pwd']['isat']."&serviceid=".$sid."&transid=".$param['trx_id']."&smstype=0&msisdn=".$param['msisdn']."&sms=".urlencode($param['reply_sms']);
							}

							$sms->isPost = 0;
							$result = trim($sms->curl());
						}

						$data = "==============================\n";
						$data .= "DATE:".date("Y-m-d H:i:s")."\n";
						$data .= "MSISDN:".$param['msisdn']."\n";
						$data .= "SESSION ID:".$param['session_id']."\n";
						$data .= "URL:".$sms->url."\n";
						$data .= "RESULT:".$result."\n";
						$data .= "==============================\n";

						break;
				}

				// log sms in file
				$pathLOG = $config['base_path']."/logs/mt/isat/";

				$m_FileLOG = fopen($pathLOG."log_retry_".$folder."_".date("Ymd").".txt","a");

				fwrite($m_FileLOG, $data);
				fclose($m_FileLOG);

				$mt_ok = true;
				if( $result == "" ) {
					$hasil = false;
				}
				else {

					if( $result == "BLACKLIST" ) {

						$ack_status = "BLACKLIST";
					}
					else {
						$xml = simplexml_load_string($result);
						$ar_transid = $xml->TRANSID;
						$ar_status = $xml->STATUS;

						$ack_status = trim($ar_status[0]);
						$param['trx_id'] 	= $ar_transid[0];
					}

					if(in_array( $ack_status , array("99", "3") )) {
						$spool3 = range (1,SizeOf($config['spool_retry_isat']));
						srand ((double)microtime()*1000000);
						shuffle ($spool3);

						if(copy($path."/".$mFile,$config['base_path'].$config['spool_retry_isat'][$spool3[0]]."/".$mFile)) {
							$hasil = true;
							$mt_ok = false;
						}
						else {
							$hasil = false;
						}
					}
					else $hasil = true;

				}

				if( $hasil && $mt_ok ) {
					// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
					$date_now = date("Y-m-d H:i:s");
					$sql = "INSERT IGNORE INTO mt_outgoing_log_".date("Ymd")." (appid, mo_origin, msisdn, prepaid, sms, subject, message, url, telco, shortcode, charge, mt_type, trx_id, trx_date, session_id, media_id, partner_id, sid, ack_status, url_dr, priority, shortname, datetime)
								VALUES ('".$param['appid']."', '".$param['mo_origin']."', '".$param['msisdn']."', '".$param['prepaid']."', '".mysql_escape_string($param['sms'])."', '".$param['subject']."', '".mysql_escape_string($param['reply_sms'])."', '".$param['url']."', '".$param['telco']."', '".$param['shortcode']."', '".$param['charge']."', '".$param['mt_type']."', '".$param['trx_id']."', '".$param['trx_date']."', '".$param['session_id']."', '".$param['media_id']."', '".$param['partner_id']."', '$sid', '".mysql_escape_string($ack_status)."', '".$param['url_dr']."', '".$param['priority']."', '".$param['shortname']."', '$date_now');";

					$spool2 = range (1,SizeOf($config['spool_live_traffic_mt']));
					srand ((double)microtime()*1000000);
					shuffle ($spool2);

					$fp2 = fopen ($config['base_path'].$config['spool_live_traffic_mt'][$spool2[0]]."/sql-".$param['session_id'].".sql", "w+");
					fputs($fp2, $sql );
					fclose($fp2);

				}

				return $hasil;

			}

		}
		else {
			return false;
		}
		//
	}
	else {
		return false;
	}

}

function Convert($str) {
	$strchr = '';
	$j = strlen($str);
	for($i=0; $i < $j; $i++)
	{
		$chrdec = ord($str[$i]);
		$chrhex = dechex($chrdec);
		$strchr .= $chrhex;

	}
	return $strchr;
}

?>
