<?php

#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: ack_picker.php
#			FUNCTION			: Read SMS from File, process to application
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for ack picker tsel!\n");

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
include($file_path."include/class_aiom.php");

$i = 1;
$folder = trim($argv[1]);

$path = $config['ack_picker_tsel_path'].$folder."/";

$file_monitoring = $config['ack_process_tsel_monitoring_path']."ack_picker_".$folder.".txt";

while(true){
	
	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
	
}

exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $db, $path, $config;
	
	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);
		
		$SMSExploded = explode("#@@#",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		//msisdn,shortcode,trx_id,session_id,status_dr,status,url_dr
		$msisdn = $mText[1];
		$shortcode = $mText[2];
		$trx_id = $mText[3];
		$session_id = $mText[4];
		$status_dr = $mText[5];
		$status_telco = $mText[6];
		$url_dr = $mText[7];
		
		if($url_dr <> "") {
			
			$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr."&status_telco=".$status_telco;
			$url = $url_dr.$param;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 5);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
			
			$data = curl_exec($ch);
			
			curl_close($ch);
			
		}
		
		return true;
	
	}
	else {
		return false;
	}
}

?>