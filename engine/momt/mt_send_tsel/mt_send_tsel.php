<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mt_send_pull_tsel.php
#			FUNCTION			: Read SMS from File, send to telco
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for mt send pull tsel!\n");

require("config.tsel.php");

$file_path = "/var/www/sms/engine/";
require($file_path."include/config.php");
include($file_path."include/class_aiom.php");
require($file_path."include/class_sms_system.php");  // sms system class

$folder = $argv[1];

$sms = new SMS_System();

$i = 1;

$path = $config['read_mt_tsel_path'].$folder."/";

$file_monitoring = $config['mt_send_tsel_monitoring_path']."mt_send_tsel_".$folder.".txt";

while(true){
	
	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}
	
	$cekFile = opendir($path);
	
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
}

exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $folder;
	global $config_tsel, $sms, $file_path;
	
	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);
		
		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
		$param['mo_origin'] = $mText[1];
		$param['msisdn'] = $mText[2];
		$param['prepaid'] = $mText[3];
		$param['sms'] = $mText[4];
		$param['telco'] = $mText[5];
		$param['shortcode'] = $mText[6];
		$param['trx_id'] = $mText[7];
		$param['trx_date'] = $mText[8];
		$param['session_id'] = $mText[9];
		
		$param['charge'] = $mText[10];
		$param['media_id'] = $mText[11];
		$param['partner_id'] = $mText[12];
		$param['appid'] = $mText[13];
		$param['subject'] = $mText[14];
		$param['priority'] = $mText[15];
		$param['url'] = $mText[16];
		$param['mt_type'] = $mText[17];
		$param['url_dr'] = $mText[18];
		$param['shortname'] = $mText[19];
		$param['reply_sms'] = $mText[20];
		
		$sid = $config_tsel['sid'][$param['telco']][$param['charge']];
		
		if( preg_match("/^PUSH/",strtoupper($param['charge'])) ) $isPushMessage = 1;
		else $isPushMessage = 0;
		
		switch($param['mt_type']) {
			case 2 : // SMS WAPPUSH
				$content_id = $config_tsel['content_id'][$param['charge']];
				
				$sid = $config_tsel['sid'][$param['telco']][$param['charge']];
				
				$strxml ="<?xml version='1.0'?>";
				$strxml = $strxml."<wap-push>";
				$strxml = $strxml."<url>".htmlentities($param['url'])."</url>";
				$strxml = $strxml."<msisdn_sender>".$param['msisdn']."</msisdn_sender>";
				$strxml = $strxml."<msisdn_receipient>".$param['msisdn']."</msisdn_receipient>";
				$strxml = $strxml."<sid>".$sid."</sid>";
				$strxml = $strxml."<text>".$param['reply_sms']."</text>";
				$strxml = $strxml."<trx_id>".$param['trx_id']."</trx_id>";
				$strxml = $strxml."<trx_date>".$param['trx_date']."</trx_date>";
				$strxml = $strxml."<contentid>".$content_id."</contentid>";
				$strxml = $strxml."</wap-push>";
				$sms->isPost = 1;
				$sms->poststring = "msg=".$strxml;
				$sms->poststring .= "&cpid=".$config_tsel['user']['tsel']['wappush'];
				$sms->poststring .= "&pwd=".$config_tsel['pwd']['tsel']['wappush'];
				
				$ip_mo = substr($param["session_id"],0,1);
				if($ip_mo == 2) $sms->url = $config_tsel['url']['tsel']['pull_wpdc'][2];
				else $sms->url = $config_tsel['url']['tsel']['pull_wpdc'][3];
				
				$result = trim($sms->curl());
				
				$data = "==============================\n";
				$data .= "DATE:".date("Y-m-d H:i:s")."\n";
				$data .= "MSISDN:".$param['msisdn']."\n";
				$data .= "SESSION ID:".$param['session_id']."\n";
				$data .= "XML:\n";
				$data .= $strxml."\n";
				$data .= "URL:".$sms->url."\n";
				$data .= "PARAM:".$sms->poststring."\n";
				$data .= "RESULT:".$result."\n";
				$data .= "==============================\n";
				
				break;
			case 3 : // SMS BINARY
				$content_id = $config_tsel['content_id'][$param['charge']];
				
				$UDH	= trim(substr($param['reply_sms'],0,14));
				$finalData	= trim(substr($param['reply_sms'],14));
				
				$lines = "";
				$line = ceil(strlen($finalData) / 256);
			    $j = 0;
			   
			   $lines .= "<packet totalpacket=\"".$line."\">";
			   
			   for($i=1; $i <= $line; $i++) {
				$udh_part = $UDH."0".$i;
				  $lines .= "<data pkt=\"$i\" udh=\"$udh_part\" msgsrc=\"".substr($finalData, $j, 256)."\" />";
				  $j = 256 * $i;
			   }
			   $lines .= "</packet>";
			   
			   $sid = $config_tsel['sid'][$param['telco']][$param['charge']];
				
				$xmldata="<?xml version=\"1.0\"?>";
				$xmldata=$xmldata."<smartmessaging>";
				$xmldata=$xmldata."<status>00</status>";
				$xmldata=$xmldata."<trx_date>".$param['trx_date']."</trx_date>";
				$xmldata=$xmldata."<trx_id>".$param['trx_id']."</trx_id>";
				$xmldata=$xmldata."<sid>".$sid."</sid>";
				$xmldata=$xmldata."<contentid>".$content_id."</contentid>";
				$xmldata=$xmldata."<msisdn_sender>".$param['msisdn']."</msisdn_sender>";
				$xmldata=$xmldata."<msisdn_recipient>".$param['msisdn']."</msisdn_recipient>";
				$xmldata=$xmldata.$lines;
				$xmldata=$xmldata."</smartmessaging>";
				$sms->isPost = 1;
				$sms->poststring = "msg=".$xmldata;
				$sms->poststring .= "&cpid=".$config_tsel['user']['tsel']['binary'];
				$sms->poststring .= "&pwd=".$config_tsel['pwd']['tsel']['binary'];
				$sms->poststring .= "&type=RT";
				
				$ip_mo = substr($param["session_id"],0,1);
				if($ip_mo == 2) $sms->url = $config_tsel['url']['tsel']['pull_binary'][2];
				else $sms->url = $config_tsel['url']['tsel']['pull_binary'][3];
				
				$result = trim($sms->curl());
				
				$data = "==============================\n";
				$data .= "DATE:".date("Y-m-d H:i:s")."\n";
				$data .= "MSISDN:".$param['msisdn']."\n";
				$data .= "SESSION ID:".$param['session_id']."\n";
				$data .= "XML:\n";
				$data .= $xmldata."\n";
				$data .= "URL:".$sms->url."\n";
				$data .= "PARAM:".$sms->poststring."\n";
				$data .= "RESULT:".$result."\n";
				$data .= "==============================\n";
				
				break;
			default : // SMS TEXT
				
				if( preg_match("/^REG/",strtoupper($param['subject'])) ) {
					$result = "1";
					$data = "==============================\n";
					$data .= "DATE:".date("Y-m-d H:i:s")."\n";
					$data .= "MSISDN:".$param['msisdn']."\n";
					$data .= "SESSION ID:".$param['session_id']."\n";
					$data .= "URL:OK\n";
					$data .= "RESULT:1:".$param['trx_id']."\n";
					$data .= "==============================\n";
				}
				elseif( preg_match("/^RENEW/",strtoupper($param['subject'])) ) {
					$result = "1";
					$data = "==============================\n";
					$data .= "DATE:".date("Y-m-d H:i:s")."\n";
					$data .= "MSISDN:".$param['msisdn']."\n";
					$data .= "SESSION ID:".$param['session_id']."\n";
					$data .= "URL:OK\n";
					$data .= "RESULT:1:".$param['trx_id']."\n";
					$data .= "==============================\n";
				}
				elseif( preg_match("/^UNREG/",strtoupper($param['subject'])) ) {
					$result = "1";
					$data = "==============================\n";
					$data .= "DATE:".date("Y-m-d H:i:s")."\n";
					$data .= "MSISDN:".$param['msisdn']."\n";
					$data .= "SESSION ID:".$param['session_id']."\n";
					$data .= "URL:OK\n";
					$data .= "RESULT:1:".$param['trx_id']."\n";
					$data .= "==============================\n";
				}
				else {
					////
					
					$sid = $config_tsel['sid'][$param['telco']][$param['charge']];
					
					// cek push message atau iod message
					if($isPushMessage){
						$sms->url = $config_tsel['url']['tsel']['push_text'];
						$sms->poststring = "sid=".$sid;
						$sms->poststring .= "&pwd=".$config_tsel['pwd']['tsel']['push_text'];
						$sms->poststring .= "&cpid=".$config_tsel['user']['tsel']['push_text'];
						$sms->poststring .= "&msisdn=".$param['msisdn'];
						$sms->poststring .= "&sms=".urlencode($param['reply_sms']);
						$sms->isPost = 1;
						$result = trim($sms->curl());
						
						$data = "==============================\n";
						$data .= "DATE:".date("Y-m-d H:i:s")."\n";
						$data .= "MSISDN:".$param['msisdn']."\n";
						$data .= "SESSION ID:".$param['session_id']."\n";
						$data .= "URL:".$sms->url."\n";
						$data .= "PARAM:".$sms->poststring."\n";
						$data .= "RESULT:".$result."\n";
						$data .= "==============================\n";
						
					}
					else {
						$sms->url = $config_tsel['url']['tsel']['push_text'];
						$sms->poststring = "sid=".$sid;
						$sms->poststring .= "&pwd=".$config_tsel['pwd']['tsel']['push_text'];
						$sms->poststring .= "&cpid=".$config_tsel['user']['tsel']['push_text'];
						$sms->poststring .= "&msisdn=".$param['msisdn'];
						$sms->poststring .= "&trx_id=".$param['trx_id'];
						$sms->poststring .= "&sms=".urlencode($param['reply_sms']);
						$sms->isPost = 1;
						$result = trim($sms->curl());
						
						$data = "==============================\n";
						$data .= "DATE:".date("Y-m-d H:i:s")."\n";
						$data .= "MSISDN:".$param['msisdn']."\n";
						$data .= "SESSION ID:".$param['session_id']."\n";
						$data .= "URL:".$sms->url."\n";
						$data .= "PARAM:".$sms->poststring."\n";
						$data .= "RESULT:".$result."\n";
						$data .= "==============================\n";
					}
					////
				}
				
				break;
		}
		
		// log sms in file
		$pathLOG = $config['base_path']."/logs/mt/tsel/";
		
		$m_FileLOG = fopen($pathLOG."log_".$folder."_".date("Ymd").".txt","a");
		
		fwrite($m_FileLOG, $data);
		fclose($m_FileLOG);
		
		$mt_ok = true;
		
		if( $result == "" ) {
			$hasil = false;
			$mt_ok = false;
		}
		else {
			$arrStatus = explode(":",$result);
			if($arrStatus[0] == "1" && count($arrStatus) > 1) {
				$status = $arrStatus[0];
				$trx_id = $arrStatus[1];
			}
			else {
				$status = $result;
				$trx_id = $param['trx_id'];
			}
			
			if( $param['mt_type']  == 2 ) { // wappush
				$hasil = true;
			}
			elseif( $param['mt_type']  == 3 ) { // SM
				$hasil = true;
			}
			else { // Text
				if($isPushMessage) {
					if( $result == "3:101" || $result == "3:109" ) {
						$spool3 = range (1,SizeOf($config['spool_retry_tsel']));
						srand ((double)microtime()*1000000);
						shuffle ($spool3);
						
						if(copy($path."/".$mFile,$config['base_path'].$config['spool_retry_tsel'][$spool3[0]]."/".$mFile)) {
							$hasil = true;
							$mt_ok = false;
						}
						else {
							$hasil = false;
						}
					}
					else $hasil = true;
				}
				else {
					if( $result == "3" || $result == "7" ) {
						$spool3 = range (1,SizeOf($config['spool_retry_tsel']));
						srand ((double)microtime()*1000000);
						shuffle ($spool3);
						
						if(copy($path."/".$mFile,$config['base_path'].$config['spool_retry_tsel'][$spool3[0]]."/".$mFile)) {
							$hasil=  true;
							$mt_ok = false;
						}
						else {
							$hasil = false;
						}
					}
					else $hasil = true;
				}
			}
		}
		
		if( $hasil && $mt_ok ) {
			$status_dr = get_status($status,$param['charge'],$param['mt_type']);
			
			// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
			$date_now = date("Y-m-d H:i:s");
			$sql = "INSERT IGNORE INTO mt_outgoing_log".date("Ym")." (appid, mo_origin, msisdn, prepaid, sms, subject, message, url, telco, shortcode, charge, mt_type, trx_id, trx_date, session_id, media_id, partner_id, sid, dn_telco, dn_status, url_dr, priority, shortname, datetime)
						VALUES ('".$param['appid']."', '".$param['mo_origin']."', '".$param['msisdn']."', '".$param['prepaid']."', '".mysql_escape_string($param['sms'])."', '".$param['subject']."', '".mysql_escape_string($param['reply_sms'])."', '".$param['url']."', '".$param['telco']."', '".$param['shortcode']."', '".$param['charge']."', '".$param['mt_type']."', '".$param['trx_id']."', '".$param['trx_date']."', '".$param['session_id']."', '".$param['media_id']."', '".$param['partner_id']."', '$sid', '".mysql_escape_string($status)."', '".mysql_escape_string($status_dr)."', '".$param['url_dr']."', '".$param['priority']."', '".$param['shortname']."', '$date_now');";

			$spool2 = range (1,SizeOf($config['spool_live_traffic_mt']));
			srand ((double)microtime()*1000000);
			shuffle ($spool2);
			
			$fp2 = fopen ($config['base_path'].$config['spool_live_traffic_mt'][$spool2[0]]."/sql-".$param['session_id'].".sql", "w+");
			fputs($fp2, $sql );
			fclose($fp2);
			
			$a = $param['msisdn']."#@@#".$param['shortcode']."#@@#".$param['trx_id']."#@@#".$param['session_id']."#@@#".$status_dr."#@@#".$status."#@@#".$param['url_dr'];
			
			$spool3 = range (1,SizeOf($config['spool_ack_process_tsel']));
			srand ((double)microtime()*1000000);
			shuffle ($spool3);
			
			$fp3 = fopen ($config['base_path'].$config['spool_ack_process_tsel'][$spool3[0]]."/ack-".$param['session_id'].".sms", "w+");
			fputs($fp3, $a );
			fclose($fp3);
			
		}
		
		return $hasil;
	}
	else {
		return false;
	}
	
}

?>
