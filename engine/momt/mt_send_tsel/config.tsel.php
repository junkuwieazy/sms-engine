<?php

$config_tsel['user']['tsel']['pull_text']		= "trias";
$config_tsel['pwd']['tsel']['pull_text']		= "p0litick3";

$config_tsel['user']['tsel']['push_text']		= "trias";
$config_tsel['pwd']['tsel']['push_text']		= "p0litick3";

$config_tsel['user']['tsel']['binary']			= "trias";
$config_tsel['pwd']['tsel']['binary']			= "p0litick3";

$config_tsel['user']['tsel']['wappush']		    = "trias";
$config_tsel['pwd']['tsel']['wappush']			= "p0litick3";

$config_tsel['url']['tsel']['pull_text'][2]		= "http://202.3.208.203:55000/cp/submitSM.jsp";
$config_tsel['url']['tsel']['pull_text'][3]		= "http://202.3.208.203:55000/cp/submitSM.jsp";
$config_tsel['url']['tsel']['push_text']		= "http://202.3.208.203:55000/cp/submitSM.jsp";
$config_tsel['url']['tsel']['pull_binary'][2]		= "http://202.3.219.2:8100/cp/smartmessaging/send.jsp";
$config_tsel['url']['tsel']['pull_binary'][3]		= "http://202.3.219.3:8100/cp/smartmessaging/send.jsp";

$config_tsel['url']['tsel']['pull_wappush'][2]		= "http://202.3.219.2:8100/cp/wappush/regular/send.jsp";
$config_tsel['url']['tsel']['pull_wappush'][3]		= "http://202.3.219.3:8100/cp/wappush/regular/send.jsp";

$config_tsel['url']['tsel']['pull_wpdc'][2]		= "http://202.3.219.2:8100/cp/wappush/noalias/send.jsp";
$config_tsel['url']['tsel']['pull_wpdc'][3]		= "http://202.3.219.3:8100/cp/wappush/noalias/send.jsp";

$config_tsel['sid']['tsel']['PULL-3000-ARTIS']	= "00000";
$config_tsel['sid']['tsel']['PULL-REG']			= "00000";
$config_tsel['sid']['tsel']['PULL-UNREG']		= "00000";
$config_tsel['sid']['tsel']['PULL-2000-ARTIS']	= "TXTGENRDIRARTIS_IOD";
$config_tsel['sid']['tsel']['PUSH-0-ARTIS']		= "TXTGENRDIRREGARTIS_Subs";

$config_tsel['sid']['tsel']['PULL-3000-BELAJAR']	= "00000";
$config_tsel['sid']['tsel']['PUSH-0-BELAJAR']		= "TXTGENRDIRREGBELAJAR_Subs";
$config_tsel['sid']['tsel']['PULL-2000-BELAJAR']	= "TXTGENRDIRBELAJAR_IOD";

$config_tsel['sid']['tsel']['PULL-3000-HIJAB']	= "00000";
$config_tsel['sid']['tsel']['PUSH-0-HIJAB']		= "TXTGENRDIRHIJABTRIAS_Subs";
$config_tsel['sid']['tsel']['PULL-2000-HIJAB']	= "TXTGENRDIRHIJAB_IOD";

$config_tsel['sid']['tsel']['PULL-3000-SEHAT']	= "00000";
$config_tsel['sid']['tsel']['PUSH-0-SEHAT']		= "TXTGENRDIRREGSEHAT_Subs";
$config_tsel['sid']['tsel']['PULL-2000-SEHAT']	= "TXTGENRDIRSEHAT_IOD";

function get_status($status_telco,$charge,$mt_type) {
	switch($mt_type) {
		case 2 : // WAPPUSH DIRECT CHARGING
			switch($status_telco) {
				case "1" : $status_dr = "DELIVERED"; break;
				default 	: $status_dr = "REJECTED"; break;
			}
			break;
		case 3 : // BINARY
			switch($status_telco) {
				case "1" : $status_dr = "DELIVERED"; break;
				default 	: $status_dr = "REJECTED"; break;
			}
			break;
		default : // TEXT
			switch(substr($charge,0,2)) {
				case "PUSH" :
					switch($status_telco) {
						case "1" : $status_dr = "DELIVERED"; break;
						default : $status_dr = "REJECTED"; break;
					}
					break;
				default :
					switch($status_telco) {
						case "1" : $status_dr = "DELIVERED"; break;
						default 	: $status_dr = "REJECTED"; break;
					}
					break;
			}
			break;
	}
	return $status_dr;
}

?>
