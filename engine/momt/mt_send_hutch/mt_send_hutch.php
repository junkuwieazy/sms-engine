<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mt_send_pull_hutch.php
#			FUNCTION			: Read SMS from File, send to telco
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for mt send push hutch!\n");

require("config.hutch.php");

$file_path = "/var/www/sms/engine/";
require($file_path."include/config.php");
include($file_path."include/class_aiom.php");
require($file_path."include/class_sms_system.php");  // sms system class
include_once($file_path."include/xmlarray.php");

$folder = $argv[1];

$path = $config['read_mt_hutch_path'].$folder."/";

$sms = new SMS_System();

$i = 1;

$file_monitoring = $config['mt_send_hutch_monitoring_path']."mt_send_hutch_".$folder.".txt";

while(true){
	
	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}
	
	$cekFile = opendir($path);
	
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
	
}

$db->close();

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $folder;
	global $config_hutch, $file_path, $sms;
	
	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);
		
		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
		$param['mo_origin'] = $mText[1];
		$param['msisdn'] = $mText[2];
		$param['prepaid'] = $mText[3];
		$param['sms'] = $mText[4];
		$param['telco'] = $mText[5];
		$param['shortcode'] = $mText[6];
		$param['trx_id'] = $mText[7];
		$param['trx_date'] = $mText[8];
		$param['session_id'] = $mText[9];
		
		$param['charge'] = $mText[10];
		$param['media_id'] = $mText[11];
		$param['partner_id'] = $mText[12];
		$param['appid'] = $mText[13];
		$param['subject'] = $mText[14];
		$param['priority'] = $mText[15];
		$param['url'] = $mText[16];
		$param['mt_type'] = $mText[17];
		$param['url_dr'] = $mText[18];
		$param['shortname'] = $mText[19];
		$param['reply_sms'] = $mText[20];
		
		$sid = $config_hutch['sid'][$param['telco']][$param['shortcode']][$param['charge']];
		
		$sms->url = $config_hutch['url']['hutch']."?USERNAME=".$config_hutch['user']['hutch'][$param['shortcode']][$param['charge']]."&PASSWORD=".$config_hutch['pwd']['hutch'][$param['shortcode']][$param['charge']]."&REG_DELIVERY=1&ORIGIN_ADDR=".$sid."&MOBILENO=".$param['msisdn']."&TYPE=0&udh=0&MESSAGE=".urlencode($param['reply_sms']);
		
		$sms->isPost = 0;
		$result = trim($sms->curl());
		
		$data = "==============================\n";
		$data .= "DATE:".date("Y-m-d H:i:s")."\n";
		$data .= "MSISDN:".$param['msisdn']."\n";
		$data .= "SESSION ID:".$param['session_id']."\n";
		$data .= "URL:".$sms->url."\n";
		$data .= "RESULT:".$result."\n";
		$data .= "==============================\n";
		
		// log sms in file
		$pathLOG = $config['base_path']."/logs/mt/hutch/";
		
		$m_FileLOG = fopen($pathLOG."log_".$folder."_".date("Ymd").".txt","a");
		
		fwrite($m_FileLOG, $data);
		fclose($m_FileLOG);
		
		if( $result == "" ) {
			$hasil = false;
		}
		else {
			
			$xmlresponse = trim($result);
			$xmlresponse = str_replace( "\\" , "", $xmlresponse ) ;
			$xmlresponse = preg_replace( "/<\?([^;]+)\?>[\r\n]/im" , "" , $xmlresponse) ;
			
			$findme   = '<RESPONSES>';
			$pos = strpos($xmlresponse, $findme);

			if ($pos === false) {
				$ar_a = explode("\n",$xmlresponse);
				$ar_b = explode(":",$ar_a[2]);
				$ar_c = explode(":",$ar_a[0]);
				
				$ack_status = $ar_c[1];
				$param['trx_id'] = $ar_b[1];
				
			} else {
				if($xmlcresponse = XMLtoArray($xmlresponse, $xml_arrayresponse)){
					$ack_status = trim($xml_arrayresponse["RESPONSES"]["RESPONSE"]["CODE"]);
					$param['trx_id'] = trim($xml_arrayresponse["RESPONSES"]["RESPONSE"]["SUBMITTED_ID"]);
					
				}
				else {
					$ack_status = "000";
				}
			}
			
			
			
			// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
			$date_now = date("Y-m-d H:i:s");
			$sql = "INSERT IGNORE INTO mt_outgoing_log".date("Ym")." (appid, mo_origin, msisdn, prepaid, sms, subject, message, url, telco, shortcode, charge, mt_type, trx_id, trx_date, session_id, media_id, partner_id, sid, ack_status, dn_status, url_dr, priority, shortname, datetime)
						VALUES ('".$param['appid']."', '".$param['mo_origin']."', '".$param['msisdn']."', '".$param['prepaid']."', '".$param['sms']."', '".$param['subject']."', '".mysql_escape_string($param['reply_sms'])."', '".$param['url']."', '".$param['telco']."', '".$param['shortcode']."', '".$param['charge']."', '".$param['mt_type']."', '".$param['trx_id']."', '".$param['trx_date']."', '".$param['session_id']."', '".$param['media_id']."', '".$param['partner_id']."', '".$config_hutch['sid'][$param['telco']][$param['shortcode']][$param['charge']]."', '$ack_status', 'REJECTED', '".$param['url_dr']."', '".$param['priority']."', '".$param['shortname']."', '$date_now');";

			$spool2 = range (1,SizeOf($config['spool_live_traffic_mt']));
			srand ((double)microtime()*1000000);
			shuffle ($spool2);
			
			$fp2 = fopen ($config['base_path'].$config['spool_live_traffic_mt'][$spool2[0]]."/sql-".$param['session_id'].".sql", "w+");
			fputs($fp2, $sql );
			fclose($fp2);
			
			$hasil = true;
			
		}
		
		return $hasil;
		
	}
	else {
		return false;
	}
	
}

?>