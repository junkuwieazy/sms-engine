<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: sms_gateway_pull.php
#			FUNCTION			: Receive sms from application for sms pull, store to database and file
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($_GET['serverkey'])) die('Empty Parameter');
if ($_GET['serverkey'] != 's1st3m') die('AUTH');

require("../../include/config.php");

if(!in_array($_SERVER['REMOTE_ADDR'], $config['allow_ip'])) die ("You are not authorized to view this page");

if(empty($_GET['mt_type'])) $mt_type 	= 1;
else $mt_type 	= $_GET['mt_type'];
if(empty($_GET['appid'])) die("Empty APPID");
$appid 		= $_GET['appid'];
if(empty($_GET['mo_origin'])) $mo_origin 	= "";
else $mo_origin 	= $_GET['mo_origin'];
if(empty($_GET['msisdn'])) die("Empty MSISDN");
$msisdn 		= $_GET['msisdn'];
if(empty($_GET['prepaid'])) $prepaid 	= 0;
else $prepaid 	= $_GET['prepaid'];
if(empty($_GET['sms'])) $sms = "";
else $sms 			= $_GET['sms'];
if(empty($_GET['subject'])) die("Empty SUBJECT");
$subject 			= $_GET['subject'];
if(empty($_GET['reply_sms'])) $reply_sms 	= "";
else $reply_sms 	= $_GET['reply_sms'];
if(empty($_GET['url'])) $url 			= "";
else $url 			= $_GET['url'];
if(empty($_GET['telco'])) die("Empty TELCO");
$telco 		= $_GET['telco'];
if(empty($_GET['shortcode'])) die("Empty SHORTCODE");
$shortcode 	= $_GET['shortcode'];
if(empty($_GET['charge'])) die("Empty CHARGE");
$charge 		= $_GET['charge'];
if(empty($_GET['trx_id'])) $trx_id 		= "";
else $trx_id 		= $_GET['trx_id'];
if(empty($_GET['trx_date'])) $trx_date 	= "";
else $trx_date 	= $_GET['trx_date'];
if(empty($_GET['session_id'])) die("Empty SESSION ID");
$session_id = $_GET['session_id'];
if(empty($_GET['media_id'])) $media_id 	= 0;
else $media_id 	= $_GET['media_id'];
if(empty($_GET['partner_id'])) $partner_id = 0;
else $partner_id = $_GET['partner_id'];
if(empty($_GET['url_dr'])) $url_dr 		= "";
else $url_dr 		= $_GET['url_dr'];
if(empty($_GET['priority'])) $priority 		= 99;
else $priority 		= $_GET['priority'];
if(empty($_GET['shortname'])) $shortname 		= "";
else $shortname 		= $_GET['shortname'];

if( eregi("^PUSH",strtoupper($subject)) ) $spool = range (2,3); // PUSH
else $spool = range (1,1); // PULL
srand ((double)microtime()*1000000);
shuffle ($spool);

$full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".$charge."@#@".$media_id."@#@".$partner_id."@#@".$appid."@#@".$subject."@#@".$priority."@#@".$url."@#@".$mt_type."@#@".$url_dr."@#@".$shortname."@#@".$reply_sms;

$fp = fopen ($config['base_path'].$config['spool_mt_flexi'][$spool[0]]."/mt-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

echo "1";

?>