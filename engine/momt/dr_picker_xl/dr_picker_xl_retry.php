<?php

#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: dr_picker.php
#			FUNCTION			: Read SMS from File, process to application
#			MODIFIED			: October, 22 2008
#
#################################################################################

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
require($file_path."include/mysql.class.php");
include($file_path."include/class_aiom.php");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$i = 1;

$path = $config['dr_picker_xl_path']."unsent/";

while(true){

	if (!$db->ping()) {
		$db->closeDb();
		$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);
	}

	$cekFile = opendir($path);
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}

	closedir($cekFile);
	sleep(1);
}
$db->close();
exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $db, $path, $config;

	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);

		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}

		//$full_sms = $trx_id."@#@".$status."@#@".$error_code."@#@".$sid_success."@#@".$charge_success."@#@".date("YmdHis");
		//100001200301160605051109425661@#@102@#@@#@@#@@#@20160605121041 , 100001200401160604064311623871@#@102@#@@#@@#@@#@20160604134243
		// 100001200401160603045540979771@#@102@#@@#@@#@@#@20160603115512
		$trx_id = $mText[1];
		$status_xl = $mText[2];
		$errorcode = $mText[3];
		$sid = $mText[4];
		$charge = $mText[5];
		$trx_date = $mText[6];

		$status_telco = $status_xl."@#@".$errorcode;

		if( trim($status_xl) == "102" ) $status_dr = "DELIVERED";
		else $status_dr = "REJECTED";

		if( $status_xl == "102" && !empty( $sid ) ) {
			$sqlsqll = ",charge='".$charge."',sid='".$sid."'";
		}
		else {
			$sqlsqll = "";
		}

		$sql = "SELECT msisdn,shortcode,trx_id,telco,url_dr,session_id,shortname FROM mt_outgoing_log_".date("Ymd")." WHERE trx_id='$trx_id'";
		$result = $db->query($sql);
		if ($row = $db->fetch($result)){
			$session_id = $row['session_id'];
			$msisdn = $row['msisdn'];
			$shortcode = $row['shortcode'];
			$trx_id = $row['trx_id'];
			$telco = $row['telco'];

			if( $errorcode == "0000000000001001" ) {
				$session_id_unreg = $msisdn.rand(0,10000);
				$sqluuu = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 6, unreg_reason = '6' WHERE msisdn = '".$msisdn."'";
				$db->query($sqluuu);
			}

			if($row['url_dr'] <> "") {

				$url_dr = $row['url_dr'];

				$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr;
				$url = $url_dr.$param;

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 5);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

				$data = curl_exec($ch);

				curl_close($ch);

				$sql = "UPDATE mt_outgoing_log_".date("Ymd")." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
				$sql_result = $db->query($sql);
				if($sql_result) {

					$sqli = "INSERT IGNORE INTO dr_incoming_log".date("Ym")." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
								VALUES ('$telco','$shortcode','$msisdn','$session_id','$sid','$status_telco',NOW());";
					$result = $db->query($sqli);
					if($result) {
						return true;
					}
					else {
						return false;
					}

				}
				else {
					return false;
				}

			}
			else {

				$sql = "UPDATE mt_outgoing_log_".date("Ymd")." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
				$sql_result = $db->query($sql);
				if($sql_result) {

					$sqli = "INSERT IGNORE INTO dr_incoming_log".date("Ym")." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
								VALUES ('$telco','$shortcode','$msisdn','$session_id','$sid','$status_telco',NOW());";
					$result = $db->query($sqli);
					if($result) {
						return true;
					}
					else {
						return false;
					}

				}
				else {
					return false;
				}

			}

		}
		else {

			$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
			$bln = date('Ymd', $date);
			$blndr = date('Ym', $date);

			$sql = "SELECT msisdn,shortcode,trx_id,telco,url_dr,session_id FROM mt_outgoing_log_".$bln." WHERE trx_id='$trx_id'";
			$result = $db->query($sql);
			if ($row = $db->fetch($result)){
				$session_id = $row['session_id'];
				$msisdn = $row['msisdn'];
				$shortcode = $row['shortcode'];
				$trx_id = $row['trx_id'];
				$telco = $row['telco'];

				if( $errorcode == "0000000000001001" ) {
				$session_id_unreg = $msisdn.rand(0,10000);
				$sqluuu = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id_unreg."', date_unreg = NOW(), status = 6, unreg_reason = '6' WHERE msisdn = '".$msisdn."'";
				$db->query($sqluuu);
			}

				if($row['url_dr'] <> "") {

					$url_dr = $row['url_dr'];

					$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr;
					$url = $url_dr.$param;

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 5);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

					$data = curl_exec($ch);

					curl_close($ch);

					$sql = "UPDATE mt_outgoing_log_".$bln." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
					$sql_result = $db->query($sql);
					if($sql_result) {

						$sqli = "INSERT IGNORE INTO dr_incoming_log".$blndr." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
									VALUES ('$telco','$shortcode','$msisdn','$session_id','$sid','$status_telco',NOW());";
						$result = $db->query($sqli);
						if($result) {
							return true;
						}
						else {
							return false;
						}

					}
					else {
						return false;
					}

				}
				else {

					$sql = "UPDATE mt_outgoing_log_".$bln." SET dn_telco = '$status_telco', dn_status = '".mysql_escape_string($status_dr)."' $sqlsqll WHERE msisdn='$msisdn' AND session_id='$session_id'";
					$sql_result = $db->query($sql);
					if($sql_result) {

						$sqli = "INSERT IGNORE INTO dr_incoming_log".$blndr." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
									VALUES ('$telco','$shortcode','$msisdn','$session_id','$sid','$status_telco',NOW());";
						$result = $db->query($sqli);
						if($result) {
							return true;
						}
						else {
							return false;
						}

					}
					else {
						return false;
					}

				}

			}
			else {
				if( substr($trx_date,0,8) == date("Ymd") ) {
					return false;
				}
				else {
					return true;
				}

			}
		}

	}
	else {
		return false;
	}
}

?>
