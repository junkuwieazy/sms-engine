<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Telkomsel
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: read_mo_tsel.php
#			FUNCTION			: Read SMS from File, store it to file for further process and log to database
#			MODIFIED			: October, 20 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for live traffic mt!\n");

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
include($file_path."include/class_aiom.php");
require($file_path."include/mysql.class.php");

$i = 1;
$folder = trim($argv[1]);

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$path = $config['read_live_traffic_mt_path'].$folder."/";

$file_monitoring = $config['live_traffic_mt_monitoring_path']."live_traffic_mt_".$folder.".txt";

while(true){
	
	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);
	
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
}
exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $db;
	
	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);
		
		if( $SMSRow == "" ) {
			return true;
		}
		else {
			$sql = $SMSRow;
			$result = $db->query($sql);
			if( $result ) return true;
			else return false;
		}
		
	}
	else {
		return false;
	}
}

?>