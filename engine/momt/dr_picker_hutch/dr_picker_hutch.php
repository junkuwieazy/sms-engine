<?php

#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: dr_picker.php
#			FUNCTION			: Read SMS from File, process to application
#			MODIFIED			: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for dr picker hutch!\n");

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
require($file_path."include/mysql.class.php");
include($file_path."include/class_aiom.php");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$i = 1;
$folder = trim($argv[1]);

$path = $config['dr_picker_hutch_path'].$folder."/";

$file_monitoring = $config['dr_process_hutch_monitoring_path']."dr_picker_".$folder.".txt";

while(true){
	
	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}
	
	if (!$db->ping()) {
		$db->closeDb();
		$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);
	}

	$cekFile = opendir($path);
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	
	closedir($cekFile);
	sleep(1);
}
$db->close();
exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $db, $path, $config;
	
	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);
		
		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		
		//6289622625884@#@hutch@#@92000@#@4d0b8416-0cac-469e-b8b9-6f69b1386ed9@#@20150909173919@#@62896226258842015090917391989175260@#@SUBMITTED@#@20150909173919
		//6289622625884@#@hutch@#@92000@#@4d0b8416-0cac-469e-b8b9-6f69b1386ed9@#@20150909173926@#@62896226258842015090917392650233339@#@DELIVRD@#@20150909173926
		$msisdn = $mText[1];
		$telco = $mText[2];
		$shortcode = $mText[3];
		$trx_id = $mText[4];
		$trx_date = $mText[5];
		$status_telco = $mText[7];
		
		$status_dr = get_status($status_telco);
		
		if( $status_telco == "SUBMITTED" ) {
			return true;
		}
		else {
			////
			$sql = "SELECT url_dr,session_id,shortcode,partner_id,charge,sms FROM mt_outgoing_log".date("Ym")." WHERE trx_id='$trx_id'";
			$result = $db->query($sql);
			if ($row = $db->fetch($result)){
				$session_id = $row['session_id'];
				$shortcode = $row['shortcode'];
				if($row['url_dr'] <> "") {
					
					$url_dr = $row['url_dr'];
					
					$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr;
					$url = $url_dr.$param;
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 5);
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
					
					$data = curl_exec($ch);
					
					curl_close($ch);

					$sql = "UPDATE mt_outgoing_log".date("Ym")." SET dn_telco = '$status_telco', dn_status = '$status_dr' WHERE session_id='$session_id'";
					$sql_result = $db->query($sql);
					if($sql_result) {
						
						$sqli = "INSERT IGNORE INTO dr_incoming_log".date("Ym")." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
									VALUES ('$telco','$shortcode','$msisdn','$trx_id','','$status_telco',NOW());";
						$result = $db->query($sqli);
						if($result) {
							return true;
						}
						else {
							return false;
						}
						
					}
					
					return true;
					
				}
				else {
					
					$sql = "UPDATE mt_outgoing_log".date("Ym")." SET dn_telco = '$status_telco', dn_status = '$status_dr' WHERE session_id='$session_id'";
					$sql_result = $db->query($sql);
					if($sql_result) {
						
						$sqli = "INSERT IGNORE INTO dr_incoming_log".date("Ym")." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
									VALUES ('$telco','$shortcode','$msisdn','$trx_id','','$status_telco',NOW());";
						$result = $db->query($sqli);
						if($result) {
							return true;
						}
						else {
							return false;
						}
						
					}
					
					return true;
					
				}
				
			}
			else {
				
				$tanggal  = mktime(0, 0, 0, date("m")-1,  date("y"));
				$bln = date('Ym', $tanggal);
				
				$sql = "SELECT url_dr,session_id,shortcode,partner_id,charge,sms FROM mt_outgoing_log".$bln." WHERE trx_id='$trx_id'";
				$result = $db->query($sql);
				if ($row = $db->fetch($result)){
					$session_id = $row['session_id'];
					$shortcode = $row['shortcode'];
					if($row['url_dr'] <> "") {
						
						$url_dr = $row['url_dr'];
						
						$param = "?msisdn=".$msisdn."&shortcode=$shortcode&trx_id=".$trx_id."&session_id=".$session_id."&status=".$status_dr;
						$url = $url_dr.$param;
						
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_TIMEOUT, 5);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
						
						$data = curl_exec($ch);
						
						curl_close($ch);
						
					}
					else {
						
						$sql = "UPDATE mt_outgoing_log".$bln." SET dn_telco = '$status_telco', dn_status = '$status_dr' WHERE session_id='$session_id'";
						$sql_result = $db->query($sql);
						if($sql_result) {
							
							$sqli = "INSERT IGNORE INTO dr_incoming_log".$bln." (telco,shortcode,msisdn,trx_id,sid,status,datetime)
										VALUES ('$telco','$shortcode','$msisdn','$trx_id','','$status_telco',NOW());";
							$result = $db->query($sqli);
							if($result) {
								return true;
							}
							else {
								return false;
							}
							
						}
						
						return true;
						
					}
					
				}
				else return false;
			}
			////
		}
		
	}
	else {
		return false;
	}
}

function get_status($status_telco) {
	switch(strtoupper($status_telco)) {
		case "DELIVRD" : $status_dr = "DELIVERED"; break;
		default : $status_dr = "REJECTED"; break;
	}
	return $status_dr;
}

?>