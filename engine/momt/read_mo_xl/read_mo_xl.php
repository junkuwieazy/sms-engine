<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Telkomsel
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: read_mo_xl.php
#			FUNCTION			: Read SMS from File, store it to file for further process and log to database
#			MODIFIED			: October, 20 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for read mo xl!\n");

$file_path = "/var/www/sms/engine/";

require($file_path."include/config.php");
include($file_path."include/class_aiom.php");

$i = 1;
$folder = trim($argv[1]);

$path = $config['read_mo_xl_path'].$folder."/";

$file_monitoring = $config['read_mo_xl_monitoring_path']."read_mo_xl_".$folder.".txt";

while(true){

	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);

	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
}
exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $msisdn_blacklist, $folder;

	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);

		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		foreach ($SMSExploded as $mField) {
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}

		$mo_origin = $mText[1];
		$msisdn = $mText[2];
		$prepaid = $mText[3];
		$sms = $mText[4];
		$telco = $mText[5];
		$shortcode = $mText[6];
		$trx_id = $mText[7];
		$trx_date = $mText[8];
		$session_id = $mText[9];

		$url = $config['url_app'];

		$param = "?mo_origin=".$mo_origin."&msisdn=".$msisdn."&prepaid=".urlencode($prepaid)."&sms=".urlencode($sms)."&telco=$telco&shortcode=$shortcode&trx_id=".$trx_id."&trx_date=".$trx_date."&session_id=".$session_id;
		$url_logic = $url.$param;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_logic);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

		$data = curl_exec($ch);

		curl_close($ch);

		$date_now = date("Y-m-d H:i:s");
		$sql = "insert ignore into mp_system.mo_incoming_log".date("Ym")." (mo_origin, msisdn, prepaid, sms, telco, shortcode,trx_id, trx_date, session_id, datetime, mo_status) values ";
		$sql = $sql."('$mo_origin', '".mysql_escape_string($msisdn)."', '$prepaid', '".mysql_escape_string($sms)."', '$telco', '$shortcode','".mysql_escape_string($trx_id)."', '$trx_date', '$session_id', '$date_now','".$url_logic." -- ".$data."');";

		$spool = range (1,SizeOf($config['spool_live_traffic']));
		srand ((double)microtime()*1000000);
		shuffle ($spool);

		$fp = fopen ($config['base_path'].$config['spool_live_traffic'][$spool[0]]."/sql-".$session_id.".sql", "w+");
		fputs($fp, $sql );
		fclose($fp);

		if (preg_match ("/\bError 404\b/i", $data) || $data == "") {
			return false;
		}
		else {
			 return true;
		}

	}
	else {
		return false;
	}
}

?>
