#!/bin/sh

allprocess="
/mt_send_sakti.php 1
/mt_send_sakti.php 2
/mt_send_sakti.php 3
/mt_send_sakti.php 4
/mt_send_sakti.php 5
/mt_send_sakti.php 6
/mt_send_sakti.php 7
/mt_send_sakti.php 8
/mt_send_sakti.php 9
/mt_send_sakti.php 10
/mt_send_sakti.php 11
/mt_send_sakti.php 12
/mt_send_sakti.php 13
/mt_send_sakti.php 14
/mt_send_sakti.php 15
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/mt_send_sakti/
		/usr/bin/php /var/www/sms/engine/momt/mt_send_sakti/mt_send_sakti.php $counter &
	fi
	
counter=`expr $counter + 1`
done
