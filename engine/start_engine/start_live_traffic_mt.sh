#!/bin/sh

allprocess="
/live_traffic_mt.php 1
/live_traffic_mt.php 2
/live_traffic_mt.php 3
/live_traffic_mt.php 4
/live_traffic_mt.php 5
/live_traffic_mt.php 6
/live_traffic_mt.php 7
/live_traffic_mt.php 8
/live_traffic_mt.php 9
/live_traffic_mt.php 10
/live_traffic_mt.php 11
/live_traffic_mt.php 12
/live_traffic_mt.php 13
/live_traffic_mt.php 14
/live_traffic_mt.php 15
/live_traffic_mt.php 16
/live_traffic_mt.php 17
/live_traffic_mt.php 18
/live_traffic_mt.php 19
/live_traffic_mt.php 20
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/live_traffic/
		/usr/bin/php /var/www/sms/engine/momt/live_traffic/live_traffic_mt.php $counter &
	fi
	
counter=`expr $counter + 1`
done
