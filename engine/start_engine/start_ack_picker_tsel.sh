#!/bin/sh

allprocess="
/ack_picker_tsel.php 1
/ack_picker_tsel.php 2
/ack_picker_tsel.php 3
/ack_picker_tsel.php 4
/ack_picker_tsel.php 5
/ack_picker_tsel.php 6
/ack_picker_tsel.php 7
/ack_picker_tsel.php 8
/ack_picker_tsel.php 9
/ack_picker_tsel.php 10
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/ack_picker_tsel/
		/usr/bin/php /var/www/sms/engine/momt/ack_picker_tsel/ack_picker_tsel.php $counter &
	fi
	
counter=`expr $counter + 1`
done
