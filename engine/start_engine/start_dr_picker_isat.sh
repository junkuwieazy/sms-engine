#!/bin/sh

allprocess="
/dr_picker_isat.php 1
/dr_picker_isat.php 2
/dr_picker_isat.php 3
/dr_picker_isat.php 4
/dr_picker_isat.php 5
/dr_picker_isat.php 6
/dr_picker_isat.php 7
/dr_picker_isat.php 8
/dr_picker_isat.php 9
/dr_picker_isat.php 10
/dr_picker_isat.php 11
/dr_picker_isat.php 12
/dr_picker_isat.php 13
/dr_picker_isat.php 14
/dr_picker_isat.php 15
/dr_picker_isat.php 16
/dr_picker_isat.php 17
/dr_picker_isat.php 18
/dr_picker_isat.php 19
/dr_picker_isat.php 20
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do

	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`

	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/dr_picker_isat/
		/usr/bin/php /var/www/sms/engine/momt/dr_picker_isat/dr_picker_isat.php $counter &
	fi

counter=`expr $counter + 1`
done
