#!/bin/sh

allprocess="
/read_mo_tsel.php 1
/read_mo_tsel.php 2
/read_mo_tsel.php 3
/read_mo_tsel.php 4
/read_mo_tsel.php 5
/read_mo_tsel.php 6
/read_mo_tsel.php 7
/read_mo_tsel.php 8
/read_mo_tsel.php 9
/read_mo_tsel.php 10
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/read_mo_tsel/
		/usr/bin/php /var/www/sms/engine/momt/read_mo_tsel/read_mo_tsel.php $counter &
	fi
	
counter=`expr $counter + 1`
done
