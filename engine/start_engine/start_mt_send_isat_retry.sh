#!/bin/sh

allprocess="
/mt_send_isat_retry.php 1
/mt_send_isat_retry.php 2
/mt_send_isat_retry.php 3
/mt_send_isat_retry.php 4
/mt_send_isat_retry.php 5
/mt_send_isat_retry.php 6
/mt_send_isat_retry.php 7
/mt_send_isat_retry.php 8
/mt_send_isat_retry.php 9
/mt_send_isat_retry.php 10
/mt_send_isat_retry.php 11
/mt_send_isat_retry.php 12
/mt_send_isat_retry.php 13
/mt_send_isat_retry.php 14
/mt_send_isat_retry.php 15
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/mt_send_isat/
		/usr/bin/php /var/www/sms/engine/momt/mt_send_isat/mt_send_isat_retry.php $counter &
	fi
	
counter=`expr $counter + 1`
done
