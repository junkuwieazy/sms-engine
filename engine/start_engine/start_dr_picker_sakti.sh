#!/bin/sh

allprocess="
/dr_picker_sakti.php 1
/dr_picker_sakti.php 2
/dr_picker_sakti.php 3
/dr_picker_sakti.php 4
/dr_picker_sakti.php 5
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/dr_picker_sakti/
		/usr/bin/php /var/www/sms/engine/momt/dr_picker_sakti/dr_picker_sakti.php $counter &
	fi
	
counter=`expr $counter + 1`
done
