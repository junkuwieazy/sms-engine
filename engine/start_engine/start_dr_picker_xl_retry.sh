#!/bin/sh

allprocess="
/dr_picker_xl_retry.php
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/dr_picker_xl/
		/usr/bin/php /var/www/sms/engine/momt/dr_picker_xl/dr_picker_xl_retry.php &
	fi
	
counter=`expr $counter + 1`
done
