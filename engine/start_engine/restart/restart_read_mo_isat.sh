#!/bin/sh

allprocess="
/read_mo_isat.php 1
/read_mo_isat.php 2
/read_mo_isat.php 3
/read_mo_isat.php 4
/read_mo_isat.php 5
/read_mo_isat.php 6
/read_mo_isat.php 7
/read_mo_isat.php 8
/read_mo_isat.php 9
/read_mo_isat.php 10
"

# Redefine IFS with >
IFS=">
"

counter=1
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo "Stoppping " $i
		kill $apa
	fi
	
counter=`expr $counter + 1`
done

counterstart=1
apastart=""

for j in $allprocess
do
	
	apastart=`ps -eo "%p %a" | grep -P "$j$" | grep -v grep | cut -c1-5`
	
	if [[ $apastart == ""  ]]; then
		echo "starting" $j
		cd /var/www/sms/engine/momt/read_mo_isat/
		/usr/bin/php /var/www/sms/engine/momt/read_mo_isat/read_mo_isat.php $counterstart &
	fi
	
counterstart=`expr $counterstart + 1`
done
