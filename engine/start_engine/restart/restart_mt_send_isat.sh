#!/bin/sh

allprocess="
/mt_send_isat.php 1
/mt_send_isat.php 2
/mt_send_isat.php 3
/mt_send_isat.php 4
/mt_send_isat.php 5
/mt_send_isat.php 6
/mt_send_isat.php 7
/mt_send_isat.php 8
/mt_send_isat.php 9
/mt_send_isat.php 10
/mt_send_isat.php 11
/mt_send_isat.php 12
/mt_send_isat.php 13
/mt_send_isat.php 14
/mt_send_isat.php 15
/mt_send_isat.php 16
/mt_send_isat.php 17
/mt_send_isat.php 18
/mt_send_isat.php 19
/mt_send_isat.php 20
/mt_send_isat.php 21
/mt_send_isat.php 22
/mt_send_isat.php 23
/mt_send_isat.php 24
/mt_send_isat.php 25
/mt_send_isat.php 26
/mt_send_isat.php 27
/mt_send_isat.php 28
/mt_send_isat.php 29
/mt_send_isat.php 30
/mt_send_isat.php 31
/mt_send_isat.php 32
/mt_send_isat.php 33
/mt_send_isat.php 34
/mt_send_isat.php 35
/mt_send_isat.php 36
/mt_send_isat.php 37
/mt_send_isat.php 38
/mt_send_isat.php 39
/mt_send_isat.php 40
/mt_send_isat.php 41
/mt_send_isat.php 42
/mt_send_isat.php 43
/mt_send_isat.php 44
/mt_send_isat.php 45
/mt_send_isat.php 46
/mt_send_isat.php 47
/mt_send_isat.php 48
/mt_send_isat.php 49
/mt_send_isat.php 50
/mt_send_isat.php 51
/mt_send_isat.php 52
/mt_send_isat.php 53
/mt_send_isat.php 54
/mt_send_isat.php 55
/mt_send_isat.php 56
/mt_send_isat.php 57
/mt_send_isat.php 58
/mt_send_isat.php 59
/mt_send_isat.php 60
/mt_send_isat.php 61
/mt_send_isat.php 62
/mt_send_isat.php 63
/mt_send_isat.php 64
/mt_send_isat.php 65
/mt_send_isat.php 66
/mt_send_isat.php 67
/mt_send_isat.php 68
/mt_send_isat.php 69
/mt_send_isat.php 70
/mt_send_isat.php 71
/mt_send_isat.php 72
/mt_send_isat.php 73
/mt_send_isat.php 74
/mt_send_isat.php 75
/mt_send_isat.php 76
/mt_send_isat.php 77
/mt_send_isat.php 78
/mt_send_isat.php 79
/mt_send_isat.php 80
/mt_send_isat.php 81
/mt_send_isat.php 82
/mt_send_isat.php 83
/mt_send_isat.php 84
/mt_send_isat.php 85
/mt_send_isat.php 86
/mt_send_isat.php 87
/mt_send_isat.php 88
/mt_send_isat.php 89
/mt_send_isat.php 90
/mt_send_isat.php 91
/mt_send_isat.php 92
/mt_send_isat.php 93
/mt_send_isat.php 94
/mt_send_isat.php 95
/mt_send_isat.php 96
/mt_send_isat.php 97
/mt_send_isat.php 98
/mt_send_isat.php 99
/mt_send_isat.php 100
/mt_send_isat.php 101
/mt_send_isat.php 102
/mt_send_isat.php 103
/mt_send_isat.php 104
/mt_send_isat.php 105
/mt_send_isat.php 106
/mt_send_isat.php 107
/mt_send_isat.php 108
/mt_send_isat.php 109
/mt_send_isat.php 110
/mt_send_isat.php 111
/mt_send_isat.php 112
/mt_send_isat.php 113
/mt_send_isat.php 114
/mt_send_isat.php 115
/mt_send_isat.php 116
/mt_send_isat.php 117
/mt_send_isat.php 118
/mt_send_isat.php 119
/mt_send_isat.php 120
/mt_send_isat.php 121
/mt_send_isat.php 122
/mt_send_isat.php 123
/mt_send_isat.php 124
/mt_send_isat.php 125
/mt_send_isat.php 126
/mt_send_isat.php 127
/mt_send_isat.php 128
/mt_send_isat.php 129
/mt_send_isat.php 130
/mt_send_isat.php 131
/mt_send_isat.php 132
/mt_send_isat.php 133
/mt_send_isat.php 134
/mt_send_isat.php 135
/mt_send_isat.php 136
/mt_send_isat.php 137
/mt_send_isat.php 138
/mt_send_isat.php 139
/mt_send_isat.php 140
/mt_send_isat.php 141
/mt_send_isat.php 142
/mt_send_isat.php 143
/mt_send_isat.php 144
/mt_send_isat.php 145
/mt_send_isat.php 146
/mt_send_isat.php 147
/mt_send_isat.php 148
/mt_send_isat.php 149
/mt_send_isat.php 150
"

# Redefine IFS with >
IFS=">
"

counter=1
apa=""

for i in $allprocess
do

	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`

	if [[ $apa != ""  ]]; then
		echo "Stoppping " $i
		kill $apa
	fi

counter=`expr $counter + 1`
done

counterstart=1
apastart=""

for j in $allprocess
do

	apastart=`ps -eo "%p %a" | grep -P "$j$" | grep -v grep | cut -c1-5`

	if [[ $apastart == ""  ]]; then
		echo "starting" $j
		cd /var/www/sms/engine/momt/mt_send_isat/
		/usr/bin/php /var/www/sms/engine/momt/mt_send_isat/mt_send_isat.php $counterstart &
	fi

counterstart=`expr $counterstart + 1`
done
