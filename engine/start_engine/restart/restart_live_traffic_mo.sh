#!/bin/sh

allprocess="
/live_traffic.php 1
/live_traffic.php 2
/live_traffic.php 3
/live_traffic.php 4
/live_traffic.php 5
/live_traffic.php 6
/live_traffic.php 7
/live_traffic.php 8
/live_traffic.php 9
/live_traffic.php 10
"

# Redefine IFS with >
IFS=">
"

counter=1
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo "Stoppping " $i
		kill $apa
	fi
	
counter=`expr $counter + 1`
done

counterstart=1
apastart=""

for j in $allprocess
do
	
	apastart=`ps -eo "%p %a" | grep -P "$j$" | grep -v grep | cut -c1-5`
	
	if [[ $apastart == ""  ]]; then
		echo "starting" $j
		cd /var/www/sms/engine/momt/live_traffic/
		/usr/bin/php /var/www/sms/engine/momt/live_traffic/live_traffic.php $counterstart &
	fi
	
counterstart=`expr $counterstart + 1`
done
