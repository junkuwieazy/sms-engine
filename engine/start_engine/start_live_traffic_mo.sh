#!/bin/sh

allprocess="
/live_traffic.php 1
/live_traffic.php 2
/live_traffic.php 3
/live_traffic.php 4
/live_traffic.php 5
/live_traffic.php 6
/live_traffic.php 7
/live_traffic.php 8
/live_traffic.php 9
/live_traffic.php 10
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/live_traffic/
		/usr/bin/php /var/www/sms/engine/momt/live_traffic/live_traffic.php $counter &
	fi
	
counter=`expr $counter + 1`
done
