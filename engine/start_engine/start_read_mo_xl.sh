#!/bin/sh

allprocess="
/read_mo_xl.php 1
/read_mo_xl.php 2
/read_mo_xl.php 3
/read_mo_xl.php 4
/read_mo_xl.php 5
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/read_mo_xl/
		/usr/bin/php /var/www/sms/engine/momt/read_mo_xl/read_mo_xl.php $counter &
	fi
	
counter=`expr $counter + 1`
done
