#!/bin/sh

allprocess="
/mt_send_xl.php 1
/mt_send_xl.php 2
/mt_send_xl.php 3
/mt_send_xl.php 4
/mt_send_xl.php 5
/mt_send_xl.php 6
/mt_send_xl.php 7
/mt_send_xl.php 8
/mt_send_xl.php 9
/mt_send_xl.php 10
/mt_send_xl.php 11
/mt_send_xl.php 12
/mt_send_xl.php 13
/mt_send_xl.php 14
/mt_send_xl.php 15
/mt_send_xl.php 16
/mt_send_xl.php 17
/mt_send_xl.php 18
/mt_send_xl.php 19
/mt_send_xl.php 20
/mt_send_xl.php 21
/mt_send_xl.php 22
/mt_send_xl.php 23
/mt_send_xl.php 24
/mt_send_xl.php 25
/mt_send_xl.php 26
/mt_send_xl.php 27
/mt_send_xl.php 28
/mt_send_xl.php 29
/mt_send_xl.php 30
/mt_send_xl.php 31
/mt_send_xl.php 32
/mt_send_xl.php 33
/mt_send_xl.php 34
/mt_send_xl.php 35
/mt_send_xl.php 36
/mt_send_xl.php 37
/mt_send_xl.php 38
/mt_send_xl.php 39
/mt_send_xl.php 40
/mt_send_xl.php 41
/mt_send_xl.php 42
/mt_send_xl.php 43
/mt_send_xl.php 44
/mt_send_xl.php 45
/mt_send_xl.php 46
/mt_send_xl.php 47
/mt_send_xl.php 48
/mt_send_xl.php 49
/mt_send_xl.php 50
"

# Redefine IFS with >
IFS=">
"

counter=1

run_flag="false"
apa=""

for i in $allprocess
do

	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`

	if [[ $apa != ""  ]]; then
		echo $i "is already running with PID $apa"
	else
		echo "starting" $i
		cd /var/www/sms/engine/momt/mt_send_xl/
		/usr/bin/php /var/www/sms/engine/momt/mt_send_xl/mt_send_xl.php $counter &
	fi

counter=`expr $counter + 1`
done
