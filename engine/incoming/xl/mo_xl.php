<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Indosat
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mo_xl.php
#			FUNCTION			: Receive parameter SMS from Telco
#			MODIFIED			: October, 20 2008
#
#################################################################################
///sms/engine/incoming/xl/mo_http_xl.php?X-Source-Addr=119957018&X-Dest-Addr=99595&X-Pull-Trx-Id=11161148075604805124&_SC=Jodoh&_TID=100001200401141211091148612851
// get parameter from telco
if(empty($_GET["X-Source-Addr"])) {
	die("Empty MSISDN");
}
$shortname = "";
$mo_origin	= "sms";
$msisdn		= trim($_GET["X-Source-Addr"]);
$msisdn		= ConvertToFullMsisdn($msisdn);
$trx_date	= date("YmdHis");
$sms			= $_GET['_SC'];
$arsms = explode(" ",$sms);

$prepaid	= "0";
$telco	  	= "xl";

if(empty($_GET["X-Pull-Trx-Id"]) ) $trx_id	= "";
else $trx_id	= trim($_GET["X-Pull-Trx-Id"]);

if(empty($_GET['_TID']) ) $session_id = $msisdn.date("YmdHis").rand();
else $session_id	= $_GET['_TID'];

if(empty($_GET["X-Dest-Addr"]) ) $shortcode	= "95799";
else $shortcode	= trim($_GET["X-Dest-Addr"]);

// spool config
include_once("/var/www/sms/engine/include/config.php");

$spool = range (1,SizeOf($config['spool_mo_xl']));
srand ((double)microtime()*1000000);
shuffle ($spool);

// spool mo sms in file
$full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."-".$shortname."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_mo_xl'][$spool[0]]."/mo-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/mo/xl/";

$m_FileLOG = fopen($pathLOG."smslog".date("Ymd").".txt","a");
$file = "mo=".$mo_origin."|msisdn=".$msisdn."|prepaid=".$prepaid."|sms=".$sms."|telco=".$telco."|shortcode=".$shortcode."|trx_id=".$trx_id."|trx_date=".$trx_date."|session_id=".$session_id."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);

echo "OK";

function ConvertToFullMsisdn($msisdn) {
	$msisdn = trim($msisdn);
	if ($msisdn{0} == "0") {
		$msisdn = "62". substr($msisdn, 1);
	}
	$msisdn = str_replace("+","",$msisdn);
	return $msisdn;
}

?>
