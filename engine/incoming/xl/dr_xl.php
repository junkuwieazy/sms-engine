<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Indosat
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mo_xl.php
#			FUNCTION			: Receive parameter SMS from Telco
#			MODIFIED			: October, 20 2008
#
#################################################################################
///sms/engine/incoming/xl/dr_http_xl.php?_tid=100001200301141211114754903141&status_id=101&dtdone=20141211114754&errorcode=0030000000002208&errordescription=000000
///sms/engine/incoming/xl/dr_http_xl.php?_tid=100001200401150212071443597421&status_id=102&dtdone=20150212071447
// get parameter from telco
if(empty( $_GET['_tid'] )) {
	die("Empty TID");
}
$trx_id	= $_GET['_tid'];

$status	= $_GET['status_id'];

$trx_date	= date("YmdHis");
$telco	  	= "xl";

$sid_success = "";

if( !empty($_GET['errorcode']) ) $error_code = $_GET['errorcode'];
else $error_code = "";

$session_id = date("YmdHis").rand(0,10000);

// spool config
include_once("../../include/config.php");
include_once("/var/www/sms/engine/momt/mt_send_xl/config.xl.php");

if( $sid_success <> "" ) $charge_success = $config_xl[ $sid_success ];
else $charge_success = "";

$spool = range (1,SizeOf($config['spool_dr_process_xl']));
srand ((double)microtime()*1000000);
shuffle ($spool);

$full_sms = $trx_id."@#@".$status."@#@".$error_code."@#@".$sid_success."@#@".$charge_success."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_dr_process_xl'][$spool[0]]."/dr-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/dr/xl/";

$m_FileLOG = fopen($pathLOG."smsdr".date("Ymd").".txt","a");
$file = "trx_id=".$trx_id."|status=".$status."|error_code=".$error_code."|sid_success=".$sid_success."|charge_success=".$charge_success."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);
  
echo 'OK';

?>