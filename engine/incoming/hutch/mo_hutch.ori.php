<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Telkomsel
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mo_hutch.php
#			FUNCTION			: Receive parameter SMS from Telco
#			MODIFIED			: October, 20 2008
#
#################################################################################

// get parameter from telco
if(empty($_GET['mobile_no'])) {
	die("Empty MSISDN");
}
$msisdn		= $_GET['mobile_no'];
$msisdn		= ConvertToFullMsisdn($msisdn);
$trx_date 	= date("YmdHis");
if( !empty($_GET['trx_id']) ) $trx_id   	= $_GET['trx_id'];
else $trx_id   	= "";
$sms	   	= $_GET['message'];
if(!empty($_GET['prepaid'])) {
	$prepaid	= $_GET['prepaid'];
}
else {
	$prepaid	= 0;
}
if(!empty($_GET['short_code'])) {
	$shortcode	= $_GET['short_code'];
}
else {
	$shortcode	= "97555";
}
$telco	  	= "hutch";
$mo_origin	= "sms";

// spool config
include_once("../../include/config.php");

$spool = range (1,SizeOf($config['spool_mo_hutch']));
srand ((double)microtime()*1000000);
shuffle ($spool);

// spool mo sms in file
$session_id = $msisdn.date("YmdHis").rand(1000,9999).rand(1000,9999);

$full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_mo_hutch'][$spool[0]]."/mo-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/mo/hutch/";

$m_FileLOG = fopen($pathLOG."smslog".date("Ymd").".txt","a");
$file = "mo=".$mo_origin."|msisdn=".$msisdn."|prepaid=".$prepaid."|sms=".$sms."|telco=".$telco."|shortcode=".$shortcode."|trx_id=".$trx_id."|trx_date=".$trx_date."|session_id=".$session_id."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);

echo "OK";

function ConvertToFullMsisdn($msisdn) {
	$msisdn = trim($msisdn);
	if ($msisdn{0} == "0") {
		$msisdn = "62". substr($msisdn, 1);
	}
	$msisdn = str_replace("+","",$msisdn);
	return $msisdn;
}

?>