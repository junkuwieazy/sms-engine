<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Telkomsel
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: dr_hutch.php
#			FUNCTION			: Receive parameter SMS from Telco
#			MODIFIED			: October, 20 2008
#
#################################################################################

// get parameter from telco
if(empty($_GET['src'])) {
	die("Empty MSISDN");
}
$msisdn		= $_GET['src'];
$msisdn		= ConvertToFullMsisdn($msisdn);
$trx_date 	= date("YmdHis");
if(!empty($_GET['dst'])) {
	$shortcode	= $_GET['dst'];
}
else {
	$shortcode	= "97555";
}
$status	= $_GET['dlrstatus'];
$trx_id = $_GET['dlrmsgid'];
$telco	  	= "hutch";

// spool config
include_once("../../include/config.php");

$spool = range (1,SizeOf($config['spool_dr_process_hutch']));
srand ((double)microtime()*1000000);
shuffle ($spool);

// spool mo sms in file
$session_id = $msisdn.date("YmdHis").rand(1000,9999).rand(1000,9999);

$full_sms = $msisdn."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".$status."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_dr_process_hutch'][$spool[0]]."/dr-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/dr/hutch/";

$m_FileLOG = fopen($pathLOG."drlog".date("Ymd").".txt","a");
$file = "msisdn=".$msisdn."|telco=".$telco."|shortcode=".$shortcode."|trx_id=".$trx_id."|trx_date=".$trx_date."|session_id=".$session_id."|status=".$status."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);

echo "6000";

function ConvertToFullMsisdn($msisdn) {
	$msisdn = trim($msisdn);
	if ($msisdn{0} == "0") {
		$msisdn = "62". substr($msisdn, 1);
	}
	$msisdn = str_replace("+","",$msisdn);
	return $msisdn;
}

?>