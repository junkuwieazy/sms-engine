<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Telkomsel
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mo_tsel.php
#			FUNCTION			: Receive parameter SMS from Telco
#			MODIFIED			: October, 20 2008
#
#################################################################################

// Restrict Access
$tsel_allow_ip = array(
						"202.3.208.1", "202.3.208.2", "202.3.208.3", "202.3.208.4", 
						"202.152.30.1", "202.152.30.2", "202.152.30.3", "202.152.30.4", "202.152.30.5", 
						"202.152.30.6", "202.152.30.7", "202.152.30.8", "202.152.30.9", "202.152.30.10", 
						"202.152.30.11", "202.152.30.12", "202.152.30.13", "202.152.30.14", "202.152.30.15"
						);

//if(!in_array($_SERVER['REMOTE_ADDR'], $tsel_allow_ip)) die ("You are not authorized to view this page");

// get parameter from telco
if(empty($_GET['msisdn'])) {
	die("Empty MSISDN");
}
$msisdn		= $_GET['msisdn'];
$msisdn		= ConvertToFullMsisdn($msisdn);
$trx_date 	= date("YmdHis");
$trx_id   	= $_GET['trx_id'];
$sms	   	= $_GET['sms'];
if(!empty($_GET['prepaid'])) {
	$prepaid	= $_GET['prepaid'];
}
else {
	$prepaid	= 0;
}
if(!empty($_GET['adn'])) {
	$shortcode	= $_GET['adn'];
}
else {
	$shortcode	= "9117";
}
$telco	  	= "tsel";
$mo_origin	= "umb";

// spool config
include_once("../../include/config.php");

$spool = range (1,SizeOf($config['spool_mo_tsel']));
srand ((double)microtime()*1000000);
shuffle ($spool);

$ip = $_SERVER["REMOTE_ADDR"];
$ip_blkg = substr($ip,-1,1);

// spool mo sms in file
$session_id = $ip_blkg.$msisdn.date("YmdHis").rand(1000,9999).rand(1000,9999);

$full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_mo_tsel'][$spool[0]]."/mo-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/mo/tsel/";

$m_FileLOG = fopen($pathLOG."smslog".date("Ymd").".txt","a");
$file = "mo=".$mo_origin."|msisdn=".$msisdn."|prepaid=".$prepaid."|sms=".$sms."|telco=".$telco."|shortcode=".$shortcode."|trx_id=".$trx_id."|trx_date=".$trx_date."|session_id=".$session_id."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);

echo "1";

function ConvertToFullMsisdn($msisdn) {
	$msisdn = trim($msisdn);
	if ($msisdn{0} == "0") {
		$msisdn = "62". substr($msisdn, 1);
	}
	$msisdn = str_replace("+","",$msisdn);
	return $msisdn;
}

?>