<?php
#################################################################################
#
#			PROJECT			: SMS Center POLDA 7007
#			OPERATOR		: Indosat
#			AUTHOR			: Mujibur Rohman
#			EMAIL				: muji@muloska.com
#			FILENAME		: app_incoming.php
#			FUNCTION		: Receive parameter Delivery SMS from Telco
#			MODIFIED		: November 2012
#
#################################################################################

// get parameter from telco
if(empty($_GET['dest'])) {
	die("Empty MSISDN");
}
$msisdn		= $_GET['dest'];
$msisdn		= ConvertToFullMsisdn($msisdn);
$trx_date	= $_GET["time"];
$trx_id		= $_GET["tid"];
$sid			= $_GET["serviceid"];
$status 		= $_GET["status"];
$telco		= "isat";
$shortcode	= substr($sid,0,5);

// spool config
include_once("../../include/config.php");
include_once("/var/www/sms/engine/momt/mt_send_isat/config.isat.php");

//cool

if( substr( strtolower($trx_id),0,4) == "cool" ) {

	$url_logic2 = "http://pcr.replay.id/pconn/DR/index.php?time=".$_GET["time"]."&dest=".$_GET['dest']."&status=".$_GET["status"]."&tid=".$_GET["tid"]."&serviceid=".$_GET["serviceid"];

	$ch2 = curl_init();
	curl_setopt($ch2, CURLOPT_URL, $url_logic2);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch2, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch2, CURLOPT_HTTPGET, 1 );
	curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);

	$datamo2 = curl_exec($ch2);

	error_log("cool:".$url_logic2." --- ".$datamo2);

	curl_close($ch2);

	header("Content-type: text/xml");

	echo "<?xml version=\"1.0\"?>";
	echo '<DR>';
	echo '<STATUS>2</STATUS>';
	echo '<TRANSID>'.$trx_id.'</TRANSID>';
	echo '<MSG>OK</MSG>';
	echo '</DR>';
	exit();

}

$charge_success= $config_isat[ $sid ];

$spool = range (1,SizeOf($config['spool_dr_process_isat']));
srand ((double)microtime()*1000000);
shuffle ($spool);

// spool mo sms in file
$session_id = $msisdn.date("YmdHis").rand(1000,9999).rand(1000,9999);

$full_sms = $msisdn."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".$sid."@#@".$status."@#@".$charge_success."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_dr_process_isat'][$spool[0]]."/dr-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/dr/isat/";

$m_FileLOG = fopen($pathLOG."smsdr".date("Ymd").".txt","a");
$file = "msisdn=".$msisdn."|telco=".$telco."|shortcode=".$shortcode."|trx_id=".$trx_id."|trx_date=".$trx_date."|session_id=".$session_id."|sid=".$sid."|charge_success=".$charge_success."|status=".$status."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);

header("Content-type: text/xml");

echo "<?xml version=\"1.0\"?>";
echo '<DR>';
echo '<STATUS>2</STATUS>';
echo '<TRANSID>'.$trx_id.'</TRANSID>';
echo '<MSG>OK</MSG>';
echo '</DR>';

function ConvertToFullMsisdn($msisdn) {
	$msisdn = trim($msisdn);
	if ($msisdn{0} == "0") {
		$msisdn = "62". substr($msisdn, 1);
	}
	$msisdn = str_replace("+","",$msisdn);
	return $msisdn;
}

?>
