<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR			: Indosat
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME			: mo_isat.php
#			FUNCTION			: Receive parameter SMS from Telco
#			MODIFIED			: October, 20 2008
#
#################################################################################

$file_path = "/var/www/sms/engine/";

// get parameter from telco
if(empty($_GET['msisdn'])) {
	die("Empty MSISDN");
}
$msisdn		= $_GET['msisdn'];
$msisdn		= ConvertToFullMsisdn($msisdn);
$trx_date	= $_GET['trx_time'];
$trx_id		= $_GET['transid'];
$sms			= $_GET['sms'];
if(!empty($_GET['substype'])) {
	$prepaid	= $_GET['substype'];
}
else {
	$prepaid	= 0;
}
if(!empty($_GET['sc'])) {
	$shortcode	= $_GET['sc'];
}
else {
	$shortcode	= "95899";
}
$telco	  	= "isat";
$mo_origin	= "sms";

$arsms = split(" ",$sms);
/*
if( strtolower($arsms[1]) == "cool" ) {
	$url_logic2 = "http://pcr.replay.id/pconn/MO/index.php?msisdn=".$msisdn."&sms=".urlencode($sms)."&trx_time=".date("YmdHis")."&substype=".$_GET['substype']."&transid=".$_GET['transid']."&sc=".$_GET['sc'];

	$ch2 = curl_init();
	curl_setopt($ch2, CURLOPT_URL, $url_logic2);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch2, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch2, CURLOPT_HTTPGET, 1 );
	curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);

	$datamo2 = curl_exec($ch2);
	error_log("cool:".$url_logic2." --- ".$datamo2);
	curl_close($ch2);

	header("Content-type: text/xml");

	echo "<?xml version=\"1.0\"?>";
	echo '<MO>';
	echo '<STATUS>0</STATUS>';
	echo '<TRANSID>'.$trx_id.'</TRANSID>';
	echo '<MSG>OK</MSG>';
	echo '</MO>';
	exit();

}
*/
// spool config
include_once("../../include/config.php");

$session_id = $msisdn.date("YmdHis").rand(1000,9999).rand(1000,9999);

$spool = range (1,SizeOf($config['spool_mo_isat']));
srand ((double)microtime()*1000000);
shuffle ($spool);

$full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".date("YmdHis");

$fp = fopen ($config['base_path'].$config['spool_mo_isat'][$spool[0]]."/mo-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

// log sms in file
$pathLOG = $config['base_path']."/logs/mo/isat/";

$m_FileLOG = fopen($pathLOG."smslog".date("Ymd").".txt","a");
$file = "mo=".$mo_origin."|msisdn=".$msisdn."|prepaid=".$prepaid."|sms=".$sms."|telco=".$telco."|shortcode=".$shortcode."|trx_id=".$trx_id."|trx_date=".$trx_date."|session_id=".$session_id."|date=".date("YmdHis")."\r\n";

fwrite($m_FileLOG, $file);
fclose($m_FileLOG);

header("Content-type: text/xml");

echo "<?xml version=\"1.0\"?>";
echo '<MO>';
echo '<STATUS>0</STATUS>';
echo '<TRANSID>'.$trx_id.'</TRANSID>';
echo '<MSG>OK</MSG>';
echo '</MO>';

function ConvertToFullMsisdn($msisdn) {
	$msisdn = trim($msisdn);
	if ($msisdn{0} == "0") {
		$msisdn = "62". substr($msisdn, 1);
	}
	$msisdn = str_replace("+","",$msisdn);
	return $msisdn;
}

?>
