<?php

$telco = $_SERVER['argv'][1];

$path = "/var/www/sms/engine/logs/mt/".$telco."/";

$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
$yesterday	= date("Ymd",$date);

if(!is_dir( $path."archive/".$yesterday )) {
	if(!mkdir( $path."archive/".$yesterday )) {
		die("Create direktori gagal");
	}
}

$cekFile = opendir( $path );
while (false !== ($mFilename = readdir($cekFile))) {
	if ($mFilename != "." && $mFilename != ".."  && $mFilename != "archive" && $mFilename != "retry" ) {
	
		if( substr($mFilename,-12) <> date("Ymd").".txt" ) {
			print( $mFilename ."\n");
			if(copy($path.$mFilename, $path."archive/".$yesterday."/".$mFilename)) {
				$mHapus = Unlink($path.$mFilename);
			}
		}
		
	}
}

closedir($cekFile);
	
?>