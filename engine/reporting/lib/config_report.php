<?php

$config['menu'] = array(
					'Application' => "application.php" ,
					'Keyword' => "keyword.php" ,
					'Reg Keyword' => "reg_keyword.php" ,
					'Partner Name' => "partner.php" ,
					'Media Name' => "media.php" ,
					'List Charging' => "list_charging.php" ,
					'MO LOG' => "mo_log.php" ,
					'MT LOG' => "mt_log.php" ,
					'DN LOG' => "dn_log.php" ,
					'MO LOG PARTNER' => "mo_log_partner.php" ,
					'MT LOG PARTNER' => "mt_log_partner.php" ,
					'Daily MT SMS' => "mt_sms.php" ,
					'MO REG' => "mo_reg.php" ,
					'SMS Subscriber' => "subscriber.php" ,
					'Cost per MSISDN' => "cost_per_msisdn.php" ,
					'Daily Traffic' => "daily_traffic.php" ,
					'Monthly Traffic' => "monthly_traffic.php" ,
					'Traffic PER SID' => "traffic_per_sid.php" ,
					'Top Service' => "top_service.php" ,
					'Push SMS' => "push_sms.php" ,
					'Partner Traffic' => "partner_traffic.php" ,
					'Profit Loss' => "profit_loss.php" ,
					'Cost Service' => "cost_service.php" ,
					'Co-Branding Traffic' => "tsel_traffic.php" ,
					'MT Report' => "mt_report.php" ,
					'Daily DN Telco' => "daily_dn_telco.php" ,
					);

$title_header = ":::- Reporting SMS -:::";
$title = "::: SMS REPORTING :::";
$copyright = "copyright &copy; ".date("Y")." - CP. All Right Reserved";

?>
