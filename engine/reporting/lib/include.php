<?php
session_start();

include_once("/var/www/sms/engine/include/config.php");
include_once("/var/www/sms/engine/reporting/lib/config_report.php");
include_once('../include/smarty/Smarty.class.php');
include_once('../include/adodb/adodb.inc.php');
define("TEMPLATES_DIR","/var/www/sms/engine/reporting/templates/");
define("IMG_DIR","images/");
define("CSS_FILE","css/style.css");
define("JAVA_FILE","js/java.js");

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

//Create Template Object
$smarty = new smarty;

$smarty->template_dir = 'templates/';
$smarty->compile_dir = '/var/www/sms/engine/reporting/templates_c/';
$smarty->config_dir = 'configs/';
$smarty->cache_dir = 'cache/';

$smarty->assign("img_dir",IMG_DIR);
$smarty->assign("css_file",CSS_FILE);
$smarty->assign("java_file",JAVA_FILE);

$smarty->assign("title_header",$title_header);
$smarty->assign("title",$title);
$smarty->assign("copyright",$copyright);
if(isset($_SESSION["ses_username"])) {
	$smarty->assign("ses_username",$_SESSION["ses_username"]);
}
if(isset($_SESSION["ses_operator"])) {
	$smarty->assign("ses_operator",$_SESSION["ses_operator"]);
}
if(isset($_SESSION["ses_service"])) {
	$smarty->assign("ses_service",$_SESSION["ses_service"]);
}
if(isset($_SESSION["ses_menu_module"])) {
	$smarty->assign("ses_menu_module",$_SESSION["ses_menu_module"]);
}

$array_menu = explode(",",$_SESSION["ses_menu_module"]);
$menu_html = '';
foreach($array_menu as $menu_order => $menu_name) {
	$menu_html .= '
				  <tr>
					<td height="20"><div align="center"><a href="'.$config['menu'][$menu_name].'" class="link1">:: '.$menu_name.' :: </a></div></td>
				  </tr>'
				  ;
}
$smarty->assign("menu_html",$menu_html);

include_once('functions.php');

$mFunc =& new myFunctions();

?>