<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';
$msg = '';

if(!empty($_GET['filter'])) {
	$filter = " WHERE partner_name like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}
if(!empty($_POST['partner_name'])) {
	$filter = " WHERE partner_name like '%".$_POST['partner_name']."%'";
	$smarty->assign("filter",$_POST['partner_name']);
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['partner_name'])) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				$sql_add = "UPDATE app_sms.partner SET partner_name = '".$_POST['partner_name']."', datetime = NOW() WHERE partner_id= ".$_POST['partner_id'];
				$db_app->Execute($sql_add) or die( $db_app->ErrorMsg() );
				$msg = "Partner berhasil diedit";
			
			}
		}
		else {
			$sqlcek = "SELECT partner_name FROM app_sms.partner WHERE partner_name = '".$_POST['partner_name']."'";
			$rs = $db_app->Execute($sqlcek);
			if ( $rs->RecordCount() > 0 ) {
				$msg = "Partner ".$_POST['partner_name']." sudah ada";
			}
			else {
				
				$sql = "SELECT partner_id FROM app_sms.partner ORDER BY partner_id DESC LIMIT 1";
				$rs = $db_app->Execute($sql);
				if ( $rs->RecordCount() > 0 ) {
					$id = $rs->fields[0];
				}
				
				$sql_add = "INSERT INTO app_sms.partner (partner_id,partner_name,datetime)
								VALUES ($id + 1,'".$_POST['partner_name']."',NOW())";
				$db_app->Execute($sql_add) or die( $db_app->ErrorMsg() );
				$msg = "Partner berhasil ditambah";
			}
		}
		
	}
	else {
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM app_sms.partner WHERE partner_id = ".$_GET['partner_id'];
		$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$partner_id_edit = $rs->fields[1];
				$partner_edit = $rs->fields[2];
				
				$rs->MoveNext();
			}
			
			$smarty->assign("partner_id_edit",$partner_id_edit);
			$smarty->assign("partner_edit",$partner_edit);
			$smarty->assign("action","edit");
		}
	}
	elseif($_GET['action'] == "delete") {
		$sql_add = "DELETE FROM app_sms.partner WHERE partner_id=".$_GET['partner_id'];
		$db_app->Execute($sql_add) or die( $db_app->ErrorMsg() );
		$msg = "Partner berhasil didelete";
	}
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM app_sms.partner $filter ORDER BY partner_id LIMIT $begin, $limit";
$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$partner_id[] = $rs->fields[1];
		$partner_name[] = $rs->fields[2];
		$datetime[] = $rs->fields[3];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("partner_id",$partner_id);
	$smarty->assign("partner_name",$partner_name);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
	$sql_count = "SELECT partner_id FROM app_sms.partner $filter";
	$numresult = $db_app->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'partner.html');

?>