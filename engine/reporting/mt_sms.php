<?php
include_once('lib/include.php');
/*
include ("lib/chart/jpgraph.php");
include ("lib/chart/jpgraph_line.php");
include ("lib/chart/jpgraph_bar.php");
*/
if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( empty($_GET['submit']) ) {
	$h = 0;
	$tgl_now = date("d");
	$_GET['submit'] = '';
}
else {
	if( $_GET['bln'] == date("m") ) {
		$h = 0;
		$tgl_now = date("d");
	}
	else {
		$h = 0;
		$tgl_now = $mFunc->get_jumlah_hari($_GET['bln']);
	}
}

$smarty->assign("submit",$_GET['submit']);

$where = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$where .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$where .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $config['telco'][$_GET['telco']] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("bln",$bulan);
$smarty->assign("thn",$tahun);

$smarty->assign("bln_now",date("m"));
$smarty->assign("thn_now",date("Y"));

if( empty($_GET['submit']) ) {
	$bulan_traffic = date("m");
	$tahun_traffic = date("Y");
}
else {
	$bulan_traffic = $_GET['bln'];
	$tahun_traffic = $_GET['thn'];
}

$smarty->assign("bulan_traffic",$bulan_traffic);
$smarty->assign("tahun_traffic",$tahun_traffic);

$dataJum = array();
$dataTh = array();

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

for($i=$h;$i<=$tgl_now;$i++) {
	
	$hasil = $tgl_now - $i;
	if( $hasil > 0 ) {
		if( strlen($hasil) == 1) $tgl_traffic = "0".$hasil;
		else $tgl_traffic = $hasil;
		
		$tgl_print[] = $hasil;
		$tot_mt = 0;
		
		$sql2 = "SELECT appid, description FROM mp_system.application ORDER BY appid ASC";
		$rs2 = $db->Execute($sql2) or die( $db->ErrorMsg() );
		if ( $rs2->RecordCount() > 0 ) {
			while ( !$rs2->EOF ) {
				$service_code = $rs2->fields[0];
				$service_name = $rs2->fields[1];
				
				//echo $tgl_traffic."<BR>";
				
				$sql = "SELECT appid, datetime, SUM(jumlah) as jml FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic." WHERE appid='".$service_code."' AND datetime ='".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' $where GROUP BY appid, datetime";
				
				//echo $sql."<BR/>";
				$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
				if ( $rs->RecordCount() > 0 ) {
					while ( !$rs->EOF ) {
						$mt[$service_name."_".$service_code][$hasil] = $rs->fields[2];
						$tot_mt += $rs->fields[2];
						$rs->MoveNext();
					}
				}
				else {
					$mt[$service_name."_".$service_code][$hasil] = 0;
				}
				
				//mt sms today
				if( empty($_GET['submit']) ) {
					if( $tgl_traffic == date("d") ) {
						$sqlt = "SELECT appid, sum( jumlah ) 
										FROM mp_system.mt_summary_hourly 
										WHERE 1
										AND appid = '".$service_code."'
										GROUP BY appid";
						
						//echo $sqlt."<BR/>";
						$rst = $db->Execute($sqlt) or die( $db->ErrorMsg() );
						if ( $rst->RecordCount() > 0 ) {
							while ( !$rst->EOF ) {
								$mt[$service_name."_".$service_code][$hasil] = $rst->fields[1];
								$tot_mt += $rst->fields[1];
								$rst->MoveNext();
							}
						}
						else {
							$mt[$service_name."_".$service_code][$hasil] = 0;
						}
					}
				}
				else {
					if( $_GET['bln'] == date("m") && $tgl_traffic == date("d") ) {
						$sqlt = "SELECT appid, sum( jumlah ) 
										FROM mp_system.mt_summary_hourly 
										WHERE 1
										AND appid = '".$service_code."'
										GROUP BY appid";
						
						//echo $sqlt."<BR/>";
						$rst = $db->Execute($sqlt) or die( $db->ErrorMsg() );
						if ( $rst->RecordCount() > 0 ) {
							while ( !$rst->EOF ) {
								$mt[$service_name."_".$service_code][$hasil] = $rst->fields[1];
								$tot_mt += $rst->fields[1];
								$rst->MoveNext();
							}
						}
						else {
							$mt[$service_name."_".$service_code][$hasil] = 0;
						}
					}
				}
				
				//mt sms today
				
				$rs2->MoveNext();
				
			}
		}
		
		//array_unshift($dataJum, $tot_mt);
		//array_unshift($dataTh, $hasil);
		
	}
}

foreach($mt as $service => $key) {
	$num[$service] = 0;
	foreach($key as $tgl => $item) {
		if( $item <> 0) {
			$num[$service] += 1;
		}
	}
}

foreach($mt as $service => $key) {
	if( $num[$service] <> 0) {
		foreach($key as $tgl => $item) {
			$mtfix[$service][$tgl] = $item;
		}
	}
}

$smarty->assign("mtfix",$mtfix);
$smarty->assign("tgl_print",$tgl_print);

//print_r($mt);
$smarty->display(TEMPLATES_DIR.'mt_sms.html');

?>