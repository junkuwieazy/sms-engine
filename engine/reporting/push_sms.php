<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$msg = '';

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $config['telco'][$_GET['telco']] );
	}
	else $smarty->assign("tlc", "All" );
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['msisdns']) && !empty($_POST['content']) ) {
		
		if( $_POST['service'] == "--" || $_POST['shortcode'] == "--" || $_POST['telco'] == "--" ) {
			$msg = "You must choose one of the option";
		}
		else {
			$_POST['msisdns'] = trim($_POST['msisdns']);
			$_POST['msisdns'] = str_replace("\n","",$_POST['msisdns']);
			
			$ar_msisdn = explode(",",$_POST['msisdns']);
			if( count($ar_msisdn) > 1 ) {
				
				for( $ii=0;$ii<count($ar_msisdn);$ii++) {
					$sql_add_type = "INSERT INTO app_sms.push_sms (appid,msisdn,shortcode,telco,sms,entry_date) 
									VALUES ('".$_POST['service']."','".$ar_msisdn[$ii]."','".$_POST['shortcode']."','".$_POST['telco']."','".$_POST['content']."',NOW())";
					$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
					
					include "/var/www/sms/app/include/config_app.php";
					include "/var/www/sms/app/include/functions.php";
					
					$push_price = $_POST['price'];
					$trx_date = date("YmdHis");
					$subject = "PULL;INFO;PUSH_SMS";
					$session_id = date("YmdHis").$ar_msisdn[$ii].rand(10000,99999);
					
					create_mt($mo_origin="",$ar_msisdn[$ii],$prepaid=0,$sms="",$_POST['telco'],$_POST['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,0,0,$_POST['service'],$subject,99,$url="",$mt_type=1,$url_dr="",$shortname="main_95799",$_POST['content']);
					
				}
				
			}
			else {
				$sql_add_type = "INSERT INTO app_sms.push_sms (appid,msisdn,shortcode,telco,sms,entry_date) 
								VALUES ('".$_POST['service']."','".$_POST['msisdns']."','".$_POST['shortcode']."','".$_POST['telco']."','".$_POST['content']."',NOW())";
				$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
				
				include "/var/www/sms/app/include/config_app.php";
				include "/var/www/sms/app/include/functions.php";
				
				$push_price = $_POST['price'];
				$trx_date = date("YmdHis");
				$subject = "PULL;INFO;PUSH_SMS";
				$session_id = date("YmdHis").$_POST['msisdns'].rand(10000,99999);
				
				create_mt($mo_origin="",$_POST['msisdns'],$prepaid=0,$sms="",$_POST['telco'],$_POST['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,0,0,$_POST['service'],$subject,99,$url="",$mt_type=1,$url_dr="",$shortname="main_95799",$_POST['content']);
				//create_mt_debug($mo_origin="",$_POST['msisdns'],$prepaid=0,$sms="",$_POST['telco'],$_POST['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,0,0,$_POST['service'],$subject,99,$url="",$mt_type=1,$url_dr="",$shortname="",$_POST['content']);
				
			}
			
			$msg = "Push SMS successfull";
		}
		
	}
	else {
		$msg = "Column with mark * must be filled";
	}
	
}

$sql = "SELECT * FROM app_sms.push_sms WHERE 1 ORDER BY entry_date DESC LIMIT 10";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$id[] = $rs->fields[0];
		$appid[] = $rs->fields[1];
		$description[] = $mFunc->get_description($db, $rs->fields[1]);
		$msisdn[] = $rs->fields[2];
		$shortcode[] = $rs->fields[3];
		$telco[] = $rs->fields[4];
		$sms[] = $rs->fields[5];
		$datetime[] = $rs->fields[6];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("id",$id);
	$smarty->assign("description",$description);
	$smarty->assign("msisdn",$msisdn);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("telco",$telco);
	$smarty->assign("sms",$sms);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'push_sms.html');

?>