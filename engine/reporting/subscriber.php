<?php

include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$where = '';

if( $_SESSION["ses_operator"] == "flexi" ) {
	$smarty->assign("restrict_telco","flexi");
	$smarty->assign("restrict_telco_name","Flexi");
	$_GET['telco2'] = $_SESSION["ses_operator"];
}
else {
	$smarty->assign("restrict_telco","all");
	$smarty->assign("restrict_telco_name","");
}

if( $_SESSION["ses_service"] == "all" ) {
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where3 = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where3 .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where3);
	$where2 = ")";
	$fix_where = $where1.$where2;
}

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$where .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$where .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $config['telco'][$_GET['telco']] );
	}
	else $smarty->assign("tlc", "All" );
}

$sql = "select appid,status,count(1) as jml from app_sms.msisdn_subscriber WHERE 1 $where $fix_where group by appid,status ORDER BY appid ASC";
//echo $sql;
$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );

$apid = ""; $i = 0; $total = 0; $total_reg_aktif = 0; $total_reg_nonaktif = 0;
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		
		if($apid == $rs->fields[0]) {
			if( $rs->fields[1] == 1 ) {
				$aktif[$i] += $rs->fields[2];
				$total_member[$i] += $rs->fields[2];
				$total_reg_aktif += $rs->fields[2];
				$total += $rs->fields[2];
			}
			else {
				$non_aktif[$i] += $rs->fields[2];
				$total_reg_nonaktif += $rs->fields[2];
				$total_member[$i] += $rs->fields[2];
				$total += $rs->fields[2];
			}
			/*
			switch($rs->fields[1]) {
				case "1" :
					$aktif[$i] = $rs->fields[2];
					$total_member[$i] += $rs->fields[2];
					$total_reg_aktif += $rs->fields[2];
					$total += $rs->fields[2];
					break;
				case "0" :
					$non_aktif[$i] = $rs->fields[2];
					$total_reg_nonaktif += $rs->fields[2];
					$total_member[$i] += $rs->fields[2];
					$total += $rs->fields[2];
					break;
				case "2" :
					$non_aktif[$i] = $rs->fields[2];
					$total_reg_nonaktif += $rs->fields[2];
					$total_member[$i] += $rs->fields[2];
					$total += $rs->fields[2];
					break;
			}
			*/
		}
		else {
			$i++;
			$appid[$i] = $rs->fields[0];
			$desc[$i] = $mFunc->get_description($db, $rs->fields[0]);
			$aktif[$i] = 0; $total_member[$i] = 0; $non_aktif[$i] = 0;
			
			if( $rs->fields[1] == 1 ) {
				$aktif[$i] += $rs->fields[2];
				$total_member[$i] += $rs->fields[2];
				$total_reg_aktif += $rs->fields[2];
				$total += $rs->fields[2];
			}
			else {
				$non_aktif[$i] += $rs->fields[2];
				$total_reg_nonaktif += $rs->fields[2];
				$total_member[$i] += $rs->fields[2];
				$total += $rs->fields[2];
			}
			
			/*
			switch($rs->fields[1]) {
				case "1" :
					$aktif[$i] = $rs->fields[2];
					$total_member[$i] += $rs->fields[2];
					$total_reg_aktif += $rs->fields[2];
					$total += $rs->fields[2];
					break;
				case "0" :
					$non_aktif[$i] = $rs->fields[2];
					$total_reg_nonaktif += $rs->fields[2];
					$total_member[$i] += $rs->fields[2];
					$total += $rs->fields[2];
					break;
				case "2" :
					$non_aktif[$i] = $rs->fields[2];
					$total_reg_nonaktif += $rs->fields[2];
					$total_member[$i] += $rs->fields[2];
					$total += $rs->fields[2];
					break;
			}
			*/
			$apid = $rs->fields[0];
		}
		
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	for($k=1;$k<=$i;$k++) {
		$no[] = $k;
		$appids[] = $appid[$k];
		$descs[] = $desc[$k];
		$aktifs[] = $aktif[$k];
		$non_aktifs[] = $non_aktif[$k];
		$total_members[] = $total_member[$k];
	}
	
	$smarty->assign("no",$no);
	$smarty->assign("appids",$appids);
	$smarty->assign("descs",$descs);
	$smarty->assign("aktifs",$aktifs);
	$smarty->assign("non_aktifs",$non_aktifs);
	$smarty->assign("total_members",$total_members);
	$smarty->assign("total_reg_aktif",$total_reg_aktif);
	$smarty->assign("total_reg_nonaktif",$total_reg_nonaktif);
	$smarty->assign("total",$total);
	$smarty->assign("warna",$warna);
	
}

$smarty->display(TEMPLATES_DIR.'subscriber.html');

?>