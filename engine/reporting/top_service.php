<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$top_service = array();

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}

$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}

$sqlss = "SELECT appid FROM mp_system.application";
$rsss = $db->Execute($sqlss) or die( $db->ErrorMsg() );
if ( $rsss->RecordCount() > 0 ) {
	while ( !$rsss->EOF ) {
		
		$service = $rsss->fields[0];
		
		///
		
		$total_nett = 0;
		
		$sql = "SELECT charge,jumlah,bulan,telco FROM mp_system.nett_monthly_service WHERE 1 AND appid='".$service."' $str_sql";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			
			while ( !$rs->EOF ) {
				
				$ar_price = explode("-",$rs->fields[0]);
				$price = $ar_price[1];
				if( $price == 250 ) $price = 0;
				
				if( $rs->fields[2] == "2011-03" ) $telco_share = 0.5;
				elseif( $rs->fields[2] == "2011-04" ) $telco_share = 0.5;
				elseif( $rs->fields[2] == "2011-05" ) $telco_share = 0.5;
				else $telco_share = $config['share'][$rs->fields[3]];
				
				$total_nett += $rs->fields[1] * ($price * $telco_share );
				
				$rs->MoveNext();
			}
			
		}
		
		$sqlbn = "SELECT telco, charge, SUM( jumlah ) AS jml FROM mp_system.mt_summary_daily_".date("Ym")." WHERE 1 AND appid = '".$service."' AND dn_status = 'delivered' $str_sql GROUP BY telco,charge";
		$rsbn = $db->Execute($sqlbn) or die( $db->ErrorMsg() );
		if ( $rsbn->RecordCount() > 0 ) {
			
			while ( !$rsbn->EOF ) {
				
				$ar_pricebn = explode("-",$rsbn->fields[1]);
				$pricebn = $ar_pricebn[1];
				if( $pricebn == 250 ) $pricebn = 0;
				
				$telco_sharebn = $config['share'][$rsbn->fields[0]];
				
				$total_nett += $rsbn->fields[2] * ($pricebn * $telco_sharebn );
				
				$rsbn->MoveNext();
			}
			
		}
		
		$sqlbnh = "SELECT telco, charge, SUM( jumlah ) AS jml FROM mp_system.mt_summary_hourly WHERE 1 AND appid = '".$service."' AND dn_status = 'delivered' $str_sql GROUP BY telco,charge";
		$rsbnh = $db->Execute($sqlbnh) or die( $db->ErrorMsg() );
		if ( $rsbnh->RecordCount() > 0 ) {
			
			while ( !$rsbnh->EOF ) {
				
				$ar_pricebnh = explode("-",$rsbnh->fields[1]);
				$pricebnh = $ar_pricebnh[1];
				if( $pricebnh == 250 ) $pricebnh = 0;
				
				$telco_sharebnh = $config['share'][$rsbnh->fields[0]];
				
				$total_nett += $rsbnh->fields[2] * ($pricebnh * $telco_sharebnh );
				
				$rsbnh->MoveNext();
			}
			
		}
		
		$top_service[$service] = $total_nett;
		
		///
		
		$rsss->MoveNext();
	}
}

arsort($top_service);

$t = 1;
foreach ($top_service as $key => $val) {
		if( $t == 11 ) break;
    $appid[] = $key;
    $desc[] = $mFunc->get_description($db, $key);
    $nett[] = $val;
    $no[] = $t;
    $t++;
}

$smarty->assign("no", $no );
$smarty->assign("appid", $appid );
$smarty->assign("desc", $desc );
$smarty->assign("nett", $nett );

$smarty->display(TEMPLATES_DIR.'top_service.html');

?>