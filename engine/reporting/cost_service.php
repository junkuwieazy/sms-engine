<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';
$msg = '';

$str_sql = "";

if( isset($_POST['service']) ) $_GET['service'] = $_POST['service'];
if( isset($_POST['telco']) ) $_GET['telco'] = $_POST['telco'];
if( isset($_POST['shortcode']) ) $_GET['shortcode'] = $_POST['shortcode'];

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service);

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['cost_name']) && !empty($_POST['cost_amount']) ) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				$sql_add = "UPDATE mp_system.application SET description = '".$_POST['desc']."',partner = '".$_POST['partner']."',contact = '".$_POST['contact']."',marketing = '".$_POST['marketing']."',pic = '".$_POST['pic']."',datetime = NOW() WHERE id= ".$_POST['id'];
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				
				$sql = "SELECT appid FROM mp_system.application WHERE id= ".$_POST['id'];
				$rs = $db->Execute($sql);
				if ( $rs->RecordCount() > 0 ) {
					if ( !$rs->EOF ) {
						$app = $rs->fields[0];
					}
					
					$sql_add_type = "UPDATE app_sms.appid_sms set description='".$_POST['desc']."', datetime=NOW() WHERE appid = '".$app."'";
					$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
					
				}
				
				$msg = "Application berhasil diedit";
			
			}
		}
		else {
			
			if( $_POST['telco'] == "--" ) $_POST['telco'] = "All";
			if( $_POST['shortcode'] == "--" ) $_POST['shortcode'] = "All";
			
			$sql_add_type = "INSERT INTO mp_system.cost_service (appid,telco,shortcode,cost_name,cost_amount,entry_date) 
										VALUES ('".$_POST['service']."','".$_POST['telco']."','".$_POST['shortcode']."','".$_POST['cost_name']."','".$_POST['cost_amount']."',NOW())";
			$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
			
			$msg = "Cost Service berhasil ditambah";
		}
		
	}
	else {
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM mp_system.application WHERE id = ".$_GET['id']." ORDER BY appid";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$id_edit = $rs->fields[0];
				$appid_edit = $rs->fields[1];
				$description_edit = $rs->fields[2];
				$partner_edit = $rs->fields[3];
				$contact_edit = $rs->fields[4];
				$marketing_edit = $rs->fields[5];
				$pic_edit = $rs->fields[6];
				$datetime_edit = $rs->fields[7];
				
				$rs->MoveNext();
			}
			
			$smarty->assign("appid_edit",$appid_edit);
			$smarty->assign("id_edit",$id_edit);
			$smarty->assign("description_edit",$description_edit);
			$smarty->assign("partner_edit",$partner_edit);
			$smarty->assign("contact_edit",$contact_edit);
			$smarty->assign("marketing_edit",$marketing_edit);
			$smarty->assign("pic_edit",$pic_edit);
			$smarty->assign("datetime_edit",$datetime_edit);
			$smarty->assign("action","edit");
		}
	}
	elseif($_GET['action'] == "delete") {
		
		$sql_add = "DELETE FROM mp_system.cost_service WHERE id=".$_GET['id'];
		$db->Execute($sql_add) or die( $db->ErrorMsg() );
		$msg = "Cost Service berhasil didelete";
	}
}

if(isset($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
	
	if(!empty($_GET['service'])) {
		if($_GET['service'] <> "--") {
			$str_sql .= " AND appid = '".$_GET['service']."'";
		}
		$s_name = $mFunc->get_description($db, $_GET['service']);
		$smarty->assign("service",$_GET['service']);
		$smarty->assign("s_name",$s_name);
	}
}
else {
	$smarty->assign("s_name","All");
	$smarty->assign("submit","");
}
//echo $str_sql;
$cost_tot = 0;
$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM mp_system.cost_service WHERE 1 $str_sql ORDER BY appid LIMIT $begin, $limit";
//echo $sql;
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$id[] = $rs->fields[0];
		$appid[] = $rs->fields[1];
		$description[] = $mFunc->get_description($db, $rs->fields[1]);
		$telco[] = $rs->fields[2];
		$shortcode[] = $rs->fields[3];
		$cost_name[] = $rs->fields[4];
		$cost_amount[] = $rs->fields[5];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$cost_tot += $rs->fields[5];
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("id",$id);
	$smarty->assign("description",$description);
	$smarty->assign("telco",$telco);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("cost_name",$cost_name);
	$smarty->assign("cost_amount",$cost_amount);
	$smarty->assign("cost_tot",$cost_tot);
	$smarty->assign("warna",$warna);
	
	$sql_count = "SELECT id FROM mp_system.cost_service WHERE 1 $str_sql";
	$numresult = $db->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'cost_service.html');

?>