<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( empty($_GET['submit']) ) {
	$_GET['submit'] = '';
}


$smarty->assign("submit",$_GET['submit']);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}

$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("bln",$bulan);
$smarty->assign("thn",$tahun);

$smarty->assign("bln_now",date("m"));
$smarty->assign("thn_now",date("Y"));

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$bulan_traffic = date("m");
	$tahun_traffic = date("Y");
	$smarty->assign("s_name","All");
	$str_service = "";
}
else {
	$bulan_traffic = $_GET['bln'];
	$tahun_traffic = $_GET['thn'];
	$fix_where = '';
	
}

$smarty->assign("bulan_traffic",$bulan_traffic);
$smarty->assign("tahun_traffic",$tahun_traffic);

$tot_gross = 0;
$tot_nett = 0;

$sql = "SELECT charge, sid, count( 1 ) AS jml FROM mp_system.mt_outgoing_log".$tahun.$bulan." WHERE 1 $str_sql AND dn_status = 'delivered' AND telco='isat' AND sid not in('95799331001004') GROUP BY charge, sid";
$rs = $dblocal->Execute($sql) or die( $dblocal->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$charge[] = $rs->fields[0];
		$ar_price = explode("-",$rs->fields[0]);
		
		if( $ar_price[1] == "1650" ) {
			$bearer1 = 0;
			$bearer[] = 0;
		}
		else {
			$bearer1 = 350;
			$bearer[] = 350;
		}
		
		if( strtoupper($ar_price[1]) == "REG" ) {
			$price[] = 0;
			$price1 = 0;
			$bearer[] = 0;
			$bearer1 = 0;
		}
		elseif( strtoupper($ar_price[1]) == "UNREG" ) {
			$price[] = 0;
			$price1 = 0;
			$bearer[] = 0;
			$bearer1 = 0;
		}
		else {
			$price[] = $ar_price[1];
			$price1 = $ar_price[1];
		}
		
		$content_value[] = $price1 - $bearer1;
		$content_value1 = $price1 - $bearer1;
		
		$sid[] += $rs->fields[1];
		
		if( in_array( $bulan_traffic.$tahun_traffic , array("092015", "102015", "112015") ) ) {
			$jumlah[] = round($rs->fields[2]);
			$jumlah_ = round($rs->fields[2]);
			$gross[] = $content_value1 * $jumlah_;
			$gross_ = $content_value1 * $jumlah_;
			
			$nett[] = round($gross_ * 0.7);
			$nett_ = round($gross_ * 0.7);
		}
		else {
			$jumlah[] = round($rs->fields[2] + ($rs->fields[2] * 0.17));
			$jumlah_ = round($rs->fields[2] + ($rs->fields[2] * 0.17));
			$gross[] = $content_value1 * $jumlah_;
			$gross_ = $content_value1 * $jumlah_;
			
			$nett[] = round($gross_ * 0.7);
			$nett_ = round($gross_ * 0.7);
		}
		
		
		/*
		if( $ar_price[1] == "1650" ) {
			$jumlah[] += round($rs->fields[2] + ($rs->fields[2] * 0.17));
			$jumlah_ += round($rs->fields[2] + ($rs->fields[2] * 0.17));
			$gross[] = $content_value1 * $jumlah_;
			$gross_ = $content_value1 * $jumlah_;
			
			$nett[] = round($gross_ * 0.7);
			$nett_ = round($gross_ * 0.7);
			
			//$nett[] = (($content_value1 * $rs->fields[2]) * 0.7) + (($content_value1 * $rs->fields[2] * 0.7) *  0.17);
			//$nett_ = (($content_value1 * $rs->fields[2]) * 0.7) + (($content_value1 * $rs->fields[2] * 0.7) *  0.17);
		}
		else {
			$jumlah[] += $rs->fields[2];
			$gross[] = $content_value1 * $rs->fields[2];
			$gross_ = $content_value1 * $rs->fields[2];
			$nett[] = ($content_value1 * $rs->fields[2]) * 0.7;
			$nett_ = ($content_value1 * $rs->fields[2]) * 0.7;
		}
		*/
		//$tot_gross += $content_value1 * $rs->fields[2];
		$tot_gross += $gross_;
		//$tot_nett += ($content_value1 * $rs->fields[2]) * 0.7;
		$tot_nett += $nett_;
		
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		
		$rs->MoveNext();
	}
}

$smarty->assign("warna",$warna);
$smarty->assign("tarif",$price);
$smarty->assign("bearer",$bearer);
$smarty->assign("content_value",$content_value);
$smarty->assign("sid",$sid);
$smarty->assign("jumlah",$jumlah);
$smarty->assign("gross",$gross);
$smarty->assign("nett",$nett);
$smarty->assign("tot_gross",$tot_gross);
$smarty->assign("tot_nett",$tot_nett);

$smarty->display(TEMPLATES_DIR.'traffic_per_sid.html');

?>