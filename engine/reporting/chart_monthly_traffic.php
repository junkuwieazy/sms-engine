<?php

include_once('lib/include.php');

include ("lib/chart/jpgraph.php");
include ("lib/chart/jpgraph_line.php");
include ("lib/chart/jpgraph_bar.php");

$dataJum = array();
$dataTh = array();

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( empty($_GET['submit']) ) {
	$_GET['submit'] = '';
}

$smarty->assign("submit",$_GET['submit']);
$fix_where = '';
if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
$jc = 1; $jj = 1;
$checkbox = "<table width='100%'  border='1' cellpadding='0' cellspacing='0'><tr>";
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		if( empty($_GET['submit']) ) $checked = "checked";
		else {
			if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
			else $checked = "";
		}
		
		$checkbox .= "<td><input type='checkbox' name='check_list".$jj."' id='check_list".$jj."' value='".$rs->fields[0]."' $checked>".$rs->fields[1]."</td>";
		if( $jc == 5 ) {
			$checkbox .= '</tr>'; 
			$jc = 0;
		}
		$jc++;
		$jj++;
		$rs->MoveNext();
	}
}
$checkbox .= "<tr>
				<input type='button' name='Check_All' value='Check All' onClick='CheckAll()'>
				<input type='button' name='Un_CheckAll' value='Uncheck All' onClick='UnCheckAll()'>
			</tr></table>";
$smarty->assign("checkbox",$checkbox);


/* service get using drop box 

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service); */

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("thn",$tahun);


$smarty->assign("thn_now",date("Y"));

$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$tahun_traffic = date("Y");
	$smarty->assign("s_name","All");
	$str_service = ""; //addition checkbox
}
else {
	$tahun_traffic = $_GET['thn'];
	$fix_where = '';
/* 	if($_GET['service'] <> "--") {
		$str_sql .= " AND service='".$_GET['service']."'";
		$s_name = $mFunc->get_description($db, $_GET['service']);
		$smarty->assign("s_name",$s_name);
	}
	else $smarty->assign("s_name","All"); 
	
	// addition checkbox, */
	
	$service_in = ""; $is_check = false; $s_name = "";
	for($r=1;$r<=$jj;$r++) {
		if( isset($_GET['check_list'.$r]) ) {
			$service_in .= "'".$_GET['check_list'.$r]."',";
			$is_check = true;
			$s_name .= $mFunc->get_description($db, $_GET['check_list'.$r]).", ";
		}
	}
	$service_in = ereg_replace(',$','',$service_in);
	
	if( $is_check ) $smarty->assign("s_name",$s_name);
	else $smarty->assign("s_name","All");
	
	if( $service_in <> "" ) $str_service = "AND appid in ( ".$service_in." )";
	else $str_service = "AND appid = '0000'";
	
}
$smarty->assign("service",$_GET['service']);

$smarty->assign("tahun_traffic",$tahun_traffic);

$mo_tot = 0; $mt_ok_tot = 0; $mt_nok_tot = 0; $total_mt_tot = 0; $gross_tot = 0; $net_tot = 0;
$hh = 0;

if( $tahun_traffic == date("Y") ) $j_bln = date("m");
else $j_bln = 12;

for($t=$j_bln;$t>=1;$t--) {
	
	if( strlen($t) == 1) $blnn = "0".$t;
	else $blnn = $t;
	
	$mo[$hh] = 0;
	$mt_ok[$hh] = 0;
	$mt_nok[$hh] = 0;
	$total_mt[$hh] = 0;
	$gross[$hh] = 0;
	$net[$hh] = 0;
	
	for($d=1;$d<=31;$d++) {
		if( strlen($d) == 1) $dayy = "0".$d;
		else $dayy = $d;
		
		//
		$tgl_print[$hh] = $mFunc->monthName($blnn);
		
		$sql_mo = "SELECT sum( jumlah )
								FROM mp_system.mt_summary_daily_".$tahun_traffic.$blnn." 
								WHERE datetime = '".$tahun_traffic."-".$blnn."-".$dayy."' $str_sql $str_service $fix_where 
								AND subject NOT LIKE 'push%'";
		//echo $sql_mo."<BR>";
		$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
		if ( $rs_mo->RecordCount() > 0 ) {
			while ( !$rs_mo->EOF ) {
				$mo[$hh] += $rs_mo->fields[0];
				$rs_mo->MoveNext();
			}
		}
		
		$sql = "SELECT telco, charge, dn_status, sum( jumlah )
							FROM mp_system.mt_summary_daily_".$tahun_traffic.$blnn." 
							WHERE datetime = '".$tahun_traffic."-".$blnn."-".$dayy."' $str_sql $str_service $fix_where 
							GROUP BY telco,charge,dn_status";
		//echo $sql."<br>";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if ( $rs->RecordCount() > 0 ) {
			
			while ( !$rs->EOF ) {
				
				if( $rs->fields[2] == "DELIVERED" ) {
					
					if( $blnn.$tahun_traffic == "032011" ) $telco_share = 0.5;
					elseif( $blnn.$tahun_traffic == "042011" ) $telco_share = 0.5;
					elseif( $blnn.$tahun_traffic == "052011" ) $telco_share = 0.5;
					else $telco_share = $config['share'][$rs->fields[0]];
					
					$ar_price = explode("-",$rs->fields[1]);
					$price = $ar_price[1];
					
					$mt_ok[$hh] += $rs->fields[3];
					if( $price == 250 ) $price = 0;
					
					$gross[$hh] += $rs->fields[3] * $price;
					$net[$hh] += $rs->fields[3] * ($price * $telco_share );
					
				}
				else {
					$mt_nok[$hh] += $rs->fields[3];
				}
				
				$total_mt[$hh] += $rs->fields[3];
				
				$rs->MoveNext();
			}
		}
		
		//Today Traffic
		if( $tahun_traffic == date("Y") && $blnn == date("m") && $dayy == date("d") ) {
			////
			$sql_mo = "SELECT sum( jumlah )
									FROM mp_system.mt_summary_hourly 
									WHERE 1 $str_sql $str_service $fix_where 
									AND subject NOT LIKE 'push%'";
			//echo $sql_mo."<BR>";
			$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
			if ( $rs_mo->RecordCount() > 0 ) {
				while ( !$rs_mo->EOF ) {
					$mo[$hh] += $rs_mo->fields[0];
					$rs_mo->MoveNext();
				}
			}
			
			$sql = "SELECT telco, charge, dn_status, sum( jumlah )
								FROM mp_system.mt_summary_hourly 
								WHERE 1 $str_sql $str_service $fix_where 
								GROUP BY telco,charge,dn_status";
			//echo $sql."<br>";
			$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
			
			if ( $rs->RecordCount() > 0 ) {
				
				while ( !$rs->EOF ) {
					
					if( $rs->fields[2] == "DELIVERED" ) {
						
						if( $blnn.$tahun_traffic == "032011" ) $telco_share = 0.5;
						elseif( $blnn.$tahun_traffic == "042011" ) $telco_share = 0.5;
						elseif( $blnn.$tahun_traffic == "052011" ) $telco_share = 0.5;
						else $telco_share = $config['share'][$rs->fields[0]];
						
						$ar_price = explode("-",$rs->fields[1]);
						$price = $ar_price[1];
						
						$mt_ok[$hh] += $rs->fields[3];
						if( $price == 250 ) $price = 0;
						
						$gross[$hh] += $rs->fields[3] * $price;
						$net[$hh] += $rs->fields[3] * ($price * $telco_share );
						
					}
					else {
						$mt_nok[$hh] += $rs->fields[3];
					}
					
					$total_mt[$hh] += $rs->fields[3];
					
					$rs->MoveNext();
				}
			}
			////
		}
		//Today Traffic
		
	}
	
	array_unshift($dataJum, $net[$hh]);
	array_unshift($dataTh, $blnn);
	
	$hh++;
}

if( sizeof($dataJum) > 0 ) {
	// chart
	$graph = new Graph(1100,300,"auto");    
	$graph->SetScale("textlin");

	// menampilkan plot batang dari data jumlah penduduk
	$bplot = new BarPlot($dataJum);
	$graph->Add($bplot);

	// menampilkan plot garis dari data jumlah penduduk
	$lineplot=new LinePlot($dataJum);
	$graph->Add($lineplot);

	$graph->img->SetMargin(70,20,20,40);
	$graph->title->Set("Grafik Monthly Traffic Net Revenue");
	$graph->xaxis->title->Set("Bulan");
	//$graph->yaxis->title->Set("Jumlah");
	$graph->xaxis->SetTickLabels($dataTh);

	$graph->title->SetFont(FF_FONT1,FS_BOLD);

	$lineplot->SetColor("blue");
	$bplot->SetFillColor("red");

	//$graph->SetShadow();
	$graph->Stroke();
	//$smarty->assign("chart",$graph->Stroke());
	// chart
}

?>