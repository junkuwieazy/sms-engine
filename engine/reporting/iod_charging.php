<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';
$msg = '';

if(!empty($_GET['filter'])) {
	$filter = " WHERE description like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}
if(!empty($_POST['desc'])) {
	$filter = " WHERE description like '%".$_POST['desc']."%'";
	$smarty->assign("filter",$_POST['desc']);
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['desc']) && !empty($_POST['marketing']) && !empty($_POST['pic'])) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				
				if( $_POST['is_push'] == 1 ) $p_time_e = $_POST['push_time'];
				else $p_time_e = "";
				
				$sql_add = "UPDATE mp_system.application SET description = '".$_POST['desc']."',partner = '".$_POST['partner']."',contact = '".$_POST['contact']."',marketing = '".$_POST['marketing']."',pic = '".$_POST['pic']."',datetime = NOW(),is_push = ".$_POST['is_push'].",push_time = '".$p_time_e."',status = ".$_POST['status'].",push_day = '".$_POST['push_day']."' WHERE id= ".$_POST['id'];
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				
				$sql = "SELECT appid FROM mp_system.application WHERE id= ".$_POST['id'];
				$rs = $db->Execute($sql);
				if ( $rs->RecordCount() > 0 ) {
					if ( !$rs->EOF ) {
						$app = $rs->fields[0];
					}
					
					$sql_add_type = "UPDATE app_sms.appid_sms set description='".$_POST['desc']."', datetime=NOW() WHERE appid = '".$app."'";
					$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
					
				}
				
				$msg = "Application berhasil diedit";
			
			}
		}
		else {
			$sqlcek = "SELECT appid FROM mp_system.application WHERE appid <> '99999' ORDER BY appid DESC LIMIT 1";
			$rs = $db->Execute($sqlcek);
			if ( $rs->RecordCount() > 0 ) {
				if ( !$rs->EOF ) {
					$app = $rs->fields[0] + 1;
				}
			}
			else {
				$app = "10010";
			}
			
			if( $_POST['is_push'] == 0 ) $p_time = "";
			else $p_time = $_POST['push_time'];
			
			$sql_add = "INSERT INTO mp_system.application (appid,description,partner,contact,marketing,pic,datetime,is_push,push_time,status,push_day)
							VALUES ('$app','".$_POST['desc']."','".$_POST['partner']."','".$_POST['contact']."','".$_POST['marketing']."','".$_POST['pic']."',NOW(),".$_POST['is_push'].",'".$p_time."',".$_POST['status'].",'".$_POST['push_day']."')";
			$db->Execute($sql_add) or die( $db->ErrorMsg() );
			
			$sql_add_type = "INSERT INTO app_sms.appid_sms (appid,description,datetime) 
										VALUES ('$app','".$_POST['desc']."',NOW())";
			$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
			
			$msg = "Application berhasil ditambah";
		}
		
	}
	else {
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM mp_system.application WHERE id = ".$_GET['id']." ORDER BY appid";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$id_edit = $rs->fields[0];
				$appid_edit = $rs->fields[1];
				$description_edit = $rs->fields[2];
				$partner_edit = $rs->fields[3];
				$contact_edit = $rs->fields[4];
				$marketing_edit = $rs->fields[5];
				$pic_edit = $rs->fields[6];
				$datetime_edit = $rs->fields[7];
				$is_push_edit = $rs->fields[8];
				$push_time_edit = $rs->fields[9];
				$status_edit = $rs->fields[10];
				$push_day_edit = $rs->fields[11];
				
				$rs->MoveNext();
			}
			
			$smarty->assign("appid_edit",$appid_edit);
			$smarty->assign("id_edit",$id_edit);
			$smarty->assign("description_edit",$description_edit);
			$smarty->assign("partner_edit",$partner_edit);
			$smarty->assign("contact_edit",$contact_edit);
			$smarty->assign("marketing_edit",$marketing_edit);
			$smarty->assign("pic_edit",$pic_edit);
			$smarty->assign("datetime_edit",$datetime_edit);
			$smarty->assign("is_push_edit",$is_push_edit);
			$smarty->assign("push_time_edit",$push_time_edit);
			$smarty->assign("status_edit",$status_edit);
			$smarty->assign("push_day_edit",$push_day_edit);
			$smarty->assign("action","edit");
			
			$_GET['is_push'] = $is_push_edit;
			$_GET['push_time'] = $push_time_edit;
			$_GET['status'] = $status_edit;
			$_GET['push_day'] = $push_day_edit;
			
		}
	}
	elseif($_GET['action'] == "delete") {
		
		$sql = "SELECT appid FROM mp_system.application WHERE id= ".$_GET['id'];
		$rs = $db->Execute($sql);
		if ( $rs->RecordCount() > 0 ) {
			if ( !$rs->EOF ) {
				$app = $rs->fields[0];
			}
			
			$sql_add_type = "DELETE FROM app_sms.appid_sms WHERE appid = '$app'";
			$db_app->Execute($sql_add_type) or die( $db_app->ErrorMsg() );
			
		}
		
		$sql_add = "DELETE FROM mp_system.application WHERE id=".$_GET['id'];
		$db->Execute($sql_add) or die( $db->ErrorMsg() );
		$msg = "Application berhasil didelete";
	}
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM mp_system.application $filter ORDER BY appid LIMIT $begin, $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$id[] = $rs->fields[0];
		$appid[] = $rs->fields[1];
		$description[] = $rs->fields[2];
		$partner[] = $rs->fields[3];
		$contact[] = $rs->fields[4];
		$marketing[] = $rs->fields[5];
		$pic[] = $rs->fields[6];
		$datetime[] = $rs->fields[7];
		if( $rs->fields[8] == 1 ) $is_push[] = "Yes";
		else $is_push[] = "No";
		if( $rs->fields[10] == 1 ) $status[] = "Aktif";
		else $status[] = "Non Aktif";
		$push_time[] = $rs->fields[9];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$push_day[] = $rs->fields[11];
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("id",$id);
	$smarty->assign("description",$description);
	$smarty->assign("partner",$partner);
	$smarty->assign("contact",$contact);
	$smarty->assign("marketing",$marketing);
	$smarty->assign("pic",$pic);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("is_push",$is_push);
	$smarty->assign("push_time",$push_time);
	$smarty->assign("status",$status);
	$smarty->assign("warna",$warna);
	$smarty->assign("push_day",$push_day);
	
	$sql_count = "SELECT appid FROM mp_system.application $filter ORDER BY appid";
	$numresult = $db->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
}

$sql = "SELECT partner_id,partner_name FROM app_sms.partner";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_partner[$rs->fields[1]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_partner = $mFunc->crtDropDown($arr_partner,$arr_partner, 'partner', $_POST['partner'], '', 'class="text"');
$smarty->assign("dd_partner",$dd_partner);

$dd_is_push = $mFunc->crtDropDownMonth($config['is_push'],$config['is_push'], 'is_push', $_GET['is_push'], '', 'class="text"');
$smarty->assign("dd_is_push",$dd_is_push);

$dd_push_time = $mFunc->crtDropDownMonth($config['push_time'],$config['push_time'], 'push_time', $_GET['push_time'], '', 'class="text"');
$smarty->assign("dd_push_time",$dd_push_time);

$dd_status = $mFunc->crtDropDownMonth($config['status'],$config['status'], 'status', $_GET['status'], '', 'class="text"');
$smarty->assign("dd_status",$dd_status);

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'application.html');

?>