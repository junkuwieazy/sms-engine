<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$bln_now = date("m");
$smarty->assign("bln_now",$bln_now);

$thn_now = date("Y");
$smarty->assign("thn_now",$thn_now);

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], '', 'class="text"');
$smarty->assign("dd_service",$dd_service);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if(isset($_GET['submit'])) {
	
	$smarty->assign("bln",$_GET['bln']);
	$smarty->assign("thn",$_GET['thn']);
	$smarty->assign("service",$_GET['service']);
	
	if($_GET['service'] <> "--") {
		
		$mt_ok_total = 0;
		$mt_nok_total = 0;
		$mt_tot_total = 0;
		$gross_total = 0;
		$nett_total = 0;
		
		for($d=0;$d<=30;$d++) {
			$dd = $d + 1;
			if( strlen($dd) == 1 ) $day = "0".$dd;
			else $day = $dd;
			
			$tgl[$d] = $day."-".$_GET['bln']."-".$_GET['thn'];
			$mt_ok[$d] = 0;
			$mt_nok[$d] = 0;
			$gross[$d] = 0;
			$nett[$d] = 0;
			$mt_tot[$d] = 0;
			
			//Depend on partner
			$str_sql_p = "";
			if( $_SESSION["ses_partner_id"] <> 0 && in_array( $_GET['service'] , array("10017", "10029", "10030", "10031", "10032") ) ) {
				$str_sql_p .= " AND subject like 'PULL;IOD;DL;".$_SESSION["ses_partner_id"]."-%'";
			}
			//Depend on partner
			
			if( ($_SESSION["ses_username"] == "indosis" || $_SESSION["ses_username"] == "olvie") && $_GET['service'] == "10031" ) {
				$str_sql_app = " AND appid in ('10017','10031')";
			}
			elseif( $_SESSION["ses_username"] == "suara" && $_GET['service'] == "10032" ) {
				$str_sql_app = " AND appid in ('10032') AND subject IN ('PULL;IOD;DL;3-20001',  'PULL;IOD;DL;3-20011',  'PULL;IOD;DL;3-20021',  'PULL;IOD;DL;3-20031')";
			}
			elseif( $_SESSION["ses_username"] == "partnernoor" ) {
				if( ($_GET['thn'].$_GET['bln'].$day) > "20120605" ) {
					$str_sql_app = " AND appid = '".$_GET['service']."' AND telco='apa'";
				}
				else $str_sql_app = " AND appid = '".$_GET['service']."'";
			}
			else {
				$str_sql_app = " AND appid = '".$_GET['service']."'";
			}
			
			$sql = "SELECT telco, charge, dn_status, sum( jumlah )
							FROM mp_system.mt_summary_daily_".$_GET['thn'].$_GET['bln']."
							WHERE 1 $str_sql_app 
							AND datetime = '".$_GET['thn']."-".$_GET['bln']."-".$day."' $str_sql $str_sql_p
							GROUP BY telco,charge,dn_status";
			//echo $sql."<BR>";
			$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
			if ( $rs->RecordCount() > 0 ) {
				
				while ( !$rs->EOF ) {
					
					if( $rs->fields[2] == "DELIVERED" ) {
						
						if( $_GET['bln'].$_GET['thn'] == "032011" ) $telco_share = 0.5;
						elseif( $_GET['bln'].$_GET['thn'] == "042011" ) $telco_share = 0.5;
						elseif( $_GET['bln'].$_GET['thn'] == "052011" ) $telco_share = 0.5;
						else $telco_share = $config['share'][$rs->fields[0]];
						
						$ar_price = explode("-",$rs->fields[1]);
						$price = $ar_price[1];
						if( $price == 250 ) $price = 0;
						
						$mt_ok[$d] += $rs->fields[3];
						$gross[$d] += $rs->fields[3] * $price;
						$nett[$d] += $rs->fields[3] * ($price * $telco_share );
						
						$mt_ok_total += $rs->fields[3];
			
						$gross_total += $rs->fields[3] * $price;
						$nett_total += $rs->fields[3] * ($price * $telco_share );
						
					}
					else {
						$mt_nok[$d] += $rs->fields[3];
						$mt_nok_total += $rs->fields[3];
					}
					
					$mt_tot[$d] += $rs->fields[3];
					$mt_tot_total += $rs->fields[3];
					
					$rs->MoveNext();
				}
			}
			else {
				$mt_tot[$d] = 0;
			}
			
		}
		
		$smarty->assign("tgl",$tgl);
		$smarty->assign("mt_ok",$mt_ok);
		$smarty->assign("mt_nok",$mt_nok);
		$smarty->assign("mt_tot",$mt_tot);
		$smarty->assign("gross",$gross);
		$smarty->assign("nett",$nett);
		
		$smarty->assign("mt_ok_total",$mt_ok_total);
		$smarty->assign("mt_nok_total",$mt_nok_total);
		$smarty->assign("mt_tot_total",$mt_tot_total);
		$smarty->assign("gross_total",$gross_total);
		$smarty->assign("nett_total",$nett_total);
		
		if(!empty($_GET['service'])) {
			if($_GET['service'] <> "--") {
				$str_sql .= " AND appid = '".$_GET['service']."'";
			}
			$s_name = $mFunc->get_description($db, $_GET['service']);
			$smarty->assign("service",$_GET['service']);
			$smarty->assign("s_name",$s_name);
		}
		
	}
	
}

if(!empty($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
}

$smarty->display(TEMPLATES_DIR.'partner_traffic.html');

?>
