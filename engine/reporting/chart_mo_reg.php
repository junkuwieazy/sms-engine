<?php
include_once('lib/include.php');

include ("lib/chart/jpgraph.php");
include ("lib/chart/jpgraph_line.php");
include ("lib/chart/jpgraph_bar.php");

$datareg = array();
$dataunreg = array();
$datajam = array();

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $config['telco'][$_GET['telco']] );
	}
	else $smarty->assign("tlc", "All" );
}

if(isset($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
	if(!empty($_GET['tgl2'])) {
		$smarty->assign("tgl2", $_GET['tgl2'] );
		$tgl = $_GET['tgl2'];
	}
	else {
		$smarty->assign("tgl2", date("Y-m-d") );
		$tgl = date("Y-m-d");
	}
	
	if(!empty($_GET['service'])) {
		if($_GET['service'] <> "--") {
			$str_sql .= " AND appid = '".$_GET['service']."'";
		}
		$s_name = $mFunc->get_description($db, $_GET['service']);
		$smarty->assign("service",$_GET['service']);
		$smarty->assign("s_name",$s_name);
	}
}
else {
	$smarty->assign("tgl2", date("Y-m-d") );
	$tgl = date("Y-m-d");
	$smarty->assign("s_name","All");
	$smarty->assign("submit","");
}

// REG
$sql_reg = "SELECT date_format( date_reg, '%H' ) AS jam, count( 1 ) AS jml
						FROM app_sms.msisdn_subscriber
						WHERE 1 $str_sql AND date_format( date_reg, '%Y-%m-%d' ) = '".$tgl."' 
						GROUP BY jam";
//echo $sql_reg."<br>";
$rs_reg = $db_app->Execute($sql_reg) or die( $db_app->ErrorMsg() );
if ( $rs_reg->RecordCount() > 0 ) {
	while ( !$rs_reg->EOF ) {
		$reg[$rs_reg->fields[0]] = $rs_reg->fields[1];
		$rs_reg->MoveNext();
	}
}

// UNREG
$sql_unreg = "SELECT date_format( date_unreg, '%H' ) AS jam, count( 1 ) AS jml
						FROM app_sms.msisdn_subscriber
						WHERE 1 $str_sql AND date_format( date_unreg, '%Y-%m-%d' ) = '".$tgl."' 
						GROUP BY jam";
//echo $sql_reg."<br>";
$rs_unreg = $db_app->Execute($sql_unreg) or die( $db_app->ErrorMsg() );
if ( $rs_unreg->RecordCount() > 0 ) {
	while ( !$rs_unreg->EOF ) {
		$unreg[$rs_unreg->fields[0]] = $rs_unreg->fields[1];
		$rs_unreg->MoveNext();
	}
}

$total_reg = 0; $total_unreg = 0; $tot_total_reg_unreg = 0;

for($i=0;$i<=23;$i++) {
	if( strlen($i) == 1) $jam = "0".$i;
	else $jam = $i;
	
	$fix_jam[$i] = $jam;
	
	if( $reg[$jam] <> "" ) {
		$tot_reg[$i] = $reg[$jam];
	}
	else {
		$tot_reg[$i] = 0;
	}
	
	$total_reg += $tot_reg[$i];
	
	if( $unreg[$jam] <> "" ) {
		$tot_unreg[$i] = $unreg[$jam];
	}
	else {
		$tot_unreg[$i] = 0;
	}
	$total_unreg += $tot_unreg[$i];
	
	$total_reg_unreg[$i] = $tot_reg[$i] + $tot_unreg[$i];
	$tot_total_reg_unreg += $tot_reg[$i] + $tot_unreg[$i];
	
	array_unshift($datajam, $fix_jam[$i]);
  array_unshift($datareg, $tot_reg[$i]);
  array_unshift($dataunreg, $tot_unreg[$i]);
	
}

// membuat image dengan ukuran 400x200 px
$graph = new Graph(1100,300,"auto");    
$graph->SetScale("textlin");

// menampilkan diagram batang untuk data pria dengan warna orange
// pada diagram batang ditampilkan value data
//$bplot1 = new BarPlot($datareg);
//$bplot1->SetFillColor("orange");
//$bplot1->value->show();

// menampilkan diagram batang untuk data wanita dengan warna biru
// pada diagram batang ditampilkan value data
//$bplot2 = new BarPlot($dataunreg);
//$bplot2->SetFillColor("blue");
//$bplot2->value->show();

$lineplot1=new LinePlot($datareg);
$graph->Add($lineplot1);
$lineplot1->SetColor("red");

$lineplot2=new LinePlot($dataunreg);
$graph->Add($lineplot2);
$lineplot2->SetColor("green");

// mengelompokkan grafik batang berdasarkan pria dan wanita
//$gbplot = new GroupBarPlot(array($bplot1,$bplot2));
//$graph->Add($gbplot);

//$lineplot = new GroupLinePlot(array($lineplot1,$lineplot2));
//$graph->Add($lineplot);

// membuat legend untuk keterangan pria dan wanita
//$bplot1->SetLegend("REG");
//$bplot2->SetLegend("UNREG");
$lineplot1->SetLegend("REG");
$lineplot2->SetLegend("UNREG");
$graph->legend->Pos(0.01,0.1,"right","center");

// mengatur margin image
$graph->img->SetMargin(40,80,20,40);

// menampilkan title grafik dan nama masing-masing sumbu
$graph->title->Set("Grafik MO Reg/Unreg Per Jam");
$graph->xaxis->title->Set("Jam");

// menampilkan nama negara ke sumbu x
$graph->xaxis->SetTickLabels($datajam);

// format font title grafik
$graph->title->SetFont(FF_FONT1,FS_BOLD);

// menampilkan efek shadow pada image
//$graph->SetShadow();

// menampilkan image ke browser
$graph->Stroke();
?>