<?php

include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if(!empty($_GET['bln'])) {
	$bulan = $_GET['bln'];
	$bulan_name = get_bulan_name($_GET['bln']);
}
else {
	$bulan = date("m");
	$bulan_name = date("F");
}
$smarty->assign("bulan",$bulan);
$smarty->assign("bulan_name",$bulan_name);

$tahun = (!empty($_GET['thn'])) ? $_GET['thn'] : date("Y");
$smarty->assign("tahun",$tahun);

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

$log = $tahun.$bulan;

$dd_subject = $mFunc->crtDropDown($config['subject'],$config['subject'], 'subject2', $_GET['subject2'], 'All', 'class="text"');
$smarty->assign("dd_subject",$dd_subject);

$dd_mt_type = $mFunc->crtDropDown($config['mt_type'],$config['mt_type'], 'mt_type2', $_GET['mt_type2'], 'All', 'class="text"');
$smarty->assign("dd_mt_type",$dd_mt_type);

$where = " WHERE date_format(datetime,'%Y-%m') = '".$tahun."-".$bulan."'";

if(!empty($_GET['appid2'])) {
	$where .= " AND appid = '".$_GET['appid2']."'";
	$smarty->assign("appid2",$_GET['appid2']);
}
if(!empty($_GET['subject2']) && $_GET['subject2'] <> "--" ) {
	$where .= " AND subject like '".$_GET['subject2']."%'"; 
	$smarty->assign("subject2",$_GET['subject2']);
}
if(!empty($_GET['msisdn2'])) {
	$where .= " AND msisdn = '".$_GET['msisdn2']."'"; 
	$smarty->assign("msisdn2",$_GET['msisdn2']);
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco2', $_GET['telco2'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(!empty($_GET['telco2'])) {
	if($_GET['telco2'] <> "--") {
		$where .= " AND telco like '".$_GET['telco2']."%'";
	}
	$telco_name = $mFunc->get_telco_name($_GET['telco2']);
	$smarty->assign("telco2",$_GET['telco2']);
	$smarty->assign("telco_name",$telco_name);
}
if(!empty($_GET['shortcode2'])) {
	$where .= " AND shortcode = '".$_GET['shortcode2']."'"; 
	$smarty->assign("shortcode2",$_GET['shortcode2']);
}
if(!empty($_GET['mt_type2']) && $_GET['mt_type2'] <> "--" ) {
	$where .= " AND mt_type = '".$_GET['mt_type2']."'"; 
	$smarty->assign("mt_type2",$_GET['mt_type2']);
}
if(!empty($_GET['trx_id2'])) {
	$where .= " AND trx_id = '".$_GET['trx_id2']."'"; 
	$smarty->assign("trx_id2",$_GET['trx_id2']);
}
if(!empty($_GET['session_id2'])) {
	$where .= " AND session_id = '".$_GET['session_id2']."'"; 
	$smarty->assign("session_id2",$_GET['session_id2']);
}

if( $_SESSION["ses_partner_id"] == 0 ) $str_sql = "";
else $str_sql = " AND partner_id=".$_SESSION["ses_partner_id"];

$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : 20;
$sql = "SELECT * FROM mp_system.mt_outgoing_log".$log." $where $str_sql ORDER BY datetime DESC LIMIT $limit";
//echo $sql;
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$appid[] = $rs->fields[1];
		$msisdn[] = $rs->fields[3];
		$sms[] = $rs->fields[5];
		$subject[] = $rs->fields[6];
		$message[] = $rs->fields[7];
		$url[] = $rs->fields[8];
		$telco[] = $rs->fields[9];
		$shortcode[] = $rs->fields[10];
		$charge[] = $rs->fields[11];
		$mt_type[] = $rs->fields[12];
		$trx_id[] = $rs->fields[13];
		$session_id[] = $rs->fields[15];
		$media_id[] = $rs->fields[16];
		$partner_id[] = $rs->fields[17];
		$ack_status[] = $rs->fields[18];
		$sid[] = $rs->fields[19];
		$dn_telco[] = $rs->fields[20];
		$dn_status[] = $rs->fields[21];
		$datetime[] = $rs->fields[23];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("msisdn",$msisdn);
	$smarty->assign("sms",$sms);
	$smarty->assign("subject",$subject);
	$smarty->assign("message",$message);
	$smarty->assign("url",$url);
	$smarty->assign("telco",$telco);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("charge",$charge);
	$smarty->assign("mt_type",$mt_type);
	$smarty->assign("trx_id",$trx_id);
	$smarty->assign("session_id",$session_id);
	$smarty->assign("media_id",$media_id);
	$smarty->assign("partner_id",$partner_id);
	$smarty->assign("ack_status",$ack_status);
	$smarty->assign("sid",$sid);
	$smarty->assign("dn_telco",$dn_telco);
	$smarty->assign("dn_status",$dn_status);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
}
/*
$sql_count = "SELECT msisdn FROM mp_system.mt_outgoing_log".$log." $where";
$numresult =ecute($sql_count);

$numrows = $numresult->RecordCount();

$smarty->assign("numrows",$numrows);
*/
$smarty->display(TEMPLATES_DIR.'mt_log_partner.html');

function get_bulan_name($bln) {
	switch($bln) {
		case "01" : $bulan_name = "January"; break;
		case "02" : $bulan_name = "February"; break;
		case "03" : $bulan_name = "March"; break;
		case "04" : $bulan_name = "April"; break;
		case "05" : $bulan_name = "May"; break;
		case "06" : $bulan_name = "June"; break;
		case "07" : $bulan_name = "July"; break;
		case "08" : $bulan_name = "August"; break;
		case "09" : $bulan_name = "September"; break;
		case "10" : $bulan_name = "October"; break;
		case "11" : $bulan_name = "November"; break;
		case "12" : $bulan_name = "December"; break;
	}
	return $bulan_name;
}

?>