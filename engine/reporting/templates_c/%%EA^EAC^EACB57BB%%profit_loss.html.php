<?php /* Smarty version 2.6.18, created on 2017-03-15 12:49:49
         compiled from /var/www/sms/engine/reporting/templates/profit_loss.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting/templates/profit_loss.html', 129, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<body>
<?php echo '
<script type="text/javascript" language="JavaScript"> 
<!-- 
function go_delete(d,e) 
{ 
 var where_to= confirm("Yakin Anda akan menghapus ini?");
 var i;
 var j;
 i = d;
 j = e;
 if (where_to== true)
   window.location=i + j;
} 
//-->
</script>
<SCRIPT LANGUAGE="JavaScript">
<!--

<!-- Begin
function CheckAll()
{
	var allInputs = document.getElementsByTagName("input")
	for (var i = 0; i < allInputs.length; i++) {
		if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
			allInputs[i].checked = true;
		} else continue;
	}
}

function UnCheckAll()
{
	var allInputs = document.getElementsByTagName("input")
	for (var i = 0; i < allInputs.length; i++) {
		if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
			allInputs[i].checked = false;
		} else continue;
	}
}
// End -->
</script>
'; ?>

<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0" class="tabel">
  <tr>
    <td height="106" valign="center" background="<?php echo $this->_tpl_vars['img_dir']; ?>
topbarbkg.gif"><div align="center" class="txt-title"><?php echo $this->_tpl_vars['title']; ?>
</td>
  </tr>
  <tr>
    <td height="435" valign="top"><table width="100%" height="91%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="16%" height="444" valign="top" background="<?php echo $this->_tpl_vars['img_dir']; ?>
leftcollumbkg2.gif"><div align="left">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div></td>
        <td width="84%" valign="top">
		  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="20">
            <td width="97%">&nbsp;</td>
            <td width="3%">&nbsp;</td>
          </tr>
          <tr>
            <td height="401" valign="top"><div align="center">
            <!-- content begin -->
            	<div align="left" class="text">
            		<div align="left" class="text">
            		<strong>PROFIT LOSS</strong>
            	<br>
            	<br>
            	 <!-- Add -->
					  <a name="add">
					  <form name="add" method="get" action="">
						  <table width="392" border="0" cellpadding="0" cellspacing="0" bgcolor="#000033">
						    <tr>
						      <td width="850"><table width="100%" height="100"  border="0" cellpadding="3" cellspacing="1">
										<tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">SHORTCODE</td>
						          <td><div align="left">
						            <?php echo $this->_tpl_vars['dd_shortcode']; ?>

						          </div></td>
						        </tr>
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">TELCO</td>
						          <td><div align="left">
						            <?php echo $this->_tpl_vars['dd_telco']; ?>

						          </div></td>
						        </tr>
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">SERVICE</td>
						          <td><div align="left">
						            <?php echo $this->_tpl_vars['dd_service']; ?>

						          </div></td>
						        </tr>
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">&nbsp;</td>
						          <td><div align="left">
						            <input type="submit" name="submit" value="Submit" class="text">
						          </div></td>
						        </tr>
						      </table></td>
						    </tr>
						  </table>
						</form>
<?php if ($this->_tpl_vars['submit'] != ""): ?>
					  <!-- Add -->
					  	<br>
					  	<table border="0" cellspacing="0" cellpadding="0">
							 <tr>
							 <td><div align="left"><strong>PROFIT LOSS : [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ] [ Service : <?php echo $this->_tpl_vars['s_name']; ?>
 ]</strong></div></td>
							 </tr>
							 <tr>
							 <td>&nbsp;</td>
							 </tr>
							 <tr>
							   <td bgcolor="#000033">
									<table width="100%" height="100%"  border="0" cellpadding="3" cellspacing="1">
										<tr bgcolor="#9DC1F5">
											<th align="center" >APPID</th>
											<th align="center" >DESCRIPTION</th>
											<th align="center" >SHORTCODE</th>
											<th align="center" >TELCO</th>
											<th align="center" >TOTAL NET REVENUE</th>
											<th align="center" >TOTAL COST</th>
											<th align="center" >PROFIT/LOSS</th>
										</tr>
										<tr bgcolor="#FFFFFF">
											<td align="left"><?php echo $this->_tpl_vars['appid']; ?>
</td>
											<td align="left"><?php echo $this->_tpl_vars['s_name']; ?>
</td>
											<td align="center"><?php echo $this->_tpl_vars['sc']; ?>
</td>
											<td align="center"><?php echo $this->_tpl_vars['tlc']; ?>
</td>
											<td align="right"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total_nett'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
											<td align="right"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total_cost'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
											<td align="right"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['profit_loss'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
										</tr>
								 </table>
							   </td>
							 </tr>
						  </table>
						<!-- add -->
<?php endif; ?>
            	</div>
            <!-- content end -->
            </div></td>
            <td>&nbsp;</td>
          </tr>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </table>
		</td>
      </tr>
    </table></td>
  </tr>
</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>