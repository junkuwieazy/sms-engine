<?php /* Smarty version 2.6.18, created on 2017-10-16 16:22:24
         compiled from /var/www/sms/engine/reporting2/templates/push_sms.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Push SMS</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
         var where_to= confirm("Yakin Anda akan menghapus ini?");
         var i;
         var j;
         i = d;
         j = e;
         if (where_to== true)
           window.location=i + j;
   } 
//-->
</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Application</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <form class="form-horizontal" action="" method="POST">
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>SERVICE / KEYWORD</label>
                                            </div>
                                            <div class="col-md-7">
                                                <?php echo $this->_tpl_vars['dd_service']; ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>MSISDN</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input name="msisdn" type="text" class="form-control"  placeholder="MSISDN">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>SHORTCODE</label>
                                            </div>
                                            <div class="col-md-7">
                                                <?php echo $this->_tpl_vars['dd_shortcode']; ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>TELCO</label>
                                            </div>
                                            <div class="col-md-7">
                                                <?php echo $this->_tpl_vars['dd_telco']; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>PRICE</label>
                                            </div>
                                            <div class="col-md-7">
                                                <select name='price' class="form-control">
                                                    <option value='PULL-0' >PULL-0</option>
                                                    <option value='PUSH-0' >PUSH-0</option>
                                                    <option value='PULL-2000' >PULL-2000</option>
                                                    <option value='PUSH-2000' >PUSH-2000</option>
                                                    <option value='PUSH-1650' >PUSH-1650</option>
                                                    <option value='PUSH-2000-musik' >PUSH-2000-cool</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>SMS</label>
                                            </div>
                                            <div class="col-md-7">
                                                <textarea class="form-control" name="content" cols="30" rows="3" onKeyDown="textCounter(this.form.content,this.form.remLentext,160);" onKeyUp="textCounter(this.form.content,this.form.remLentext,160);"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>CHARACTER COUNT</label>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input name="remLentext" type="text" class="form-control" id="remLentext" value="160" disabled>
                                                    </div>
                                                    <button type="submit" class="btn btn-success col-md-6"><i class="fa fa-send"></i> Send</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-body" style="overflow-x:auto">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">APPID</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">KEYWORD</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">MSISDN</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">SHORTCODE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">TELCO</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">SMS</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">ENTRY DATE</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                    <tr>
                                        <td><?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['description'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['msisdn'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['shortcode'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['telco'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['sms'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['datetime'][$this->_sections['row']['index']]; ?>
</td>
                                    </tr>
                                    <?php endfor; endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->


    </div> 
    <footer class="main-footer">
        <?php echo $this->_tpl_vars['copyright']; ?>

    </footer>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>