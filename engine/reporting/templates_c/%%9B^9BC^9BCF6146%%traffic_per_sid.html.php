<?php /* Smarty version 2.6.18, created on 2017-03-14 11:19:52
         compiled from /var/www/sms/engine/reporting/templates/traffic_per_sid.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting/templates/traffic_per_sid.html', 131, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<body>
<?php echo '
<script type="text/javascript" language="JavaScript"> 
<!-- 
function go_delete(d,e) 
{ 
 var where_to= confirm("Yakin Anda akan menghapus ini?");
 var i;
 var j;
 i = d;
 j = e;
 if (where_to== true)
   window.location=i + j;
} 
//-->
</script>
<SCRIPT LANGUAGE="JavaScript">
<!--

<!-- Begin
function CheckAll()
{
	var allInputs = document.getElementsByTagName("input")
	for (var i = 0; i < allInputs.length; i++) {
		if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
			allInputs[i].checked = true;
		} else continue;
	}
}

function UnCheckAll()
{
	var allInputs = document.getElementsByTagName("input")
	for (var i = 0; i < allInputs.length; i++) {
		if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
			allInputs[i].checked = false;
		} else continue;
	}
}
// End -->
</script>
'; ?>

<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0" class="tabel">
  <tr>
    <td height="106" valign="center" background="<?php echo $this->_tpl_vars['img_dir']; ?>
topbarbkg.gif"><div align="center" class="txt-title"><?php echo $this->_tpl_vars['title']; ?>
</td>
  </tr>
  <tr>
    <td height="435" valign="top"><table width="100%" height="91%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="16%" height="444" valign="top" background="<?php echo $this->_tpl_vars['img_dir']; ?>
leftcollumbkg2.gif"><div align="left">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div></td>
        <td width="95%" valign="top">
		  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
          <tr height="20">
            <td width="97%">&nbsp;</td>
            <td width="3%">&nbsp;</td>
          </tr>
          <tr>
            <td height="401" valign="top"><div align="center">
            <!-- content begin -->
            	<div align="left" class="text">
            		<div align="left" class="text">
            		<strong>TRAFFIC PER SID</strong>
            	<br>
            	<br>
            	 <!-- Add -->
					  <a name="add">
					  <form name="add" method="get" action="">
						  <table border="0" cellpadding="0" cellspacing="0" bgcolor="#000033">
						    <tr>
						      <td ><table width="100%" height="100"  border="0" cellpadding="3" cellspacing="1">
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">BULAN</td>
						          <td><div align="left">
						            <?php echo $this->_tpl_vars['dd_month']; ?>

						            <?php echo $this->_tpl_vars['dd_year']; ?>

						          </div></td>
						        </tr>
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">SHORTCODE</td>
						          <td><div align="left">
						            <?php echo $this->_tpl_vars['dd_shortcode']; ?>

						          </div></td>
						        </tr>
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">TELCO</td>
						          <td><div align="left">
						            <?php echo $this->_tpl_vars['dd_telco']; ?>

						          </div></td>
						        </tr>
						        <tr bgcolor="#FFFFFF">
						          <td bgcolor="#9DC1F5">&nbsp;</td>
						          <td><div align="left">
						            <input type="submit" name="submit" value="Submit" class="text">
						          </div></td>
						        </tr>
						      </table></td>
						    </tr>
						  </table>
						</form>
					  <!-- Add -->
					  	<br>
					  	<br>
					  	<!-- List -->
			        <table border="0" cellspacing="0" cellpadding="0">
						  	<tr>
								 <td><div align="left"><strong> TRAFFIC PER SID -- : [ Bulan : <?php echo $this->_tpl_vars['bulan_traffic']; ?>
 , Tahun : <?php echo $this->_tpl_vars['tahun_traffic']; ?>
 ] [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ]</strong></div></td>
								 </tr>
								 <tr>
								 <td>&nbsp;</td>
								 </tr>
					  	</table>
							
						<table border="0" cellspacing="0" cellpadding="0">
						 <tr>
						   <td bgcolor="#000033">
						    	<table width="100%" height="100%"  border="0" cellpadding="3" cellspacing="1">
								<tr bgcolor="#006699" class="txt-menu">
									<td><div align="center"><strong>TARIFF</strong></div></td>
									<td><div align="center"><strong>BEARER COST</strong></div></td>
									<td><div align="center"><strong>CONTENT VALUE</strong></div></td>
									<td><div align="center"><strong>SID</strong></div></td>
									<td><div align="center"><strong>JUMLAH</strong></div></td>
									<td><div align="center"><strong>GROSS REVENUE</strong></div></td>
									<td><div align="center"><strong>NETT REVENUE</strong></div></td>
								</tr>
<?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['sid']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
								<tr bgcolor="<?php echo $this->_tpl_vars['warna'][$this->_sections['row']['index']]; ?>
" class="text">
									<td ><div align="right"><?php echo ((is_array($_tmp=$this->_tpl_vars['tarif'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</div></td>
									<td ><div align="right"><?php echo $this->_tpl_vars['bearer'][$this->_sections['row']['index']]; ?>
</div></td>
									<td ><div align="right"><?php echo ((is_array($_tmp=$this->_tpl_vars['content_value'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</div></td>
									<td ><div align="left"><?php echo $this->_tpl_vars['sid'][$this->_sections['row']['index']]; ?>
</div></td>
									<td ><div align="right"><?php echo ((is_array($_tmp=$this->_tpl_vars['jumlah'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</div></td>
									<td ><div align="right"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</div></td>
									<td ><div align="right"><?php echo ((is_array($_tmp=$this->_tpl_vars['nett'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</div></td>
								</tr>
<?php endfor; endif; ?>
								<tr bgcolor="#CCCCCC" class="text">
									<td ><div align="center"></div></td>
									<td ><div align="left"></div></td>
									<td ><div align="center"></div></td>
									<td ><div align="center"></div></td>
									<td ><div align="center"></div></td>
									<td ><div align="right"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['tot_gross'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></div></td>
									<td ><div align="right"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['tot_nett'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></div></td>
								</tr>
							 </table>
						   </td>
						 </tr>
					  </table>
							
            	</div>
            <!-- content end -->
            </div></td>
            <td>&nbsp;</td>
          </tr>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "copyright.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </table>
		</td>
      </tr>
    </table></td>
  </tr>
</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>