<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $config['telco'][$_GET['telco']] );
	}
	else $smarty->assign("tlc", "All" );
}

if(isset($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
	if(!empty($_GET['tgl2'])) {
		$smarty->assign("tgl2", $_GET['tgl2'] );
		$tgl = $_GET['tgl2'];
	}
	else {
		$smarty->assign("tgl2", date("Y-m-d") );
		$tgl = date("Y-m-d");
	}

	if(!empty($_GET['service'])) {
		if($_GET['service'] <> "--") {
			$str_sql .= " AND appid = '".$_GET['service']."'";
		}
		$s_name = $mFunc->get_description($db, $_GET['service']);
		$smarty->assign("service",$_GET['service']);
		$smarty->assign("s_name",$s_name);
	}
}
else {
	$smarty->assign("tgl2", date("Y-m-d") );
	$tgl = date("Y-m-d");
	$smarty->assign("s_name","All");
	$smarty->assign("submit","");
}

if( $_SESSION["ses_username"] == "admin" ) {
	$str_sql .= " AND telco = 'isat'";
}

// REG
$sql_reg = "SELECT jam, sum(jumlah) AS jml
						FROM app_sms.msisdn_hourly_reg
						WHERE 1 AND tanggal = '".$tgl."'
						GROUP BY jam";
//$sql_reg = "SELECT date_format( date_reg, '%H' ) AS jam, count( 1 ) AS jml FROM app_sms.msisdn_subscriber WHERE 1 $str_sql AND date_format( date_reg, '%Y-%m-%d' ) = '".$tgl."' GROUP BY jam";
//echo $sql_reg."<br>";
$rs_reg = $db_app->Execute($sql_reg) or die( $db_app->ErrorMsg() );
if ( $rs_reg->RecordCount() > 0 ) {
	while ( !$rs_reg->EOF ) {
		$reg[$rs_reg->fields[0]] = $rs_reg->fields[1];
		$rs_reg->MoveNext();
	}
}

// UNREG
$sql_unreg = "SELECT jam, sum(jumlah) AS jml
						FROM app_sms.msisdn_hourly_unreg
						WHERE 1 AND tanggal = '".$tgl."'
						GROUP BY jam";
//$sql_unreg = "SELECT date_format( date_unreg, '%H' ) AS jam, count( 1 ) AS jml FROM app_sms.msisdn_subscriber WHERE 1 $str_sql AND date_format( date_unreg, '%Y-%m-%d' ) = '".$tgl."' GROUP BY jam";
//echo $sql_reg."<br>";
$rs_unreg = $db_app->Execute($sql_unreg) or die( $db_app->ErrorMsg() );
if ( $rs_unreg->RecordCount() > 0 ) {
	while ( !$rs_unreg->EOF ) {
		$unreg[$rs_unreg->fields[0]] = $rs_unreg->fields[1];
		$rs_unreg->MoveNext();
	}
}
/*
// UNREG
$sql_mo = "SELECT date_format( datetime, '%H' ) AS jam, count( 1 ) AS jml FROM mp_system.mo_incoming_log".date("Ym")." WHERE 1 $str_sql AND date_format( datetime, '%Y-%m-%d' ) = '".$tgl."' GROUP BY jam";
//echo $sql_reg."<br>";
$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
if ( $rs_mo->RecordCount() > 0 ) {
	while ( !$rs_mo->EOF ) {
		$mo[$rs_mo->fields[0]] = $rs_mo->fields[1];
		$rs_mo->MoveNext();
	}
}
*/
$total_reg = 0; $total_unreg = 0; $tot_total_reg_unreg = 0; $total_mo = 0;

for($i=0;$i<=23;$i++) {
	if( strlen($i) == 1) $jam = "0".$i;
	else $jam = $i;

	$fix_jam[$i] = $jam;

	if( $reg[$jam] <> "" ) {
		$tot_reg[$i] = $reg[$jam];
	}
	else {
		$tot_reg[$i] = 0;
	}

	$total_reg += $tot_reg[$i];

	if( $unreg[$jam] <> "" ) {
		$tot_unreg[$i] = $unreg[$jam];
	}
	else {
		$tot_unreg[$i] = 0;
	}
	$total_unreg += $tot_unreg[$i];
/*
	if( $mo[$jam] <> "" ) {
		$tot_mo[$i] = $mo[$jam];
	}
	else {
		$tot_mo[$i] = 0;
	}
	$total_mo += $tot_mo[$i];
*/
	$total_reg_unreg[$i] = $tot_reg[$i] + $tot_unreg[$i];
	$tot_total_reg_unreg += $tot_reg[$i] + $tot_unreg[$i];

}

$smarty->assign("i",$i);
$smarty->assign("fix_jam",$fix_jam);
$smarty->assign("t_reg",$tot_reg);
$smarty->assign("total_reg",$total_reg);
$smarty->assign("t_unreg",$tot_unreg);
$smarty->assign("total_unreg",$total_unreg);
$smarty->assign("total_reg_unreg",$total_reg_unreg);
$smarty->assign("tot_total_reg_unreg",$tot_total_reg_unreg);

//$smarty->assign("tot_mo",$tot_mo);
//$smarty->assign("total_mo",$total_mo);

$smarty->display(TEMPLATES_DIR.'mo_reg_d.html');

?>
