<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';
$msg = '';

if(!empty($_GET['filter'])) {
	$filter = " WHERE keyword like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}
if(!empty($_POST['keyword'])) {
	$filter = " WHERE keyword like '%".$_POST['keyword']."%'";
	$smarty->assign("filter",$_POST['keyword']);
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['keyword']) && !empty($_POST['telco']) && !empty($_POST['shortcode']) && !empty($_POST['appid']) && $_POST['appid'] <> "--" ) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				$sql_add = "UPDATE mp_system.keyword SET appid = '".$_POST['appid']."',keyword = '".$_POST['keyword']."',telco = '".$_POST['telco']."',shortcode = '".$_POST['shortcode']."', datetime = NOW() WHERE id= ".$_POST['id'];
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				$msg = "Keyword berhasil diedit";
			
			}
		}
		else {
			$sqlcek = "SELECT keyword FROM mp_system.keyword WHERE keyword = '".$_POST['keyword']."' AND telco = '".$_POST['telco']."' AND shortcode = '".$_POST['shortcode']."' AND appid = '".$_POST['appid']."'";
			$rs = $db->Execute($sqlcek);
			if ( $rs->RecordCount() > 0 ) {
				$msg = "KEYWORD ".$_POST['keyword']." sudah ada";
			}
			else {
				$sql_add = "INSERT INTO mp_system.keyword (keyword,telco,shortcode,appid,datetime)
								VALUES ('".$_POST['keyword']."','".$_POST['telco']."','".$_POST['shortcode']."','".$_POST['appid']."',NOW())";
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				$msg = "Keyword berhasil ditambah";
			}
		}
		
	}
	else {
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM mp_system.keyword WHERE id = ".$_GET['id']." ORDER BY keyword";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$id_edit = $rs->fields[0];
				$keyword_edit = $rs->fields[1];
				$telco_edit = $rs->fields[2];
				$shortcode_edit = $rs->fields[3];
				$appid_edit = $rs->fields[4];
				$datetime_edit = $rs->fields[5];
				
				$rs->MoveNext();
			}
			
			$sql_appid = "SELECT description FROM mp_system.application WHERE appid = '".$appid_edit."'";
			$rs_appid = $db->Execute($sql_appid) or die( $db->ErrorMsg() );
			if ( $rs_appid->RecordCount() > 0 ) {
				$description_edit = $rs_appid->fields[0];
			}
			
			$_POST['telco'] = $telco_edit;
			$_POST['shortcode'] = $shortcode_edit;
			$_POST['appid'] = $appid_edit;
			$smarty->assign("appid_edit",$appid_edit);
			$smarty->assign("id_edit",$id_edit);
			$smarty->assign("keyword_edit",$keyword_edit);
			$smarty->assign("shortcode_edit",$shortcode_edit);
			$smarty->assign("telco_edit",$telco_edit);
			$smarty->assign("description_edit",$description_edit);
			$smarty->assign("datetime_edit",$datetime_edit);
			$smarty->assign("action","edit");
		}
	}
	elseif($_GET['action'] == "delete") {
		$sql_add = "DELETE FROM mp_system.keyword WHERE id=".$_GET['id'];
		$db->Execute($sql_add) or die( $db->ErrorMsg() );
		$msg = "Keyword berhasil didelete";
	}
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM mp_system.keyword $filter ORDER BY keyword LIMIT $begin, $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$id[] = $rs->fields[0];
		$keyword[] = $rs->fields[1];
		$telco[] = $rs->fields[2];
		$shortcode[] = $rs->fields[3];
		$appid[] = $rs->fields[4];
		$datetime[] = $rs->fields[5];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("id",$id);
	$smarty->assign("keyword",$keyword);
	$smarty->assign("telco",$telco);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
	$sql_count = "SELECT keyword FROM mp_system.keyword $filter ORDER BY keyword";
	$numresult = $db->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
}

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application ORDER BY appid ASC";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where ORDER BY appid ASC";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDownKW($arr_service,$arr_service, 'appid', $_POST['appid'], '--', 'class="text"');
$smarty->assign("dd_service",$dd_service);

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_POST['shortcode'], '', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_POST['telco'], '', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_POST['telco'], '', 'class="text"');
}
$smarty->assign("dd_telco",$dd_telco);

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'keyword.html');

?>