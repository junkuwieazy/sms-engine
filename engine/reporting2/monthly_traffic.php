<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( empty($_GET['submit']) ) {
	$_GET['submit'] = '';
}

$smarty->assign("submit",$_GET['submit']);
$fix_where = '';
if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
$jc = 1; $jj = 1;
$checkbox = "<table class='table table-responsive' border='1' cellpadding='0' cellspacing='0'><tr>";
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		if( empty($_GET['submit']) ) $checked = "checked";
		else {
			if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
			else $checked = "";
		}

		$checkbox .= "<td><input type='checkbox' name='check_list".$jj."' id='check_list".$jj."' value='".$rs->fields[0]."' $checked>".$rs->fields[1]."</td>";
		if( $jc == 5 ) {
			$checkbox .= '</tr>';
			$jc = 0;
		}
		$jc++;
		$jj++;
		$rs->MoveNext();
	}
}
$checkbox .= "<tr>
				<input class='btn btn-info' type='button' name='Check_All' value='Check All' onClick='CheckAll()'>
				<input class='btn btn-warning' type='button' name='Un_CheckAll' value='Uncheck All' onClick='UnCheckAll()'>
				<button type='submit' name='submit' value='Submit' class='btn btn-success'><i class='fa fa-search'></i> Search</button>
				<input type='hidden' name='active' value='report'>
                                <input type='hidden' name='menu' value='Monthly Traffic'>
			</tr></table>";
$smarty->assign("checkbox",$checkbox);


/* service get using drop box

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], 'All', 'class="form-control"');
$smarty->assign("dd_service",$dd_service); */

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="form-control"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], $_GET['telco'], 'class="form-control"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="form-control"');
}
//$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="form-control"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
		$telcooo = $_GET['telco'];
	}
	else {
		$smarty->assign("tlc", "All" );
		$telcooo = 'All';
	}
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("thn",$tahun);


$smarty->assign("thn_now",date("Y"));

$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="form-control"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$tahun_traffic = date("Y");
	$smarty->assign("s_name","All");
	$str_service = ""; //addition checkbox
}
else {
	$tahun_traffic = $_GET['thn'];
	$fix_where = '';
/* 	if($_GET['service'] <> "--") {
		$str_sql .= " AND service='".$_GET['service']."'";
		$s_name = $mFunc->get_description($db, $_GET['service']);
		$smarty->assign("s_name",$s_name);
	}
	else $smarty->assign("s_name","All");

	// addition checkbox, */

	$service_in = ""; $is_check = false; $s_name = "";
	for($r=1;$r<=$jj;$r++) {
		if( isset($_GET['check_list'.$r]) ) {
			$service_in .= "'".$_GET['check_list'.$r]."',";
			$is_check = true;
			$s_name .= $mFunc->get_description($db, $_GET['check_list'.$r]).", ";
		}
	}
	$service_in = ereg_replace(',$','',$service_in);

	if( $is_check ) $smarty->assign("s_name",$s_name);
	else $smarty->assign("s_name","All");

	if( $service_in <> "" ) $str_service = "AND appid in ( ".$service_in." )";
	else $str_service = "AND appid = '0000'";

}
$smarty->assign("service",$_GET['service']);

$smarty->assign("tahun_traffic",$tahun_traffic);

$mo_tot = 0; $mt_ok_tot = 0; $mt_nok_tot = 0; $total_mt_tot = 0; $gross_tot = 0; $net_tot = 0;
$hh = 0;

if( $tahun_traffic == date("Y") ) $j_bln = date("m");
else $j_bln = 12;

for($t=$j_bln;$t>=1;$t--) {

	if( strlen($t) == 1) $blnn = "0".$t;
	else $blnn = $t;

	$tel_ok[$hh] = '';

	$mo[$hh] = 0;
	$mt_ok[$hh] = 0;
	$mt_nok[$hh] = 0;
	$total_mt[$hh] = 0;
	$gross[$hh] = 0;
	$net[$hh] = 0;

	for($d=1;$d<=31;$d++) {
		if( strlen($d) == 1) $dayy = "0".$d;
		else $dayy = $d;

		//
		$tgl_print[$hh] = $mFunc->monthName($blnn);

		$sql_mo = "SELECT sum( jumlah )
								FROM mp_system.mt_summary_daily_".$tahun_traffic.$blnn."
								WHERE datetime = '".$tahun_traffic."-".$blnn."-".$dayy."' $str_sql $str_service $fix_where
								AND subject NOT LIKE 'push%'";
		//echo $sql_mo."<BR>";
		if( $tahun_traffic == "2015" ) {
			$rs_mo = $dblocal->Execute($sql_mo) or die( $dblocal->ErrorMsg() );
		}
		else {
			$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
		}

		if ( $rs_mo->RecordCount() > 0 ) {
			while ( !$rs_mo->EOF ) {
				$mo[$hh] += $rs_mo->fields[0];
				$rs_mo->MoveNext();
			}
		}

		$sql = "SELECT telco, charge, dn_status, sum( jumlah ),sid
							FROM mp_system.mt_summary_daily_".$tahun_traffic.$blnn."
							WHERE datetime = '".$tahun_traffic."-".$blnn."-".$dayy."' $str_sql $str_service $fix_where
							GROUP BY telco,charge,sid,dn_status";

		if( $tahun_traffic == "2015" ) {
			$rs = $dblocal->Execute($sql) or die( $dblocal->ErrorMsg() );
		}
		else {
			$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		}

		if ( $rs->RecordCount() > 0 ) {

			while ( !$rs->EOF ) {

				if( $rs->fields[2] == "DELIVERED" ) {

					$sqlbearer = "SELECT bearer_cost FROM mp_system.list_sid WHERE sid='".$rs->fields[4]."'";
					$rsbearer = $db->Execute($sqlbearer) or die( $db->ErrorMsg() );
					if ( $rsbearer->RecordCount() > 0 ) {
						while ( !$rsbearer->EOF ) {

							$bearer_cost = $rsbearer->fields[0];

							$rsbearer->MoveNext();
						}
					}
					else {
						$bearer_cost = 0;
					}

					if( $blnn.$tahun_traffic == "032011" ) $telco_share = 0.5;
					elseif( $blnn.$tahun_traffic == "042011" ) $telco_share = 0.5;
					elseif( $blnn.$tahun_traffic == "052011" ) $telco_share = 0.5;
					else $telco_share = $config['share'][$rs->fields[0]];

					$ar_price = explode("-",$rs->fields[1]);
					$price = $ar_price[1] - $bearer_cost;

					$tel_ok[$hh] += $rs->fields[1];

					$mt_ok[$hh] += $rs->fields[3];
					if( $price == 250 ) $price = 0;

					$gross[$hh] += $rs->fields[3] * $price;
					$net[$hh] += $rs->fields[3] * ($price * $telco_share );

				}
				else {
					$mt_nok[$hh] += $rs->fields[3];
				}

				$total_mt[$hh] += $rs->fields[3];

				$rs->MoveNext();
			}
		}

		//Today Traffic
		if( $tahun_traffic == date("Y") && $blnn == date("m") && $dayy == date("d") ) {
			////
			$sql_mo = "SELECT sum( jumlah )
									FROM mp_system.mt_summary_hourly
									WHERE 1 $str_sql $str_service $fix_where
									AND subject NOT LIKE 'push%'";
			//echo $sql_mo."<BR>";
			if( $tahun_traffic == "2015" ) {
				$rs_mo = $dblocal->Execute($sql_mo) or die( $dblocal->ErrorMsg() );
			}
			else {
				$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
			}

			if ( $rs_mo->RecordCount() > 0 ) {
				while ( !$rs_mo->EOF ) {
					$mo[$hh] += $rs_mo->fields[0];
					$rs_mo->MoveNext();
				}
			}

			$sql = "SELECT telco, charge, dn_status, sum( jumlah ),sid
								FROM mp_system.mt_summary_hourly
								WHERE 1 $str_sql $str_service $fix_where
								GROUP BY telco,charge,sid,dn_status";
			//echo $sql."<br>";
			if( $tahun_traffic == "2015" ) {
				$rs = $dblocal->Execute($sql) or die( $dblocal->ErrorMsg() );
			}
			else {
				$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
			}
			//$rs = $db->Execute($sql) or die( $db->ErrorMsg() );

			if ( $rs->RecordCount() > 0 ) {

				while ( !$rs->EOF ) {

					if( $rs->fields[2] == "DELIVERED" ) {

						$sqlbearer2 = "SELECT bearer_cost FROM mp_system.list_sid WHERE sid='".$rs->fields[4]."'";
						$rsbearer2 = $db->Execute($sqlbearer2) or die( $db->ErrorMsg() );
						if ( $rsbearer2->RecordCount() > 0 ) {
							while ( !$rsbearer2->EOF ) {

								$bearer_cost2 = $rsbearer2->fields[0];

								$rsbearer2->MoveNext();
							}
						}
						else {
							$bearer_cost2 = 0;
						}

						if( $blnn.$tahun_traffic == "032011" ) $telco_share = 0.5;
						elseif( $blnn.$tahun_traffic == "042011" ) $telco_share = 0.5;
						elseif( $blnn.$tahun_traffic == "052011" ) $telco_share = 0.5;
						else $telco_share = $config['share'][$rs->fields[0]];

						$ar_price = explode("-",$rs->fields[1]);
						$price = $ar_price[1] - $bearer_cost2;

						$tel_ok[$hh] += $rs->fields[1];

						$mt_ok[$hh] += $rs->fields[3];
						if( $price == 250 ) $price = 0;

						$gross[$hh] += $rs->fields[3] * $price;
						$net[$hh] += $rs->fields[3] * ($price * $telco_share );

					}
					else {
						$mt_nok[$hh] += $rs->fields[3];
					}

					$total_mt[$hh] += $rs->fields[3];

					$rs->MoveNext();
				}
			}
			////
		}
		//Today Traffic

	}

	$tel += $tel_ok[$hh];

	$mo_tot += $mo[$hh];
	$mt_ok_tot += $mt_ok[$hh];
	$mt_nok_tot += $mt_nok[$hh];
	$total_mt_tot += $total_mt[$hh];
	$gross_tot += $gross[$hh];
	$net_tot += $net[$hh];

	$hh++;
}

// echo $mt_ok_tot.'-'.$telcooo.'-'.$gross_tot;

$smarty->assign("mo",$mo);
$smarty->assign("mt_ok",$mt_ok);
$smarty->assign("mt_nok",$mt_nok);
$smarty->assign("total_mt",$total_mt);
$smarty->assign("gross",$gross);
$smarty->assign("net",$net);
$smarty->assign("tgl_print",$tgl_print);
$smarty->assign("mo_tot",$mo_tot);
$smarty->assign("mt_ok_tot",$mt_ok_tot);
$smarty->assign("mt_nok_tot",$mt_nok_tot);
$smarty->assign("total_mt_tot",$total_mt_tot);
$smarty->assign("gross_tot",$gross_tot);
$smarty->assign("net_tot",$net_tot);

$smarty->display(TEMPLATES_DIR.'monthly_traffic.html');

?>
