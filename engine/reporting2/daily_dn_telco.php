<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}
//echo "satu";
$smarty->assign("is_today","tdk");
$smarty->assign("subject_detail","tdk");

if( empty($_GET['submit']) ) {
	$_GET['submit'] = '';
	$h = 1;
	$h2 = 1;
	$tgl_now = date("d");
	$j_hari = $mFunc->get_jumlah_hari(date("m"));
}
else {
	$j_hari = $mFunc->get_jumlah_hari($_GET['bln']);
	if( $_GET['bln'] == date("m") ) {
		$h = 1;
		$h2 = 1;
		$tgl_now = date("d");
	}
	else {
		$h = 0;
		$h2 = 1;
		$tgl_now = $mFunc->get_jumlah_hari($_GET['bln']);
	}
}

if( empty($_SERVER['QUERY_STRING']) ) $smarty->assign("php_query_string","");
else $smarty->assign("php_query_string","?".$_SERVER['QUERY_STRING']);

$smarty->assign("submit",$_GET['submit']);
$fix_where = '';
if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
$jc = 1; $jj = 1;
$checkbox = "<table width='100%'  border='1' cellpadding='0' cellspacing='0'><tr>";
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		if( empty($_GET['submit']) ) $checked = "checked";
		else {
			if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
			else $checked = "";
		}

		$checkbox .= "<td><input type='checkbox' name='check_list".$jj."' id='check_list".$jj."' value='".$rs->fields[0]."' $checked>".$rs->fields[1]."</td>";
		if( $jc == 5 ) {
			$checkbox .= '</tr>';
			$jc = 0;
		}
		$jc++;
		$jj++;
		$rs->MoveNext();
	}
}
$checkbox .= "<tr>
				<input type='button' name='Check_All' value='Check All' onClick='CheckAll()'>
				<input type='button' name='Un_CheckAll' value='Uncheck All' onClick='UnCheckAll()'>
			</tr></table>";
$smarty->assign("checkbox",$checkbox);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("bln",$bulan);
$smarty->assign("thn",$tahun);

$smarty->assign("bln_now",date("m"));
$smarty->assign("thn_now",date("Y"));

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$bulan_traffic = date("m");
	$tahun_traffic = date("Y");
	$smarty->assign("s_name","All");
	$str_service = "";
}
else {
	$bulan_traffic = $_GET['bln'];
	$tahun_traffic = $_GET['thn'];
	$fix_where = '';
	$j_check = 0;

	$service_in = ""; $is_check = false; $s_name = "";
	for($r=1;$r<=$jj;$r++) {
		if( isset($_GET['check_list'.$r]) ) {
			$j_check += 1;
			$j_check_r = $r;
			$service_in .= "'".$_GET['check_list'.$r]."',";
			$is_check = true;
			$s_name .= $mFunc->get_description($db, $_GET['check_list'.$r]).", ";
		}
	}
	$service_in = ereg_replace(',$','',$service_in);

	if( $is_check ) $smarty->assign("s_name",$s_name);
	else $smarty->assign("s_name","All");

	if( $service_in <> "" ) $str_service = "AND appid in ( ".$service_in." )";
	else $str_service = "AND appid = '0000'";

}
$smarty->assign("service",$_GET['service']);

$smarty->assign("bulan_traffic",$bulan_traffic);
$smarty->assign("tahun_traffic",$tahun_traffic);

$mo_tot = 0; $mt_ok_tot = 0; $mt_nok_tot = 0; $total_mt_tot = 0; $gross_tot = 0; $net_tot = 0; $persentage_tot = 0; $mo_reg_tot = 0; $mo_unreg_tot = 0;

for($i=$h;$i<=$tgl_now;$i++) {
	if($h == 1) $k = $i - 1;
	else $k = $i;
	$hasil = $tgl_now - $i;
	if( $hasil > 0 ) {
		if( strlen($hasil) == 1) $tgl_traffic = "0".$hasil;
		else $tgl_traffic = $hasil;

		$tgl_print[$k] = $hasil;

		$mo_regtot[$k] = 0;
		$sql_regtot = "SELECT sum( jumlah ) AS jmltot
								FROM mp_system.dn_telco_summary_".$tahun_traffic.$bulan_traffic."
								WHERE 1 AND charge not in ('PULL-0','PULL-REG','PULL-UNREG')
								AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
								//echo $sql_reg."<br/>";
		$rs_regtot = $db->Execute($sql_regtot) or die( $db->ErrorMsg() );
		if ( $rs_regtot->RecordCount() > 0 ) {
			while ( !$rs_regtot->EOF ) {
				$mo_regtot[$k] += $rs_regtot->fields[0];
				$rs_regtot->MoveNext();
			}
		}

		//
		$mo_reg[$k] = "<table border=1>
											<tr>
												<td>DR</td><td>JML</td><td>%</td>
											</tr>";
		$sql_reg = "SELECT dn_telco, sum( jumlah ) AS jml
								FROM mp_system.dn_telco_summary_".$tahun_traffic.$bulan_traffic."
								WHERE 1 AND charge not in ('PULL-0','PULL-REG','PULL-UNREG')
								AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'
								GROUP BY dn_telco";
								//echo $sql_reg."<br/>";
		$rs_reg = $db->Execute($sql_reg) or die( $db->ErrorMsg() );
		if ( $rs_reg->RecordCount() > 0 ) {
			while ( !$rs_reg->EOF ) {
				$persen[$k] = ($rs_reg->fields[1] / $mo_regtot[$k]) * 100;
				$mo_reg[$k] .= "<tr>
													<td>".$rs_reg->fields[0]."</td><td>".$rs_reg->fields[1]."</td><td>".$persen[$k]."</td>
												</tr>";
				$rs_reg->MoveNext();
			}
		}
		$mo_reg[$k] .= "<tr>
			<td>TOT</td><td>".$mo_regtot[$k]."</td><td></td></table>";
//echo $mo_reg[$k]."<br/>";
	}
}

///
if( $_GET['bln'] == date("m") || !isset($_GET['bln']) ) {
	//
	$smarty->assign("is_today","ya");
	$tgl_printt = date("j");
	$tgl_printte = date("d");
	$smarty->assign("tgl_printte",$tgl_printte);

	//
	$mo_regt = "<table border=1>
										<tr>
											<td>DR</td><td>JML</td>
										</tr>";

	$sql_regt = "SELECT dn_telco, sum( jumlah ) AS jml
							FROM mp_system.dn_telco_summary_".$tahun_traffic.$bulan_traffic."
							WHERE 1
							AND datetime = '".date("Y-m-d")."'
							GROUP BY dn_telco";
	//echo $sql_regt."<BR>";
	$rs_regt = $db->Execute($sql_regt) or die( $db->ErrorMsg() );
	if ( $rs_regt->RecordCount() > 0 ) {
		while ( !$rs_regt->EOF ) {
			$mo_regt .= "<tr>
												<td>".$rs_regt->fields[0]."</td><td>".$rs_regt->fields[1]."</td>
											</tr>";
			$rs_regt->MoveNext();
		}
	}
	$mo_regt .= "</table>";


}
///


$smarty->assign("tgl_print",$tgl_print);
$smarty->assign("tgl_printt",$tgl_printt);
$smarty->assign("mo_reg",$mo_reg);
$smarty->assign("mo_regt",$mo_regt);


$smarty->display(TEMPLATES_DIR.'daily_dn_telco.html');

?>
