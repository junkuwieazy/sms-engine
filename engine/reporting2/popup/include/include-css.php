<?php include "../config.php"; ?>
<link rel = "stylesheet" href = "<?php echo base_url(); ?>../dist/bootstrap/css/bootstrap.min.css">
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel = "stylesheet" href = "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<link rel = "stylesheet" href = "<?php echo base_url(); ?>../dist/css/AdminLTE.min.css">
<link rel = "stylesheet" href = "<?php echo base_url(); ?>../dist/css/skins/_all-skins.min.css">

<link rel = "stylesheet" href = "https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">