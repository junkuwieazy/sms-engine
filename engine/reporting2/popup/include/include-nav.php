<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CMS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>CMS</b> SMS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><i class="fa fa-user"></i>  Jati</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <div class="btn btn-default btn-flat">Change password</div>
                            </div>
                            <div class="pull-right">
                                <div class="btn btn-default btn-flat">Signout</div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>User</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="<?php echo base_url(); ?>../user/adduser.php"><i class="fa fa-pencil-square-o"></i> Add New User</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../user/manageuser.php"><i class="fa fa-users"></i> Manage Users</a></li>
                </ul>
            </li>

            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="<?php echo base_url(); ?>../report/application.php"><i class="fa fa-circle-o"></i> Application</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/keyword.php"><i class="fa fa-circle-o"></i> Keyword</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/partnername.php"><i class="fa fa-circle-o"></i> Partner Name</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/molog.php"><i class="fa fa-circle-o"></i> MO Log</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/mtlog.php"><i class="fa fa-circle-o"></i> MT Log</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/dnlog.php"><i class="fa fa-circle-o"></i> DN Log</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/dailymtsms.php"><i class="fa fa-circle-o"></i> Daily MT SMS</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/moreg.php"><i class="fa fa-circle-o"></i> MO REG</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/smssubscriber.php"><i class="fa fa-circle-o"></i> SMS Subscriber</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/dailytraffic.php"><i class="fa fa-circle-o"></i> Daily Traffic</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/monthlytraffic.php"><i class="fa fa-circle-o"></i> Monthly Traffic</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../report/pushsms.php"><i class="fa fa-circle-o"></i> Push SMS</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>CMS Text</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Content Info
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class=""><a href="<?php echo base_url(); ?>../text/addinfo.php"><i class="fa fa-pencil-square-o"></i> Add Content Info</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>../text/manageinfo.php"><i class="fa fa-list"></i> Manage Content Info</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Content Quiz
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class=""><a href="<?php echo base_url(); ?>../text/addquiz.php"><i class="fa fa-pencil-square-o"></i> Add Content Quiz</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>../text/managequiz.php"><i class="fa fa-list"></i> Manage Content Quiz</a></li>
                        </ul>
                    </li>
                    <li class=" treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Content Wap
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class=""><a href="<?php echo base_url(); ?>../text/addwap.php"><i class="fa fa-pencil-square-o"></i> Add Content WAP</a></li>
                            <li class=""><a href="<?php echo base_url(); ?>../text/managewap.php"><i class="fa fa-list"></i> Manage Content WAP</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>CMS Rich Content</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="<?php echo base_url(); ?>../content/content.php"><i class="fa fa-pencil-square-o"></i> Content</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../content/category.php"><i class="fa fa-list"></i> Category</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../content/market.php"><i class="fa fa-barcode"></i> Market Code</a></li>
                </ul>

            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>CMS CS Tools</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="<?php echo base_url(); ?>../cs/managemsisdn.php"><i class="fa fa-mobile"></i> Check MSISDN</a></li>
                    <li class=""><a href="<?php echo base_url(); ?>../cs/blacklistmsisdn.php"><i class="fa fa-list-ol"></i> Blacklist MSISDN</a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>