<?php include "../config.php"; ?>

<!-- jQuery 3.1.1 -->
<script src="<?php echo base_url(); ?>../dist/plugins/jQuery/jquery-3.1.1.min.js"></script>

<script src="<?php echo base_url(); ?>../dist/plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>../dist/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>../dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>../dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>../dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url(); ?>../dist/plugins/iCheck/icheck.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>../dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>../dist/plugins/datepicker/bootstrap-datepicker.js"></script>

