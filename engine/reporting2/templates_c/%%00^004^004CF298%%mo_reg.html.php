<?php /* Smarty version 2.6.18, created on 2017-10-23 16:03:35
         compiled from /var/www/sms/engine/reporting2/templates/mo_reg.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/mo_reg.html', 119, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>MO Reg</title>

<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript">
        <!-- 
        function go_delete(d, e)
        {
            var where_to = confirm("Yakin Anda akan menghapus ini?");
            var i;
            var j;
            i = d;
            j = e;
            if (where_to == true)
                window.location = i + j;
        }
        //-->
    </script>
    '; ?>

    <div class="wrapper">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="content-wrapper" style="min-height:0px;">

            <section class="content-header">
                <h1>Mo REG</h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <form class="form-horizontal" name="add" style="margin-top:25px;" method="get" action="">

                                    <div class="col-md-6">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>TANGGAL</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input name="tgl2" type="text" class="form-control col-md-10" id="a" value="<?php echo $this->_tpl_vars['tgl2']; ?>
" style="width:85%"><input name="reset" type="reset" class="text" onclick="return showCalendar('a', '%Y-%m-%d', '24', true);" value=" ... ">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>SHORTCODE</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_shortcode']; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>TELCO</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_telco']; ?>

                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                        <label>SERVICE</label>
                                                </div>
                                                <div class="col-md-4">
                                                        <?php echo $this->_tpl_vars['dd_service']; ?>

                                                </div>
                                                <div class="col-md-3">
                                                    <button style="width:100%" type="submit" name="submit" value="Submit" class="btn btn-success">Submit</button>
                                                    <input type="hidden" name="active" value="report">
                                                    <input type="hidden" name="menu" value="MO REG"> 
                                                </div>						
                                            </div> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->

                            <div class="col-md-12">
                                <strong style="color: #4183D7">MO STAT : [ Tanggal : <?php echo $this->_tpl_vars['tgl2']; ?>
 ] [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ] [ Service : <?php echo $this->_tpl_vars['s_name']; ?>
 ]</strong>
                            </div>

                            <div class="box-body" style="overflow-x:auto">
                                <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">JAM</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">MO REG</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">MO UNREG</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">MO TOTAL</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php unset($this->_sections['p']);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['loop'] = is_array($_loop=$this->_tpl_vars['i']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['show'] = true;
$this->_sections['p']['max'] = $this->_sections['p']['loop'];
$this->_sections['p']['step'] = 1;
$this->_sections['p']['start'] = $this->_sections['p']['step'] > 0 ? 0 : $this->_sections['p']['loop']-1;
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = $this->_sections['p']['loop'];
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['fix_jam'][$this->_sections['p']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['t_reg'][$this->_sections['p']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['t_unreg'][$this->_sections['p']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_reg_unreg'][$this->_sections['p']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                        <tr>
                                            <td style="background-color: rgba(34, 167, 240,0.8);color: #fff;text-align: center;">Total</td>
                                            <td style="background-color: rgba(34, 167, 240,0.8);color: #fff;text-align: center;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_reg'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color: rgba(34, 167, 240,0.8);color: #fff;text-align: center;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_unreg'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color: rgba(34, 167, 240,0.8);color: #fff;text-align: center;"><?php echo ((is_array($_tmp=$this->_tpl_vars['tot_total_reg_unreg'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </section>



        </div> 
        <footer class="main-footer">
            <?php echo $this->_tpl_vars['copyright']; ?>

        </footer>
    </div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>