<?php /* Smarty version 2.6.18, created on 2017-10-19 22:45:41
         compiled from /var/www/sms/engine/reporting2/templates/subscriber.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/subscriber.html', 87, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>SMS Subscriber</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
         var where_to= confirm("Yakin Anda akan menghapus ini?");
         var i;
         var j;
         i = d;
         j = e;
         if (where_to== true)
           window.location=i + j;
   } 
//-->
</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>SMS Subscriber</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <form class="form-horizontal" action="" method="GET">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3">SHORTCODE</label>
                                        <div class="col-md-8">
                                            <?php echo $this->_tpl_vars['dd_shortcode']; ?>
                                                
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3">TELCO</label>
                                        <div class="col-md-8">
                                            <?php echo $this->_tpl_vars['dd_telco']; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="col-md-5 col-md-offset-3">
                                        <button type="submit" class="btn btn-success" name="app_add"><i class="fa fa-search"></i> Search</button>
                                        <input type="hidden" name="active" value="report">
                                        <input type="hidden" name="menu" value="SMS Subscriber">
                                    </div>
                                </div>                                     
                            </form>
                        </div>
			<div class="col-md-12">
                        	<strong style="color: #4183D7">SUBSCRIBER : [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ]</strong>
			</div>
                        <div class="box-body" style="overflow-x:auto">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">NO</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">SERVICE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">DESCRIPTION</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">MEMBER AKTIF</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">MEMBER NON AKTIF</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">TOTAL MEMBER</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['no']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                    <tr>
                                        <td style="text-align:center"><?php echo $this->_tpl_vars['no'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center"><?php echo $this->_tpl_vars['appids'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center"><?php echo $this->_tpl_vars['descs'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center"><?php echo ((is_array($_tmp=$this->_tpl_vars['aktifs'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="text-align:center"><?php echo ((is_array($_tmp=$this->_tpl_vars['non_aktifs'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="text-align:center"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_members'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                    </tr>
                                    <?php endfor; endif; ?>

                                    <tr>
                                        <td style="text-align: center;background-color: #4183D7;color: #fff;" colspan="3">TOTAL</td>
                                        <td style="text-align: center;background-color: #4183D7;color: #fff;"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total_reg_aktif'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                        <td style="text-align: center;background-color: #4183D7;color: #fff;"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total_reg_nonaktif'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                        <td style="text-align: center;background-color: #4183D7;color: #fff;"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                                <!--                                <div class="box-body">
                                    <p style="color:#446CB3">
                                        SUBSCRIBER : [ SHORTCODE : All ] [ TELCO : All ]
                                    </p>
                                </div>-->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    <!-- /.row -->

                </section>
                <!-- /.content -->


            </div> 
            <footer class="main-footer">
                <?php echo $this->_tpl_vars['copyright']; ?>

            </footer>
        </div>

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>