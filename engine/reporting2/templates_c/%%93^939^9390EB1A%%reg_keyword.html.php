<?php /* Smarty version 2.6.18, created on 2017-10-23 15:47:51
         compiled from /var/www/sms/engine/reporting2/templates/reg_keyword.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Reg Keyword</title>
<?php echo '
<script type="text/javascript" language="JavaScript"> 
    <!-- 
    function go_delete(d,e) 
    { 
       var where_to= confirm("Yakin Anda akan menghapus ini?");
       var i;
       var j;
       i = d;
       j = e;
       if (where_to== true)
         window.location=i + j;
 } 
//-->
</script>
'; ?>


<body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="content-wrapper" style="min-height:0px;">

            <section class="content-header">
                <h1>Keyword REG</h1>
            </section>

            <!-- Main content -->
            <section class="content" style="min-height:0px">
                <div class="row">
                    <div class="col-md-12">
                        <!-- AREA CHART -->
                        <div class="box box-primary">
                            <div class="box-body">

                                <div class="col-md-6">
                                    <form class="form-horizontal" name="filter" action="" method="GET">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button name="searchbtn" class="btn btn-warning" type="submit">
                                                    <i class="fa fa-search" style="margin-right:5px;"></i>Search
                                                </button>
                                            </div>
                                            <input type="text" class="form-control" name="filter" value="<?php echo $this->_tpl_vars['filter']; ?>
" placeholder="Type what you want to search">
                                            <input type="hidden" name="active" value="report">
                                            <input type="hidden" name="menu" value="Reg Keyword">
                                        </div>
                                    </form>
                                </div>
                                <b style="color:red;"><?php echo $this->_tpl_vars['msg']; ?>
</b>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-body" style="overflow-x:auto">
                                <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">KEYWORD</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">TELCO</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">SHORTCODE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">APPID</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">ENTRY DATE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">EDIT</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">DELETE</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['appid']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['keyword'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['telco'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['shortcode'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['datetime'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;">
                                                <a href="reg_keyword.php?action=edit&id=<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
&active=report&menu=Reg Keyword"><i class="fa fa-pencil-square-o"></i><small>Edit</small></a>
                                            </td>
                                            <td style="text-align:center;">
                                                <a href="javascript:go_delete('reg_keyword.php?action=delete&id=',<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
)"><i class="fa fa-trash"></i><small>Delete</small></a>
                                            </td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                    </tbody>
                                </table>

                                <?php if ($this->_tpl_vars['numrows'] != ""): ?>
                                <table width="85%" border="0" cellpadding="5" cellspacing="0" style="margin-bottom: 30px;">
                                   <tr class="text">
                                      <td align="left" valign="top">
                                         <a href="reg_keyword.php?begin=<?php echo $this->_tpl_vars['first']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="first" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_firstpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                                         <a href="reg_keyword.php?begin=<?php echo $this->_tpl_vars['prev']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="prev" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_prevpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="prev" /></a><?php echo $this->_tpl_vars['dispfrom']; ?>
 - <?php echo $this->_tpl_vars['dispto']; ?>
 / <?php echo $this->_tpl_vars['numrows']; ?>

                                         <a href="reg_keyword.php?begin=<?php echo $this->_tpl_vars['next']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="next" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_nextpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="next" /></a>
                                         <a href="reg_keyword.php?begin=<?php echo $this->_tpl_vars['last']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="last" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_lastpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                                     </td>
                                     <td align="right" class="text">Total Data: <?php echo $this->_tpl_vars['numrows']; ?>
</td>
                                 </tr>
                             </table>
                             <?php endif; ?>
                         </div>
                     </div>
                 </div>
             </div>
         </section>

         <!-- Main content -->
         <section class="content" style="min-height:0px">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="box-header with-border">
                                <h3 class="box-title">Add Keyword REG</h3>
                            </div>
                            <form class="form-horizontal" style="margin-top:25px;" method="post" action="">

                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>Keyword</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="keyword" class="form-control"  placeholder="KEYWORD" value="<?php echo $this->_tpl_vars['keyword_edit']; ?>
"">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>Telco</label>
                                            </div>
                                            <div class="col-md-7">
                                                <?php echo $this->_tpl_vars['dd_telco']; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-5">
                                                <label>App ID</label>
                                            </div>
                                            <div class="col-md-7">
                                                <?php echo $this->_tpl_vars['dd_service']; ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                      <div class="form-group">
                                       <div class="col-md-5">
                                          <label>Shortcode</label>
                                      </div>
                                      <div class="col-md-4">
                                          <?php echo $this->_tpl_vars['dd_shortcode']; ?>

                                      </div>
                                      <div class="col-md-3">
                                        <button style="width:100%" name="app_add" value="Submit" type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Submit</button>
                                         <input type="hidden" name="action" value="<?php echo $this->_tpl_vars['action']; ?>
" class="text">
                                         <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id_edit']; ?>
" class="text"> 
                                     </div>						
                                 </div> 
                             </div>
                         </div>
                     </form>
                 </div>
                 <!-- /.box-body -->
             </div>
         </div>
     </div>
     <!-- /.row -->

 </section>
 <!-- /.content --> 


</div> 
<footer class="main-footer">
    <?php echo $this->_tpl_vars['copyright']; ?>

</footer>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>