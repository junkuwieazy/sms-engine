<?php /* Smarty version 2.6.18, created on 2017-10-23 14:47:24
         compiled from /var/www/sms/engine/reporting2/templates/contents_text.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Content Text</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript">
        function submit_form()
        {
            document.frmView.submit()
        }

        function go_delete(i,j) 
					{ 
					 var where_to= confirm("Yakin Anda akan menghapus content ini?");
					 var a;
					 var b;
					 a = i;
					 b = j;

					 if (where_to== true)
					   window.location= "delete_content.php?id=" + a + "&action=delete&appid=" + b;
					}
    </script>
    '; ?>

    <div class="wrapper">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="content-wrapper" style="min-height:0px;">

            <section class="content-header">
                <h1>Content Text</h1> 
                <b style="color:red"><?php echo $this->_tpl_vars['msg']; ?>
</b>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- AREA CHART -->
                        <div class="box box-primary">
                            <div class="box-body">
                                <form class="form-horizontal" name="form1" action="" method="POST">
                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>SERVICE / KEYWORD</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_service']; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>CONTENT</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <textarea class="form-control" name="content" cols="30" rows="3" onKeyDown="textCounter(this.form.content, this.form.remLentext, 160);" onKeyUp="textCounter(this.form.content, this.form.remLentext, 160);"><?php echo $this->_tpl_vars['contente']; ?>
</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>CHARACTER COUNT</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input name="remLentext" type="text" class="form-control" id="remLentext" value="160" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>CATEGORY</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input name="category" type="text" class="form-control" value="<?php echo $this->_tpl_vars['categorye']; ?>
"  placeholder="Category">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>PUBLISH DATE</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input name="publish_date" type="text" class="form-control col-md-10" id="a" value="<?php echo $this->_tpl_vars['publish_datee']; ?>
" style="width:85%" placeholder="DATE"><input name="reset" type="reset" class="text" onclick="return showCalendar('a', '%Y-%m-%d', '24', true);" value=" ... ">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>PUBLISH SEQ</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input name="publish_seq" type="text" class="form-control" value="<?php echo $this->_tpl_vars['publish_seqe']; ?>
"  placeholder="Publish Seq">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['ide']; ?>
">
                                            <input type="submit" name="<?php echo $this->_tpl_vars['smt']; ?>
" value="Submit" class="btn btn-success">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-body" style="overflow-x:auto">
                                <table id="example2" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">SERVICE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">CATEGORY</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">CONTENT</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">PUBLISH DATE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">PUBLISH SEQ</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">ENTRY DATE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">EDIT</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">DELETE</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                        <tr>
                                            <td><?php echo $this->_tpl_vars['desc'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['category'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['content'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['publish_date'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['publish_seq'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['entry_date'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><a href="edit_content.php?active=cms&menu=Content Text&id=<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
&appid=<?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
&action=edit" class="link-page">EDIT</a></td>
                                            <td><a href="javascript:go_delete(<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
,'<?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
')" class="link-page">DELETE</a></td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                    </tbody>
                                </table>

                                <?php if ($this->_tpl_vars['numrows'] != ""): ?>
                                <table width="85%" border="0" cellpadding="5" cellspacing="0" style="margin-bottom: 30px;">
                                    <tr class="text">
                                        <td align="left" valign="top">
                                            <a href="contents_text.php?begin=<?php echo $this->_tpl_vars['first']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="first" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_firstpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                                            <a href="contents_text.php?begin=<?php echo $this->_tpl_vars['prev']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="prev" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_prevpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="prev" /></a><?php echo $this->_tpl_vars['dispfrom']; ?>
 - <?php echo $this->_tpl_vars['dispto']; ?>
 / <?php echo $this->_tpl_vars['numrows']; ?>

                                            <a href="contents_text.php?begin=<?php echo $this->_tpl_vars['next']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="next" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_nextpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="next" /></a>
                                            <a href="contents_text.php?begin=<?php echo $this->_tpl_vars['last']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="last" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_lastpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                                        </td>
                                        <td align="right" class="text">Total Data: <?php echo $this->_tpl_vars['numrows']; ?>
</td>
                                    </tr>
                                </table>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                <!-- /.row -->

            </section>
            <!-- /.content -->


        </div> 
        <footer class="main-footer">
            <?php echo $this->_tpl_vars['copyright']; ?>

        </footer>
    </div>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>