<?php /* Smarty version 2.6.18, created on 2017-10-23 13:54:04
         compiled from /var/www/sms/engine/reporting2/templates/blacklist.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Blacklist MSISDN</title>
<body class="hold-transition skin-blue sidebar-mini">
	<?php echo '
	<script type="text/javascript" language="JavaScript"> 
		<!-- 
		function go_delete(d,e) 
		{ 
			var where_to= confirm("Yakin Anda akan menghapus ini?");
			var i;
			var j;
			i = d;
			j = e;
			if (where_to== true)
				window.location=i + j;
		} 
//-->
</script>
'; ?>

<div class="wrapper">

	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<div class="content-wrapper" style="min-height:0px;">

		<section class="content-header">
			<h1>Blacklist MSISDN</h1>
		</section>

		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<!-- AREA CHART -->
					<div class="box box-primary">
						<div class="box-body">

							<div class="col-md-12">
								<form name="frmS" class="form-horizontal" name="filter" action="" method="GET">
									<div class="input-group">
										<div class="input-group-btn">
											<button name="Scari" value="Cari" class="btn btn-warning" type="submit">
												<i class="fa fa-search" style="margin-right:5px;"></i>Search
											</button>
										</div>
										<input type="hidden" name="active" value="cs">
										<input type="hidden" name="menu" value="Blacklist">
										<input type="text" class="form-control" name="msisdnS" value="<?php echo $this->_tpl_vars['msisdnS']; ?>
" placeholder="Type MSISDN">
									</div>
								</form>
							</div>

							<div class="col-md-12">
								<form name="form1" class="form-horizontal" action="" method="POST">
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-md-3">MSISDN</label>
											<div class="col-md-8">
												<input name="msisdn" class="form-control" type="text" placeholder="Type MSISDN">
											</div>
										</div>
									</div>

									<div class="col-md-5">
										<div class="form-group">
											<label class="col-md-3">TELCO</label>
											<div class="col-md-8">
												<select name='telco' class="form-control">
													<option value='tsel'>Telkomsel</option>
													<option value='xl'>XL</option>
													<option value='isat'>Indosat</option>
													<option value='hutch'>Hutch</option>
												</select>
											</div>
										</div>
									</div>

									<div class="col-md-2">
										<div class="col-md-5 col-md-offset-3">
											<button type="submit" class="btn btn-danger" name="AddBL" value="Add"><i class="fa fa-gear"></i> Blacklist</button>
											<input type="hidden" name="active" value="cs">
											<input type="hidden" name="menu" value="Blacklist">
										</div>
									</div>                                     
								</form>
							</div>
						</div>
						<div class="col-md-12">
							<strong style="color: #4183D7"></strong>
						</div>
						<div class="box-body" style="overflow-x:auto">
							<table id="example2" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th style="text-align: center;background-color: #4183D7;color: #fff;">MSISDN</th>
										<th style="text-align: center;background-color: #4183D7;color: #fff;">TELCO</th>
										<th style="text-align: center;background-color: #4183D7;color: #fff;">ENTRY DATE</th>
										<th style="text-align: center;background-color: #4183D7;color: #fff;">DELETE</th>
									</tr>
								</thead>

								<tbody>
									<?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
									<tr>
										<td style="text-align:center"><?php echo $this->_tpl_vars['msisdn'][$this->_sections['row']['index']]; ?>
</td>
										<td style="text-align:center"><?php echo $this->_tpl_vars['telco'][$this->_sections['row']['index']]; ?>
</td>
										<td style="text-align:center"><?php echo $this->_tpl_vars['entry_date'][$this->_sections['row']['index']]; ?>
</td>
										<td style="text-align:center"><a href="javascript:go_delete(<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
)"><i class="fa fa-trash"></i></a></td>
									</tr>
									<?php endfor; endif; ?>

									<?php echo '
									<script type="text/javascript" language="JavaScript"> 
										<!-- 
										function go_delete(i,j) 
										{ 
											var where_to= confirm("Yakin Anda akan menghapus content ini?");
											var a;
											var b;
											a = i;
											b = j;

											if (where_to== true)
												window.location= "blacklist.php?id=" + a + "&action=delete";
										}
										//-->
									</script>
									'; ?>

								</tbody>
							</table>
							<?php if ($this->_tpl_vars['numrows'] != ""): ?>
							<table width="100%" border="0" cellpadding="5" cellspacing="0" style="margin-bottom: 30px;">
								<tr class="text">
									<td align="left" valign="center" width="60%"><div class="garis">
										<a href="blacklist.php?begin=<?php echo $this->_tpl_vars['first']; ?>
" class="link-page"><strong><<</strong></a>
										<a href="blacklist.php?begin=<?php echo $this->_tpl_vars['prev']; ?>
" class="link-page"><strong>< </strong></a><?php echo $this->_tpl_vars['dispfrom']; ?>
 - <?php echo $this->_tpl_vars['dispto']; ?>
 / <?php echo $this->_tpl_vars['numrows']; ?>
 
										<a href="blacklist.php?begin=<?php echo $this->_tpl_vars['next']; ?>
" class="link-page"><strong>></strong></a>
										<a href="blacklist.php?begin=<?php echo $this->_tpl_vars['last']; ?>
" class="link-page"><strong>>></strong></a> </div></td>
										<td width="40%" align="right" style="padding-top: 5px; font-weight: bold;" class="text"><div class="garis">Total Data: <?php echo $this->_tpl_vars['numrows']; ?>
</div></td>
									</tr>
								</table>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>

			</section>
			<!-- /.content -->

		</div> 
		<footer class="main-footer">
			<?php echo $this->_tpl_vars['copyright']; ?>

		</footer>
	</div>

	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>