<?php /* Smarty version 2.6.18, created on 2017-10-23 10:49:41
         compiled from /var/www/sms/engine/reporting2/templates/top_service.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/top_service.html', 110, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Top Service</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
           var where_to= confirm("Yakin Anda akan menghapus ini?");
           var i;
           var j;
           i = d;
           j = e;
           if (where_to== true)
             window.location=i + j;
     } 
    //-->
</script>
<script LANGUAGE="JavaScript">
    <!--

    <!-- Begin
    function CheckAll()
    {
        var allInputs = document.getElementsByTagName("input")
        for (var i = 0; i < allInputs.length; i++) {
            if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
                allInputs[i].checked = true;
            } else continue;
        }
    }

    function UnCheckAll()
    {
        var allInputs = document.getElementsByTagName("input")
        for (var i = 0; i < allInputs.length; i++) {
            if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
                allInputs[i].checked = false;
            } else continue;
        }
    }
    // End -->
</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Top Service</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <form class="form-horizontal" action="" method="GET">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3">SHORTCODE</label>
                                        <div class="col-md-8">
                                            <?php echo $this->_tpl_vars['dd_shortcode']; ?>
                                                
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-md-3">TELCO</label>
                                        <div class="col-md-8">
                                            <?php echo $this->_tpl_vars['dd_telco']; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="col-md-5 col-md-offset-3">
                                        <button type="submit" class="btn btn-success" name="app_add"><i class="fa fa-search"></i> Search</button>
                                        <input type="hidden" name="active" value="report">
                                        <input type="hidden" name="menu" value="Top Service">
                                    </div>
                                </div>                                     
                            </form>
                        </div>
                        <div class="col-md-12">
                            <strong style="color: #4183D7">TOP 10 SERVICE : [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ]</strong>
                        </div>
                        <div class="box-body" style="overflow-x:auto">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">NO</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">APP ID</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">DESCRIPTION</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">TOTAL NETT</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php unset($this->_sections['p']);
$this->_sections['p']['name'] = 'p';
$this->_sections['p']['loop'] = is_array($_loop=$this->_tpl_vars['appid']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['p']['show'] = true;
$this->_sections['p']['max'] = $this->_sections['p']['loop'];
$this->_sections['p']['step'] = 1;
$this->_sections['p']['start'] = $this->_sections['p']['step'] > 0 ? 0 : $this->_sections['p']['loop']-1;
if ($this->_sections['p']['show']) {
    $this->_sections['p']['total'] = $this->_sections['p']['loop'];
    if ($this->_sections['p']['total'] == 0)
        $this->_sections['p']['show'] = false;
} else
    $this->_sections['p']['total'] = 0;
if ($this->_sections['p']['show']):

            for ($this->_sections['p']['index'] = $this->_sections['p']['start'], $this->_sections['p']['iteration'] = 1;
                 $this->_sections['p']['iteration'] <= $this->_sections['p']['total'];
                 $this->_sections['p']['index'] += $this->_sections['p']['step'], $this->_sections['p']['iteration']++):
$this->_sections['p']['rownum'] = $this->_sections['p']['iteration'];
$this->_sections['p']['index_prev'] = $this->_sections['p']['index'] - $this->_sections['p']['step'];
$this->_sections['p']['index_next'] = $this->_sections['p']['index'] + $this->_sections['p']['step'];
$this->_sections['p']['first']      = ($this->_sections['p']['iteration'] == 1);
$this->_sections['p']['last']       = ($this->_sections['p']['iteration'] == $this->_sections['p']['total']);
?>
                                    <tr>
                                        <td style="text-align:center"><?php echo $this->_tpl_vars['no'][$this->_sections['p']['index']]; ?>
</td>
                                        <td style="text-align:center"><?php echo $this->_tpl_vars['appid'][$this->_sections['p']['index']]; ?>
</td>
                                        <td style="text-align:center"><?php echo $this->_tpl_vars['desc'][$this->_sections['p']['index']]; ?>
</td>
                                        <td style="text-align:center"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['nett'][$this->_sections['p']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                    </tr>
                                    <?php endfor; endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box --> 
                </div>
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->


    </div> 
    <footer class="main-footer">
        <?php echo $this->_tpl_vars['copyright']; ?>

    </footer>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>