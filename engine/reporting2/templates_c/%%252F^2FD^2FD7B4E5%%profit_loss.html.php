<?php /* Smarty version 2.6.18, created on 2017-10-23 10:49:57
         compiled from /var/www/sms/engine/reporting2/templates/profit_loss.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/profit_loss.html', 125, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Profit Loss</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
         var where_to= confirm("Yakin Anda akan menghapus ini?");
         var i;
         var j;
         i = d;
         j = e;
         if (where_to== true)
           window.location=i + j;
   } 
    //-->
</script>
<script LANGUAGE="JavaScript">
    <!--

    <!-- Begin
    function CheckAll()
    {
        var allInputs = document.getElementsByTagName("input")
        for (var i = 0; i < allInputs.length; i++) {
            if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
                allInputs[i].checked = true;
            } else continue;
        }
    }

    function UnCheckAll()
    {
        var allInputs = document.getElementsByTagName("input")
        for (var i = 0; i < allInputs.length; i++) {
            if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
                allInputs[i].checked = false;
            } else continue;
        }
    }
    // End -->
</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Profit Loss</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->

                    <!-- <td width="16%" height="444" valign="top" background="<?php echo $this->_tpl_vars['img_dir']; ?>
leftcollumbkg2.gif"><div align="left">
                        <td height="106" valign="center" background="<?php echo $this->_tpl_vars['img_dir']; ?>
topbarbkg.gif"><div align="center" class="txt-title"><?php echo $this->_tpl_vars['title']; ?>
</td> -->

                            <div class="box box-primary">
                                <div class="box-body">
                                    <form class="form-horizontal" action="" method="GET">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-md-3">SHORTCODE</label>
                                                <div class="col-md-8">
                                                    <?php echo $this->_tpl_vars['dd_shortcode']; ?>
                                                
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-md-3">TELCO</label>
                                                <div class="col-md-8">
                                                    <?php echo $this->_tpl_vars['dd_telco']; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-md-5" style="width: 80px">
                                                    <label>SERVICE</label>
                                                </div>
                                                <div class="col-md-4" style="width: 40%">
                                                    <?php echo $this->_tpl_vars['dd_service']; ?>

                                                </div>
                                                <div class="col-md-3" style="padding-right: 0px; padding-left: 0px">
                                                    <button style="width:100%" type="submit" class="btn btn-success">Submit</button>
                                                    <input type="hidden" name="active" value="report">
                                                    <input type="hidden" name="menu" value="Profit Loss"> 
                                                </div>                      
                                            </div> 
                                        </div>                                    
                                    </form>
                                </div>
                                <div class="col-md-12">
                                    <strong style="color: #4183D7">PROFIT LOSS : [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ] [ Service : <?php echo $this->_tpl_vars['s_name']; ?>
 ]</strong>
                                </div>
                                <div class="box-body" style="overflow-x:auto">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">APPID</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">DESCRIPTION</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">SHORTCODE</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">TELCO</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">TOTAL NET REVENUE</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">TOTAL COST</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">PROFIT/LOSS</th>
                                            </tr>
                                        </thead>
                                        <?php if ($this->_tpl_vars['submit'] != ""): ?>
                                        <tbody>
                                            <tr>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['appid']; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['s_name']; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['sc']; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['tlc']; ?>
</td>
                                                <td style="text-align:center"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total_nett'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                                <td style="text-align:center"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['total_cost'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                                <td style="text-align:center"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['profit_loss'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                            </tr>
                                        </tbody>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                            <!-- /.box --> 
                        </div>
                    </div>
                    <!-- /.row -->

                </section>
                <!-- /.content -->


            </div> 
            <footer class="main-footer">
                <?php echo $this->_tpl_vars['copyright']; ?>

            </footer>
        </div>

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>