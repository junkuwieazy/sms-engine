<?php /* Smarty version 2.6.18, created on 2017-10-24 22:02:03
         compiled from /var/www/sms/engine/reporting2/templates/check_msisdn.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Check MSISDN</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
         var where_to= confirm("Yakin Anda akan menghapus ini?");
         var i;
         var j;
         i = d;
         j = e;
         if (where_to== true)
           window.location=i + j;
   } 
//-->
</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Check MSISDN</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-body">

                            <div class="col-md-6">
                                <form class="form-horizontal" name="filter" action="" method="GET">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button name="Submit" value="Search" class="btn btn-warning" type="submit">
                                                <i class="fa fa-search" style="margin-right:5px;"></i>Search
                                            </button>
                                        </div>
                                        <input type="hidden" name="active" value="cs">
                                        <input type="hidden" name="menu" value="Check MSISDN">
                                        <input type="text" class="form-control" name="msisdn" value="<?php echo $this->_tpl_vars['msisdn']; ?>
" placeholder="Type MSISDN">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-body" style="overflow-x:auto">
                            <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">ACTION</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">SERVICE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">MSISDN</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">SHORTCODE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">DATE REG</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">DATE UNREG</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">STATUS</th>  
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php if ($this->_tpl_vars['smt'] == 'y'): ?>
                                        <?php if ($this->_tpl_vars['data'] == 'FOUND'): ?>
                                            <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                                <form name="frm<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
" method="post" action="">
                                                    <tr>
                                                        <td style="text-align:center;">
                                                            <?php if ($this->_tpl_vars['status'][$this->_sections['row']['index']] == 'Aktif'): ?>
                                                            <input type="hidden" name="msisdn" value="<?php echo $this->_tpl_vars['msisdn2'][$this->_sections['row']['index']]; ?>
" class="text">
                                                            <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
" class="text">
                                                            <input type="submit" name="SubmitU" value="Unreg" class="text">
                                                            <?php endif; ?>
                                                        </td>
                                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
 - <?php echo $this->_tpl_vars['desc'][$this->_sections['row']['index']]; ?>
</td>
                                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['msisdn2'][$this->_sections['row']['index']]; ?>
</td>
                                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['shortcode'][$this->_sections['row']['index']]; ?>
</td>
                                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['date_reg'][$this->_sections['row']['index']]; ?>
</td>
                                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['date_unreg'][$this->_sections['row']['index']]; ?>
</td>
                                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['status'][$this->_sections['row']['index']]; ?>
</td>
                                                    </tr>
                                                </form>
                                            <?php endfor; endif; ?>
                                        <?php else: ?>
                                            <?php echo $this->_tpl_vars['data']; ?>

                                        <?php endif; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div> 
    <footer class="main-footer">
        <?php echo $this->_tpl_vars['copyright']; ?>

    </footer>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>