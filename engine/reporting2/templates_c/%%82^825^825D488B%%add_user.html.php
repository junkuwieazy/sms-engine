<?php /* Smarty version 2.6.18, created on 2017-10-23 10:36:04
         compiled from /var/www/sms/engine/reporting2/templates/add_user.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Application</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 

        function ca(maksudnya) {
            if(maksudnya === 1) {
                var mksdnya = document.getElementsByName(\'app[]\');
            } else {
                var mksdnya = document.getElementsByName(\'telco[]\');
            }
            
            for(var i = 0; i < mksdnya.length; i++) {
                if (mksdnya[i].type == \'checkbox\') {
                   mksdnya[i].checked = true;
               }
           }
       }

       function ua(maksudnya) {
        if(maksudnya === 1) {
            var mksdnya = document.getElementsByName(\'app[]\');
        } else {
            var mksdnya = document.getElementsByName(\'telco[]\');
        }

        for(var i = 0; i < mksdnya.length; i++) {
            if (mksdnya[i].type == \'checkbox\') {
                   mksdnya[i].checked = false;
               }
        }
    }

</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Register New User</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <form class="form-horizontal" method="POST" action="">
                                <div class="box-body">
                                    <strong style="color: red"><?php echo $this->_tpl_vars['msg']; ?>
</strong>
                                    <div class="form-group">
                                        <label class="col-sm-2">Username</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="username" id="inputEmail3" placeholder="Username">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2">Password</label>

                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2">Telco</label>

                                        <div class="col-sm-10">
                                            <?php echo $this->_tpl_vars['checkboxTelco']; ?>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2">App ID</label>

                                        <div class="col-sm-10">
                                            <?php echo $this->_tpl_vars['checkbox']; ?>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2">Accessable Page</label>

                                        <div class="col-sm-10">
                                            <div class="col-md-3">
                                                <label>Page User</label>
                                                <select multiple class="form-control" name="page[]">
                                                    <option value="U_Add User">Add New User</option>
                                                    <option value="U_Manage User">Manage User</option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Page CS Tool</label>
                                                <select multiple class="form-control" name="page[]">
                                                    <option value="T_Blacklist">Blacklist</option>
                                                    <option value="T_Check MSISDN">Check MSISDN</option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Page Report</label>
                                                <select multiple class="form-control" name="page[]">
                                                    <option value="R_Application">Application</option>
                                                    <option value="R_Keyword">Keyword</option>
                                                    <option value="R_Partner Name">Partner Name</option>
                                                    <option value="R_Media Name">Media Name</option>
                                                    <option value="R_MO LOG">MO LOG</option>
                                                    <option value="R_MT LOG">MT LOG</option>
                                                    <option value="R_DN LOG">DN LOG</option>
                                                    <option value="R_Daily MT SMS">Daily MT SMS</option>
                                                    <option value="R_MO REG">MO REG</option>
                                                    <option value="R_SMS Subscriber">SMS Subscriber</option>
                                                    <option value="R_Daily Traffic">Daily Traffic</option>
                                                    <option value="R_Monthly Traffic">Monthly Traffic</option>
                                                    <option value="R_Top Service">Top Service</option>
                                                    <option value="R_Push SMS">Push SMS</option> 
                                                    <option value="R_Profit Loss">Profit Loss</option>
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Page Content</label>
                                                <select multiple class="form-control" name="page[]">
                                                    <option value="C_Content Text">Content Text</option>
                                                    <option value="C_Content Download">Content Download</option>
                                                    <option value="C_Content Quiz">Content Quiz</option> 
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-offset-2">
                                        <button name="userSubmit" type="submit" class="btn btn-success" style="margin-left:5px;">
                                            <i class="fa fa-plus"></i> Register 
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>

            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->


    </div> 
    <footer class="main-footer">
        <?php echo $this->_tpl_vars['copyright']; ?>

    </footer>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>