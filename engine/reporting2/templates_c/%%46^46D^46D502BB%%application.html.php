<?php /* Smarty version 2.6.18, created on 2017-11-28 16:42:10
         compiled from /home/web/orion/engine/reporting2/lib/../../reporting2/templates/application.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Application</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
         var where_to= confirm("Yakin Anda akan menghapus ini?");
         var i;
         var j;
         i = d;
         j = e;
         if (where_to== true)
           window.location=i + j;
        }

        $(document).ready(function () {
            if ($(\'#is_retry\').val() === \'0\') {
                $(\'#retry_time\').attr(\'readonly\', true);
                $(\'#retry_type\').attr(\'readonly\', true);
                $(\'#retry_day\').attr(\'readonly\', true);
            } else{
                $(\'#retry_time\').attr(\'readonly\', false);
                $(\'#retry_type\').attr(\'readonly\', false);
                $(\'#retry_day\').attr(\'readonly\', false);
            }
        });


        function retryChange() {
            if ($(\'#is_retry\').val() === \'0\') {
                $(\'#retry_time\').attr(\'readonly\', true);
                $(\'#retry_type\').attr(\'readonly\', true);
                $(\'#retry_day\').attr(\'readonly\', true);
            } else{
                $(\'#retry_time\').attr(\'readonly\', false);
                $(\'#retry_type\').attr(\'readonly\', false);
                $(\'#retry_day\').attr(\'readonly\', false);
            }
        }




        // $(\'#dynamic-content\').empty();
        // $.ajax({
        //     type: \'GET\',
        //     url: "../content/getcategory?q=" + $(\'#typeContent\').val(),
        //     success: function (data) {
        //         $(\'#load\').hide();
        //         $(\'#dynamic-content\').append(data);
        //     },
        //     error: function (data) {
        //         console.log(data);
        //     }
        // });
//-->
    </script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Application</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-body">

                            <div class="col-md-6">
                                <form class="form-horizontal" name="filter" action="" method="GET">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button name="searchbtn" class="btn btn-warning" type="submit">
                                                <i class="fa fa-search" style="margin-right:5px;"></i>Search
                                            </button>
                                        </div>
                                        <input type="hidden" name="active" value="report">
                                        <input type="hidden" name="menu" value="Application">
                                        <input type="text" class="form-control" name="filter" value="<?php echo $this->_tpl_vars['filter']; ?>
" placeholder="Type what you want to search">
                                    </div>
                                </form>
                            </div>
                            <b style="color:red;"><?php echo $this->_tpl_vars['msg']; ?>
</b>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-body" style="overflow-x:auto">
                            <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">ACTION</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">APPID</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">DESCRIPTION</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">PUSH SERVICE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">PUSH TIME</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">PUSH DAY</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">RETRY SERVICE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">RETRY TIME</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">RETRY DAY</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">RETRY TYPE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">PARTNER</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">CONTACT</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">MARKETING</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">PIC</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">ENTRY DATE</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">STATUS</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['appid']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                    <tr>
                                        <td style="text-align:center;">
                                            <a href="application.php?action=edit&id=<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
&active=report&menu=Application"><i class="fa fa-pencil-square-o"></i><small>Edit</small></a>
                                            <br>
                                            <a href="javascript:go_delete('application.php?action=delete&id=',<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
)"><i class="fa fa-trash"></i><small>Delete</small></a>
                                        </td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['description'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['is_push'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['push_time'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['push_day'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['is_retry'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['retry_time'][$this->_sections['row']['index']]; ?>
</td>
                                        <td><?php echo $this->_tpl_vars['retry_day'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['retry_type'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['partner'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['contact'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['marketing'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['pic'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['datetime'][$this->_sections['row']['index']]; ?>
</td>
                                        <td style="text-align:center;"><?php echo $this->_tpl_vars['status'][$this->_sections['row']['index']]; ?>
</td>
                                    </tr>
                                    <?php endfor; endif; ?>
                                </tbody>
                            </table>

                            <?php if ($this->_tpl_vars['numrows'] != ""): ?>
                            <table width="85%" border="0" cellpadding="5" cellspacing="0" style="margin-bottom: 30px;">
                             <tr class="text">
                              <td align="left" valign="top">
                               <a href="application.php?begin=<?php echo $this->_tpl_vars['first']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
&active=report&menu=Application"><img class="first" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_firstpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                               <a href="application.php?begin=<?php echo $this->_tpl_vars['prev']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
&active=report&menu=Application"><img class="prev" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_prevpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="prev" /></a><?php echo $this->_tpl_vars['dispfrom']; ?>
 - <?php echo $this->_tpl_vars['dispto']; ?>
 / <?php echo $this->_tpl_vars['numrows']; ?>

                               <a href="application.php?begin=<?php echo $this->_tpl_vars['next']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
&active=report&menu=Application"><img class="next" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_nextpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="next" /></a>
                               <a href="application.php?begin=<?php echo $this->_tpl_vars['last']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
&active=report&menu=Application"><img class="last" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_lastpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                           </td>
                           <td align="right" class="text">Total Data: <?php echo $this->_tpl_vars['numrows']; ?>
</td>
                       </tr>
                   </table>
                   <?php endif; ?>
               </div>
           </div>
       </div>
   </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Application</h3>
                    </div>
                    <form class="form-horizontal" style="margin-top:25px;" method="post" action="">

                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Description</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" name="desc" class="form-control"  placeholder="DESCRIPTION" value="<?php echo $this->_tpl_vars['description_edit']; ?>
">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Push Service</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_is_push']; ?>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Push Hour</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_push_time']; ?>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Push Day</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="push_day" placeholder="wed,tue... or date" value="<?php echo $this->_tpl_vars['push_day_edit']; ?>
">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Partner</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_partner']; ?>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Contact</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" name="contact" value="<?php echo $this->_tpl_vars['contact_edit']; ?>
" class="form-control"  placeholder="CONTACT">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Marketing</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" name="marketing" class="form-control" value="<?php echo $this->_tpl_vars['marketing_edit']; ?>
"  placeholder="MARKETING">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>PIC</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" name="pic" class="form-control" value="<?php echo $this->_tpl_vars['pic_edit']; ?>
"  placeholder="PIC">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Retry Service</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_is_retry']; ?>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Retry Time</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_retry_time']; ?>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Retry Type</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_retry_type']; ?>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Retry Day</label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" id='retry_day' name="retry_day" class="form-control" value="<?php echo $this->_tpl_vars['retry_day_edit']; ?>
"  placeholder="mon,wed... or date">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label>Status</label>
                                    </div>
                                    <div class="col-md-7">
                                        <?php echo $this->_tpl_vars['dd_status']; ?>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" name="app_add" class="btn btn-success">
                                    <i class="fa fa-plus"></i> Submit</button>
                                    <input type="hidden" name="action" value="<?php echo $this->_tpl_vars['action']; ?>
" class="text">
                                    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id_edit']; ?>
" class="text">    
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->


</div> 
<footer class="main-footer">
    <?php echo $this->_tpl_vars['copyright']; ?>

</footer>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>