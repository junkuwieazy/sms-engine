<?php /* Smarty version 2.6.18, created on 2017-10-19 15:30:48
         compiled from /var/www/sms/engine/reporting2/templates/mo_log.html */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>MO Log</title>

<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript">
        <!-- 
        function go_delete(d, e)
        {
            var where_to = confirm("Yakin Anda akan menghapus ini?");
            var i;
            var j;
            i = d;
            j = e;
            if (where_to == true)
                window.location = i + j;
        }
        //-->
    </script>
    '; ?>

    <div class="wrapper">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="content-wrapper" style="min-height:0px;">

            <section class="content-header">
                <h1>Mo Incoming Log</h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <form class="form-horizontal" style="margin-top:25px;" method="get" action="">

                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>MO Origin</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="mo_origin2" class="form-control"  placeholder="Mo Origin" value="<?php echo $this->_tpl_vars['mo_origin2']; ?>
">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>MSISDN</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="msisdn2" class="form-control"  placeholder="MSISDN" value="<?php echo $this->_tpl_vars['msisdn2']; ?>
">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>TELCO</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_telco']; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>SHORTCODE</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="shortcode2" class="form-control"  placeholder="Shortcode" value="<?php echo $this->_tpl_vars['shortcode2']; ?>
">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>TRX ID</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="trx_id2" class="form-control"  placeholder="Trx ID" value="<?php echo $this->_tpl_vars['trx_id2']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>SESSION ID</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="session_id2" class="form-control"  placeholder="Session ID" value="<?php echo $this->_tpl_vars['session_id2']; ?>
">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>DATE</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input name="tgl2" type="text" class="form-control col-md-10" id="a" value="<?php echo $this->_tpl_vars['tgl2']; ?>
" style="width:85%" placeholder="DATE"><input name="reset" type="reset" class="text" onclick="return showCalendar('a', '%Y-%m-%d', '24', true);" value=" ... ">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>LIMIT</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="limit" class="form-control"  placeholder="Limit">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">

                                                <div class="form-group">
                                                    <div class="col-md-5">
                                                        <label>TIME</label>
                                                    </div>
                                                    <div class="col-md-7" style="padding-left:0px">
                                                        <div class="col-md-7">
                                                            <?php echo $this->_tpl_vars['dd_month']; ?>

                                                        </div>
                                                        <div class="col-md-5" style="padding-right:0px">
                                                            <?php echo $this->_tpl_vars['dd_year']; ?>

                                                        </div>
                                                    </div>
                                                </div>

                                        </div>

                                        <div class="col-md-12">
                                            <button type="submit" name="app_add" class="btn btn-success">Submit</button>
					    <input type="hidden" name="active" value="report">
					    <input type="hidden" name="menu" value="MO LOG">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-body" style="overflow-x:auto">
                                <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">MO ORIGIN</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">MSISDN</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">PREPAID</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">SMS</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">TELCO</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">SHORTCODE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">TRX ID</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">TRX DATE</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">SESSION ID</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">DATETIME</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['msisdn']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['mo_origin'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><a href="mt_log.php?msisdn2=<?php echo $this->_tpl_vars['msisdn'][$this->_sections['row']['index']]; ?>
&active=report&menu=MT LOG" class="link-page"><?php echo $this->_tpl_vars['msisdn'][$this->_sections['row']['index']]; ?>
</a></td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['prepaid'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['sms'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['telco'][$this->_sections['row']['index']]; ?>
</td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['shortcode'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['trx_id'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><?php echo $this->_tpl_vars['trx_date'][$this->_sections['row']['index']]; ?>
</td>
                                            <td><a href="mt_log.php?session_id2=<?php echo $this->_tpl_vars['session_id'][$this->_sections['row']['index']]; ?>
&active=report&menu=MT LOG" class="link-page"><?php echo $this->_tpl_vars['session_id'][$this->_sections['row']['index']]; ?>
</a></td>
                                            <td style="text-align:center;"><?php echo $this->_tpl_vars['datetime'][$this->_sections['row']['index']]; ?>
</td>
                                        </tr>
                                        <?php endfor; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </section>



        </div> 
        <footer class="main-footer">
            <?php echo $this->_tpl_vars['copyright']; ?>

        </footer>
    </div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>