<?php /* Smarty version 2.6.18, created on 2017-10-19 13:28:04
         compiled from /var/www/sms/engine/reporting2/templates/daily_traffic.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/daily_traffic.html', 151, false),array('modifier', 'cat', '/var/www/sms/engine/reporting2/templates/daily_traffic.html', 448, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Daily Traffic</title>

<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
<style>
#container {
                max-width: 1000px;
                height: 400px;
                margin: 0 auto;
                margin-top: 35px;
            }
</style>
<script type="text/javascript" language="JavaScript">
<!--
function go_delete(d,e)
{
 var where_to= confirm("Yakin Anda akan menghapus ini?");
 var i;
 var j;
 i = d;
 j = e;
 if (where_to== true)
   window.location=i + j;
}
//-->
</script>
<SCRIPT LANGUAGE="JavaScript">
<!--

<!-- Begin
function CheckAll()
{
	var allInputs = document.getElementsByTagName("input")
	for (var i = 0; i < allInputs.length; i++) {
		if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
			allInputs[i].checked = true;
		} else continue;
	}
}

function UnCheckAll()
{
	var allInputs = document.getElementsByTagName("input")
	for (var i = 0; i < allInputs.length; i++) {
		if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
			allInputs[i].checked = false;
		} else continue;
	}
}
// End -->
</script>

'; ?>

    <div class="wrapper">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="content-wrapper" style="min-height:0px;">

            <section class="content-header">
                <h1>Daily Trafic</h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <form class="form-horizontal" style="margin-top:25px;" method="get" action="">
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>BULAN</label>
                                            </div>
                                            <div class="col-md-5">
                                                <?php echo $this->_tpl_vars['dd_month']; ?>

                                            </div>

                                            <div class="col-md-4">
                                                <?php echo $this->_tpl_vars['dd_year']; ?>

                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>SHORTCODE</label>
                                            </div>
                                            <div class="col-md-9">
                                                <?php echo $this->_tpl_vars['dd_shortcode']; ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label>TELCO</label>
                                            </div>
                                            <div class="col-md-9">
                                                <?php echo $this->_tpl_vars['dd_telco']; ?>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-1">
                                                    <label>SERVICE</label>
                                                </div> 
                                                <div style="margin-left:-10px" class="col-md-11">
                                                    <div class="col-md-12">
                                                        <?php echo $this->_tpl_vars['checkbox']; ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                            
                            <div class="col-md-12">
                                <strong style="color: #4183D7">DAILY TRAFFIC -- : [ Bulan : <?php echo $this->_tpl_vars['bulan_traffic']; ?>
 , Tahun : <?php echo $this->_tpl_vars['tahun_traffic']; ?>
 ] [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ] [ Service : <?php echo $this->_tpl_vars['s_name']; ?>
 ]</strong>
                            </div>

                            <div class="box-body" style="overflow-x:auto">
                                <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">Tanggal</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">Total</th>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">Projection</th>
                                            <!--tanggal-->
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_printt']; ?>
</th>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_print'][$this->_sections['r']['index']]; ?>
</th>
                                            <?php endfor; endif; ?>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <!--MO-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmo']);
$this->_sections['rmo']['name'] = 'rmo';
$this->_sections['rmo']['loop'] = is_array($_loop=$this->_tpl_vars['mo']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo']['show'] = true;
$this->_sections['rmo']['max'] = $this->_sections['rmo']['loop'];
$this->_sections['rmo']['step'] = 1;
$this->_sections['rmo']['start'] = $this->_sections['rmo']['step'] > 0 ? 0 : $this->_sections['rmo']['loop']-1;
if ($this->_sections['rmo']['show']) {
    $this->_sections['rmo']['total'] = $this->_sections['rmo']['loop'];
    if ($this->_sections['rmo']['total'] == 0)
        $this->_sections['rmo']['show'] = false;
} else
    $this->_sections['rmo']['total'] = 0;
if ($this->_sections['rmo']['show']):

            for ($this->_sections['rmo']['index'] = $this->_sections['rmo']['start'], $this->_sections['rmo']['iteration'] = 1;
                 $this->_sections['rmo']['iteration'] <= $this->_sections['rmo']['total'];
                 $this->_sections['rmo']['index'] += $this->_sections['rmo']['step'], $this->_sections['rmo']['iteration']++):
$this->_sections['rmo']['rownum'] = $this->_sections['rmo']['iteration'];
$this->_sections['rmo']['index_prev'] = $this->_sections['rmo']['index'] - $this->_sections['rmo']['step'];
$this->_sections['rmo']['index_next'] = $this->_sections['rmo']['index'] + $this->_sections['rmo']['step'];
$this->_sections['rmo']['first']      = ($this->_sections['rmo']['iteration'] == 1);
$this->_sections['rmo']['last']       = ($this->_sections['rmo']['iteration'] == $this->_sections['rmo']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo'][$this->_sections['rmo']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <!--MO REG-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO REG</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_reg_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_regt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmo_reg']);
$this->_sections['rmo_reg']['name'] = 'rmo_reg';
$this->_sections['rmo_reg']['loop'] = is_array($_loop=$this->_tpl_vars['mo_reg']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo_reg']['show'] = true;
$this->_sections['rmo_reg']['max'] = $this->_sections['rmo_reg']['loop'];
$this->_sections['rmo_reg']['step'] = 1;
$this->_sections['rmo_reg']['start'] = $this->_sections['rmo_reg']['step'] > 0 ? 0 : $this->_sections['rmo_reg']['loop']-1;
if ($this->_sections['rmo_reg']['show']) {
    $this->_sections['rmo_reg']['total'] = $this->_sections['rmo_reg']['loop'];
    if ($this->_sections['rmo_reg']['total'] == 0)
        $this->_sections['rmo_reg']['show'] = false;
} else
    $this->_sections['rmo_reg']['total'] = 0;
if ($this->_sections['rmo_reg']['show']):

            for ($this->_sections['rmo_reg']['index'] = $this->_sections['rmo_reg']['start'], $this->_sections['rmo_reg']['iteration'] = 1;
                 $this->_sections['rmo_reg']['iteration'] <= $this->_sections['rmo_reg']['total'];
                 $this->_sections['rmo_reg']['index'] += $this->_sections['rmo_reg']['step'], $this->_sections['rmo_reg']['iteration']++):
$this->_sections['rmo_reg']['rownum'] = $this->_sections['rmo_reg']['iteration'];
$this->_sections['rmo_reg']['index_prev'] = $this->_sections['rmo_reg']['index'] - $this->_sections['rmo_reg']['step'];
$this->_sections['rmo_reg']['index_next'] = $this->_sections['rmo_reg']['index'] + $this->_sections['rmo_reg']['step'];
$this->_sections['rmo_reg']['first']      = ($this->_sections['rmo_reg']['iteration'] == 1);
$this->_sections['rmo_reg']['last']       = ($this->_sections['rmo_reg']['iteration'] == $this->_sections['rmo_reg']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_reg'][$this->_sections['rmo_reg']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <!--MO UNREG-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO UNREG</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unreg_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unregt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmo_unreg']);
$this->_sections['rmo_unreg']['name'] = 'rmo_unreg';
$this->_sections['rmo_unreg']['loop'] = is_array($_loop=$this->_tpl_vars['mo_unreg']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo_unreg']['show'] = true;
$this->_sections['rmo_unreg']['max'] = $this->_sections['rmo_unreg']['loop'];
$this->_sections['rmo_unreg']['step'] = 1;
$this->_sections['rmo_unreg']['start'] = $this->_sections['rmo_unreg']['step'] > 0 ? 0 : $this->_sections['rmo_unreg']['loop']-1;
if ($this->_sections['rmo_unreg']['show']) {
    $this->_sections['rmo_unreg']['total'] = $this->_sections['rmo_unreg']['loop'];
    if ($this->_sections['rmo_unreg']['total'] == 0)
        $this->_sections['rmo_unreg']['show'] = false;
} else
    $this->_sections['rmo_unreg']['total'] = 0;
if ($this->_sections['rmo_unreg']['show']):

            for ($this->_sections['rmo_unreg']['index'] = $this->_sections['rmo_unreg']['start'], $this->_sections['rmo_unreg']['iteration'] = 1;
                 $this->_sections['rmo_unreg']['iteration'] <= $this->_sections['rmo_unreg']['total'];
                 $this->_sections['rmo_unreg']['index'] += $this->_sections['rmo_unreg']['step'], $this->_sections['rmo_unreg']['iteration']++):
$this->_sections['rmo_unreg']['rownum'] = $this->_sections['rmo_unreg']['iteration'];
$this->_sections['rmo_unreg']['index_prev'] = $this->_sections['rmo_unreg']['index'] - $this->_sections['rmo_unreg']['step'];
$this->_sections['rmo_unreg']['index_next'] = $this->_sections['rmo_unreg']['index'] + $this->_sections['rmo_unreg']['step'];
$this->_sections['rmo_unreg']['first']      = ($this->_sections['rmo_unreg']['iteration'] == 1);
$this->_sections['rmo_unreg']['last']       = ($this->_sections['rmo_unreg']['iteration'] == $this->_sections['rmo_unreg']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unreg'][$this->_sections['rmo_unreg']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <!--MT Delivered Firstpush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Delivered Firstpush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok_totf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_oktf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmt_okf']);
$this->_sections['rmt_okf']['name'] = 'rmt_okf';
$this->_sections['rmt_okf']['loop'] = is_array($_loop=$this->_tpl_vars['mt_okf']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_okf']['show'] = true;
$this->_sections['rmt_okf']['max'] = $this->_sections['rmt_okf']['loop'];
$this->_sections['rmt_okf']['step'] = 1;
$this->_sections['rmt_okf']['start'] = $this->_sections['rmt_okf']['step'] > 0 ? 0 : $this->_sections['rmt_okf']['loop']-1;
if ($this->_sections['rmt_okf']['show']) {
    $this->_sections['rmt_okf']['total'] = $this->_sections['rmt_okf']['loop'];
    if ($this->_sections['rmt_okf']['total'] == 0)
        $this->_sections['rmt_okf']['show'] = false;
} else
    $this->_sections['rmt_okf']['total'] = 0;
if ($this->_sections['rmt_okf']['show']):

            for ($this->_sections['rmt_okf']['index'] = $this->_sections['rmt_okf']['start'], $this->_sections['rmt_okf']['iteration'] = 1;
                 $this->_sections['rmt_okf']['iteration'] <= $this->_sections['rmt_okf']['total'];
                 $this->_sections['rmt_okf']['index'] += $this->_sections['rmt_okf']['step'], $this->_sections['rmt_okf']['iteration']++):
$this->_sections['rmt_okf']['rownum'] = $this->_sections['rmt_okf']['iteration'];
$this->_sections['rmt_okf']['index_prev'] = $this->_sections['rmt_okf']['index'] - $this->_sections['rmt_okf']['step'];
$this->_sections['rmt_okf']['index_next'] = $this->_sections['rmt_okf']['index'] + $this->_sections['rmt_okf']['step'];
$this->_sections['rmt_okf']['first']      = ($this->_sections['rmt_okf']['iteration'] == 1);
$this->_sections['rmt_okf']['last']       = ($this->_sections['rmt_okf']['iteration'] == $this->_sections['rmt_okf']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_okf'][$this->_sections['rmt_okf']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--MT Rejected Firstpush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Rejected Firstpush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok_totf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_noktf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmt_nokf']);
$this->_sections['rmt_nokf']['name'] = 'rmt_nokf';
$this->_sections['rmt_nokf']['loop'] = is_array($_loop=$this->_tpl_vars['mt_nokf']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_nokf']['show'] = true;
$this->_sections['rmt_nokf']['max'] = $this->_sections['rmt_nokf']['loop'];
$this->_sections['rmt_nokf']['step'] = 1;
$this->_sections['rmt_nokf']['start'] = $this->_sections['rmt_nokf']['step'] > 0 ? 0 : $this->_sections['rmt_nokf']['loop']-1;
if ($this->_sections['rmt_nokf']['show']) {
    $this->_sections['rmt_nokf']['total'] = $this->_sections['rmt_nokf']['loop'];
    if ($this->_sections['rmt_nokf']['total'] == 0)
        $this->_sections['rmt_nokf']['show'] = false;
} else
    $this->_sections['rmt_nokf']['total'] = 0;
if ($this->_sections['rmt_nokf']['show']):

            for ($this->_sections['rmt_nokf']['index'] = $this->_sections['rmt_nokf']['start'], $this->_sections['rmt_nokf']['iteration'] = 1;
                 $this->_sections['rmt_nokf']['iteration'] <= $this->_sections['rmt_nokf']['total'];
                 $this->_sections['rmt_nokf']['index'] += $this->_sections['rmt_nokf']['step'], $this->_sections['rmt_nokf']['iteration']++):
$this->_sections['rmt_nokf']['rownum'] = $this->_sections['rmt_nokf']['iteration'];
$this->_sections['rmt_nokf']['index_prev'] = $this->_sections['rmt_nokf']['index'] - $this->_sections['rmt_nokf']['step'];
$this->_sections['rmt_nokf']['index_next'] = $this->_sections['rmt_nokf']['index'] + $this->_sections['rmt_nokf']['step'];
$this->_sections['rmt_nokf']['first']      = ($this->_sections['rmt_nokf']['iteration'] == 1);
$this->_sections['rmt_nokf']['last']       = ($this->_sections['rmt_nokf']['iteration'] == $this->_sections['rmt_nokf']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nokf'][$this->_sections['rmt_nokf']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--MT Total Firstpush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Total Firstpush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt_totf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mttf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rtotal_mtf']);
$this->_sections['rtotal_mtf']['name'] = 'rtotal_mtf';
$this->_sections['rtotal_mtf']['loop'] = is_array($_loop=$this->_tpl_vars['total_mtf']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rtotal_mtf']['show'] = true;
$this->_sections['rtotal_mtf']['max'] = $this->_sections['rtotal_mtf']['loop'];
$this->_sections['rtotal_mtf']['step'] = 1;
$this->_sections['rtotal_mtf']['start'] = $this->_sections['rtotal_mtf']['step'] > 0 ? 0 : $this->_sections['rtotal_mtf']['loop']-1;
if ($this->_sections['rtotal_mtf']['show']) {
    $this->_sections['rtotal_mtf']['total'] = $this->_sections['rtotal_mtf']['loop'];
    if ($this->_sections['rtotal_mtf']['total'] == 0)
        $this->_sections['rtotal_mtf']['show'] = false;
} else
    $this->_sections['rtotal_mtf']['total'] = 0;
if ($this->_sections['rtotal_mtf']['show']):

            for ($this->_sections['rtotal_mtf']['index'] = $this->_sections['rtotal_mtf']['start'], $this->_sections['rtotal_mtf']['iteration'] = 1;
                 $this->_sections['rtotal_mtf']['iteration'] <= $this->_sections['rtotal_mtf']['total'];
                 $this->_sections['rtotal_mtf']['index'] += $this->_sections['rtotal_mtf']['step'], $this->_sections['rtotal_mtf']['iteration']++):
$this->_sections['rtotal_mtf']['rownum'] = $this->_sections['rtotal_mtf']['iteration'];
$this->_sections['rtotal_mtf']['index_prev'] = $this->_sections['rtotal_mtf']['index'] - $this->_sections['rtotal_mtf']['step'];
$this->_sections['rtotal_mtf']['index_next'] = $this->_sections['rtotal_mtf']['index'] + $this->_sections['rtotal_mtf']['step'];
$this->_sections['rtotal_mtf']['first']      = ($this->_sections['rtotal_mtf']['iteration'] == 1);
$this->_sections['rtotal_mtf']['last']       = ($this->_sections['rtotal_mtf']['iteration'] == $this->_sections['rtotal_mtf']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mtf'][$this->_sections['rtotal_mtf']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--Persen Firstpush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Persentage Firstpush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"></td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentagetf'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rpersentagef']);
$this->_sections['rpersentagef']['name'] = 'rpersentagef';
$this->_sections['rpersentagef']['loop'] = is_array($_loop=$this->_tpl_vars['persentagef']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rpersentagef']['show'] = true;
$this->_sections['rpersentagef']['max'] = $this->_sections['rpersentagef']['loop'];
$this->_sections['rpersentagef']['step'] = 1;
$this->_sections['rpersentagef']['start'] = $this->_sections['rpersentagef']['step'] > 0 ? 0 : $this->_sections['rpersentagef']['loop']-1;
if ($this->_sections['rpersentagef']['show']) {
    $this->_sections['rpersentagef']['total'] = $this->_sections['rpersentagef']['loop'];
    if ($this->_sections['rpersentagef']['total'] == 0)
        $this->_sections['rpersentagef']['show'] = false;
} else
    $this->_sections['rpersentagef']['total'] = 0;
if ($this->_sections['rpersentagef']['show']):

            for ($this->_sections['rpersentagef']['index'] = $this->_sections['rpersentagef']['start'], $this->_sections['rpersentagef']['iteration'] = 1;
                 $this->_sections['rpersentagef']['iteration'] <= $this->_sections['rpersentagef']['total'];
                 $this->_sections['rpersentagef']['index'] += $this->_sections['rpersentagef']['step'], $this->_sections['rpersentagef']['iteration']++):
$this->_sections['rpersentagef']['rownum'] = $this->_sections['rpersentagef']['iteration'];
$this->_sections['rpersentagef']['index_prev'] = $this->_sections['rpersentagef']['index'] - $this->_sections['rpersentagef']['step'];
$this->_sections['rpersentagef']['index_next'] = $this->_sections['rpersentagef']['index'] + $this->_sections['rpersentagef']['step'];
$this->_sections['rpersentagef']['first']      = ($this->_sections['rpersentagef']['iteration'] == 1);
$this->_sections['rpersentagef']['last']       = ($this->_sections['rpersentagef']['iteration'] == $this->_sections['rpersentagef']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentagef'][$this->_sections['rpersentagef']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--MT Delivered-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Delivered</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_okt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmt_ok']);
$this->_sections['rmt_ok']['name'] = 'rmt_ok';
$this->_sections['rmt_ok']['loop'] = is_array($_loop=$this->_tpl_vars['mt_ok']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_ok']['show'] = true;
$this->_sections['rmt_ok']['max'] = $this->_sections['rmt_ok']['loop'];
$this->_sections['rmt_ok']['step'] = 1;
$this->_sections['rmt_ok']['start'] = $this->_sections['rmt_ok']['step'] > 0 ? 0 : $this->_sections['rmt_ok']['loop']-1;
if ($this->_sections['rmt_ok']['show']) {
    $this->_sections['rmt_ok']['total'] = $this->_sections['rmt_ok']['loop'];
    if ($this->_sections['rmt_ok']['total'] == 0)
        $this->_sections['rmt_ok']['show'] = false;
} else
    $this->_sections['rmt_ok']['total'] = 0;
if ($this->_sections['rmt_ok']['show']):

            for ($this->_sections['rmt_ok']['index'] = $this->_sections['rmt_ok']['start'], $this->_sections['rmt_ok']['iteration'] = 1;
                 $this->_sections['rmt_ok']['iteration'] <= $this->_sections['rmt_ok']['total'];
                 $this->_sections['rmt_ok']['index'] += $this->_sections['rmt_ok']['step'], $this->_sections['rmt_ok']['iteration']++):
$this->_sections['rmt_ok']['rownum'] = $this->_sections['rmt_ok']['iteration'];
$this->_sections['rmt_ok']['index_prev'] = $this->_sections['rmt_ok']['index'] - $this->_sections['rmt_ok']['step'];
$this->_sections['rmt_ok']['index_next'] = $this->_sections['rmt_ok']['index'] + $this->_sections['rmt_ok']['step'];
$this->_sections['rmt_ok']['first']      = ($this->_sections['rmt_ok']['iteration'] == 1);
$this->_sections['rmt_ok']['last']       = ($this->_sections['rmt_ok']['iteration'] == $this->_sections['rmt_ok']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok'][$this->_sections['rmt_ok']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--MT Rejected-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Rejected</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nokt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rmt_nok']);
$this->_sections['rmt_nok']['name'] = 'rmt_nok';
$this->_sections['rmt_nok']['loop'] = is_array($_loop=$this->_tpl_vars['mt_nok']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_nok']['show'] = true;
$this->_sections['rmt_nok']['max'] = $this->_sections['rmt_nok']['loop'];
$this->_sections['rmt_nok']['step'] = 1;
$this->_sections['rmt_nok']['start'] = $this->_sections['rmt_nok']['step'] > 0 ? 0 : $this->_sections['rmt_nok']['loop']-1;
if ($this->_sections['rmt_nok']['show']) {
    $this->_sections['rmt_nok']['total'] = $this->_sections['rmt_nok']['loop'];
    if ($this->_sections['rmt_nok']['total'] == 0)
        $this->_sections['rmt_nok']['show'] = false;
} else
    $this->_sections['rmt_nok']['total'] = 0;
if ($this->_sections['rmt_nok']['show']):

            for ($this->_sections['rmt_nok']['index'] = $this->_sections['rmt_nok']['start'], $this->_sections['rmt_nok']['iteration'] = 1;
                 $this->_sections['rmt_nok']['iteration'] <= $this->_sections['rmt_nok']['total'];
                 $this->_sections['rmt_nok']['index'] += $this->_sections['rmt_nok']['step'], $this->_sections['rmt_nok']['iteration']++):
$this->_sections['rmt_nok']['rownum'] = $this->_sections['rmt_nok']['iteration'];
$this->_sections['rmt_nok']['index_prev'] = $this->_sections['rmt_nok']['index'] - $this->_sections['rmt_nok']['step'];
$this->_sections['rmt_nok']['index_next'] = $this->_sections['rmt_nok']['index'] + $this->_sections['rmt_nok']['step'];
$this->_sections['rmt_nok']['first']      = ($this->_sections['rmt_nok']['iteration'] == 1);
$this->_sections['rmt_nok']['last']       = ($this->_sections['rmt_nok']['iteration'] == $this->_sections['rmt_nok']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok'][$this->_sections['rmt_nok']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--MT Total-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Total</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><a class='ajax' href="detail_dn_telco.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_printt']; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
" title="Detail DN TELCO"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mtt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rtotal_mt']);
$this->_sections['rtotal_mt']['name'] = 'rtotal_mt';
$this->_sections['rtotal_mt']['loop'] = is_array($_loop=$this->_tpl_vars['total_mt']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rtotal_mt']['show'] = true;
$this->_sections['rtotal_mt']['max'] = $this->_sections['rtotal_mt']['loop'];
$this->_sections['rtotal_mt']['step'] = 1;
$this->_sections['rtotal_mt']['start'] = $this->_sections['rtotal_mt']['step'] > 0 ? 0 : $this->_sections['rtotal_mt']['loop']-1;
if ($this->_sections['rtotal_mt']['show']) {
    $this->_sections['rtotal_mt']['total'] = $this->_sections['rtotal_mt']['loop'];
    if ($this->_sections['rtotal_mt']['total'] == 0)
        $this->_sections['rtotal_mt']['show'] = false;
} else
    $this->_sections['rtotal_mt']['total'] = 0;
if ($this->_sections['rtotal_mt']['show']):

            for ($this->_sections['rtotal_mt']['index'] = $this->_sections['rtotal_mt']['start'], $this->_sections['rtotal_mt']['iteration'] = 1;
                 $this->_sections['rtotal_mt']['iteration'] <= $this->_sections['rtotal_mt']['total'];
                 $this->_sections['rtotal_mt']['index'] += $this->_sections['rtotal_mt']['step'], $this->_sections['rtotal_mt']['iteration']++):
$this->_sections['rtotal_mt']['rownum'] = $this->_sections['rtotal_mt']['iteration'];
$this->_sections['rtotal_mt']['index_prev'] = $this->_sections['rtotal_mt']['index'] - $this->_sections['rtotal_mt']['step'];
$this->_sections['rtotal_mt']['index_next'] = $this->_sections['rtotal_mt']['index'] + $this->_sections['rtotal_mt']['step'];
$this->_sections['rtotal_mt']['first']      = ($this->_sections['rtotal_mt']['iteration'] == 1);
$this->_sections['rtotal_mt']['last']       = ($this->_sections['rtotal_mt']['iteration'] == $this->_sections['rtotal_mt']['total']);
?>
                                                <td style="text-align: right;"><a class='ajax' href="detail_dn_telco.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_print'][$this->_sections['rtotal_mt']['index']]; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
" title="Detail DN TELCO"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt'][$this->_sections['rtotal_mt']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--Persentage Dailypush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Persentage Dailypush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"></td>
                                            <td style="background-color:#646464; color:#fff"></td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentaget'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rpersentage']);
$this->_sections['rpersentage']['name'] = 'rpersentage';
$this->_sections['rpersentage']['loop'] = is_array($_loop=$this->_tpl_vars['persentage']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rpersentage']['show'] = true;
$this->_sections['rpersentage']['max'] = $this->_sections['rpersentage']['loop'];
$this->_sections['rpersentage']['step'] = 1;
$this->_sections['rpersentage']['start'] = $this->_sections['rpersentage']['step'] > 0 ? 0 : $this->_sections['rpersentage']['loop']-1;
if ($this->_sections['rpersentage']['show']) {
    $this->_sections['rpersentage']['total'] = $this->_sections['rpersentage']['loop'];
    if ($this->_sections['rpersentage']['total'] == 0)
        $this->_sections['rpersentage']['show'] = false;
} else
    $this->_sections['rpersentage']['total'] = 0;
if ($this->_sections['rpersentage']['show']):

            for ($this->_sections['rpersentage']['index'] = $this->_sections['rpersentage']['start'], $this->_sections['rpersentage']['iteration'] = 1;
                 $this->_sections['rpersentage']['iteration'] <= $this->_sections['rpersentage']['total'];
                 $this->_sections['rpersentage']['index'] += $this->_sections['rpersentage']['step'], $this->_sections['rpersentage']['iteration']++):
$this->_sections['rpersentage']['rownum'] = $this->_sections['rpersentage']['iteration'];
$this->_sections['rpersentage']['index_prev'] = $this->_sections['rpersentage']['index'] - $this->_sections['rpersentage']['step'];
$this->_sections['rpersentage']['index_next'] = $this->_sections['rpersentage']['index'] + $this->_sections['rpersentage']['step'];
$this->_sections['rpersentage']['first']      = ($this->_sections['rpersentage']['iteration'] == 1);
$this->_sections['rpersentage']['last']       = ($this->_sections['rpersentage']['iteration'] == $this->_sections['rpersentage']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentage'][$this->_sections['rpersentage']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--Gross Rev Firstpush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Gross Rev Firstpush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_totf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_projectionf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['grosstf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rgrossf']);
$this->_sections['rgrossf']['name'] = 'rgrossf';
$this->_sections['rgrossf']['loop'] = is_array($_loop=$this->_tpl_vars['grossf']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rgrossf']['show'] = true;
$this->_sections['rgrossf']['max'] = $this->_sections['rgrossf']['loop'];
$this->_sections['rgrossf']['step'] = 1;
$this->_sections['rgrossf']['start'] = $this->_sections['rgrossf']['step'] > 0 ? 0 : $this->_sections['rgrossf']['loop']-1;
if ($this->_sections['rgrossf']['show']) {
    $this->_sections['rgrossf']['total'] = $this->_sections['rgrossf']['loop'];
    if ($this->_sections['rgrossf']['total'] == 0)
        $this->_sections['rgrossf']['show'] = false;
} else
    $this->_sections['rgrossf']['total'] = 0;
if ($this->_sections['rgrossf']['show']):

            for ($this->_sections['rgrossf']['index'] = $this->_sections['rgrossf']['start'], $this->_sections['rgrossf']['iteration'] = 1;
                 $this->_sections['rgrossf']['iteration'] <= $this->_sections['rgrossf']['total'];
                 $this->_sections['rgrossf']['index'] += $this->_sections['rgrossf']['step'], $this->_sections['rgrossf']['iteration']++):
$this->_sections['rgrossf']['rownum'] = $this->_sections['rgrossf']['iteration'];
$this->_sections['rgrossf']['index_prev'] = $this->_sections['rgrossf']['index'] - $this->_sections['rgrossf']['step'];
$this->_sections['rgrossf']['index_next'] = $this->_sections['rgrossf']['index'] + $this->_sections['rgrossf']['step'];
$this->_sections['rgrossf']['first']      = ($this->_sections['rgrossf']['iteration'] == 1);
$this->_sections['rgrossf']['last']       = ($this->_sections['rgrossf']['iteration'] == $this->_sections['rgrossf']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['grossf'][$this->_sections['rgrossf']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--Net Rev Firstpushh-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Net Rev Firstpush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_totf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_projectionf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['nettf'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rnetf']);
$this->_sections['rnetf']['name'] = 'rnetf';
$this->_sections['rnetf']['loop'] = is_array($_loop=$this->_tpl_vars['netf']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rnetf']['show'] = true;
$this->_sections['rnetf']['max'] = $this->_sections['rnetf']['loop'];
$this->_sections['rnetf']['step'] = 1;
$this->_sections['rnetf']['start'] = $this->_sections['rnetf']['step'] > 0 ? 0 : $this->_sections['rnetf']['loop']-1;
if ($this->_sections['rnetf']['show']) {
    $this->_sections['rnetf']['total'] = $this->_sections['rnetf']['loop'];
    if ($this->_sections['rnetf']['total'] == 0)
        $this->_sections['rnetf']['show'] = false;
} else
    $this->_sections['rnetf']['total'] = 0;
if ($this->_sections['rnetf']['show']):

            for ($this->_sections['rnetf']['index'] = $this->_sections['rnetf']['start'], $this->_sections['rnetf']['iteration'] = 1;
                 $this->_sections['rnetf']['iteration'] <= $this->_sections['rnetf']['total'];
                 $this->_sections['rnetf']['index'] += $this->_sections['rnetf']['step'], $this->_sections['rnetf']['iteration']++):
$this->_sections['rnetf']['rownum'] = $this->_sections['rnetf']['iteration'];
$this->_sections['rnetf']['index_prev'] = $this->_sections['rnetf']['index'] - $this->_sections['rnetf']['step'];
$this->_sections['rnetf']['index_next'] = $this->_sections['rnetf']['index'] + $this->_sections['rnetf']['step'];
$this->_sections['rnetf']['first']      = ($this->_sections['rnetf']['iteration'] == 1);
$this->_sections['rnetf']['last']       = ($this->_sections['rnetf']['iteration'] == $this->_sections['rnetf']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['netf'][$this->_sections['rnetf']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--Gross Rev Dailypush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Gross Rev Dailypush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_projection'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;">
							<?php echo ((is_array($_tmp=$this->_tpl_vars['grosst'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

						</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rgross']);
$this->_sections['rgross']['name'] = 'rgross';
$this->_sections['rgross']['loop'] = is_array($_loop=$this->_tpl_vars['gross']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rgross']['show'] = true;
$this->_sections['rgross']['max'] = $this->_sections['rgross']['loop'];
$this->_sections['rgross']['step'] = 1;
$this->_sections['rgross']['start'] = $this->_sections['rgross']['step'] > 0 ? 0 : $this->_sections['rgross']['loop']-1;
if ($this->_sections['rgross']['show']) {
    $this->_sections['rgross']['total'] = $this->_sections['rgross']['loop'];
    if ($this->_sections['rgross']['total'] == 0)
        $this->_sections['rgross']['show'] = false;
} else
    $this->_sections['rgross']['total'] = 0;
if ($this->_sections['rgross']['show']):

            for ($this->_sections['rgross']['index'] = $this->_sections['rgross']['start'], $this->_sections['rgross']['iteration'] = 1;
                 $this->_sections['rgross']['iteration'] <= $this->_sections['rgross']['total'];
                 $this->_sections['rgross']['index'] += $this->_sections['rgross']['step'], $this->_sections['rgross']['iteration']++):
$this->_sections['rgross']['rownum'] = $this->_sections['rgross']['iteration'];
$this->_sections['rgross']['index_prev'] = $this->_sections['rgross']['index'] - $this->_sections['rgross']['step'];
$this->_sections['rgross']['index_next'] = $this->_sections['rgross']['index'] + $this->_sections['rgross']['step'];
$this->_sections['rgross']['first']      = ($this->_sections['rgross']['iteration'] == 1);
$this->_sections['rgross']['last']       = ($this->_sections['rgross']['iteration'] == $this->_sections['rgross']['total']);
?>
                                                <td style="text-align: right;">
							<?php echo ((is_array($_tmp=$this->_tpl_vars['gross'][$this->_sections['rgross']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

						</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                        <tr>
                                            <!--NET Rev Dailypush-->
                                            <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Net Rev Dailypush</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_projection'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['nett'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endif; ?>
                                            <?php unset($this->_sections['rnet']);
$this->_sections['rnet']['name'] = 'rnet';
$this->_sections['rnet']['loop'] = is_array($_loop=$this->_tpl_vars['net']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rnet']['show'] = true;
$this->_sections['rnet']['max'] = $this->_sections['rnet']['loop'];
$this->_sections['rnet']['step'] = 1;
$this->_sections['rnet']['start'] = $this->_sections['rnet']['step'] > 0 ? 0 : $this->_sections['rnet']['loop']-1;
if ($this->_sections['rnet']['show']) {
    $this->_sections['rnet']['total'] = $this->_sections['rnet']['loop'];
    if ($this->_sections['rnet']['total'] == 0)
        $this->_sections['rnet']['show'] = false;
} else
    $this->_sections['rnet']['total'] = 0;
if ($this->_sections['rnet']['show']):

            for ($this->_sections['rnet']['index'] = $this->_sections['rnet']['start'], $this->_sections['rnet']['iteration'] = 1;
                 $this->_sections['rnet']['iteration'] <= $this->_sections['rnet']['total'];
                 $this->_sections['rnet']['index'] += $this->_sections['rnet']['step'], $this->_sections['rnet']['iteration']++):
$this->_sections['rnet']['rownum'] = $this->_sections['rnet']['iteration'];
$this->_sections['rnet']['index_prev'] = $this->_sections['rnet']['index'] - $this->_sections['rnet']['step'];
$this->_sections['rnet']['index_next'] = $this->_sections['rnet']['index'] + $this->_sections['rnet']['step'];
$this->_sections['rnet']['first']      = ($this->_sections['rnet']['iteration'] == 1);
$this->_sections['rnet']['last']       = ($this->_sections['rnet']['iteration'] == $this->_sections['rnet']['total']);
?>
                                                <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net'][$this->_sections['rnet']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                            <?php endfor; endif; ?>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                            <div id="container" style="padding-bottom:20px;"></div> 
                        </div>
                    </div>
                </div>


            </section>


	
        </div> 
        <footer class="main-footer">
            <?php echo $this->_tpl_vars['copyright']; ?>

        </footer>
    </div>
<?php echo '
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script type="text/javascript" language="JavaScript">
            Highcharts.chart(\'container\', {
                colors: [\'#2b908f\', \'#90ee7e\', \'#f45b5b\', \'#7798BF\', \'#aaeeee\', \'#ff0066\', \'#eeaaee\',
                    \'#55BF3B\', \'#DF5353\', \'#7798BF\', \'#aaeeee\'],
                chart: {
                    backgroundColor: {
                        linearGradient: {x1: 0, y1: 0, x2: 1, y2: 1},
                        stops: [
                            [0, \'#2a2a2b\'],
                            [1, \'#3e3e40\']
                        ]
                    },
                    style: {
                        fontFamily: \'\\\'Unica One\\\', sans-serif\'
                    },
                    plotBorderColor: \'#606063\'
                },
                title: {
                    style: {
                        color: \'#E0E0E3\',
                        textTransform: \'uppercase\',
                        fontSize: \'20px\'
                    },
                    text: \'Grafik Daily Traffic Gross Revenue\'
                },

                subtitle: {
                    text: \'\'
                },

                yAxis: {
                    gridLineColor: \'#707073\',
                    labels: {
                        style: {
                            color: \'#E0E0E3\'
                        }
                    },
                    lineColor: \'#707073\',
                    minorGridLineColor: \'#505053\',
                    tickColor: \'#707073\',
                    tickWidth: 1,
                    title: {
                        style: {
                            color: \'#A0A0A3\'
                        }
                    },
                    title: {
                        text: \'\'
                    }
                },
                legend: {
                    layout: \'vertical\',
                    align: \'right\',
                    verticalAlign: \'middle\'
                },

                xAxis: {
                    gridLineColor: \'#707073\',
                    labels: {
                        style: {
                            color: \'#E0E0E3\'
                        }
                    },
                    lineColor: \'#707073\',
                    minorGridLineColor: \'#505053\',
                    tickColor: \'#707073\',
                    title: {
                        text: \'Tanggal\',
                        style: {
                            color: \'#fff\'

                        }
                    },
                    categories: [
				'; ?>

					<?php echo $this->_tpl_vars['tgl_printt']; ?>
,
                        		<?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
						<?php echo ((is_array($_tmp=$this->_tpl_vars['tgl_print'][$this->_sections['r']['index']])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ',')); ?>

					<?php endfor; endif; ?>
				<?php echo '
				]

                },

                series: [{
                        showInLegend: false,
                        data: [
			'; ?>

				<?php echo $this->_tpl_vars['grosst']; ?>
,
                        	<?php unset($this->_sections['rgross']);
$this->_sections['rgross']['name'] = 'rgross';
$this->_sections['rgross']['loop'] = is_array($_loop=$this->_tpl_vars['gross']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rgross']['show'] = true;
$this->_sections['rgross']['max'] = $this->_sections['rgross']['loop'];
$this->_sections['rgross']['step'] = 1;
$this->_sections['rgross']['start'] = $this->_sections['rgross']['step'] > 0 ? 0 : $this->_sections['rgross']['loop']-1;
if ($this->_sections['rgross']['show']) {
    $this->_sections['rgross']['total'] = $this->_sections['rgross']['loop'];
    if ($this->_sections['rgross']['total'] == 0)
        $this->_sections['rgross']['show'] = false;
} else
    $this->_sections['rgross']['total'] = 0;
if ($this->_sections['rgross']['show']):

            for ($this->_sections['rgross']['index'] = $this->_sections['rgross']['start'], $this->_sections['rgross']['iteration'] = 1;
                 $this->_sections['rgross']['iteration'] <= $this->_sections['rgross']['total'];
                 $this->_sections['rgross']['index'] += $this->_sections['rgross']['step'], $this->_sections['rgross']['iteration']++):
$this->_sections['rgross']['rownum'] = $this->_sections['rgross']['iteration'];
$this->_sections['rgross']['index_prev'] = $this->_sections['rgross']['index'] - $this->_sections['rgross']['step'];
$this->_sections['rgross']['index_next'] = $this->_sections['rgross']['index'] + $this->_sections['rgross']['step'];
$this->_sections['rgross']['first']      = ($this->_sections['rgross']['iteration'] == 1);
$this->_sections['rgross']['last']       = ($this->_sections['rgross']['iteration'] == $this->_sections['rgross']['total']);
?>
					<?php echo ((is_array($_tmp=$this->_tpl_vars['gross'][$this->_sections['rgross']['index']])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ',')); ?>

				<?php endfor; endif; ?>
				
			<?php echo '
                        ]                    
			}]

            });
        </script>
'; ?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>