<?php /* Smarty version 2.6.18, created on 2017-11-29 11:26:55
         compiled from /home/web/orion/engine/reporting2/lib/../../reporting2/templates/daily_traffic.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/home/web/orion/engine/reporting2/lib/../../reporting2/templates/daily_traffic.html', 167, false),array('modifier', 'cat', '/home/web/orion/engine/reporting2/lib/../../reporting2/templates/daily_traffic.html', 537, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Daily Traffic</title>

<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <style>
    #container {
        max-width: 1000px;
        height: 400px;
        margin: 0 auto;
        margin-top: 35px;
    }
</style>
<script type="text/javascript" language="JavaScript">
    <!--
    function go_delete(d,e)
    {
     var where_to= confirm("Yakin Anda akan menghapus ini?");
     var i;
     var j;
     i = d;
     j = e;
     if (where_to== true)
       window.location=i + j;
}
//-->
</script>
<SCRIPT LANGUAGE="JavaScript">
    <!--

    <!-- Begin
    function CheckAll()
    {
     var allInputs = document.getElementsByTagName("input")
     for (var i = 0; i < allInputs.length; i++) {
      if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
       allInputs[i].checked = true;
   } else continue;
}
}

function UnCheckAll()
{
 var allInputs = document.getElementsByTagName("input")
 for (var i = 0; i < allInputs.length; i++) {
  if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
   allInputs[i].checked = false;
} else continue;
}
}

function printReport(area) {
     var printContents = document.getElementById(area).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
// End -->
</script>

'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Daily Trafic</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body">
                            <form class="form-horizontal" style="margin-top:25px;" method="get" action="">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>BULAN</label>
                                        </div>
                                        <div class="col-md-5">
                                            <?php echo $this->_tpl_vars['dd_month']; ?>

                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $this->_tpl_vars['dd_year']; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>SHORTCODE</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?php echo $this->_tpl_vars['dd_shortcode']; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label>TELCO</label>
                                        </div>
                                        <div class="col-md-9">
                                            <?php echo $this->_tpl_vars['dd_telco']; ?>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-1">
                                            <label>SERVICE</label>
                                        </div>
                                        <div style="margin-left:-10px" class="col-md-11">
                                            <div class="col-md-12">
                                                <?php echo $this->_tpl_vars['checkbox']; ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box-body -->

                        <div class="col-md-12">
                            <strong style="color: #4183D7">DAILY TRAFFIC -- : [ Bulan : <?php echo $this->_tpl_vars['bulan_traffic']; ?>
 , Tahun : <?php echo $this->_tpl_vars['tahun_traffic']; ?>
 ] [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ] [ Service : <?php echo $this->_tpl_vars['s_name']; ?>
 ]</strong>

                            <br>
                            <button class="btn btn-danger" onclick="printReport('printreport')">
                                <i class="fa fa-print"></i> Print Report
                            </button>
                        </div>

                        <div class="box-body" style="overflow-x:auto">
                            <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">Tanggal</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">Total</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">Projection</th>
                                        <!--tanggal-->
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_printt']; ?>
</th>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_print'][$this->_sections['r']['index']]; ?>
</th>
                                        <?php endfor; endif; ?>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <!--MO REG-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO REG</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_reg_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_regt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmo_reg']);
$this->_sections['rmo_reg']['name'] = 'rmo_reg';
$this->_sections['rmo_reg']['loop'] = is_array($_loop=$this->_tpl_vars['mo_reg']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo_reg']['show'] = true;
$this->_sections['rmo_reg']['max'] = $this->_sections['rmo_reg']['loop'];
$this->_sections['rmo_reg']['step'] = 1;
$this->_sections['rmo_reg']['start'] = $this->_sections['rmo_reg']['step'] > 0 ? 0 : $this->_sections['rmo_reg']['loop']-1;
if ($this->_sections['rmo_reg']['show']) {
    $this->_sections['rmo_reg']['total'] = $this->_sections['rmo_reg']['loop'];
    if ($this->_sections['rmo_reg']['total'] == 0)
        $this->_sections['rmo_reg']['show'] = false;
} else
    $this->_sections['rmo_reg']['total'] = 0;
if ($this->_sections['rmo_reg']['show']):

            for ($this->_sections['rmo_reg']['index'] = $this->_sections['rmo_reg']['start'], $this->_sections['rmo_reg']['iteration'] = 1;
                 $this->_sections['rmo_reg']['iteration'] <= $this->_sections['rmo_reg']['total'];
                 $this->_sections['rmo_reg']['index'] += $this->_sections['rmo_reg']['step'], $this->_sections['rmo_reg']['iteration']++):
$this->_sections['rmo_reg']['rownum'] = $this->_sections['rmo_reg']['iteration'];
$this->_sections['rmo_reg']['index_prev'] = $this->_sections['rmo_reg']['index'] - $this->_sections['rmo_reg']['step'];
$this->_sections['rmo_reg']['index_next'] = $this->_sections['rmo_reg']['index'] + $this->_sections['rmo_reg']['step'];
$this->_sections['rmo_reg']['first']      = ($this->_sections['rmo_reg']['iteration'] == 1);
$this->_sections['rmo_reg']['last']       = ($this->_sections['rmo_reg']['iteration'] == $this->_sections['rmo_reg']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_reg'][$this->_sections['rmo_reg']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>

                                    </tr>

                                    <tr>
                                        <!--MO UNREG-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO UNREG</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unreg_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unregt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmo_unreg']);
$this->_sections['rmo_unreg']['name'] = 'rmo_unreg';
$this->_sections['rmo_unreg']['loop'] = is_array($_loop=$this->_tpl_vars['mo_unreg']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo_unreg']['show'] = true;
$this->_sections['rmo_unreg']['max'] = $this->_sections['rmo_unreg']['loop'];
$this->_sections['rmo_unreg']['step'] = 1;
$this->_sections['rmo_unreg']['start'] = $this->_sections['rmo_unreg']['step'] > 0 ? 0 : $this->_sections['rmo_unreg']['loop']-1;
if ($this->_sections['rmo_unreg']['show']) {
    $this->_sections['rmo_unreg']['total'] = $this->_sections['rmo_unreg']['loop'];
    if ($this->_sections['rmo_unreg']['total'] == 0)
        $this->_sections['rmo_unreg']['show'] = false;
} else
    $this->_sections['rmo_unreg']['total'] = 0;
if ($this->_sections['rmo_unreg']['show']):

            for ($this->_sections['rmo_unreg']['index'] = $this->_sections['rmo_unreg']['start'], $this->_sections['rmo_unreg']['iteration'] = 1;
                 $this->_sections['rmo_unreg']['iteration'] <= $this->_sections['rmo_unreg']['total'];
                 $this->_sections['rmo_unreg']['index'] += $this->_sections['rmo_unreg']['step'], $this->_sections['rmo_unreg']['iteration']++):
$this->_sections['rmo_unreg']['rownum'] = $this->_sections['rmo_unreg']['iteration'];
$this->_sections['rmo_unreg']['index_prev'] = $this->_sections['rmo_unreg']['index'] - $this->_sections['rmo_unreg']['step'];
$this->_sections['rmo_unreg']['index_next'] = $this->_sections['rmo_unreg']['index'] + $this->_sections['rmo_unreg']['step'];
$this->_sections['rmo_unreg']['first']      = ($this->_sections['rmo_unreg']['iteration'] == 1);
$this->_sections['rmo_unreg']['last']       = ($this->_sections['rmo_unreg']['iteration'] == $this->_sections['rmo_unreg']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unreg'][$this->_sections['rmo_unreg']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>

                                    </tr>

                                    <tr>
                                        <!--MO-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO Total</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmo']);
$this->_sections['rmo']['name'] = 'rmo';
$this->_sections['rmo']['loop'] = is_array($_loop=$this->_tpl_vars['mo']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo']['show'] = true;
$this->_sections['rmo']['max'] = $this->_sections['rmo']['loop'];
$this->_sections['rmo']['step'] = 1;
$this->_sections['rmo']['start'] = $this->_sections['rmo']['step'] > 0 ? 0 : $this->_sections['rmo']['loop']-1;
if ($this->_sections['rmo']['show']) {
    $this->_sections['rmo']['total'] = $this->_sections['rmo']['loop'];
    if ($this->_sections['rmo']['total'] == 0)
        $this->_sections['rmo']['show'] = false;
} else
    $this->_sections['rmo']['total'] = 0;
if ($this->_sections['rmo']['show']):

            for ($this->_sections['rmo']['index'] = $this->_sections['rmo']['start'], $this->_sections['rmo']['iteration'] = 1;
                 $this->_sections['rmo']['iteration'] <= $this->_sections['rmo']['total'];
                 $this->_sections['rmo']['index'] += $this->_sections['rmo']['step'], $this->_sections['rmo']['iteration']++):
$this->_sections['rmo']['rownum'] = $this->_sections['rmo']['iteration'];
$this->_sections['rmo']['index_prev'] = $this->_sections['rmo']['index'] - $this->_sections['rmo']['step'];
$this->_sections['rmo']['index_next'] = $this->_sections['rmo']['index'] + $this->_sections['rmo']['step'];
$this->_sections['rmo']['first']      = ($this->_sections['rmo']['iteration'] == 1);
$this->_sections['rmo']['last']       = ($this->_sections['rmo']['iteration'] == $this->_sections['rmo']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo'][$this->_sections['rmo']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>

                                    </tr>

                                    <tr>
                                        <!--MT Delivered-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Delivered</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_mt_deliv.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_printt']; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail MT DELIVERED"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_okt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                        
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmt_ok']);
$this->_sections['rmt_ok']['name'] = 'rmt_ok';
$this->_sections['rmt_ok']['loop'] = is_array($_loop=$this->_tpl_vars['mt_ok']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_ok']['show'] = true;
$this->_sections['rmt_ok']['max'] = $this->_sections['rmt_ok']['loop'];
$this->_sections['rmt_ok']['step'] = 1;
$this->_sections['rmt_ok']['start'] = $this->_sections['rmt_ok']['step'] > 0 ? 0 : $this->_sections['rmt_ok']['loop']-1;
if ($this->_sections['rmt_ok']['show']) {
    $this->_sections['rmt_ok']['total'] = $this->_sections['rmt_ok']['loop'];
    if ($this->_sections['rmt_ok']['total'] == 0)
        $this->_sections['rmt_ok']['show'] = false;
} else
    $this->_sections['rmt_ok']['total'] = 0;
if ($this->_sections['rmt_ok']['show']):

            for ($this->_sections['rmt_ok']['index'] = $this->_sections['rmt_ok']['start'], $this->_sections['rmt_ok']['iteration'] = 1;
                 $this->_sections['rmt_ok']['iteration'] <= $this->_sections['rmt_ok']['total'];
                 $this->_sections['rmt_ok']['index'] += $this->_sections['rmt_ok']['step'], $this->_sections['rmt_ok']['iteration']++):
$this->_sections['rmt_ok']['rownum'] = $this->_sections['rmt_ok']['iteration'];
$this->_sections['rmt_ok']['index_prev'] = $this->_sections['rmt_ok']['index'] - $this->_sections['rmt_ok']['step'];
$this->_sections['rmt_ok']['index_next'] = $this->_sections['rmt_ok']['index'] + $this->_sections['rmt_ok']['step'];
$this->_sections['rmt_ok']['first']      = ($this->_sections['rmt_ok']['iteration'] == 1);
$this->_sections['rmt_ok']['last']       = ($this->_sections['rmt_ok']['iteration'] == $this->_sections['rmt_ok']['total']);
?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_mt_deliv.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_print'][$this->_sections['rmt_ok']['index']]; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail MT DELIVERED"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok'][$this->_sections['rmt_ok']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                        
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--MT Rejected-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Rejected</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_mt_reject.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_printt']; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail MT REJECTED"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nokt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                        
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmt_nok']);
$this->_sections['rmt_nok']['name'] = 'rmt_nok';
$this->_sections['rmt_nok']['loop'] = is_array($_loop=$this->_tpl_vars['mt_nok']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_nok']['show'] = true;
$this->_sections['rmt_nok']['max'] = $this->_sections['rmt_nok']['loop'];
$this->_sections['rmt_nok']['step'] = 1;
$this->_sections['rmt_nok']['start'] = $this->_sections['rmt_nok']['step'] > 0 ? 0 : $this->_sections['rmt_nok']['loop']-1;
if ($this->_sections['rmt_nok']['show']) {
    $this->_sections['rmt_nok']['total'] = $this->_sections['rmt_nok']['loop'];
    if ($this->_sections['rmt_nok']['total'] == 0)
        $this->_sections['rmt_nok']['show'] = false;
} else
    $this->_sections['rmt_nok']['total'] = 0;
if ($this->_sections['rmt_nok']['show']):

            for ($this->_sections['rmt_nok']['index'] = $this->_sections['rmt_nok']['start'], $this->_sections['rmt_nok']['iteration'] = 1;
                 $this->_sections['rmt_nok']['iteration'] <= $this->_sections['rmt_nok']['total'];
                 $this->_sections['rmt_nok']['index'] += $this->_sections['rmt_nok']['step'], $this->_sections['rmt_nok']['iteration']++):
$this->_sections['rmt_nok']['rownum'] = $this->_sections['rmt_nok']['iteration'];
$this->_sections['rmt_nok']['index_prev'] = $this->_sections['rmt_nok']['index'] - $this->_sections['rmt_nok']['step'];
$this->_sections['rmt_nok']['index_next'] = $this->_sections['rmt_nok']['index'] + $this->_sections['rmt_nok']['step'];
$this->_sections['rmt_nok']['first']      = ($this->_sections['rmt_nok']['iteration'] == 1);
$this->_sections['rmt_nok']['last']       = ($this->_sections['rmt_nok']['iteration'] == $this->_sections['rmt_nok']['total']);
?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_mt_reject.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_print'][$this->_sections['rmt_nok']['index']]; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail MT REJECTED"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok'][$this->_sections['rmt_nok']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                        
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--MT Total-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Total</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_dn_telco.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_printt']; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail DN TELCO"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mtt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rtotal_mt']);
$this->_sections['rtotal_mt']['name'] = 'rtotal_mt';
$this->_sections['rtotal_mt']['loop'] = is_array($_loop=$this->_tpl_vars['total_mt']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rtotal_mt']['show'] = true;
$this->_sections['rtotal_mt']['max'] = $this->_sections['rtotal_mt']['loop'];
$this->_sections['rtotal_mt']['step'] = 1;
$this->_sections['rtotal_mt']['start'] = $this->_sections['rtotal_mt']['step'] > 0 ? 0 : $this->_sections['rtotal_mt']['loop']-1;
if ($this->_sections['rtotal_mt']['show']) {
    $this->_sections['rtotal_mt']['total'] = $this->_sections['rtotal_mt']['loop'];
    if ($this->_sections['rtotal_mt']['total'] == 0)
        $this->_sections['rtotal_mt']['show'] = false;
} else
    $this->_sections['rtotal_mt']['total'] = 0;
if ($this->_sections['rtotal_mt']['show']):

            for ($this->_sections['rtotal_mt']['index'] = $this->_sections['rtotal_mt']['start'], $this->_sections['rtotal_mt']['iteration'] = 1;
                 $this->_sections['rtotal_mt']['iteration'] <= $this->_sections['rtotal_mt']['total'];
                 $this->_sections['rtotal_mt']['index'] += $this->_sections['rtotal_mt']['step'], $this->_sections['rtotal_mt']['iteration']++):
$this->_sections['rtotal_mt']['rownum'] = $this->_sections['rtotal_mt']['iteration'];
$this->_sections['rtotal_mt']['index_prev'] = $this->_sections['rtotal_mt']['index'] - $this->_sections['rtotal_mt']['step'];
$this->_sections['rtotal_mt']['index_next'] = $this->_sections['rtotal_mt']['index'] + $this->_sections['rtotal_mt']['step'];
$this->_sections['rtotal_mt']['first']      = ($this->_sections['rtotal_mt']['iteration'] == 1);
$this->_sections['rtotal_mt']['last']       = ($this->_sections['rtotal_mt']['iteration'] == $this->_sections['rtotal_mt']['total']);
?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_dn_telco.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_print'][$this->_sections['rtotal_mt']['index']]; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail DN TELCO"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt'][$this->_sections['rtotal_mt']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--Persentage Dailypush-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Persentage</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"></td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_persentage.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_printt']; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail MT Persentage"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentaget'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</a></td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rpersentage']);
$this->_sections['rpersentage']['name'] = 'rpersentage';
$this->_sections['rpersentage']['loop'] = is_array($_loop=$this->_tpl_vars['persentage']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rpersentage']['show'] = true;
$this->_sections['rpersentage']['max'] = $this->_sections['rpersentage']['loop'];
$this->_sections['rpersentage']['step'] = 1;
$this->_sections['rpersentage']['start'] = $this->_sections['rpersentage']['step'] > 0 ? 0 : $this->_sections['rpersentage']['loop']-1;
if ($this->_sections['rpersentage']['show']) {
    $this->_sections['rpersentage']['total'] = $this->_sections['rpersentage']['loop'];
    if ($this->_sections['rpersentage']['total'] == 0)
        $this->_sections['rpersentage']['show'] = false;
} else
    $this->_sections['rpersentage']['total'] = 0;
if ($this->_sections['rpersentage']['show']):

            for ($this->_sections['rpersentage']['index'] = $this->_sections['rpersentage']['start'], $this->_sections['rpersentage']['iteration'] = 1;
                 $this->_sections['rpersentage']['iteration'] <= $this->_sections['rpersentage']['total'];
                 $this->_sections['rpersentage']['index'] += $this->_sections['rpersentage']['step'], $this->_sections['rpersentage']['iteration']++):
$this->_sections['rpersentage']['rownum'] = $this->_sections['rpersentage']['iteration'];
$this->_sections['rpersentage']['index_prev'] = $this->_sections['rpersentage']['index'] - $this->_sections['rpersentage']['step'];
$this->_sections['rpersentage']['index_next'] = $this->_sections['rpersentage']['index'] + $this->_sections['rpersentage']['step'];
$this->_sections['rpersentage']['first']      = ($this->_sections['rpersentage']['iteration'] == 1);
$this->_sections['rpersentage']['last']       = ($this->_sections['rpersentage']['iteration'] == $this->_sections['rpersentage']['total']);
?>
                                        <td style="text-align: right;"><a class='ajax' href="detail_persentage.php?tgl=<?php echo $this->_tpl_vars['tahun_traffic']; ?>
-<?php echo $this->_tpl_vars['bulan_traffic']; ?>
-<?php echo $this->_tpl_vars['tgl_print'][$this->_sections['rpersentage']['index']]; ?>
&telco=<?php echo $this->_tpl_vars['tlc']; ?>
&s=<?php echo $this->_tpl_vars['s_id']; ?>
&sc=<?php echo $this->_tpl_vars['sc']; ?>
" title="Detail MT Persentage"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentage'][$this->_sections['rpersentage']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</a></td>
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--Gross Rev Dailypush-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Gross Rev</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_projection'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;">
                                           <?php echo ((is_array($_tmp=$this->_tpl_vars['grosst'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

                                       </td>
                                       <?php endif; ?>
                                       <?php unset($this->_sections['rgross']);
$this->_sections['rgross']['name'] = 'rgross';
$this->_sections['rgross']['loop'] = is_array($_loop=$this->_tpl_vars['gross']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rgross']['show'] = true;
$this->_sections['rgross']['max'] = $this->_sections['rgross']['loop'];
$this->_sections['rgross']['step'] = 1;
$this->_sections['rgross']['start'] = $this->_sections['rgross']['step'] > 0 ? 0 : $this->_sections['rgross']['loop']-1;
if ($this->_sections['rgross']['show']) {
    $this->_sections['rgross']['total'] = $this->_sections['rgross']['loop'];
    if ($this->_sections['rgross']['total'] == 0)
        $this->_sections['rgross']['show'] = false;
} else
    $this->_sections['rgross']['total'] = 0;
if ($this->_sections['rgross']['show']):

            for ($this->_sections['rgross']['index'] = $this->_sections['rgross']['start'], $this->_sections['rgross']['iteration'] = 1;
                 $this->_sections['rgross']['iteration'] <= $this->_sections['rgross']['total'];
                 $this->_sections['rgross']['index'] += $this->_sections['rgross']['step'], $this->_sections['rgross']['iteration']++):
$this->_sections['rgross']['rownum'] = $this->_sections['rgross']['iteration'];
$this->_sections['rgross']['index_prev'] = $this->_sections['rgross']['index'] - $this->_sections['rgross']['step'];
$this->_sections['rgross']['index_next'] = $this->_sections['rgross']['index'] + $this->_sections['rgross']['step'];
$this->_sections['rgross']['first']      = ($this->_sections['rgross']['iteration'] == 1);
$this->_sections['rgross']['last']       = ($this->_sections['rgross']['iteration'] == $this->_sections['rgross']['total']);
?>
                                       <td style="text-align: right;">
                                           <?php echo ((is_array($_tmp=$this->_tpl_vars['gross'][$this->_sections['rgross']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

                                       </td>
                                       <?php endfor; endif; ?>
                                   </tr>

                                   <tr>
                                        <!--NET Rev Dailypush-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Net Rev</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_projection'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['nett'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rnet']);
$this->_sections['rnet']['name'] = 'rnet';
$this->_sections['rnet']['loop'] = is_array($_loop=$this->_tpl_vars['net']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rnet']['show'] = true;
$this->_sections['rnet']['max'] = $this->_sections['rnet']['loop'];
$this->_sections['rnet']['step'] = 1;
$this->_sections['rnet']['start'] = $this->_sections['rnet']['step'] > 0 ? 0 : $this->_sections['rnet']['loop']-1;
if ($this->_sections['rnet']['show']) {
    $this->_sections['rnet']['total'] = $this->_sections['rnet']['loop'];
    if ($this->_sections['rnet']['total'] == 0)
        $this->_sections['rnet']['show'] = false;
} else
    $this->_sections['rnet']['total'] = 0;
if ($this->_sections['rnet']['show']):

            for ($this->_sections['rnet']['index'] = $this->_sections['rnet']['start'], $this->_sections['rnet']['iteration'] = 1;
                 $this->_sections['rnet']['iteration'] <= $this->_sections['rnet']['total'];
                 $this->_sections['rnet']['index'] += $this->_sections['rnet']['step'], $this->_sections['rnet']['iteration']++):
$this->_sections['rnet']['rownum'] = $this->_sections['rnet']['iteration'];
$this->_sections['rnet']['index_prev'] = $this->_sections['rnet']['index'] - $this->_sections['rnet']['step'];
$this->_sections['rnet']['index_next'] = $this->_sections['rnet']['index'] + $this->_sections['rnet']['step'];
$this->_sections['rnet']['first']      = ($this->_sections['rnet']['iteration'] == 1);
$this->_sections['rnet']['last']       = ($this->_sections['rnet']['iteration'] == $this->_sections['rnet']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net'][$this->_sections['rnet']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- printable  -->

                        <div class="box-body" style="display:none" id="printreport">
                            <h1>Daily Trafic Report</h1>
                            <table id="example1" class="table table-bordered table-striped" style="font-size: 12px">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">Tanggal</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">Total</th>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;">Projection</th>
                                        <!--tanggal-->
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_printt']; ?>
</th>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
                                        <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_print'][$this->_sections['r']['index']]; ?>
</th>
                                        <?php endfor; endif; ?>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <!--MO-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmo']);
$this->_sections['rmo']['name'] = 'rmo';
$this->_sections['rmo']['loop'] = is_array($_loop=$this->_tpl_vars['mo']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo']['show'] = true;
$this->_sections['rmo']['max'] = $this->_sections['rmo']['loop'];
$this->_sections['rmo']['step'] = 1;
$this->_sections['rmo']['start'] = $this->_sections['rmo']['step'] > 0 ? 0 : $this->_sections['rmo']['loop']-1;
if ($this->_sections['rmo']['show']) {
    $this->_sections['rmo']['total'] = $this->_sections['rmo']['loop'];
    if ($this->_sections['rmo']['total'] == 0)
        $this->_sections['rmo']['show'] = false;
} else
    $this->_sections['rmo']['total'] = 0;
if ($this->_sections['rmo']['show']):

            for ($this->_sections['rmo']['index'] = $this->_sections['rmo']['start'], $this->_sections['rmo']['iteration'] = 1;
                 $this->_sections['rmo']['iteration'] <= $this->_sections['rmo']['total'];
                 $this->_sections['rmo']['index'] += $this->_sections['rmo']['step'], $this->_sections['rmo']['iteration']++):
$this->_sections['rmo']['rownum'] = $this->_sections['rmo']['iteration'];
$this->_sections['rmo']['index_prev'] = $this->_sections['rmo']['index'] - $this->_sections['rmo']['step'];
$this->_sections['rmo']['index_next'] = $this->_sections['rmo']['index'] + $this->_sections['rmo']['step'];
$this->_sections['rmo']['first']      = ($this->_sections['rmo']['iteration'] == 1);
$this->_sections['rmo']['last']       = ($this->_sections['rmo']['iteration'] == $this->_sections['rmo']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo'][$this->_sections['rmo']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>

                                    </tr>

                                    <tr>
                                        <!--MO REG-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO REG</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_reg_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_regt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmo_reg']);
$this->_sections['rmo_reg']['name'] = 'rmo_reg';
$this->_sections['rmo_reg']['loop'] = is_array($_loop=$this->_tpl_vars['mo_reg']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo_reg']['show'] = true;
$this->_sections['rmo_reg']['max'] = $this->_sections['rmo_reg']['loop'];
$this->_sections['rmo_reg']['step'] = 1;
$this->_sections['rmo_reg']['start'] = $this->_sections['rmo_reg']['step'] > 0 ? 0 : $this->_sections['rmo_reg']['loop']-1;
if ($this->_sections['rmo_reg']['show']) {
    $this->_sections['rmo_reg']['total'] = $this->_sections['rmo_reg']['loop'];
    if ($this->_sections['rmo_reg']['total'] == 0)
        $this->_sections['rmo_reg']['show'] = false;
} else
    $this->_sections['rmo_reg']['total'] = 0;
if ($this->_sections['rmo_reg']['show']):

            for ($this->_sections['rmo_reg']['index'] = $this->_sections['rmo_reg']['start'], $this->_sections['rmo_reg']['iteration'] = 1;
                 $this->_sections['rmo_reg']['iteration'] <= $this->_sections['rmo_reg']['total'];
                 $this->_sections['rmo_reg']['index'] += $this->_sections['rmo_reg']['step'], $this->_sections['rmo_reg']['iteration']++):
$this->_sections['rmo_reg']['rownum'] = $this->_sections['rmo_reg']['iteration'];
$this->_sections['rmo_reg']['index_prev'] = $this->_sections['rmo_reg']['index'] - $this->_sections['rmo_reg']['step'];
$this->_sections['rmo_reg']['index_next'] = $this->_sections['rmo_reg']['index'] + $this->_sections['rmo_reg']['step'];
$this->_sections['rmo_reg']['first']      = ($this->_sections['rmo_reg']['iteration'] == 1);
$this->_sections['rmo_reg']['last']       = ($this->_sections['rmo_reg']['iteration'] == $this->_sections['rmo_reg']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_reg'][$this->_sections['rmo_reg']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>

                                    </tr>

                                    <tr>
                                        <!--MO UNREG-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MO UNREG</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unreg_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unregt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmo_unreg']);
$this->_sections['rmo_unreg']['name'] = 'rmo_unreg';
$this->_sections['rmo_unreg']['loop'] = is_array($_loop=$this->_tpl_vars['mo_unreg']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmo_unreg']['show'] = true;
$this->_sections['rmo_unreg']['max'] = $this->_sections['rmo_unreg']['loop'];
$this->_sections['rmo_unreg']['step'] = 1;
$this->_sections['rmo_unreg']['start'] = $this->_sections['rmo_unreg']['step'] > 0 ? 0 : $this->_sections['rmo_unreg']['loop']-1;
if ($this->_sections['rmo_unreg']['show']) {
    $this->_sections['rmo_unreg']['total'] = $this->_sections['rmo_unreg']['loop'];
    if ($this->_sections['rmo_unreg']['total'] == 0)
        $this->_sections['rmo_unreg']['show'] = false;
} else
    $this->_sections['rmo_unreg']['total'] = 0;
if ($this->_sections['rmo_unreg']['show']):

            for ($this->_sections['rmo_unreg']['index'] = $this->_sections['rmo_unreg']['start'], $this->_sections['rmo_unreg']['iteration'] = 1;
                 $this->_sections['rmo_unreg']['iteration'] <= $this->_sections['rmo_unreg']['total'];
                 $this->_sections['rmo_unreg']['index'] += $this->_sections['rmo_unreg']['step'], $this->_sections['rmo_unreg']['iteration']++):
$this->_sections['rmo_unreg']['rownum'] = $this->_sections['rmo_unreg']['iteration'];
$this->_sections['rmo_unreg']['index_prev'] = $this->_sections['rmo_unreg']['index'] - $this->_sections['rmo_unreg']['step'];
$this->_sections['rmo_unreg']['index_next'] = $this->_sections['rmo_unreg']['index'] + $this->_sections['rmo_unreg']['step'];
$this->_sections['rmo_unreg']['first']      = ($this->_sections['rmo_unreg']['iteration'] == 1);
$this->_sections['rmo_unreg']['last']       = ($this->_sections['rmo_unreg']['iteration'] == $this->_sections['rmo_unreg']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mo_unreg'][$this->_sections['rmo_unreg']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>

                                    </tr>

                                    <tr>
                                        <!--MT Delivered-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Delivered</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_okt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmt_ok']);
$this->_sections['rmt_ok']['name'] = 'rmt_ok';
$this->_sections['rmt_ok']['loop'] = is_array($_loop=$this->_tpl_vars['mt_ok']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_ok']['show'] = true;
$this->_sections['rmt_ok']['max'] = $this->_sections['rmt_ok']['loop'];
$this->_sections['rmt_ok']['step'] = 1;
$this->_sections['rmt_ok']['start'] = $this->_sections['rmt_ok']['step'] > 0 ? 0 : $this->_sections['rmt_ok']['loop']-1;
if ($this->_sections['rmt_ok']['show']) {
    $this->_sections['rmt_ok']['total'] = $this->_sections['rmt_ok']['loop'];
    if ($this->_sections['rmt_ok']['total'] == 0)
        $this->_sections['rmt_ok']['show'] = false;
} else
    $this->_sections['rmt_ok']['total'] = 0;
if ($this->_sections['rmt_ok']['show']):

            for ($this->_sections['rmt_ok']['index'] = $this->_sections['rmt_ok']['start'], $this->_sections['rmt_ok']['iteration'] = 1;
                 $this->_sections['rmt_ok']['iteration'] <= $this->_sections['rmt_ok']['total'];
                 $this->_sections['rmt_ok']['index'] += $this->_sections['rmt_ok']['step'], $this->_sections['rmt_ok']['iteration']++):
$this->_sections['rmt_ok']['rownum'] = $this->_sections['rmt_ok']['iteration'];
$this->_sections['rmt_ok']['index_prev'] = $this->_sections['rmt_ok']['index'] - $this->_sections['rmt_ok']['step'];
$this->_sections['rmt_ok']['index_next'] = $this->_sections['rmt_ok']['index'] + $this->_sections['rmt_ok']['step'];
$this->_sections['rmt_ok']['first']      = ($this->_sections['rmt_ok']['iteration'] == 1);
$this->_sections['rmt_ok']['last']       = ($this->_sections['rmt_ok']['iteration'] == $this->_sections['rmt_ok']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_ok'][$this->_sections['rmt_ok']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--MT Rejected-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Rejected</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nokt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rmt_nok']);
$this->_sections['rmt_nok']['name'] = 'rmt_nok';
$this->_sections['rmt_nok']['loop'] = is_array($_loop=$this->_tpl_vars['mt_nok']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rmt_nok']['show'] = true;
$this->_sections['rmt_nok']['max'] = $this->_sections['rmt_nok']['loop'];
$this->_sections['rmt_nok']['step'] = 1;
$this->_sections['rmt_nok']['start'] = $this->_sections['rmt_nok']['step'] > 0 ? 0 : $this->_sections['rmt_nok']['loop']-1;
if ($this->_sections['rmt_nok']['show']) {
    $this->_sections['rmt_nok']['total'] = $this->_sections['rmt_nok']['loop'];
    if ($this->_sections['rmt_nok']['total'] == 0)
        $this->_sections['rmt_nok']['show'] = false;
} else
    $this->_sections['rmt_nok']['total'] = 0;
if ($this->_sections['rmt_nok']['show']):

            for ($this->_sections['rmt_nok']['index'] = $this->_sections['rmt_nok']['start'], $this->_sections['rmt_nok']['iteration'] = 1;
                 $this->_sections['rmt_nok']['iteration'] <= $this->_sections['rmt_nok']['total'];
                 $this->_sections['rmt_nok']['index'] += $this->_sections['rmt_nok']['step'], $this->_sections['rmt_nok']['iteration']++):
$this->_sections['rmt_nok']['rownum'] = $this->_sections['rmt_nok']['iteration'];
$this->_sections['rmt_nok']['index_prev'] = $this->_sections['rmt_nok']['index'] - $this->_sections['rmt_nok']['step'];
$this->_sections['rmt_nok']['index_next'] = $this->_sections['rmt_nok']['index'] + $this->_sections['rmt_nok']['step'];
$this->_sections['rmt_nok']['first']      = ($this->_sections['rmt_nok']['iteration'] == 1);
$this->_sections['rmt_nok']['last']       = ($this->_sections['rmt_nok']['iteration'] == $this->_sections['rmt_nok']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['mt_nok'][$this->_sections['rmt_nok']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--MT Total-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">MT Total</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mtt'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rtotal_mt']);
$this->_sections['rtotal_mt']['name'] = 'rtotal_mt';
$this->_sections['rtotal_mt']['loop'] = is_array($_loop=$this->_tpl_vars['total_mt']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rtotal_mt']['show'] = true;
$this->_sections['rtotal_mt']['max'] = $this->_sections['rtotal_mt']['loop'];
$this->_sections['rtotal_mt']['step'] = 1;
$this->_sections['rtotal_mt']['start'] = $this->_sections['rtotal_mt']['step'] > 0 ? 0 : $this->_sections['rtotal_mt']['loop']-1;
if ($this->_sections['rtotal_mt']['show']) {
    $this->_sections['rtotal_mt']['total'] = $this->_sections['rtotal_mt']['loop'];
    if ($this->_sections['rtotal_mt']['total'] == 0)
        $this->_sections['rtotal_mt']['show'] = false;
} else
    $this->_sections['rtotal_mt']['total'] = 0;
if ($this->_sections['rtotal_mt']['show']):

            for ($this->_sections['rtotal_mt']['index'] = $this->_sections['rtotal_mt']['start'], $this->_sections['rtotal_mt']['iteration'] = 1;
                 $this->_sections['rtotal_mt']['iteration'] <= $this->_sections['rtotal_mt']['total'];
                 $this->_sections['rtotal_mt']['index'] += $this->_sections['rtotal_mt']['step'], $this->_sections['rtotal_mt']['iteration']++):
$this->_sections['rtotal_mt']['rownum'] = $this->_sections['rtotal_mt']['iteration'];
$this->_sections['rtotal_mt']['index_prev'] = $this->_sections['rtotal_mt']['index'] - $this->_sections['rtotal_mt']['step'];
$this->_sections['rtotal_mt']['index_next'] = $this->_sections['rtotal_mt']['index'] + $this->_sections['rtotal_mt']['step'];
$this->_sections['rtotal_mt']['first']      = ($this->_sections['rtotal_mt']['iteration'] == 1);
$this->_sections['rtotal_mt']['last']       = ($this->_sections['rtotal_mt']['iteration'] == $this->_sections['rtotal_mt']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['total_mt'][$this->_sections['rtotal_mt']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--Persentage Dailypush-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Persentage</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"></td>
                                        <td style="background-color:#646464; color:#fff"></td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentaget'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rpersentage']);
$this->_sections['rpersentage']['name'] = 'rpersentage';
$this->_sections['rpersentage']['loop'] = is_array($_loop=$this->_tpl_vars['persentage']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rpersentage']['show'] = true;
$this->_sections['rpersentage']['max'] = $this->_sections['rpersentage']['loop'];
$this->_sections['rpersentage']['step'] = 1;
$this->_sections['rpersentage']['start'] = $this->_sections['rpersentage']['step'] > 0 ? 0 : $this->_sections['rpersentage']['loop']-1;
if ($this->_sections['rpersentage']['show']) {
    $this->_sections['rpersentage']['total'] = $this->_sections['rpersentage']['loop'];
    if ($this->_sections['rpersentage']['total'] == 0)
        $this->_sections['rpersentage']['show'] = false;
} else
    $this->_sections['rpersentage']['total'] = 0;
if ($this->_sections['rpersentage']['show']):

            for ($this->_sections['rpersentage']['index'] = $this->_sections['rpersentage']['start'], $this->_sections['rpersentage']['iteration'] = 1;
                 $this->_sections['rpersentage']['iteration'] <= $this->_sections['rpersentage']['total'];
                 $this->_sections['rpersentage']['index'] += $this->_sections['rpersentage']['step'], $this->_sections['rpersentage']['iteration']++):
$this->_sections['rpersentage']['rownum'] = $this->_sections['rpersentage']['iteration'];
$this->_sections['rpersentage']['index_prev'] = $this->_sections['rpersentage']['index'] - $this->_sections['rpersentage']['step'];
$this->_sections['rpersentage']['index_next'] = $this->_sections['rpersentage']['index'] + $this->_sections['rpersentage']['step'];
$this->_sections['rpersentage']['first']      = ($this->_sections['rpersentage']['iteration'] == 1);
$this->_sections['rpersentage']['last']       = ($this->_sections['rpersentage']['iteration'] == $this->_sections['rpersentage']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['persentage'][$this->_sections['rpersentage']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
%</td>
                                        <?php endfor; endif; ?>
                                    </tr>

                                    <tr>
                                        <!--Gross Rev Dailypush-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Gross Rev</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['gross_projection'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;">
                                           <?php echo ((is_array($_tmp=$this->_tpl_vars['grosst'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

                                       </td>
                                       <?php endif; ?>
                                       <?php unset($this->_sections['rgross']);
$this->_sections['rgross']['name'] = 'rgross';
$this->_sections['rgross']['loop'] = is_array($_loop=$this->_tpl_vars['gross']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rgross']['show'] = true;
$this->_sections['rgross']['max'] = $this->_sections['rgross']['loop'];
$this->_sections['rgross']['step'] = 1;
$this->_sections['rgross']['start'] = $this->_sections['rgross']['step'] > 0 ? 0 : $this->_sections['rgross']['loop']-1;
if ($this->_sections['rgross']['show']) {
    $this->_sections['rgross']['total'] = $this->_sections['rgross']['loop'];
    if ($this->_sections['rgross']['total'] == 0)
        $this->_sections['rgross']['show'] = false;
} else
    $this->_sections['rgross']['total'] = 0;
if ($this->_sections['rgross']['show']):

            for ($this->_sections['rgross']['index'] = $this->_sections['rgross']['start'], $this->_sections['rgross']['iteration'] = 1;
                 $this->_sections['rgross']['iteration'] <= $this->_sections['rgross']['total'];
                 $this->_sections['rgross']['index'] += $this->_sections['rgross']['step'], $this->_sections['rgross']['iteration']++):
$this->_sections['rgross']['rownum'] = $this->_sections['rgross']['iteration'];
$this->_sections['rgross']['index_prev'] = $this->_sections['rgross']['index'] - $this->_sections['rgross']['step'];
$this->_sections['rgross']['index_next'] = $this->_sections['rgross']['index'] + $this->_sections['rgross']['step'];
$this->_sections['rgross']['first']      = ($this->_sections['rgross']['iteration'] == 1);
$this->_sections['rgross']['last']       = ($this->_sections['rgross']['iteration'] == $this->_sections['rgross']['total']);
?>
                                       <td style="text-align: right;">
                                           <?php echo ((is_array($_tmp=$this->_tpl_vars['gross'][$this->_sections['rgross']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

                                       </td>
                                       <?php endfor; endif; ?>
                                   </tr>

                                   <tr>
                                        <!--NET Rev Dailypush-->
                                        <td style="background-color:rgba(37, 116, 169, 0.6); color:#fff">Net Rev</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <td style="background-color:#646464; color:#fff;text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net_projection'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php if ($this->_tpl_vars['is_today'] == 'ya'): ?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['nett'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endif; ?>
                                        <?php unset($this->_sections['rnet']);
$this->_sections['rnet']['name'] = 'rnet';
$this->_sections['rnet']['loop'] = is_array($_loop=$this->_tpl_vars['net']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rnet']['show'] = true;
$this->_sections['rnet']['max'] = $this->_sections['rnet']['loop'];
$this->_sections['rnet']['step'] = 1;
$this->_sections['rnet']['start'] = $this->_sections['rnet']['step'] > 0 ? 0 : $this->_sections['rnet']['loop']-1;
if ($this->_sections['rnet']['show']) {
    $this->_sections['rnet']['total'] = $this->_sections['rnet']['loop'];
    if ($this->_sections['rnet']['total'] == 0)
        $this->_sections['rnet']['show'] = false;
} else
    $this->_sections['rnet']['total'] = 0;
if ($this->_sections['rnet']['show']):

            for ($this->_sections['rnet']['index'] = $this->_sections['rnet']['start'], $this->_sections['rnet']['iteration'] = 1;
                 $this->_sections['rnet']['iteration'] <= $this->_sections['rnet']['total'];
                 $this->_sections['rnet']['index'] += $this->_sections['rnet']['step'], $this->_sections['rnet']['iteration']++):
$this->_sections['rnet']['rownum'] = $this->_sections['rnet']['iteration'];
$this->_sections['rnet']['index_prev'] = $this->_sections['rnet']['index'] - $this->_sections['rnet']['step'];
$this->_sections['rnet']['index_next'] = $this->_sections['rnet']['index'] + $this->_sections['rnet']['step'];
$this->_sections['rnet']['first']      = ($this->_sections['rnet']['iteration'] == 1);
$this->_sections['rnet']['last']       = ($this->_sections['rnet']['iteration'] == $this->_sections['rnet']['total']);
?>
                                        <td style="text-align: right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['net'][$this->_sections['rnet']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                        <?php endfor; endif; ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    <div id="container" style="padding-bottom:20px;"></div>
                </div>
            </div>
        </div>


    </section>



</div>
<footer class="main-footer">
    <?php echo $this->_tpl_vars['copyright']; ?>

</footer>
</div>
<?php echo '
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script type="text/javascript" language="JavaScript">
    Highcharts.chart(\'container\', {
        colors: [\'#2b908f\', \'#90ee7e\', \'#f45b5b\', \'#7798BF\', \'#aaeeee\', \'#ff0066\', \'#eeaaee\',
        \'#55BF3B\', \'#DF5353\', \'#7798BF\', \'#aaeeee\'],
        chart: {
            backgroundColor: {
                linearGradient: {x1: 0, y1: 0, x2: 1, y2: 1},
                stops: [
                [0, \'#2a2a2b\'],
                [1, \'#3e3e40\']
                ]
            },
            style: {
                fontFamily: \'\\\'Unica One\\\', sans-serif\'
            },
            plotBorderColor: \'#606063\'
        },
        title: {
            style: {
                color: \'#E0E0E3\',
                textTransform: \'uppercase\',
                fontSize: \'20px\'
            },
            text: \'Grafik Daily Traffic Gross Revenue\'
        },

        subtitle: {
            text: \'\'
        },

        yAxis: {
            gridLineColor: \'#707073\',
            labels: {
                style: {
                    color: \'#E0E0E3\'
                }
            },
            lineColor: \'#707073\',
            minorGridLineColor: \'#505053\',
            tickColor: \'#707073\',
            tickWidth: 1,
            title: {
                style: {
                    color: \'#A0A0A3\'
                }
            },
            title: {
                text: \'\'
            }
        },
        legend: {
            layout: \'vertical\',
            align: \'right\',
            verticalAlign: \'middle\'
        },

        xAxis: {
            gridLineColor: \'#707073\',
            labels: {
                style: {
                    color: \'#E0E0E3\'
                }
            },
            lineColor: \'#707073\',
            minorGridLineColor: \'#505053\',
            tickColor: \'#707073\',
            title: {
                text: \'Tanggal\',
                style: {
                    color: \'#fff\'

                }
            },
            categories: [
            '; ?>

            
            <?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['step'] = ((int)-1) == 0 ? 1 : (int)-1;
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = min(ceil(($this->_sections['r']['step'] > 0 ? $this->_sections['r']['loop'] - $this->_sections['r']['start'] : $this->_sections['r']['start']+1)/abs($this->_sections['r']['step'])), $this->_sections['r']['max']);
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
            <?php echo ((is_array($_tmp=$this->_tpl_vars['tgl_print'][$this->_sections['r']['index']])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ',')); ?>

            <?php endfor; endif; ?>
            <?php echo $this->_tpl_vars['tgl_printt']; ?>

            <?php echo '
            ]

        },

        series: [{
            name:\'Gros Revenue\',
            showInLegend: false,
            data: [
            '; ?>

            
            <?php unset($this->_sections['rgross']);
$this->_sections['rgross']['name'] = 'rgross';
$this->_sections['rgross']['loop'] = is_array($_loop=$this->_tpl_vars['gross']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rgross']['step'] = ((int)-1) == 0 ? 1 : (int)-1;
$this->_sections['rgross']['show'] = true;
$this->_sections['rgross']['max'] = $this->_sections['rgross']['loop'];
$this->_sections['rgross']['start'] = $this->_sections['rgross']['step'] > 0 ? 0 : $this->_sections['rgross']['loop']-1;
if ($this->_sections['rgross']['show']) {
    $this->_sections['rgross']['total'] = min(ceil(($this->_sections['rgross']['step'] > 0 ? $this->_sections['rgross']['loop'] - $this->_sections['rgross']['start'] : $this->_sections['rgross']['start']+1)/abs($this->_sections['rgross']['step'])), $this->_sections['rgross']['max']);
    if ($this->_sections['rgross']['total'] == 0)
        $this->_sections['rgross']['show'] = false;
} else
    $this->_sections['rgross']['total'] = 0;
if ($this->_sections['rgross']['show']):

            for ($this->_sections['rgross']['index'] = $this->_sections['rgross']['start'], $this->_sections['rgross']['iteration'] = 1;
                 $this->_sections['rgross']['iteration'] <= $this->_sections['rgross']['total'];
                 $this->_sections['rgross']['index'] += $this->_sections['rgross']['step'], $this->_sections['rgross']['iteration']++):
$this->_sections['rgross']['rownum'] = $this->_sections['rgross']['iteration'];
$this->_sections['rgross']['index_prev'] = $this->_sections['rgross']['index'] - $this->_sections['rgross']['step'];
$this->_sections['rgross']['index_next'] = $this->_sections['rgross']['index'] + $this->_sections['rgross']['step'];
$this->_sections['rgross']['first']      = ($this->_sections['rgross']['iteration'] == 1);
$this->_sections['rgross']['last']       = ($this->_sections['rgross']['iteration'] == $this->_sections['rgross']['total']);
?>
            <?php echo ((is_array($_tmp=$this->_tpl_vars['gross'][$this->_sections['rgross']['index']])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ',')); ?>

            <?php endfor; endif; ?>
            <?php echo $this->_tpl_vars['grosst']; ?>

            <?php echo '
            ]
        }]

    });
</script>
'; ?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>