<?php /* Smarty version 2.6.18, created on 2017-10-23 13:52:16
         compiled from /var/www/sms/engine/reporting2/templates/cost_service.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/cost_service.html', 128, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>Cost Service</title>
<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <script type="text/javascript" language="JavaScript"> 
        <!-- 
        function go_delete(d,e) 
        { 
         var where_to= confirm("Yakin Anda akan menghapus ini?");
         var i;
         var j;
         i = d;
         j = e;
         if (where_to== true)
           window.location=i + j;
   } 
    //-->
</script>
<script LANGUAGE="JavaScript">
    <!--

    <!-- Begin
    function CheckAll()
    {
        var allInputs = document.getElementsByTagName("input")
        for (var i = 0; i < allInputs.length; i++) {
            if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
                allInputs[i].checked = true;
            } else continue;
        }
    }

    function UnCheckAll()
    {
        var allInputs = document.getElementsByTagName("input")
        for (var i = 0; i < allInputs.length; i++) {
            if (allInputs[i].type == \'checkbox\' && allInputs[i].id.substring(0,10)==\'check_list\'){
                allInputs[i].checked = false;
            } else continue;
        }
    }
    // End -->
</script>
'; ?>

<div class="wrapper">

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="content-wrapper" style="min-height:0px;">

        <section class="content-header">
            <h1>Cost Service</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->

                    <!-- <td width="16%" height="444" valign="top" background="<?php echo $this->_tpl_vars['img_dir']; ?>
leftcollumbkg2.gif"><div align="left">
                        <td height="106" valign="center" background="<?php echo $this->_tpl_vars['img_dir']; ?>
topbarbkg.gif"><div align="center" class="txt-title"><?php echo $this->_tpl_vars['title']; ?>
</td> -->

                            <div class="box box-primary">
                                <?php echo $this->_tpl_vars['msg']; ?>

                                <div class="box-body">
                                    <form class="form-horizontal" action="" method="GET" name="add">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-md-3">SERVICE</label>
                                                <div class="col-md-8">
                                                    <?php echo $this->_tpl_vars['dd_service']; ?>

                                                </div>
                                            </div>
                                        </div> 
                                        
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-md-3">SHORTCODE</label>
                                                <div class="col-md-8">
                                                    <?php echo $this->_tpl_vars['dd_shortcode']; ?>
                                                
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-md-5" style="width: 80px">
                                                    <label>TELCO</label>
                                                </div>
                                                <div class="col-md-4" style="width: 40%">
                                                    <?php echo $this->_tpl_vars['dd_telco']; ?>

                                                </div>
                                                <div class="col-md-3" style="padding-right: 0px; padding-left: 0px">
                                                    <button style="width:100%" name="submit" type="submit" class="btn btn-success">Submit</button>
                                                    <input type="hidden" name="active" value="report">
                                                    <input type="hidden" name="menu" value="Cost Service"> 
                                                </div>                      
                                            </div>
                                        </div>                             
                                    </form>
                                </div>
                                <div class="col-md-12">
                                    <strong style="color: #4183D7">COST SERVICE : [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ] [ Service : <?php echo $this->_tpl_vars['s_name']; ?>
 ]</strong>
                                </div>
                                <div class="box-body" style="overflow-x:auto">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">APPID</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">DESCRIPTION</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">TELCO</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">SHORTCODE</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">COST NAME</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">COST AMOUNT</th>
                                                <th style="text-align: center;background-color: #4183D7;color: #fff;">DELETE</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php unset($this->_sections['row']);
$this->_sections['row']['name'] = 'row';
$this->_sections['row']['loop'] = is_array($_loop=$this->_tpl_vars['appid']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['row']['show'] = true;
$this->_sections['row']['max'] = $this->_sections['row']['loop'];
$this->_sections['row']['step'] = 1;
$this->_sections['row']['start'] = $this->_sections['row']['step'] > 0 ? 0 : $this->_sections['row']['loop']-1;
if ($this->_sections['row']['show']) {
    $this->_sections['row']['total'] = $this->_sections['row']['loop'];
    if ($this->_sections['row']['total'] == 0)
        $this->_sections['row']['show'] = false;
} else
    $this->_sections['row']['total'] = 0;
if ($this->_sections['row']['show']):

            for ($this->_sections['row']['index'] = $this->_sections['row']['start'], $this->_sections['row']['iteration'] = 1;
                 $this->_sections['row']['iteration'] <= $this->_sections['row']['total'];
                 $this->_sections['row']['index'] += $this->_sections['row']['step'], $this->_sections['row']['iteration']++):
$this->_sections['row']['rownum'] = $this->_sections['row']['iteration'];
$this->_sections['row']['index_prev'] = $this->_sections['row']['index'] - $this->_sections['row']['step'];
$this->_sections['row']['index_next'] = $this->_sections['row']['index'] + $this->_sections['row']['step'];
$this->_sections['row']['first']      = ($this->_sections['row']['iteration'] == 1);
$this->_sections['row']['last']       = ($this->_sections['row']['iteration'] == $this->_sections['row']['total']);
?>
                                            <tr>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['appid'][$this->_sections['row']['index']]; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['description'][$this->_sections['row']['index']]; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['telco'][$this->_sections['row']['index']]; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['shortcode'][$this->_sections['row']['index']]; ?>
</td>
                                                <td style="text-align:center"><?php echo $this->_tpl_vars['cost_name'][$this->_sections['row']['index']]; ?>
</td>
                                                <td style="text-align:center"><?php echo ((is_array($_tmp=$this->_tpl_vars['cost_amount'][$this->_sections['row']['index']])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
                                                <td style="text-align:center"><a href="javascript:go_delete('cost_service.php?action=delete&id=',<?php echo $this->_tpl_vars['id'][$this->_sections['row']['index']]; ?>
)" class="link-page">DELETE</a></td>
                                            </tr>
                                            <?php endfor; endif; ?>
                                            
                                            <tr bgcolor="#CCCCCC">
                                                <td style="text-align:center"><strong>TOTAL</strong></td>
                                                <td style="text-align:center"></td>
                                                <td style="text-align:center"></td>
                                                <td style="text-align:center"></td>
                                                <td style="text-align:center"></td>
                                                <td style="text-align:center"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['cost_tot'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</strong></td>
                                                <td style="text-align:center"></td>             
                                            </tr>
                                        </tbody>
                                        
                                    </table>
                                    
                                    <?php if ($this->_tpl_vars['numrows'] != ""): ?>
                                    <table width="70%" border="0" cellpadding="5" cellspacing="0" style="margin-bottom: 30px;">
                                        <tr class="text">
                                                <td align="left" valign="top">
                                                        <a href="cost_service.php?begin=<?php echo $this->_tpl_vars['first']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="first" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_firstpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                                                        <a href="cost_service.php?begin=<?php echo $this->_tpl_vars['prev']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="prev" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_prevpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="prev" /></a><?php echo $this->_tpl_vars['dispfrom']; ?>
 - <?php echo $this->_tpl_vars['dispto']; ?>
 / <?php echo $this->_tpl_vars['numrows']; ?>

                                                        <a href="cost_service.php?begin=<?php echo $this->_tpl_vars['next']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="next" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_nextpage.png" width="8" height="13" hspace="0" vspace="0" border="0" class="next" /></a>
                                                        <a href="cost_service.php?begin=<?php echo $this->_tpl_vars['last']; ?>
&filter=<?php echo $this->_tpl_vars['filter']; ?>
"><img class="last" src="<?php echo $this->_tpl_vars['img_dir']; ?>
bd_lastpage.png" width="16" height="13" hspace="0" vspace="0" border="0" /></a>
                                                </td>
                                                <td align="right" class="text">Total Data: <?php echo $this->_tpl_vars['numrows']; ?>
</td>
                                        </tr>
                                    </table>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- /.box --> 
                        </div>
                    </div>
                    <!-- /.row -->

                </section>
                <!-- /.content -->
                
                <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Add Cost</h3>
                                </div>
                                <form class="form-horizontal" style="margin-top:25px;" method="post" action="">

                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>Service</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_service']; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>TELCO</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_telco']; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>SHORTCODE</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_shortcode']; ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>COST NAME</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="cost_name" placeholder="Cost Name" value="<?php echo $this->_tpl_vars['cost_namee']; ?>
">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>COST AMOUNT</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="cost_amount" placeholder="Cost Name" value="<?php echo $this->_tpl_vars['cost_amounte']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                            <button type="submit" name="app_add" class="btn btn-success">
                                                <i class="fa fa-plus"></i> Submit</button>
                                            <input type="reset" name="Reset" value="Reset" class="btn btn-warning">
                                            <input type="hidden" name="action" value="<?php echo $this->_tpl_vars['action']; ?>
" class="text">
                                            <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['id_edit']; ?>
" class="text">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </section>
            <!-- /.content -->


            </div> 
            <footer class="main-footer">
                <?php echo $this->_tpl_vars['copyright']; ?>

            </footer>
        </div>

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>