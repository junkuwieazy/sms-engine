<?php /* Smarty version 2.6.18, created on 2017-10-24 12:42:30
         compiled from /var/www/sms/engine/reporting2/templates/mt_sms.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', '/var/www/sms/engine/reporting2/templates/mt_sms.html', 119, false),array('modifier', 'cat', '/var/www/sms/engine/reporting2/templates/mt_sms.html', 220, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<title>MT SMS</title>

<body class="hold-transition skin-blue sidebar-mini">
    <?php echo '
    <style>
    #container {
                    max-width: 1000px;
                    height: 400px;
                    margin: 0 auto;
                    margin-top: 35px;
                }
    </style>
    <script type="text/javascript" language="JavaScript">
        <!-- 
        function go_delete(d, e)
        {
            var where_to = confirm("Yakin Anda akan menghapus ini?");
            var i;
            var j;
            i = d;
            j = e;
            if (where_to == true)
                window.location = i + j;
        }
        //-->
    </script>
    '; ?>

    <div class="wrapper">

        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        <div class="content-wrapper" style="min-height:0px;">

            <section class="content-header">
                <h1>MT SMS</h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <form class="form-horizontal" name="add" style="margin-top:25px;" method="get" action="">

                                    <div class="col-md-6">
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>BULAN</label>
                                                </div>
                                                <div class="col-md-7" style="padding-left:0px">
                                                    <div class="col-md-7">
                                                        <?php echo $this->_tpl_vars['dd_month']; ?>

                                                    </div>
                                                    <div class="col-md-5" style="padding-right:0px">
                                                        <?php echo $this->_tpl_vars['dd_year']; ?>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>SHORTCODE</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="shortcode2" class="form-control"  placeholder="Shortcode" value="<?php echo $this->_tpl_vars['shortcode2']; ?>
">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label>TELCO</label>
                                                </div>
                                                <div class="col-md-7">
                                                    <?php echo $this->_tpl_vars['dd_telco']; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <button type="submit" name="app_add" class="btn btn-success">Submit</button>  
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->

                            <div class="col-md-12">
                                <strong style="color: #4183D7">MT SMS : [ Bulan : <?php echo $this->_tpl_vars['bulan_traffic']; ?>
 , Tahun : <?php echo $this->_tpl_vars['tahun_traffic']; ?>
 ] [ SHORTCODE : <?php echo $this->_tpl_vars['sc']; ?>
 ] [ TELCO : <?php echo $this->_tpl_vars['tlc']; ?>
 ]</strong>
                            </div>

                            <div class="box-body" style="padding-bottom:20px;overflow-x:auto">
                                <table id="example1" class="table table-bordered table-striped" style="font-size: 14px">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;">SERVICE</th>
                                            <?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
                                            <th style="text-align: center;background-color: #4183D7;color: #fff;"><?php echo $this->_tpl_vars['tgl_print'][$this->_sections['r']['index']]; ?>
</th>
                                            <?php endfor; endif; ?>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $_from = $this->_tpl_vars['mtfix']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['service'] => $this->_tpl_vars['i']):
?>
                                        <tr>
                                            <td><?php echo $this->_tpl_vars['service']; ?>
</td>
                                            <?php $_from = $this->_tpl_vars['i']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tgl'] => $this->_tpl_vars['jumlah']):
?>
                                            <td style="text-align:center;"><?php echo ((is_array($_tmp=$this->_tpl_vars['jumlah'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </tr>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            <!-- <div id="container" style="padding-bottom:20px;"></div> -->
                        </div>
                    </div>
                </div>


            </section>



        </div> 
        <footer class="main-footer">
            <?php echo $this->_tpl_vars['copyright']; ?>

        </footer>
    </div>
<?php echo '
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script type="text/javascript" language="JavaScript">
    Highcharts.chart(\'container\', {
        colors: [\'#2b908f\', \'#90ee7e\', \'#f45b5b\', \'#7798BF\', \'#aaeeee\', \'#ff0066\', \'#eeaaee\',
            \'#55BF3B\', \'#DF5353\', \'#7798BF\', \'#aaeeee\'],
        chart: {
            backgroundColor: {
                linearGradient: {x1: 0, y1: 0, x2: 1, y2: 1},
                stops: [
                    [0, \'#2a2a2b\'],
                    [1, \'#3e3e40\']
                ]
            },
            style: {
                fontFamily: \'\\\'Unica One\\\', sans-serif\'
            },
            plotBorderColor: \'#606063\'
        },
        title: {
            style: {
                color: \'#E0E0E3\',
                textTransform: \'uppercase\',
                fontSize: \'20px\'
            },
            text: \'Grafik Daily MT SMS\'
        },

        subtitle: {
            text: \'\'
        },

        yAxis: {
            gridLineColor: \'#707073\',
            labels: {
                style: {
                    color: \'#E0E0E3\'
                }
            },
            lineColor: \'#707073\',
            minorGridLineColor: \'#505053\',
            tickColor: \'#707073\',
            tickWidth: 1,
            title: {
                style: {
                    color: \'#A0A0A3\'
                }
            },
            title: {
                text: \'\'
            }
        },
        legend: {
            layout: \'vertical\',
            align: \'right\',
            verticalAlign: \'middle\'
        },

        xAxis: {
            gridLineColor: \'#707073\',
            labels: {
                style: {
                    color: \'#E0E0E3\'
                }
            },
            lineColor: \'#707073\',
            minorGridLineColor: \'#505053\',
            tickColor: \'#707073\',
            title: {
                text: \'Tanggal\',
                style: {
                    color: \'#fff\'

                }
            },
            categories: [
                        '; ?>

                                <?php unset($this->_sections['r']);
$this->_sections['r']['name'] = 'r';
$this->_sections['r']['loop'] = is_array($_loop=$this->_tpl_vars['tgl_print']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['r']['show'] = true;
$this->_sections['r']['max'] = $this->_sections['r']['loop'];
$this->_sections['r']['step'] = 1;
$this->_sections['r']['start'] = $this->_sections['r']['step'] > 0 ? 0 : $this->_sections['r']['loop']-1;
if ($this->_sections['r']['show']) {
    $this->_sections['r']['total'] = $this->_sections['r']['loop'];
    if ($this->_sections['r']['total'] == 0)
        $this->_sections['r']['show'] = false;
} else
    $this->_sections['r']['total'] = 0;
if ($this->_sections['r']['show']):

            for ($this->_sections['r']['index'] = $this->_sections['r']['start'], $this->_sections['r']['iteration'] = 1;
                 $this->_sections['r']['iteration'] <= $this->_sections['r']['total'];
                 $this->_sections['r']['index'] += $this->_sections['r']['step'], $this->_sections['r']['iteration']++):
$this->_sections['r']['rownum'] = $this->_sections['r']['iteration'];
$this->_sections['r']['index_prev'] = $this->_sections['r']['index'] - $this->_sections['r']['step'];
$this->_sections['r']['index_next'] = $this->_sections['r']['index'] + $this->_sections['r']['step'];
$this->_sections['r']['first']      = ($this->_sections['r']['iteration'] == 1);
$this->_sections['r']['last']       = ($this->_sections['r']['iteration'] == $this->_sections['r']['total']);
?>
                                        <?php echo ((is_array($_tmp=$this->_tpl_vars['tgl_print'][$this->_sections['r']['index']])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ',')); ?>

                                <?php endfor; endif; ?>
                        <?php echo '
                        ]

        },

        series: [{
                showInLegend: false,
                data: [
                '; ?>

                    <?php $_from = $this->_tpl_vars['mtfix']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['service'] => $this->_tpl_vars['i']):
?>
                        <?php $_from = $this->_tpl_vars['i']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tgl'] => $this->_tpl_vars['jumlah']):
?>
                                <?php echo ((is_array($_tmp=$this->_tpl_vars['jumlah'])) ? $this->_run_mod_handler('cat', true, $_tmp, ',') : smarty_modifier_cat($_tmp, ',')); ?>

                        <?php endforeach; endif; unset($_from); ?>
                    <?php endforeach; endif; unset($_from); ?>
                <?php echo '
                ]                    
                }]

    });
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer2.html", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>