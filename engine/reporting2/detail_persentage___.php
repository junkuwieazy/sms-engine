<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

include_once("/var/www/sms/engine/include/config.php");
include_once("/var/www/sms/engine/include/smarty/Smarty.class.php");
include_once("/var/www/sms/engine/include/adodb/adodb.inc.php");

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

?>


<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" id="clear">&times;</button>
    <h4 class="modal-title">Detail MT Persentage : <?php echo $_GET['tgl'];?></h4>
  </div>
  <div class="modal-body">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>NO</th>
          <th>SUBJECT</th>
          <th>TELCO</th>
          <th>SHORTCODE</th>
          <th>CHARGE</th>
          <th>DELIVERED</th>
          <th>TOTAL MT</th>
          <th>PERSENTAGE</th>
        </tr>
      </thead>

      <tbody>
        <?php
        $total_dn = 0;
        $i = 1;

          $ar_tgl = explode("-",$_GET['tgl']); // 2016-08-12

          if(strlen($ar_tgl[2]) == 1) {
            $tanggals = $ar_tgl[0].'-'.$ar_tgl[1].'-0'.$ar_tgl[2];
          } else {
            $tanggals = $_GET['tgl'];
          }

          $str = "";
          if( $_GET['telco'] <> "All" ) {
            $str = " AND telco='".$_GET['telco']."'";
          } else {
            $str = '';
          }

          if(!empty($_GET['s'])) {
            $b = explode('-',$_GET['s']);
            foreach($b as $bb) {
             if(!empty($bb)) {
              $c = "'".join("','",$b)."'";
            }
          }

          $idApp = " AND appid IN(".$c.")";
        } else {
          $idApp = '';
        }

        if( $_GET['sc'] <> "All" ) {
          $sCode = " AND shortcode = '".$_GET['sc']."'";
        } else {
          $sCode = '';
        }
        
        if( $tanggals == date("Y-m-d") ) {

          $sqlFP = "SELECT appid,subject,telco,shortcode,charge,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND dn_status = 'DELIVERED' AND subject LIKE '%FIRSTPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

          $sqlDP = "SELECT appid,subject,telco,shortcode,charge,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND dn_status = 'DELIVERED' AND subject LIKE '%DAILYPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

          $sqlTotalFP = "SELECT sum(jumlah) as t_jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND subject LIKE '%FIRSTPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

          $sqlTotalDP = "SELECT sum(jumlah) as t_jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND subject LIKE '%DAILYPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

        }
        else {
          $sqlFP = "SELECT appid,subject,telco,shortcode,charge,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND dn_status = 'DELIVERED' AND subject LIKE '%FIRSTPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

          $sqlDP = "SELECT appid,subject,telco,shortcode,charge,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND dn_status = 'DELIVERED' AND subject LIKE '%DAILYPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

          $sqlTotalFP = "SELECT sum(jumlah) as t_jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND subject LIKE '%FIRSTPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

          $sqlTotalDP = "SELECT sum(jumlah) as t_jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND subject LIKE '%DAILYPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

        }

        // $data = array();
        // echo $sql."<br/>";
        $rsFp = $db->Execute($sqlFP) or die( $db->ErrorMsg() );
        if ( $rsFp->RecordCount() > 0 ) {
          while ( !$rsFp->EOF ) {
           $data['firstpush'][] = $rsFp->fields;
           $total[] = $rsFp->fields[6];
           $rsFp->MoveNext();
          }
        }

       $rsDP = $db->Execute($sqlDP) or die( $db->ErrorMsg() );
       if ( $rsDP->RecordCount() > 0 ) {
        while ( !$rsDP->EOF ) {
         $data['dailypush'][] = $rsDP->fields;

         $total[] = $rsDP->fields[6];
         $rsDP->MoveNext();
        }
       }  

        // print_r($data);

        // $dataTotal = array();
      $rsTFp = $db->Execute($sqlTotalFP) or die( $db->ErrorMsg() );
      if ( $rsTFp->RecordCount() > 0 ) {
      while ( !$rsTFp->EOF ) {
       $dataTotal['firstpush'][] = $rsTFp->fields[0];
       $rsTFp->MoveNext();
        }
      }

       $rsTDP = $db->Execute($sqlTotalDP) or die( $db->ErrorMsg() );
       if ( $rsTDP->RecordCount() > 0 ) {
        while ( !$rsTDP->EOF ) {
         $dataTotal['dailypush'][] = $rsTDP->fields[0];
         $rsTDP->MoveNext();
        }
       }


        // echo $sql;
       $i = 1;
       foreach($data as $key => $value) {
        foreach ($value as $k => $z) {
          $newdata[] = $z;
        }
      }

      foreach($dataTotal as $keyTotal => $valueTotal) {

        foreach ($valueTotal as $kTot => $zTot) {
          $newdataTotal[] = $zTot;
        }
      }

      foreach ($newdata as $y => $d) {

      ?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $d['subject'];?></td>
        <td style='text-align: center;'><?php echo $d['telco'];?></td>
        <td style='text-align: center;'><?php echo $d['shortcode'];?></td>
        <td style='text-align: center;'><?php echo $d['charge'];?></td>
        <td style='text-align: right;'><?php echo number_format($d['jml']);?></td>

        <?php
        foreach ($newdataTotal as $key => $value) {

           if($key == $y) {
            $count = ($d['jml'] / $value) * 100;
            echo "<td style='text-align: right;'>".number_format($value)."</td>";
            echo "<td style='text-align: center;'>".number_format($count,2,".",".")."%</td>";
          }
         }
        ?>
      </tr>
      <?php
      $i++;
        }

      ?>
     </tbody>
    </table>
  </div>
</div>

