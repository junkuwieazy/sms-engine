<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';
$msg = '';

if(!empty($_GET['filter'])) {
	$filter = " WHERE telco like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['telco']) && isset($_POST['charge']) && !empty($_POST['shortcode']) && !empty($_POST['userid']) && !empty($_POST['pwd']) && !empty($_POST['sid'])) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				$sql_add = "UPDATE mp_system.telco_sid SET telco = '".$_POST['telco']."',charge = '".$_POST['charge']."',shortcode = '".$_POST['shortcode']."',user_id = '".$_POST['userid']."',password = '".$_POST['pwd']."',sid = '".$_POST['sid']."',description='".$_POST['description']."' WHERE id= ".$_POST['id'];
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				$msg = "SID berhasil diedit";
			
			}
		}
		else {
			$sqlcek = "SELECT telco FROM mp_system.telco_sid WHERE telco = '".$_POST['telco']."' AND charge = '".$_POST['charge']."' AND shortcode = '".$_POST['shortcode']."' AND user_id = '".$_POST['userid']."' AND password = '".$_POST['pwd']."' AND sid='".$_POST['sid']."'";
			$rs = $db->Execute($sqlcek);
			if ( $rs->RecordCount() > 0 ) {
				$msg = "SID ".$_POST['sid']." sudah ada";
			}
			else {
				$sql_add = "INSERT INTO mp_system.telco_sid (telco,charge,shortcode,user_id,password,sid,description)
								VALUES ('".$_POST['telco']."','".$_POST['charge']."','".$_POST['shortcode']."','".$_POST['userid']."','".$_POST['pwd']."','".$_POST['sid']."','".$_POST['description']."')";
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				$msg = "SID berhasil ditambah";
			}
		}
		
	}
	else {
		echo $_POST['telco'].";".$_POST['charge'].";".$_POST['shortcode'].";".$_POST['userid'].";".$_POST['pwd'].";".$_POST['sid'];
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM mp_system.telco_sid WHERE id = ".$_GET['id'];
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$id_edit = $rs->fields[0];
				$telco_edit = $rs->fields[1];
				$charge_edit = $rs->fields[2];
				$shortcode_edit = $rs->fields[3];
				$userid_edit = $rs->fields[4];
				$pwd_edit = $rs->fields[5];
				$sid_edit = $rs->fields[6];
				$description_edit = $rs->fields[7];
				
				$rs->MoveNext();
			}
			
			$smarty->assign("id_edit",$id_edit);
			$smarty->assign("telco_edit",$telco_edit);
			$smarty->assign("charge_edit",$charge_edit);
			$smarty->assign("shortcode_edit",$shortcode_edit);
			$smarty->assign("userid_edit",$userid_edit);
			$smarty->assign("pwd_edit",$pwd_edit);
			$smarty->assign("sid_edit",$sid_edit);
			$smarty->assign("description_edit",$description_edit);
			$smarty->assign("action","edit");
		}
	}
	elseif($_GET['action'] == "delete") {
		$sql_add = "DELETE FROM mp_system.telco_sid WHERE id=".$_GET['id'];
		$db->Execute($sql_add) or die( $db->ErrorMsg() );
		$msg = "SID berhasil didelete";
	}
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM mp_system.telco_sid $filter LIMIT $begin, $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$id[] = $rs->fields[0];
		$telco[] = $rs->fields[1];
		$charge[] = $rs->fields[2];
		$shortcode[] = $rs->fields[3];
		$userid[] = $rs->fields[4];
		$pwd[] = $rs->fields[5];
		$sid[] = $rs->fields[6];
		$description[] = $rs->fields[7];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("id",$id);
	$smarty->assign("telco",$telco);
	$smarty->assign("charge",$charge);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("userid",$userid);
	$smarty->assign("pwd",$pwd);
	$smarty->assign("sid",$sid);
	$smarty->assign("description",$description);
	$smarty->assign("warna",$warna);
	
	$sql_count = "SELECT telco FROM mp_system.telco_sid $filter";
	$numresult = $db->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_POST['telco'], '', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_POST['telco'], '', 'class="text"');
}
$smarty->assign("dd_telco",$dd_telco);

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'list_charging.html');

?>