<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}
//echo "satu";
$smarty->assign("is_today","tdk");
$smarty->assign("subject_detail","tdk");

if( empty($_GET['submit']) ) {
	$_GET['submit'] = '';
	$h = 1;
	$h2 = 1;
	$tgl_now = date("d");
	$j_hari = $mFunc->get_jumlah_hari(date("m"));
}
else {
	$j_hari = $mFunc->get_jumlah_hari($_GET['bln']);
	if( $_GET['bln'] == date("m") ) {
		$h = 1;
		$h2 = 1;
		$tgl_now = date("d");
	}
	else {
		$h = 0;
		$h2 = 1;
		$tgl_now = $mFunc->get_jumlah_hari($_GET['bln']);
	}
}

if( empty($_SERVER['QUERY_STRING']) ) $smarty->assign("php_query_string","");
else $smarty->assign("php_query_string","?".$_SERVER['QUERY_STRING']);

$smarty->assign("submit",$_GET['submit']);
$fix_where = '';
if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
$jc = 1; $jj = 1;
$checkbox = "<table class='table table-responsive' border='1' cellpadding='0' cellspacing='0'><tr>";
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		if( empty($_GET['submit']) ) $checked = "checked";
		else {
			if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
			else $checked = "";
		}

		$checkbox .= "<td><input type='checkbox' name='check_list".$jj."' id='check_list".$jj."' value='".$rs->fields[0]."' $checked>".$rs->fields[1]."</td>";
		if( $jc == 5 ) {
			$checkbox .= '</tr>';
			$jc = 0;
		}
		$jc++;
		$jj++;
		$rs->MoveNext();
	}
}
$checkbox .= "<tr>
				<input class='btn btn-info' type='button' name='Check_All' value='Check All' onClick='CheckAll()'>
				<input class='btn btn-warning' type='button' name='Un_CheckAll' value='Uncheck All' onClick='UnCheckAll()'>
				<button type='submit' name='submit' value='Submit' class='btn btn-success'><i class='fa fa-search'></i> Search</button>
				<input type='hidden' name='active' value='report'>
                                <input type='hidden' name='menu' value='Daily Traffic'>
			</tr></table>";
$smarty->assign("checkbox",$checkbox);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="form-control"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="form-control"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="form-control"');
}
//$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="form-control"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("bln",$bulan);
$smarty->assign("thn",$tahun);

$smarty->assign("bln_now",date("m"));
$smarty->assign("thn_now",date("Y"));

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="form-control"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="form-control"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$bulan_traffic = date("m");
	$tahun_traffic = date("Y");
	$smarty->assign("s_name","All");
	$str_service = "";
}
else {
	$bulan_traffic = $_GET['bln'];
	$tahun_traffic = $_GET['thn'];
	$fix_where = '';
	$j_check = 0;

	$service_in = ""; $is_check = false; $s_name = "";
	for($r=1;$r<=$jj;$r++) {
		if( isset($_GET['check_list'.$r]) ) {
			$j_check += 1;
			$j_check_r = $r;
			$service_in .= "'".$_GET['check_list'.$r]."',";
			$is_check = true;
			$s_name .= $mFunc->get_description($db, $_GET['check_list'.$r]).", ";
		}
	}
	$service_in = ereg_replace(',$','',$service_in);

	if( $is_check ) $smarty->assign("s_name",$s_name);
	else $smarty->assign("s_name","All");

	if( $service_in <> "" ) $str_service = "AND appid in ( ".$service_in." )";
	else $str_service = "AND appid = '0000'";

}
$smarty->assign("service",$_GET['service']);

$smarty->assign("bulan_traffic",$bulan_traffic);
$smarty->assign("tahun_traffic",$tahun_traffic);

if( $_SESSION["ses_username"] == "reportuser" ) $persen = 0.5;
else $persen = 1;

$mo_tot = 0; $mt_ok_tot = 0; $mt_ok_totf = 0; $mt_nok_tot = 0; $total_mt_tot = 0; $gross_tot = 0; $net_tot = 0; $persentage_tot = 0; $mo_reg_tot = 0; $mo_unreg_tot = 0;

for($i=$h;$i<=$tgl_now;$i++) {
	if($h == 1) $k = $i - 1;
	else $k = $i;
	$hasil = $tgl_now - $i;
	if( $hasil > 0 ) {
		if( strlen($hasil) == 1) $tgl_traffic = "0".$hasil;
		else $tgl_traffic = $hasil;

		$tgl_print[$k] = $hasil;

		//
		$mo_reg[$k] = 0;
		$sql_reg = "SELECT jumlah
									FROM mp_system.summary_reg_".$tahun_traffic.$bulan_traffic."
									WHERE 1 $str_sql $str_service $fix_where
									AND mo_type = 'REG'
									AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		/*
		$sql_reg = "SELECT count( 1 ) AS jml
					FROM app_sms.msisdn_subscriber
					WHERE 1 $str_sql $str_service AND date_format( date_reg, '%Y-%m-%d' ) = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		*/
		//echo $sql_reg."<BR>";
		$rs_reg = $db_app->Execute($sql_reg) or die( $db_app->ErrorMsg() );
		if ( $rs_reg->RecordCount() > 0 ) {
			while ( !$rs_reg->EOF ) {
				$mo_reg[$k] += round($persen * $rs_reg->fields[0]);
				$rs_reg->MoveNext();
			}
		}

		$mo_unreg[$k] = 0;
		$sql_unreg = "SELECT jumlah
									FROM mp_system.summary_reg_".$tahun_traffic.$bulan_traffic."
									WHERE 1 $str_sql $str_service $fix_where
									AND mo_type = 'UNREG'
									AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		/*
		$sql_unreg = "SELECT count( 1 ) AS jml
					FROM app_sms.msisdn_subscriber
					WHERE 1 $str_sql $str_service AND date_format( date_unreg, '%Y-%m-%d' ) = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		*/
		//echo $sql_unreg."<BR>";
		$rs_unreg = $db_app->Execute($sql_unreg) or die( $db_app->ErrorMsg() );
		if ( $rs_unreg->RecordCount() > 0 ) {
			while ( !$rs_unreg->EOF ) {
				$mo_unreg[$k] += round($persen * $rs_unreg->fields[0]);
				$rs_unreg->MoveNext();
			}
		}
		//

		$mo[$k] = 0;
		$sql_mo = "SELECT sum( jumlah )
								FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic."
								WHERE datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' $str_sql $str_service $fix_where
								AND subject NOT LIKE 'push%'";
		$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
		if ( $rs_mo->RecordCount() > 0 ) {
			while ( !$rs_mo->EOF ) {
				$mo[$k] += round($persen * $rs_mo->fields[0]);
				$rs_mo->MoveNext();
			}
		}
		/*
		///MT firstpush
		$mt_okf[$k] = 0;
		$mt_c_okf[$k] = 0;
		$mt_nokf[$k] = 0;
		$total_mt_cf[$k] = 0;
		$total_mtf[$k] = 0;
		$grossf[$k] = 0;
		$netf[$k] = 0;

		$sqlf = "SELECT telco, charge, dn_status, sum( jumlah )
							FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic."
							WHERE datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' AND subject like '%;FIRSTPUSH' $str_sql $str_service $fix_where
							GROUP BY telco,charge,dn_status";
		//echo $sql."<br>";
		$rsf = $db->Execute($sqlf) or die( $db->ErrorMsg() );

		if ( $rsf->RecordCount() > 0 ) {

			while ( !$rsf->EOF ) {

				$ar_price_cf = explode("-",$rsf->fields[1]);
				$price_cf = $ar_price_cf[1];

				if( $rsf->fields[2] == "DELIVERED" ) {

					if(in_array( $bulan_traffic.$tahun_traffic , array("032011","042011") )) {
						$telco_sharef = 0.5;
					}
					else {
						$telco_sharef = $config['share'][$rsf->fields[0]];
					}

					$ar_pricef = explode("-",$rsf->fields[1]);
					$pricef = $ar_pricef[1];

					$mt_okf[$k] += round($persen * $rsf->fields[3]);
					if( $pricef == 250 ) $pricef = 0;

					if( $pricef > 400 ) $mt_c_okf[$k] += round($persen * $rsf->fields[3]);

					$grossf[$k] += (round($persen * $rsf->fields[3])) * $pricef;
					$netf[$k] += (round($persen * $rsf->fields[3])) * ($pricef * $telco_sharef );

					$tot_gross[$k] += $grossf[$k];
					$tot_net[$k] += $netf[$k];

				}
				else {
					$mt_nokf[$k] += round($persen * $rsf->fields[3]);
				}

				$total_mtf[$k] += round($persen * $rsf->fields[3]);
				if( $price_cf > 400 ) $total_mt_cf[$k] += round($persen * $rsf->fields[3]);

				$rsf->MoveNext();
			}
		}
		///MT firstpush
		*/

		$mt_ok[$k] = 0;
		$mt_c_ok[$k] = 0;
		$mt_nok[$k] = 0;
		$total_mt_c[$k] = 0;
		$total_mt[$k] = 0;
		$gross[$k] = 0;
		$net[$k] = 0;

		$sql = "SELECT telco, charge, dn_status, sum( jumlah )
							FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic."
							WHERE datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' $str_sql $str_service $fix_where
							GROUP BY telco,charge,dn_status";
		//echo $sql."<br>";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );

		if ( $rs->RecordCount() > 0 ) {

			while ( !$rs->EOF ) {

				$ar_price_c = explode("-",$rs->fields[1]);
				$price_c = $ar_price_c[1];

				if( $rs->fields[2] == "DELIVERED" ) {

					$telco_share = $config['share'][$rs->fields[0]];

					$ar_price = explode("-",$rs->fields[1]);
					$price = $ar_price[1];

					$mt_ok[$k] += round($persen * $rs->fields[3]);
					if( $price == 250 ) $price = 0;

					if( $price > 400 ) $mt_c_ok[$k] += round($persen * $rs->fields[3]);

					$gross[$k] += (round($persen * $rs->fields[3])) * $price;
					$net[$k] += (round($persen * $rs->fields[3])) * ($price * $telco_share );

					$tot_gross[$k] += $gross[$k];
					$tot_net[$k] += $net[$k];

				}
				else {
					$mt_nok[$k] += round($persen * $rs->fields[3]);
				}

				$total_mt[$k] += round($persen * $rs->fields[3]);
				if( $price_c > 400 ) $total_mt_c[$k] += round($persen * $rs->fields[3]);

				$rs->MoveNext();
			}
		}

		//$persentage[$k] = ($mt_ok[$k] / $total_mt[$k]) * 100;
		$persentage[$k] = ($mt_c_ok[$k] / $total_mt_c[$k]) * 100;

		//$persentagef[$k] = ($mt_c_okf[$k] / $total_mt_cf[$k]) * 100;

		$mo_tot += $mo[$k];
		$mt_ok_tot += $mt_ok[$k];
		$mt_nok_tot += $mt_nok[$k];
		$total_mt_tot += $total_mt[$k];
		$gross_tot += $gross[$k];
		$net_tot += $net[$k];

		//first
		//$mt_ok_totf += $mt_okf[$k];
		//$mt_nok_totf += $mt_nokf[$k];
		//$total_mt_totf += $total_mtf[$k];
		//$gross_totf += $grossf[$k];
		//$net_totf += $netf[$k];

		$mo_reg_tot += $mo_reg[$k];
		$mo_unreg_tot += $mo_unreg[$k];

		$grand_tot_gross = $gross_tot + $gross_totf;
		$grand_tot_net = $net_tot + $net_totf;

	}
}

//$persentage[$k] = ($mt_c_ok[$k] / $total_mt_c[$k]) * 100;

///
if( $_GET['bln'] == date("m") || !isset($_GET['bln']) ) {
	//
	$smarty->assign("is_today","ya");
	$tgl_printt = date("j");

	//
	$mo_regt = 0;
	$sql_regt = "SELECT sum(jumlah) as jml FROM app_sms.msisdn_hourly_reg WHERE 1 $str_service $fix_where and tanggal='".date("Y-m-d")."'";
	/*
	$sql_regt = "SELECT count( 1 ) AS jml
				FROM app_sms.msisdn_subscriber
				WHERE 1 $str_sql $str_service $fix_where AND date_format( date_reg, '%Y-%m-%d' ) = '".date("Y-m-d")."'";
*/
	//echo $sql_reg."<BR>";
	$rs_regt = $db->Execute($sql_regt) or die( $db->ErrorMsg() );
	if ( $rs_regt->RecordCount() > 0 ) {
		while ( !$rs_regt->EOF ) {
			$mo_regt += round($persen * $rs_regt->fields[0]);
			$rs_regt->MoveNext();
		}
	}

	$mo_unregt = 0;
	$sql_unregt = "SELECT sum(jumlah) as jml FROM app_sms.msisdn_hourly_unreg WHERE 1 $str_service $fix_where and tanggal='".date("Y-m-d")."'";
	/*
	$sql_unregt = "SELECT count( 1 ) AS jml
				FROM app_sms.msisdn_subscriber
				WHERE 1 $str_sql $str_service $fix_where AND date_format( date_unreg, '%Y-%m-%d' ) = '".date("Y-m-d")."'";
	*/
	//echo $sql_unreg."<BR>";
	$rs_unregt = $db->Execute($sql_unregt) or die( $db->ErrorMsg() );
	if ( $rs_unregt->RecordCount() > 0 ) {
		while ( !$rs_unregt->EOF ) {
			$mo_unregt += round($persen * $rs_unregt->fields[0]);
			$rs_unregt->MoveNext();
		}
	}
	//

	$mot = 0;
	$sql_mot = "SELECT sum( jumlah )
							FROM mp_system.mt_summary_hourly
							WHERE 1 $str_sql $str_service $fix_where
							AND subject NOT LIKE 'push%'";
	$rs_mot = $db->Execute($sql_mot) or die( $db->ErrorMsg() );
	if ( $rs_mot->RecordCount() > 0 ) {
		while ( !$rs_mot->EOF ) {
			$mot += round($persen * $rs_mot->fields[0]);
			$rs_mot->MoveNext();
		}
	}
/*
	//mt firstpush today
	$mt_oktf = 0;
	$mt_c_oktf = 0;
	$mt_noktf = 0;
	$total_mt_ctf = 0;
	$total_mttf = 0;
	$grosstf = 0;
	$nettf = 0;

	$sqltf = "SELECT telco, charge, dn_status, sum( jumlah )
						FROM mp_system.mt_summary_hourly
						WHERE 1 AND subject like '%;FIRSTPUSH' $str_sql $str_service $fix_where
						GROUP BY telco,charge,dn_status";
	//echo $sqltf."<br>";
	$rstf = $db->Execute($sqltf) or die( $db->ErrorMsg() );

	if ( $rstf->RecordCount() > 0 ) {

		while ( !$rstf->EOF ) {

			$ar_price_ctf = explode("-",$rstf->fields[1]);
			$price_ctf = $ar_price_ctf[1];

			if( $rstf->fields[2] == "DELIVERED" ) {

				$telco_sharef = $config['share'][$rstf->fields[0]];

				$ar_pricetf = explode("-",$rstf->fields[1]);
				$pricetf = $ar_pricetf[1];

				$mt_oktf += round($persen * $rstf->fields[3]);
				if( $pricetf == 250 ) $pricetf = 0;

				if( $pricetf > 400 ) $mt_c_oktf += round($persen * $rstf->fields[3]);

				$grosstf += (round($persen * $rstf->fields[3])) * $pricetf;
				$nettf += (round($persen * $rstf->fields[3])) * ($pricetf * $telco_sharef );

				$tot_grosst += $grosstf;
				$tot_nett += $nettf;

			}
			else {
				$mt_noktf += round($persen * $rstf->fields[3]);
			}

			$total_mttf += round($persen * $rstf->fields[3]);
			if( $price_ctf > 400 ) $total_mt_ctf += round($persen * $rstf->fields[3]);

			$rstf->MoveNext();
		}
	}
	//mt firstpush today
*/
	$mt_okt = 0;
	$mt_c_okt = 0;
	$mt_nokt = 0;
	$total_mt_ct = 0;
	$total_mtt = 0;
	$grosst = 0;
	$nett = 0;

	//daily
	$mt_okt = 0;
	$mt_c_okt = 0;
	$mt_nokt = 0;
	$total_mt_ct = 0;
	$total_mtt = 0;
	$grosst = 0;
	$nett = 0;

	$sqlt = "SELECT telco, charge, dn_status, sum( jumlah )
						FROM mp_system.mt_summary_hourly
						WHERE 1 $str_sql $str_service $fix_where
						GROUP BY telco,charge,dn_status";
	//echo $sqlt."<br>";
	$rst = $db->Execute($sqlt) or die( $db->ErrorMsg() );

	if ( $rst->RecordCount() > 0 ) {

		while ( !$rst->EOF ) {

			$ar_price_ct = explode("-",$rst->fields[1]);
			$price_ct = $ar_price_ct[1];

			if( $rst->fields[2] == "DELIVERED" ) {

				$telco_share = $config['share'][$rst->fields[0]];

				$ar_pricet = explode("-",$rst->fields[1]);
				$pricet = $ar_pricet[1];

				$mt_okt += round($persen * $rst->fields[3]);
				if( $pricet == 250 ) $pricet = 0;

				if( $pricet > 400 ) $mt_c_okt += round($persen * $rst->fields[3]);

				$grosst += (round($persen * $rst->fields[3])) * $pricet;
				$nett += (round($persen * $rst->fields[3])) * ($pricet * $telco_share );

				$tot_grosst += $grosst;
				$tot_nett += $nett;

			}
			else {
				$mt_nokt += round($persen * $rst->fields[3]);
			}

			$total_mtt += round($persen * $rst->fields[3]);
			if( $price_ct > 400 ) $total_mt_ct += round($persen * $rst->fields[3]);

			$rst->MoveNext();
		}
	}

	//$persentage[$k] = ($mt_ok[$k] / $total_mt[$k]) * 100;
	$persentaget = ($mt_c_okt / $total_mt_ct) * 100;
	//$persentagetf = ($mt_c_oktf / $total_mt_ctf) * 100;

	$mo_tot += $mot;
	$mt_ok_tot += $mt_okt;
	$mt_nok_tot += $mt_nokt;
	$total_mt_tot += $total_mtt;
	$gross_tot += $grosst;
	$net_tot += $nett;
	//

	$mo_reg_tot += $mo_regt;
	$mo_unreg_tot += $mo_unregt;

}
///

$grand_tot_gross += $tot_grosst;
$grand_tot_net += $tot_nett;

$persentage_tot = ($mt_ok_tot / $total_mt_tot) * 100;
$persentage_totf = ($mt_ok_totf / $total_mt_totf) * 100;

$smarty->assign("grand_tot_gross",$grand_tot_gross);
$smarty->assign("grand_tot_net",$grand_tot_net);

$smarty->assign("mo",$mo);
$smarty->assign("mt_ok",$mt_ok);
$smarty->assign("mt_nok",$mt_nok);
$smarty->assign("total_mt",$total_mt);
$smarty->assign("gross",$gross);
$smarty->assign("net",$net);
$smarty->assign("tgl_print",$tgl_print);
$smarty->assign("mo_tot",$mo_tot);
$smarty->assign("mt_ok_tot",$mt_ok_tot);
$smarty->assign("mt_nok_tot",$mt_nok_tot);
$smarty->assign("total_mt_tot",$total_mt_tot);
$smarty->assign("gross_tot",$gross_tot);
$smarty->assign("net_tot",$net_tot);
$smarty->assign("persentage",$persentage);
$smarty->assign("persentage_tot",$persentage_tot);

//first
$smarty->assign("mt_okf",$mt_okf);
$smarty->assign("mt_nokf",$mt_nokf);
$smarty->assign("total_mtf",$total_mtf);
$smarty->assign("grossf",$grossf);
$smarty->assign("netf",$netf);

$smarty->assign("mt_ok_totf",$mt_ok_totf);
$smarty->assign("mt_nok_totf",$mt_nok_totf);
$smarty->assign("total_mt_totf",$total_mt_totf);
$smarty->assign("gross_totf",$gross_totf);
$smarty->assign("net_totf",$net_totf);

$smarty->assign("persentagef",$persentagef);
$smarty->assign("persentage_totf",$persentage_totf);

$smarty->assign("mo_reg",$mo_reg);
$smarty->assign("mo_regt",$mo_regt);
$smarty->assign("mo_reg_tot",$mo_reg_tot);
$smarty->assign("mo_unreg",$mo_unreg);
$smarty->assign("mo_unregt",$mo_unregt);
$smarty->assign("mo_unreg_tot",$mo_unreg_tot);

$smarty->assign("mot",$mot);
$smarty->assign("mt_okt",$mt_okt);
$smarty->assign("mt_nokt",$mt_nokt);
$smarty->assign("total_mtt",$total_mtt);
$smarty->assign("grosst",$grosst);
$smarty->assign("nett",$nett);
$smarty->assign("tgl_printt",$tgl_printt);
$smarty->assign("persentaget",$persentaget);
$smarty->assign("persentagetf",$persentagetf);

//first
$smarty->assign("motf",$motf);
$smarty->assign("mt_oktf",$mt_oktf);
$smarty->assign("mt_noktf",$mt_noktf);
$smarty->assign("total_mttf",$total_mttf);
$smarty->assign("grosstf",$grosstf);
$smarty->assign("nettf",$nettf);

$smarty->assign("tot_gross",$tot_gross);
$smarty->assign("tot_net",$tot_net);
$smarty->assign("tot_grosst",$tot_grosst);
$smarty->assign("tot_nett",$tot_nett);

$rata_gross = $gross_tot / $tgl_now;
$rata_net = $net_tot / $tgl_now;

$gross_projection = $rata_gross * $j_hari;
$net_projection = $rata_net * $j_hari;

$smarty->assign("gross_projection",$gross_projection);
$smarty->assign("net_projection",$net_projection);

//first
$rata_grossf = $gross_totf / $tgl_now;
$rata_netf = $net_totf / $tgl_now;

$gross_projectionf = $rata_grossf * $j_hari;
$net_projectionf = $rata_netf * $j_hari;

$smarty->assign("gross_projectionf",$gross_projectionf);
$smarty->assign("net_projectionf",$net_projectionf);

/*
$smarty->assign("subject_dlv",$subject_dlv);
$smarty->assign("subject_re",$subject_re);

$smarty->assign("j_subject_dlv",$j_subject_dlv);
$smarty->assign("j_subject_re",$j_subject_re);
*/
$smarty->display(TEMPLATES_DIR.'daily_traffic_dbg.html');

?>
