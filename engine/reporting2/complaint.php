<?php
include_once("/var/www/sms/engine/include/config.php");
include_once('/var/www/sms/engine/include/smarty/Smarty.class.php');
include_once('/var/www/sms/engine/include/adodb/adodb.inc.php');

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

$reg_today = 0;
$reg_not_today = 0;
$never_reg = 0;
$reg_total = 0;

$sql = "SELECT msisdn FROM app_sms.msisdn_blacklist WHERE 1 and blacklist_date='".$_GET['tgl']."' GROUP BY msisdn";
//echo $sql."<br/>";
$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		
		$sqlc = "SELECT date_format( date_reg, '%Y-%m-%d' ) AS tgl FROM app_sms.msisdn_subscriber WHERE 1 AND msisdn='".$rs->fields[0]."' ORDER BY tgl DESC LIMIT 1";
		//echo $sqlc."<br/>";
		$rsc = $db_app->Execute($sqlc) or die( $db_app->ErrorMsg() );
		if ( $rsc->RecordCount() > 0 ) {
			while ( !$rsc->EOF ) {
				
				if( $rsc->fields[0] == $_GET['tgl'] ) {
					$reg_today += 1;
				}
				else {
					$reg_not_today += 1;
				}
				
				$rsc->MoveNext();
			}
		}
		else {
			$never_reg += 1;
		}
		
		$reg_total += 1;
		
		$rs->MoveNext();
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Helvetica, sans-serif;
	font-weight: bold;
}
.style2 {
	font-family: Arial, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style3 {
	font-family: Arial, Arial, Helvetica, sans-serif;
	size:10px;
	font-weight: normal;
}
-->
</style>
</head>

<body>

<div align="center">
  <p class="style1">Complaint : <?php echo $_GET['tgl'];?></p>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#0099FF"><table width="100%"  border="0" cellspacing="1" cellpadding="3">
        <tr bgcolor="#FFFFFF">
          <td><div align="center" class="style2">NO</div></td>
          <td><div align="center" class="style2">DATE REG </div></td>
          <td><div align="center" class="style2">JUMLAH</div></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td><div align="center" class="style3">1</div></td>
          <td><div align="left" class="style3">TODAY</div></td>
          <td><div align="center" class="style3"><?php echo $reg_today;?></div></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td><div align="center" class="style3">2</div></td>
          <td><div align="left" class="style3">NOT TODAY </div></td>
          <td><div align="center" class="style3"><?php echo $reg_not_today;?></div></td>
        </tr>
		<tr bgcolor="#FFFFFF">
          <td><div align="center" class="style3">3</div></td>
          <td><div align="left" class="style3">NEVER REG </div></td>
          <td><div align="center" class="style3"><?php echo $never_reg;?></div></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td><span class="style3"></span></td>
          <td><div align="left" class="style3">TOTAL</div></td>
          <td><div align="center" class="style3"><strong><?php echo $reg_total;?></strong></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
</body>
</html>
