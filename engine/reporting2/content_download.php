<?
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if(!empty($_GET['msg'])) {
	$msg = $_GET['msg'];
} else {
	$msg = "";
}

$smarty->assign("smt", "Submit");

$kindContent = array('tone' => 'True Tone','ap' => 'Animated Picture', 'cp' => 'Color Picture', 'vc' => 'Video', 'ag' => 'Android Games', 'aa' => 'Android Apps', 'ft' => 'Full-Track');

if(isset($_POST['ctn'])) {
	$content_service = $mFunc->contentDropDownContent($kindContent,$kindContent, 'ctn',$_POST['ctn'], 'All', 'class="form-control"');
	$smarty->assign("content_service",$content_service);
} else {
	$content_service = $mFunc->contentDropDownContent($kindContent,$kindContent, 'ctn','', 'All', 'class="form-control"');
	$smarty->assign("content_service",$content_service);
}


if(isset($_POST['Submit'])) {
	//check data empty
	if(empty($_POST['singer'])) { $singer = 0; } else { $singer = $_POST['singer']; }
	if(empty($_POST['description'])) { $description = 0; } else { $description = $_POST['description']; }
	if(empty($_POST['partner'])) { $partner_id = 0; } else { $partner_id = $_POST['partner']; }
	

	if(!empty($_POST['ctn']) || !empty($_POST['title']) || !empty($_POST['fileContent']) || count($_POST['check_list']) > 0) {
		//Path
		$path = '/var/www/sms/files/'.$_POST['ctn'].'/'.date('Y').'/'.date('m').'/';
		$pathUrl = 'http://103.29.215.161:8319/sms/files/'.$_POST['ctn'].'/'.date('Y').'/'.date('m').'/';

		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}

		chmod($path, 0777);

		//check code
		$code = "SELECT content_code FROM content_download.cd_content WHERE content_type = '".$_POST['ctn']."' ORDER BY content_code ASC LIMIT 1";
		$rsCode = $db_cd->Execute($code) or die( $db_cd->ErrorMsg() );
		if ( $rsCode->RecordCount() > 0 ) {

			while ( !$rsCode->EOF ) {

				// Get Code
				$firstCode = substr($rsCode->fields['content_code'],0,2);
				$generateCode = substr($rsCode->fields['content_code'],2) + 1;
				$rsCode->MoveNext();
			} // end while
		} else {

			$contentCode = array('tone' => 12,'cp' => 13,'ap' => 14,'vc' => 17,'ag' => 18,'aa' => 19,'ft' => 20);
			// Get Code
			$firstCode = $contentCode[$_POST['ctn']];
			$generateCode = 1;
		}


		//FILE SYSTEM

		//for ($i=0; $i < count($_POST['check_list']); $i++) { 

		// Uniq
		$uniq = date('YmdHis');
		$id = rand(0,99999);
		// Content
		$typ = explode('/', $_FILES['fileContent']['type']);		
		$checkContent = $_FILES['fileContent']['size'];
		if($checkContent > 0) {	
			$content = $typ[1];	
			// Preview
			$ty = explode('/', $_FILES['filePreview']['type']);
			$preview = $ty[1];
			$check = $_FILES['filePreview']['size'];
			if($check > 0) {
				if($preview == "jpg" || $preview == "png" || $preview == "jpeg" || $preview == "gif" ) {
					if (move_uploaded_file($_FILES["filePreview"]["tmp_name"], $path.$uniq.'-'.$id.'-preview.'.$preview)) {
						$filePreview = $path.$uniq.'-'.$id.'-preview.'.$preview.'~'.$pathUrl.$uniq.'-'.$id.'-preview.'.$preview;
					} else {
						echo "string 2 -------------";
					}

				} else {
					echo "string 3 -------------"; 
				}
			} else {
				$filePreview = 0;
			}


			// Content
			if (move_uploaded_file($_FILES["fileContent"]["tmp_name"], $path.$uniq.'-'.$id.'-content.'.$content)) {

				$URLPATH = $pathUrl.$uniq.'-'.$id.'-content.'.$content;
				$url = "http://api.bitly.com/v3/shorten?login=mobiwin&apiKey=R_5fe21d7da0d84f89942bd2c44a30a456&longUrl=".$URLPATH;
				$ch = curl_init($url);
				if ($ch == FALSE) {
					$msg = "Hubungan ke bit.ly gagal";
				} else {
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_VERBOSE, 0);

					$result = curl_exec($ch);
                        // {"status_code":200,"status_txt":"OK","data":{"url":"http://bit.ly/2g7lZ9U","hash":"2g7lZ9U","global_hash":"2g7lZqq","long_url":"http://122.248.32.27:4000/phpmyadmin/tbl_structure.php?db=smsgw_log","new_hash":1}}

					if (json_decode($result)->status_code == 200) {



						// Var
						$content_type = $_POST['ctn'];
						$content_code = $firstCode.$generateCode;
						$title = $_POST['title'];
						$entry_date = date('Y-m-d H:i:s');
						$file_url = $path.$uniq.'-'.$id.'-content.'.$content;
						$content = ''; // null 
						$access = '0';
						$previewa = $filePreview;
						$status = '1';
						$short_url = json_decode($result)->data->url;

						// Insert
						$save = "INSERT INTO content_download.cd_content (content_type,content_code,singer,title,description,entry_date,file_url,content,partner_id,access,preview,status,short_url)
						VALUES('$content_type','$content_code','$singer','$title','$description','$entry_date','$file_url','$content','$partner_id','$access','$previewa','$status','$short_url')";

						$resultSave = $db_cd->Execute($save) or die( $db_cd->ErrorMsg() );

						if($resultSave) {
							header("Location: content_download.php?active=download&menu=Content%20Download&msg=Content baru berhasil ditambahkan");
						} else {
							$msg = "Content baru gagal ditambahkan";
						}
					} else {
						$msg = "Sambungan ke Bit.ly Gagal";
					}
				}
			} else {
				$msg = "File Content Gagal Upload";
			}
		} else {
			$msg = "File Content Tidak Support"; 
		}
	} else {
		$msg = "Form tidak boleh kosong";
	}
} 




if(isset($_POST['SubmitEdit'])) {
	if(empty($_POST['content']) ) {
		$msg = "Content harus diisi"; 
	}
	elseif( $_POST['appid'] == "all" ) {
		$msg = "Pilih content yang akan di isi"; 
	}
	else {
		$sql = "UPDATE app_sms.content_text SET appid = '".$_POST['appid']."', category='".$_POST['category']."', content='".$_POST['content']."', publish_date='".$_POST['publish_date']."', publish_seq=".$_POST['publish_seq']." WHERE id=".$_POST['id'];
		$result = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );

		if($result) {
			$msg = "Edit content berhasil";
		}
		else {
			$msg = "Edit gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "edit") {
	$smarty->assign("smt", "SubmitEdit");
	$sqle = "SELECT category,content,publish_date,publish_seq,id FROM app_sms.content_text WHERE id=".$_GET['id'];
	$rse = $db_app->Execute($sqle) or die( $db_app->ErrorMsg() );
	if ( $rse->RecordCount() > 0 ) {
		while ( !$rse->EOF ) {
			$categorye = $rse->fields[0];
			$contente = $rse->fields[1];
			$publish_datee = $rse->fields[2];
			$publish_seqe = $rse->fields[3];
			$ide = $rse->fields[4];

			$rse->MoveNext();
		}

		$smarty->assign("ide",$ide);
		$smarty->assign("categorye",$categorye);
		$smarty->assign("contente",$contente);
		$smarty->assign("publish_datee",$publish_datee);
		$smarty->assign("publish_seqe",$publish_seqe);
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "hapuz") {
	$sqlDel = "DELETE FROM content_download.cd_content WHERE content_id=".$_GET['id'];
	$resultDel = $db_cd->Execute($sqlDel) or die( $db_cd->ErrorMsg() );

	if($resultDel) {
		$msg = "Hapus content berhasil";
	}
	else {
		$msg = "Hapus gagal, silahkan ulangi lagi!";
	}
}


$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 10;

// ONLOAD DATA
if( isset($_POST['ctn']) && $_POST['ctn'] <> "all") {
	$sql = "SELECT * FROM content_download.cd_content WHERE content_type = '".$_POST['ctn']."' ORDER BY entry_date DESC LIMIT $begin, $limit";
	$sql_count = "SELECT * FROM content_download.cd_content WHERE content_type = '".$_POST['ctn']."'";
	$sql_category = "SELECT * FROM content_download.cd_category WHERE content_type = '".$_POST['ctn']."'";
} elseif( isset($_GET['cntn']) && !empty($_GET['cntn'])) {
	$sql = "SELECT * FROM content_download.cd_content WHERE content_type = '".$_GET['cntn']."' ORDER BY entry_date DESC LIMIT $begin, $limit ";
	$sql_count = "SELECT * FROM content_download.cd_content WHERE content_type = '".$_GET['cntn']."'";
	$sql_category = "SELECT * FROM content_download.cd_category WHERE content_type = '".$_GET['cntn']."'";
} elseif( isset($_GET['tittleBtn']) && !empty($_GET['titleSearch']) ){
	$sql = "SELECT * FROM content_download.cd_content WHERE title LIKE '%".$_GET['titleSearch']."%' ORDER BY entry_date DESC LIMIT $begin, $limit ";
	$sql_count = "SELECT * FROM content_download.cd_content";
	$sql_category = "SELECT * FROM content_download.cd_category";
	$smarty->assign("titleSearch",$_GET['titleSearch']);
}else {
	$sql = "SELECT * FROM content_download.cd_content ORDER BY entry_date DESC LIMIT $begin, $limit ";
	$sql_count = "SELECT * FROM content_download.cd_content";
	$sql_category = "SELECT * FROM content_download.cd_category";
}


$rsCat = $db_cd->Execute($sql_category) or die( $db_cd->ErrorMsg() );
$jc = 1; $jj = 1;
$checkbox = "<table class='table table-responsive' border='1' cellpadding='0' cellspacing='0'><tr>";
if ( $rsCat->RecordCount() > 0 ) {
	while ( !$rsCat->EOF ) {
		if( empty($_GET['submit']) ) $checked = "checked";
		else {
			if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
			else $checked = "";
		}

		$checkbox .= "<td><input type='checkbox' name='check_list[]' id='check_list".$jj."' value='".$rsCat->fields[0]."' $checked>".ucwords($rsCat->fields[2])."</td>";
		if( $jc == 5 ) {
			$checkbox .= '</tr>';
			$jc = 0;
		}
		$jc++;
		$jj++;
		$rsCat->MoveNext();
	}
}
$checkbox .= "<tr>
<input class='btn btn-info' type='button' name='Check_All' value='Check All' onClick='CheckAll()'> 
<input class='btn btn-warning' type='button' name='Un_CheckAll' value='Uncheck All' onClick='UnCheckAll()'>
<input type='hidden' name='active' value='report'>
<input type='hidden' name='menu' value='Market Code'>
</tr></table>";
$smarty->assign("checkbox",$checkbox);	


$rs = $db_cd->Execute($sql) or die( $db_cd->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$content_id[] = $rs->fields[0];
		$content_types[] = $rs->fields[1];
		$content_codes[] = $rs->fields[2];
		$singerss[] = $rs->fields[3];
		$judul[] = $rs->fields[4];
		$descriptionss[] = $rs->fields[5];
		$entry_dates[] = $rs->fields[6];
		$file_urls[] = $rs->fields[7];
		$contents[] = $rs->fields[8];
		$partner_ids[] = $rs->fields[9];
		$accesss[] = $rs->fields[10];

		$prev = explode('~', $rs->fields['preview']);
		$prevPath = $prev[0];
		$prevUrl = $prev[1];		

		if(file_exists($prevPath)) {
			$previewf[] = $prevUrl;
		} else {
			$previewf[] = '0';
		}
		$statusf[] = $rs->fields[12];
		$short_urlf[] = $rs->fields[13];

		$rs->MoveNext();
	}

	$smarty->assign("content_id",$content_id);
	$smarty->assign("content_type",$content_types);
	$smarty->assign("content_code",$content_codes);
	$smarty->assign("singers",$singerss);
	$smarty->assign("titles",$judul);
	$smarty->assign("descriptions",$descriptionss);
	$smarty->assign("entry_date",$entry_dates);
	$smarty->assign("file_url",$file_urls);

	$smarty->assign("content",$contents);
	$smarty->assign("partner_id",$partner_ids);
	$smarty->assign("access",$accesss);
	$smarty->assign("preview",$previewf);
	$smarty->assign("status",$statusf);
	$smarty->assign("short_url",$short_urlf);

	// AJAX ONCHANGE

	$numresult = $db_cd->Execute($sql_count);

	$numrows = $numresult->RecordCount();

	$smarty->assign("numrows",$numrows);


	/*** Manage movement ***/
	$first = 0;

	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;

		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;

		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;

	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}

	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
		// if ( $dispto == $dispfrom ) $dispto = "";

	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );



}

$sql = "SELECT partner_id,partner_name FROM app_sms.partner";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_partner[$rs->fields[1]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

// if( isset($_GET['tittleBtn']) && !empty($_GET['titleSearch']) ) {
// 	$smarty->assign("titleSearch",$_GET['titleSearch']);
// 	$sqlSrc = "SELECT * FROM content_download.cd_content WHERE title LIKE '%".$_GET['titleSearch']."%' LIMIT 30";
// 	$rsSrc = $db_cd->Execute($sqlSrc) or die( $db_cd->ErrorMsg() );
// 	if ( $rsSrc->RecordCount() > 0 ) {
// 		$smarty->assign("data","FOUND");
// 		while ( !$rsSrc->EOF ) {
// 			$content_id[] = $rsSrc->fields[0];
// 			$content_types[] = $rsSrc->fields[1];
// 			$content_codes[] = $rsSrc->fields[2];
// 			$singerss[] = $rsSrc->fields[3];
// 			$judul[] = $rsSrc->fields[4];
// 			$descriptionss[] = $rsSrc->fields[5];
// 			$entry_dates[] = $rsSrc->fields[6];
// 			$file_urls[] = $rsSrc->fields[7];
// 			$contents[] = $rsSrc->fields[8];
// 			$partner_ids[] = $rsSrc->fields[9];
// 			$accesss[] = $rsSrc->fields[10];

// 			$prev = explode('~', $rsSrc->fields['preview']);
// 			$prevPath = $prev[0];
// 			$prevUrl = $prev[1];		

// 			if(file_exists($prevPath)) {
// 				$previewf[] = $prevUrl;
// 			} else {
// 				$previewf[] = '0';
// 			}
// 			$statusf[] = $rsSrc->fields[12];
// 			$short_urlf[] = $rsSrc->fields[13];
			 
// 			 $rsSrc->MoveNext();
// 		}
		
// 		$smarty->assign("content_id",$content_id);
// 		$smarty->assign("content_type",$content_types);
// 		$smarty->assign("content_code",$content_codes);
// 		$smarty->assign("singers",$singerss);
// 		$smarty->assign("titles",$judul);
// 		$smarty->assign("descriptions",$descriptionss);
// 		$smarty->assign("entry_date",$entry_dates);
// 		$smarty->assign("file_url",$file_urls);

// 		$smarty->assign("content",$contents);
// 		$smarty->assign("partner_id",$partner_ids);
// 		$smarty->assign("access",$accesss);
// 		$smarty->assign("preview",$previewf);
// 		$smarty->assign("status",$statusf);
// 		$smarty->assign("short_url",$short_urlf);
// 	}
// 	else $smarty->assign("data","NO RECORD FOUND");
// }


$dd_partner = $mFunc->crtDropDown($arr_partner,$arr_partner, 'partner', '', '', 'class="form-control"');
$smarty->assign("dd_partner",$dd_partner);

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'content_download.html');

?>