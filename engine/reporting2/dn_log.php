<?php

include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if(!empty($_GET['bln'])) {
	$bulan = $_GET['bln'];
	$bulan_name = get_bulan_name($_GET['bln']);
}
else {
	$bulan = date("m");
	$bulan_name = date("F");
}
$smarty->assign("bulan",$bulan);
$smarty->assign("bulan_name",$bulan_name);

$tahun = (!empty($_GET['thn'])) ? $_GET['thn'] : date("Y");
$smarty->assign("tahun",$tahun);

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="form-control"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="form-control"');
$smarty->assign("dd_year",$dd_year);

$log = $tahun.$bulan;

$where = " WHERE date_format(datetime,'%Y-%m') = '".$tahun."-".$bulan."'";

if(!empty($_GET['msisdn2'])) {
	$where .= " AND msisdn = '".$_GET['msisdn2']."'"; 
	$smarty->assign("msisdn2",$_GET['msisdn2']);
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco2', $_GET['telco2'], 'All', 'class="form-control"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="form-control"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="form-control"');
$smarty->assign("dd_telco",$dd_telco);
if(!empty($_GET['telco2'])) {
	if($_GET['telco2'] <> "--") {
		$where .= " AND telco like '".$_GET['telco2']."%'";
	}
	$telco_name = $mFunc->get_telco_name($_GET['telco2']);
	$smarty->assign("telco2",$_GET['telco2']);
	$smarty->assign("telco_name",$telco_name);
}
if(!empty($_GET['shortcode2'])) {
	$where .= " AND shortcode = '".$_GET['shortcode2']."'"; 
	$smarty->assign("shortcode2",$_GET['shortcode2']);
}
if(!empty($_GET['trx_id2'])) {
	$where .= " AND trx_id = '".$_GET['trx_id2']."'"; 
	$smarty->assign("trx_id2",$_GET['trx_id2']);
}
if(!empty($_GET['sid2'])) {
	$where .= " AND sid = '".$_GET['sid2']."'"; 
	$smarty->assign("sid2",$_GET['sid2']);
}
if(!empty($_GET['status2'])) {
	$where .= " AND status = '".$_GET['status2']."'"; 
	$smarty->assign("status2",$_GET['status2']);
}

$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : 20;
$sql = "SELECT * FROM mp_system.dr_incoming_log".$log." $where ORDER BY datetime DESC LIMIT $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$telco[] = $rs->fields[1];
		$shortcode[] = $rs->fields[2];
		$msisdn[] = $rs->fields[3];
		$trx_id[] = $rs->fields[4];
		$sid[] = $rs->fields[5];
		$status[] = $rs->fields[6];
		$datetime[] = $rs->fields[7];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("msisdn",$msisdn);
	$smarty->assign("telco",$telco);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("trx_id",$trx_id);
	$smarty->assign("sid",$sid);
	$smarty->assign("status",$status);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
}

$smarty->display(TEMPLATES_DIR.'dn_log.html');

function get_bulan_name($bln) {
	switch($bln) {
		case "01" : $bulan_name = "January"; break;
		case "02" : $bulan_name = "February"; break;
		case "03" : $bulan_name = "March"; break;
		case "04" : $bulan_name = "April"; break;
		case "05" : $bulan_name = "May"; break;
		case "06" : $bulan_name = "June"; break;
		case "07" : $bulan_name = "July"; break;
		case "08" : $bulan_name = "August"; break;
		case "09" : $bulan_name = "September"; break;
		case "10" : $bulan_name = "October"; break;
		case "11" : $bulan_name = "November"; break;
		case "12" : $bulan_name = "December"; break;
	}
	return $bulan_name;
}

?>