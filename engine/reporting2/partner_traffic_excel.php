<?
if( empty($_GET['bln']) || empty($_GET['thn']) || empty($_GET['shortcode']) || empty($_GET['telco']) || empty($_GET['service']) ) header("Location:partner_traffic.php");

include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$export_file = "Partner_Revenue.xls";
ob_end_clean();
ini_set('zlib.output_compression','Off');

header('Pragma: public');
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
header ("Pragma: no-cache");
header("Expires: 0");
header('Content-Transfer-Encoding: none');
header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera
header("Content-type: application/x-msexcel");                    // This should work for the rest
header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 

$s_name = $mFunc->get_description($db, $_GET['service']);
$telco_name = $_GET['telco'];
$shortcode = $_GET['shortcode'];
$traffic_date = $_GET['bln']."-".$_GET['thn'];

///

$str_sql = "";
if($_GET['shortcode'] <> "All") {
	$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
}
if($_GET['telco'] <> "All") {
	$str_sql .= " AND telco = '".$_GET['telco']."'";
}

$mt_ok_total = 0;
$mt_nok_total = 0;
$mt_tot_total = 0;
$gross_total = 0;
$nett_total = 0;

for($d=0;$d<=30;$d++) {
	$dd = $d + 1;
	if( strlen($dd) == 1 ) $day = "0".$dd;
	else $day = $dd;
	
	$tgl[$d] = $day."/".$_GET['bln']."/".$_GET['thn'];
	$mt_ok[$d] = 0;
	$mt_nok[$d] = 0;
	$gross[$d] = 0;
	$nett[$d] = 0;
	$mt_tot[$d] = 0;
	
	//Depend on partner
	$str_sql_p = "";
	if( $_SESSION["ses_partner_id"] <> 0 && in_array( $_GET['service'] , array("10017", "10029", "10030", "10031", "10032") ) ) {
		$str_sql_p .= " AND subject like 'PULL;IOD;DL;".$_SESSION["ses_partner_id"]."-%'";
	}
	//Depend on partner
	
	if( ($_SESSION["ses_username"] == "indosis" || $_SESSION["ses_username"] == "olvie") && $_GET['service'] == "10031" ) {
		$str_sql_app = " AND appid in ('10017','10031')";
	}
	else {
		$str_sql_app = " AND appid = '".$_GET['service']."'";
	}
	
	$sql = "SELECT telco, charge, dn_status, sum( jumlah )
					FROM mp_system.mt_summary_daily_".$_GET['thn'].$_GET['bln']."
					WHERE 1 $str_sql_app 
					AND datetime = '".$_GET['thn']."-".$_GET['bln']."-".$day."' $str_sql $str_sql_p
					GROUP BY telco,charge,dn_status";
	//echo $sql."<BR>";
	$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
	if ( $rs->RecordCount() > 0 ) {
		
		while ( !$rs->EOF ) {
			
			if( $rs->fields[2] == "DELIVERED" ) {
				
				if( $_GET['bln'].$_GET['thn'] == "032011" ) $telco_share = 0.5;
				elseif( $_GET['bln'].$_GET['thn'] == "042011" ) $telco_share = 0.5;
				elseif( $_GET['bln'].$_GET['thn'] == "052011" ) $telco_share = 0.5;
				else $telco_share = $config['share'][$rs->fields[0]];
				
				$ar_price = explode("-",$rs->fields[1]);
				$price = $ar_price[1];
				if( $price == 250 ) $price = 0;
				
				$mt_ok[$d] += $rs->fields[3];
				$gross[$d] += $rs->fields[3] * $price;
				$nett[$d] += $rs->fields[3] * ($price * $telco_share );
				
				$mt_ok_total += $rs->fields[3];
	
				$gross_total += $rs->fields[3] * $price;
				$nett_total += $rs->fields[3] * ($price * $telco_share );
				
			}
			else {
				$mt_nok[$d] += $rs->fields[3];
				$mt_nok_total += $rs->fields[3];
			}
			
			$mt_tot[$d] += $rs->fields[3];
			$mt_tot_total += $rs->fields[3];
			
			$rs->MoveNext();
		}
	}
	else {
		$mt_tot[$d] = 0;
	}
	
}
	
///

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 12">
<link rel=File-List href="partner_traffic_files/filelist.xml">
<style id="partner_traffic_4828_Styles">
<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:"\.";}
.xl154828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:1;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl634828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:1;
	mso-number-format:"Short Date";
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl644828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	background:#8DB4E3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl654828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:1;
	mso-number-format:"\#\,\#\#0";
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl664828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\#\,\#\#0";
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	background:#8DB4E3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl674828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:1;
	mso-number-format:"\0022Rp\0022\#\,\#\#0";
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl684828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:"\0022Rp\0022\#\,\#\#0";
	text-align:general;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	background:#8DB4E3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl694828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border:.5pt solid windowtext;
	background:#8DB4E3;
	mso-pattern:black none;
	white-space:nowrap;}
.xl704828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:12.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl714828
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Office Excel's Publish
as Web Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="partner_traffic_4828" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=620 style='border-collapse:
 collapse;table-layout:fixed;width:465pt'>
 <col width=64 style='width:48pt'>
 <col width=75 style='mso-width-source:userset;mso-width-alt:2742;width:56pt'>
 <col width=83 style='mso-width-source:userset;mso-width-alt:3035;width:62pt'>
 <col width=74 style='mso-width-source:userset;mso-width-alt:2706;width:56pt'>
 <col width=99 style='mso-width-source:userset;mso-width-alt:3620;width:74pt'>
 <col width=98 style='mso-width-source:userset;mso-width-alt:3584;width:74pt'>
 <col width=127 style='mso-width-source:userset;mso-width-alt:4644;width:95pt'>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 width=64 style='height:15.0pt;width:48pt'></td>
  <td class=xl154828 width=75 style='width:56pt'></td>
  <td class=xl154828 width=83 style='width:62pt'></td>
  <td class=xl154828 width=74 style='width:56pt'></td>
  <td class=xl154828 width=99 style='width:74pt'></td>
  <td class=xl154828 width=98 style='width:74pt'></td>
  <td class=xl154828 width=127 style='width:95pt'></td>
 </tr>
 <tr height=21 style='height:15.75pt'>
  <td height=21 class=xl154828 style='height:15.75pt'></td>
  <td colspan=2 class=xl704828>TRAFFIC :</td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td colspan=2 class=xl714828>Service : <?=$s_name?></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td colspan=2 class=xl714828>Telco : <?=$telco_name;?></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td colspan=2 class=xl714828>Shortcode : <?=$shortcode;?></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td colspan=2 class=xl714828>Date : <?=$traffic_date;?></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
  <td class=xl154828></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td class=xl694828>Date</td>
  <td class=xl694828 style='border-left:none'>SMS Success</td>
  <td class=xl694828 style='border-left:none'>SMS Failed</td>
  <td class=xl694828 style='border-left:none'>Total SMS Sent</td>
  <td class=xl694828 style='border-left:none'>Gross Revenus</td>
  <td class=xl694828 style='border-left:none'>Nett Revenue GMC</td>
 </tr>
<?php
for($i=0;$i<=30;$i++) {
	//echo $mt_ok[$i]."<BR>";
?>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td class=xl634828 align=right style='border-top:none'><?=$tgl[$i];?></td>
  <td class=xl654828 align=right style='border-top:none;border-left:none'><?=$mt_ok[$i];?></td>
  <td class=xl654828 align=right style='border-top:none;border-left:none'><?=$mt_nok[$i];?></td>
  <td class=xl654828 align=right style='border-top:none;border-left:none'><?=$mt_tot[$i];?></td>
  <td class=xl674828 align=right style='border-top:none;border-left:none'>Rp<?=$gross[$i];?></td>
  <td class=xl674828 align=right style='border-top:none;border-left:none'>Rp<?=$nett[$i];?></td>
 </tr>
<?php
}
?>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl154828 style='height:15.0pt'></td>
  <td class=xl644828 style='border-top:none'>TOTAL</td>
  <td class=xl664828 align=right style='border-top:none;border-left:none'><?=$mt_ok_total;?></td>
  <td class=xl664828 align=right style='border-top:none;border-left:none'><?=$mt_nok_total;?></td>
  <td class=xl664828 align=right style='border-top:none;border-left:none'><?=$mt_tot_total;?></td>
  <td class=xl684828 align=right style='border-top:none;border-left:none'>Rp<?=$gross_total;?></td>
  <td class=xl684828 align=right style='border-top:none;border-left:none'>Rp<?=$nett_total;?></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=64 style='width:48pt'></td>
  <td width=75 style='width:56pt'></td>
  <td width=83 style='width:62pt'></td>
  <td width=74 style='width:56pt'></td>
  <td width=99 style='width:74pt'></td>
  <td width=98 style='width:74pt'></td>
  <td width=127 style='width:95pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
