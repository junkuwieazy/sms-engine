<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$msg = "";

$smarty->assign("smt", "Submit");

if( isset($_GET['appid']) && !isset($_POST['appid']) ) $_POST['appid'] = $_GET['appid'];

$smarty->assign("len_content", $len_content);

if(isset($_POST['Submit'])) {
	if(empty($_POST['content']) ) {
	    $msg = "Question harus diisi"; 
	}
	elseif( $_POST['appid'] == "all" ) {
	    $msg = "Pilih Service yang akan di isi"; 
	}
	else {
		$sql = "INSERT INTO app_sms.content_quiz (appid,question,answer,point_true,point_false,publish_date,publish_seq,datetime) 
				VALUES ('".$_POST['appid']."','".$_POST['content']."','".$_POST['answer']."',".$_POST['point_true'].",".$_POST['point_false'].",'".$_POST['publish_date']."',".$_POST['publish_seq'].",NOW())";
		$result = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if($result) {
			$msg = "Input Question berhasil";
		}
		else {
			$msg = "Input gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_POST['SubmitEdit'])) {
	if(empty($_POST['content']) ) {
	    $msg = "Question harus diisi"; 
	}
	elseif( $_POST['appid'] == "all" ) {
	    $msg = "Pilih service yang akan di isi"; 
	}
	else {
		$sql = "UPDATE app_sms.content_quiz SET appid = '".$_POST['appid']."', question='".$_POST['content']."', answer='".$_POST['answer']."', point_true=".$_POST['point_true'].", point_false=".$_POST['point_false'].", publish_date='".$_POST['publish_date']."', publish_seq=".$_POST['publish_seq']." WHERE id=".$_POST['id'];
		$result = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if($result) {
			$msg = "Edit Question berhasil";
		}
		else {
			$msg = "Edit gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "edit") {
	$smarty->assign("smt", "SubmitEdit");
	$sqle = "SELECT question,answer,point_true,point_false,publish_date,id,publish_seq FROM app_sms.content_quiz WHERE id=".$_GET['id'];
	$rse = $db->Execute($sqle) or die( $db->ErrorMsg() );
	if ( $rse->RecordCount() > 0 ) {
		while ( !$rse->EOF ) {
			 $questione = $rse->fields[0];
			 $answere = $rse->fields[1];
			 $point_truee = $rse->fields[2];
			 $point_falsee = $rse->fields[3];
			 $publish_datee = $rse->fields[4];
			 $publish_seqe = $rse->fields[6];
			 $ide = $rse->fields[5];
			 
			 $rse->MoveNext();
		}
		
		$smarty->assign("ide",$ide);
		$smarty->assign("questione",$questione);
		$smarty->assign("answere",$answere);
		$smarty->assign("publish_datee",$publish_datee);
		$smarty->assign("publish_seqe",$publish_seqe);
		$smarty->assign("point_truee",$point_truee);
		$smarty->assign("point_falsee",$point_falsee);
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "delete") {
	$sql = "DELETE FROM app_sms.content_quiz WHERE id=".$_GET['id'];
	$result = $db->Execute($sql) or die( $db->ErrorMsg() );
}

$fix_where_appid = "";
if( $ses_appid <> "all" ) {
	$array_appid = explode(",",$ses_appid);
	
	$where_appid = " AND c.appid in (";
	for($ip=0;$ip< count($array_appid);$ip++) {
		$where_appid .= "'".$array_appid[$ip]."',";
	}
	$where_appid1 = ereg_replace(',$',"",$where_appid);
	$where_appid2 = ")";
	$fix_where_appid = $where_appid1.$where_appid2;
}

$sql_app = "SELECT appid,description FROM app_sms.appid_sms c WHERE 1 $fix_where_appid ORDER BY appid ASC";
$rs_app = $db->Execute($sql_app) or die( $db->ErrorMsg() );
if ( $rs_app->RecordCount() > 0 ) {
	while ( !$rs_app->EOF ) {
		$arr_service[$rs_app->fields[0]] = $rs_app->fields[0]." - ".$rs_app->fields[1];
		$rs_app->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDownContent($arr_service,$arr_service, 'appid', $_POST['appid'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service);

if( isset($_POST['appid']) && $_POST['appid'] <> "all" ) $str_sql = " AND c.appid='".$_POST['appid']."'";

$sql = "SELECT c.appid,description,question,answer,point_true,point_false,publish_date,c.datetime,c.id,publish_seq FROM app_sms.content_quiz c, app_sms.appid_sms a WHERE c.appid=a.appid $str_sql $fix_where_appid ORDER BY publish_seq DESC , publish_date DESC LIMIT 50";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		 $appid[] = $rs->fields[0];
		 $desc[] = $rs->fields[1];
		 $question[] = $rs->fields[2];
		 $answer[] = $rs->fields[3];
		 $point_true[] = $rs->fields[4];
		 $point_false[] = $rs->fields[5];
		 $publish_date[] = $rs->fields[6];
		 $entry_date[] = $rs->fields[7];
		 $id[] = $rs->fields[8];
		 $publish_seq[] = $rs->fields[9];
		 
		 $rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("desc",$desc);
	$smarty->assign("id",$id);
	$smarty->assign("question",$question);
	$smarty->assign("answer",$answer);
	$smarty->assign("point_true",$point_true);
	$smarty->assign("point_false",$point_false);
	$smarty->assign("publish_date",$publish_date);
	$smarty->assign("entry_date",$entry_date);
	$smarty->assign("publish_seq",$publish_seq);
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'list_quiz.html');

?>