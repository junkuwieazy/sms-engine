<?
include_once('lib/include.php'); 

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}


if(!empty($_GET['uname'])) {
	$dataUser = $db->Execute("SELECT * FROM mp_system.user_reporting WHERE LOWER(username) = '".strtolower($_GET['uname'])."' LIMIT 1");
	if($dataUser->RecordCount() > 0) {
		while ( !$dataUser->EOF ) {
			$username = $dataUser->fields[0];
			$password = $dataUser->fields[1];
			$telco = $dataUser->fields[2];
			$appid = $dataUser->fields[3];
			$menu_module = $dataUser->fields[4];
			$status = $dataUser->fields[5];
			$last_login = $dataUser->fields[6];
			$entry_date = $dataUser->fields[7];
			$partner_id = $dataUser->fields[8];

			$dataUser->MoveNext();
		}

		$smarty->assign("username",$username);
		$smarty->assign("password",$password);
		$smarty->assign("telco",$telco);
		$smarty->assign("appid",$appid);
		$smarty->assign("menu_module",$menu_module);
		$smarty->assign("status",$status);
		$smarty->assign("last_login",$last_login);
		$smarty->assign("entry_date",$entry_date);
		$smarty->assign("partner_id",$partner_id);

		$telcoArr = explode(',', $telco);
		$appidArr = explode(',', $appid);
		$menuArr = explode(',', $menu_module);

		$filter = '';
		$msg = '';

		if(isset($_POST['userUpdate'])) {		
			if(!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['telco']) && !empty($_POST['app']) && count($_POST['page']) > 0) {

				$page = join(',',$_POST['page']);
				$telcoList = join(',',$_POST['telco']);
				$appList = join(',',$_POST['app']);

				// APP
				$sql = "SELECT appid,description FROM mp_system.application";
				$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
				$appCount = $rs->RecordCount();
				if($appCount == count($_POST['app'])) {
					$apps = 'all';
				} else {
					$apps = $appList;
				}

				// Telco
				$telcoCount = count($config['telco']);
				if($telcoCount == count($_POST['telco'])) {
					$telcos = 'all';
				} else {
					$telcos = $telcoList;
				}	

				$sql_add = "UPDATE mp_system.user_reporting SET password = '".md5($_POST['password'])."',telco = '".$telcos."',appid = '".$apps."',menu_module = '".$page."' WHERE username = '".$_POST['username']."'";
				if($db->Execute($sql_add)) {
					$msg = 'Update Data USer Berhasil';
				} else {
					$msg = $db->ErrorMsg();
				}
			} else {
				$msg = 'Form tidak boleh kosong';
			}

		} 

		$fix_where = '';
		if( $_SESSION["ses_service"] == "all" ) {
			$sql = "SELECT appid,description FROM mp_system.application";
		}
		else {
			$arr = explode(",",$_SESSION['ses_service']);
			$where = " AND appid in (";
			for($i=0;$i< count($arr);$i++) {
				$where .= "'".$arr[$i]."',";
			}
			$where1 = ereg_replace(',$',"",$where);
			$where2 = ")";
			$fix_where = $where1.$where2;
			$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
		}

		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		$jc = 1; $jj = 1;
		$checkbox = "<table class='table table-responsive' border='1' cellpadding='0' cellspacing='0'><tr>";
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				if( empty($_GET['submit']) ) $checked = "checked";
				else {
					if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
					else $checked = "";
				}

				$checkbox .= "<td><input type='checkbox' name='app[]' id='app' value='".$rs->fields[0]."' $checked>".$rs->fields[1]."</td>";
				if( $jc == 5 ) {
					$checkbox .= '</tr>';
					$jc = 0;
				}
				$jc++;
				$jj++;
				$rs->MoveNext();
			}
		}
		$checkbox .= "<tr>
		<input class='btn btn-info' type=button value='Check All' onClick='ca(1)'> 
		<input class='btn btn-warning' type=button value='Uncheck All' onClick='ua(1)'>
		</tr></table>";
		$smarty->assign("checkbox",$checkbox);


		// ---------------------------------------------------------------------


		$checkboxTelco = "<table class='table table-responsive' border='1' cellpadding='0' cellspacing='0'><tr>";
		if( $_SESSION["ses_operator"] <> "all" ) {
			$arr = explode(",",$_SESSION['ses_operator']);
			for($i=0;$i<count($arr);$i++) {
				$checkboxTelco .= "<td><input type='checkbox' name='telco[]' id='telco' value='".$arr[$i]."' $checked>".$arr[$i]."</td>";
			}
		}
		else {

			foreach($config['telco'] as $k => $v) {
				$checkboxTelco .= "<td><input type='checkbox' name='telco[]' id='telco' value='".$k."' $checked>".$v."</td>";
			}
		}

		$checkboxTelco .= "<tr>
		<input class='btn btn-info' type=button value='Check All' onClick='ca(2)'> 
		<input class='btn btn-warning' type=button value='Uncheck All' onClick='ua(2)'>
		</tr></table>";
		$smarty->assign("checkboxTelco",$checkboxTelco);


		$smarty->assign("msg",$msg);

		$smarty->display(TEMPLATES_DIR.'edit_user.html');
	} else {
		header("Location:manage_user.php?active=user&menu=Manage User");
	}
} else {
	header("Location:manage_user.php?active=user&menu=Manage User");
}

?>