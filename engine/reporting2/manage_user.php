<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$msg = '';

if(!empty($_GET['uname'])) {
	$sql_add = "DELETE FROM mp_system.user_reporting WHERE username = '".$_GET['uname']."'";
	$db->Execute($sql_add) or die( $db->ErrorMsg() );
	$msg = "Data User Berhasil DIhapus";
}



$filter = '';

$limit = 20;

if(!empty($_GET['filter'])) {
	$filter = " WHERE username like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$sql = "SELECT * FROM mp_system.user_reporting $filter LIMIT $begin, $limit";
$rs = $db->Execute($sql);
$numrows = $rs->RecordCount();

if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$username[] = $rs->fields[0];
		$password[] = $rs->fields[1];
		$telco[] = $rs->fields[2];
		$appid[] = $rs->fields[3];
		$menu_module[] = $rs->fields[4];
		$status[] = $rs->fields[5];
		$last_login[] = $rs->fields[6];
		$entry_date[] = $rs->fields[7];
		
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("username",$username);
	$smarty->assign("password",$password);
	$smarty->assign("telco",$telco);
	$smarty->assign("appid",$appid);
	$smarty->assign("menu_module",$menu_module);
	$smarty->assign("status",$status);
	$smarty->assign("last_login",$last_login);
	$smarty->assign("entry_date",$entry_date);


	$smarty->assign("datauser",$username);

	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
} else {
	$smarty->assign("datauser",'NO DATA');
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'manage_user.html');

?>