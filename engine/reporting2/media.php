<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';
if(isset($_GET['msg'])) {
	$msg = $_GET['msg'];
} else {
	$msg = '';
}

if(!empty($_GET['filter'])) {
	$filter = " WHERE media_name like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}
if(!empty($_POST['media_name'])) {
	$filter = " WHERE media_name like '%".$_POST['media_name']."%'";
	$smarty->assign("filter",$_POST['media_name']);
}

if(isset($_POST['app_add'])) {
	
	if(!empty($_POST['media_name']) && !empty($_POST['media_code'])) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				$sql_add = "UPDATE app_sms.media SET media_name = '".$_POST['media_name']."',media_code = '".$_POST['media_code']."', datetime = NOW() WHERE media_id= ".$_POST['media_id'];
				$db_app->Execute($sql_add) or die( $db_app->ErrorMsg() );
				header("Location: media.php?active=report&menu=Media Name&msg=Data berhasil diedit");
			
			}
		}
		else {
			$sqlcek = "SELECT media_code FROM app_sms.media WHERE media_code = '".$_POST['media_code']."'";
			$rs = $db_app->Execute($sqlcek);
			if ( $rs->RecordCount() > 0 ) {
				$msg = "Media ".$_POST['media_code']." sudah ada";
			}
			else {
				
				$sql = "SELECT media_id FROM app_sms.media ORDER BY media_id DESC LIMIT 1";
				$rs = $db_app->Execute($sql);
				if ( $rs->RecordCount() > 0 ) {
					$id = $rs->fields[0];
				}
				
				$sql_add = "INSERT INTO app_sms.media (media_id,media_code,media_name,datetime)
								VALUES ($id + 1,'".$_POST['media_code']."','".$_POST['media_name']."',NOW())";
				$db_app->Execute($sql_add) or die( $db_app->ErrorMsg() );
				$msg = "Media berhasil ditambah";
			}
		}
		
	}
	else {
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM app_sms.media WHERE media_id = ".$_GET['media_id'];
		$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$media_id_edit = $rs->fields[1];
				$media_code_edit = $rs->fields[2];
				$media_edit = $rs->fields[3];
				
				$rs->MoveNext();
			}
			
			$smarty->assign("media_id_edit",$media_id_edit);
			$smarty->assign("media_edit",$media_edit);
			$smarty->assign("media_code_edit",$media_code_edit);
			$smarty->assign("action","edit");
		}
	}
	elseif($_GET['action'] == "delete") {
		$sql_add = "DELETE FROM app_sms.media WHERE media_id=".$_GET['media_id'];
		$db_app->Execute($sql_add) or die( $db_app->ErrorMsg() );
		header("Location: media.php?active=report&menu=Media Name&msg=Data berhasil dihapus");
	}
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM app_sms.media $filter ORDER BY media_id LIMIT $begin, $limit";
$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$media_id[] = $rs->fields[1];
		$media_code[] = $rs->fields[2];
		$media_name[] = $rs->fields[3];
		$datetime[] = $rs->fields[4];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("media_id",$media_id);
	$smarty->assign("media_code",$media_code);
	$smarty->assign("media_name",$media_name);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
	$sql_count = "SELECT media_id FROM app_sms.media $filter";
	$numresult = $db_app->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'media.html');

?>