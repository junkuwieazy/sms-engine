<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// include_once("/home/web/orion/engine/include/config.php");
// include_once("/home/web/orion/engine/include/smarty/Smarty.class.php");
// include_once("/home/web/orion/engine/include/adodb/adodb.inc.php");

include_once("/var/www/sms/engine/include/config.php");
include_once("/var/www/sms/engine/include/smarty/Smarty.class.php");
include_once("/var/www/sms/engine/include/adodb/adodb.inc.php");

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

?>


<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" id="clear">&times;</button>
    <h4 class="modal-title">Detail DN TELCO : <?php echo $_GET['tgl'];?></h4>
  </div>
  <div class="modal-body">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>NO</th>
          <th>SUBJECT</th>
          <th>DN TELCO</th>
          <th>JUMLAH</th>
        </tr>
      </thead>

      <tbody>
        <?php
          $total_dn = 0;
          $i = 1;

          $ar_tgl = explode("-",$_GET['tgl']); // 2016-08-12

          if(strlen($ar_tgl[2]) == 1) {
            $tanggals = $ar_tgl[0].'-'.$ar_tgl[1].'-0'.$ar_tgl[2];
          } else {
            $tanggals = $_GET['tgl'];
          }

          $str = "";
          if( $_GET['telco'] <> "All" ) {
            $str = " AND telco='".$_GET['telco']."'";
          } else {
            $str = '';
          }

          if(!empty($_GET['s'])) {
            $b = explode('-',$_GET['s']);
            foreach($b as $bb) {
               if(!empty($bb)) {
                    $c = "'".join("','",$b)."'";
               }
            }

            $idApp = " AND appid IN(".$c.")";
          } else {
            $idApp = '';
          }

          if( $_GET['sc'] <> "All" ) {
            $sCode = " AND shortcode = '".$_GET['sc']."'";
          } else {
            $sCode = '';
          }

          if( $tanggals == date("Y-m-d") ) {
            $sql = "SELECT subject, dn_telco, sum( jumlah ) AS jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode GROUP BY subject, dn_telco";
          }
          else {
            $sql = "SELECT subject, dn_telco, sum( jumlah ) AS jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $idApp $sCode AND datetime = '".$tanggals."' GROUP BY subject, dn_telco";
          }

          //echo $sql."<br/>";
          $rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
          if ( $rs->RecordCount() > 0 ) {
            while ( !$rs->EOF ) {

        ?>
        <tr>
          <td style='text-align: right;'><?php echo $i;?></td>
          <td style='text-align: left;'><?php echo $rs->fields[0];?></td>
          <td style='text-align: left;'><?php echo $rs->fields[1];?></td>
          <td style='text-align: right;'><?php echo number_format($rs->fields[2],0,".",".");?></td>
        </tr>
        <?php

            $total_dn += $rs->fields[2];

            $i++;
            $rs->MoveNext();
          }
        }
        ?>
        <tr>
          <td colspan="3" style="text-align: right;"><strong>TOTAL</strong></td>
          <td style='text-align: right;'><strong><?php echo number_format($total_dn,0,".",".");?></strong></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

