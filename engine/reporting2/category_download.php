<?
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if(!empty($_GET['msg'])) {
	$msg = $_GET['msg'];
} else {
	$msg = "";
}

$smarty->assign("smt", "Submit");

if(!empty($_GET['idapp'])) {
	$idapp = $_GET['idapp'];
	$smarty->assign("idapp",$idapp);
}

if( isset($_GET['appid']) && !isset($_POST['appid']) ) $_POST['appid'] = $_GET['appid'];


if(isset($_POST['Submit'])) {
	if($_POST['Submit'] == 'Submit New') {
		if(empty($_POST['ctn']) || empty($_POST['category'])) {
			$msg = "Content harus diisi"; 
		} else {
			if($_POST['ctn'] != 'all') {
				$sqls = "INSERT INTO content_download.cd_category (content_type,category_name,entry_date,status) 
				VALUES ('".$_POST['ctn']."','".$_POST['category']."',NOW(),'".$_POST['status']."')";
				$results = $db_cd->Execute($sqls) or die( $db_cd->ErrorMsg() );
				
				if($results) {
					$msg = "Input Kategori Content berhasil";
				}
				else {
					$msg = "Input Kategori Content gagal, silahkan ulangi lagi!";
				}
			} else {
				$msg = "Pilihan content harus spesifik, silahkan ulangi lagi!";
			}
		}
	}
}

$smarty->assign("valBtn", "Submit New");

// if(isset($_POST['addContent'])) {
// 	if(!empty($_POST['addContent'])) {
// 		if($_POST['addContent'] == 'Submit') {

// 			if(!empty($_POST['appid']) && !empty($_POST['content'])) {

// 				if(!empty($_POST['category'])) {
// 					$category = $_POST['category'];
// 				} else {
// 					$category = "";
// 				}

// 				if(!empty($_POST['publish_seq'])) {
// 					$publish_seq = $_POST['publish_seq'];
// 				} else {
// 					$publish_seq = "";
// 				}

// 				if(!empty($_POST['publish_date'])) {
// 					$publish_date = $_POST['publish_date'];
// 				} else {
// 					$publish_date = "";
// 				}


// 				$sql = "INSERT INTO app_sms.content_text (appid,category,content,publish_date,publish_seq,datetime) VALUES ('".$_POST['appid']."','".$category."','".$_POST['content']."','".$publish_date."','".$publish_seq."',NOW())";
// 				$result = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
// 				if($result) {
// 					header("Location:contents_text.php?active=cms&menu=Content Text&msg=Konten berhasil ditambahkan&idapp=".$_POST['appid']);
// 				}
// 				else {
// 					$msg = "Input gagal, silahkan ulangi lagi!";
// 				}

// 			} else {
// 				$msg = "Form tidak boleh kosong";
// 			}

// 		}
// 	}
// }

// $objA = Option Name
// $objB = Option Value
// $objC = Field Name
// $objD = Selected Value
// $objE = Default Value

$kindContent = array('tone' => 'True Tone','ap' => 'Animated Picture', 'cp' => 'Color Picture', 'vc' => 'Video', 'ag' => 'Android Games', 'aa' => 'Android Apps', 'ft' => 'Full-Track');

if(isset($_POST['SubmitEdit'])) {
	if(empty($_POST['ctn']) ) {
		$msg = "Content harus diisi"; 
	} else {
		$sql = "UPDATE content_download.cd_category SET status = '".$_POST['status']."', category_name = '".$_POST['category']."', content_type ='".$_POST['ctn']."' WHERE category_id=".$_POST['id'];
		$result = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
		
		if($result) {
			$msg = "Edit content berhasil";
		}
		else {
			$msg = "Edit gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "edit") {
	$smarty->assign("smt", "SubmitEdit");
	$smarty->assign("valBtn", "Update Data");
	$smarty->assign("news", "<a href='category_download.php?active=download&menu=Content Category'>Add New</a>");
	$sqle = "SELECT * FROM content_download.cd_category WHERE category_id = ".$_GET['id'];
	$rse = $db_cd->Execute($sqle) or die( $db_cd->ErrorMsg() );
	if ( $rse->RecordCount() > 0 ) {
		while ( !$rse->EOF ) {
			$category_id = $rse->fields[0];
			$content_type = $rse->fields[1];
			$category_name = $rse->fields[2];
			$entry_date = $rse->fields[3];
			$status = $rse->fields[4];
			$rse->MoveNext();
		}

		$content_service = $mFunc->contentDropDownContent($kindContent,$kindContent, 'ctn', $_GET['cntn'], $_GET['cntn'], 'class="form-control"');
		$smarty->assign("content_service",$content_service);
		
		$smarty->assign("category_id_e",$category_id);
		// $smarty->assign("content_service",$content_type);
		$smarty->assign("category_name_e",$category_name);
		$smarty->assign("entry_date_e",$entry_date);
		$smarty->assign("dd_status	",$status);
	} 
} else {
	$content_service = $mFunc->contentDropDownContent($kindContent,$kindContent, 'ctn', $_POST['ctn'], 'All', 'class="form-control"');
	$smarty->assign("content_service",$content_service);
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "delete") {
	$sqldel = "DELETE FROM content_download.cd_category WHERE category_id=".$_GET['id'];
	$result = $db_cd->Execute($sqldel) or die( $db_cd->ErrorMsg() );
	if($result) {
		$msg = "Hapus Kategori berhasil";
	}
	else {
		$msg = "Hapus Kategori gagal, silahkan ulangi lagi!";
	}
}


$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 10;

// ONLOAD DATA
if( isset($_POST['ctn']) && $_POST['ctn'] <> "all") {
	$sql = "SELECT * FROM content_download.cd_category WHERE content_type = '".$_POST['ctn']."' ORDER BY category_id DESC LIMIT $begin, $limit";
	$sql_count = "SELECT * FROM content_download.cd_category WHERE content_type = '".$_POST['ctn']."'";
} elseif( isset($_GET['cntn']) && !empty($_GET['cntn'])) {
	$sql = "SELECT * FROM content_download.cd_category WHERE content_type = '".$_GET['cntn']."' ORDER BY category_id DESC LIMIT $begin, $limit";
	$sql_count = "SELECT * FROM content_download.cd_category WHERE content_type = '".$_GET['cntn']."'";
}else {
	$sql = "SELECT * FROM content_download.cd_category ORDER BY category_id DESC LIMIT $begin, $limit";
	$sql_count = "SELECT * FROM content_download.cd_category";
}

$rs = $db_cd->Execute($sql) or die( $db_cd->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$category_ids[] = $rs->fields[0];
		$content_types[] = $rs->fields[1];
		$category_names[] = $rs->fields[2];
		$entry_dates[] = $rs->fields[3];
		if( $rs->fields[4] == 1 ) $statuss[] = "Aktif";
		else $status[] = "Non Aktif";

		$rs->MoveNext();
	}

	$smarty->assign("category_id",$category_ids);
	$smarty->assign("content_type",$content_types);
	$smarty->assign("category_name",$category_names);
	$smarty->assign("entry_date",$entry_dates);
	$smarty->assign("status",$statuss);

	// AJAX ONCHANGE



	$numresult = $db_cd->Execute($sql_count);

	$numrows = $numresult->RecordCount();

	$smarty->assign("numrows",$numrows);
	$smarty->assign("ctn",$_POST['ctn']);

	/*** Manage movement ***/
	$first = 0;

	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;

		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;

		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;

	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}

	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
		// if ( $dispto == $dispfrom ) $dispto = "";

	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );

	/*** End of manage movement ***/
}


$sql = "SELECT partner_id,partner_name FROM app_sms.partner";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_partner[$rs->fields[1]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_partner = $mFunc->crtDropDown($arr_partner,$arr_partner, 'partner', $_POST['partner'], '', 'class="form-control"');
$smarty->assign("dd_partner",$dd_partner);

$dd_status = $mFunc->crtDropDownMonth($config['status'],$config['status'], 'status', $_GET['status'], '', 'class="form-control"');
$smarty->assign("dd_status",$dd_status);

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'category_download.html');

?>