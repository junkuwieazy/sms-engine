<?php
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}
//echo "satu";
$smarty->assign("is_today","tdk");
$smarty->assign("subject_detail","tdk");

if( empty($_GET['submit']) ) {
	$_GET['submit'] = '';
	$h = 1;
	$h2 = 1;
	$tgl_now = date("d");
	$j_hari = $mFunc->get_jumlah_hari(date("m"));
}
else {
	$j_hari = $mFunc->get_jumlah_hari($_GET['bln']);
	if( $_GET['bln'] == date("m") ) {
		$h = 1;
		$h2 = 1;
		$tgl_now = date("d");
	}
	else {
		$h = 0;
		$h2 = 1;
		$tgl_now = $mFunc->get_jumlah_hari($_GET['bln']);
	}
}

if( empty($_SERVER['QUERY_STRING']) ) $smarty->assign("php_query_string","");
else $smarty->assign("php_query_string","?".$_SERVER['QUERY_STRING']);

$smarty->assign("submit",$_GET['submit']);
$fix_where = '';
if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
$jc = 1; $jj = 1;
$checkbox = "<table width='100%'  border='1' cellpadding='0' cellspacing='0'><tr>";
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		if( empty($_GET['submit']) ) $checked = "checked";
		else {
			if( isset($_GET['check_list'.$jj]) ) $checked = "checked";
			else $checked = "";
		}
		
		$checkbox .= "<td><input type='checkbox' name='check_list".$jj."' id='check_list".$jj."' value='".$rs->fields[0]."' $checked>".$rs->fields[1]."</td>";
		if( $jc == 5 ) {
			$checkbox .= '</tr>'; 
			$jc = 0;
		}
		$jc++;
		$jj++;
		$rs->MoveNext();
	}
}
$checkbox .= "<tr>
				<input type='button' name='Check_All' value='Check All' onClick='CheckAll()'>
				<input type='button' name='Un_CheckAll' value='Uncheck All' onClick='UnCheckAll()'>
			</tr></table>";
$smarty->assign("checkbox",$checkbox);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$bulan = ""; $tahun = "";

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("bln",$bulan);
$smarty->assign("thn",$tahun);

$smarty->assign("bln_now",date("m"));
$smarty->assign("thn_now",date("Y"));

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$bulan_traffic = date("m");
	$tahun_traffic = date("Y");
	$smarty->assign("s_name","All");
	$str_service = "";
}
else {
	$bulan_traffic = $_GET['bln'];
	$tahun_traffic = $_GET['thn'];
	$fix_where = '';
	$j_check = 0;
	
	$service_in = ""; $is_check = false; $s_name = "";
	for($r=1;$r<=$jj;$r++) {
		if( isset($_GET['check_list'.$r]) ) {
			$j_check += 1;
			$j_check_r = $r;
			$service_in .= "'".$_GET['check_list'.$r]."',";
			$is_check = true;
			$s_name .= $mFunc->get_description($db, $_GET['check_list'.$r]).", ";
		}
	}
	$service_in = ereg_replace(',$','',$service_in);
	
	if( $is_check ) $smarty->assign("s_name",$s_name);
	else $smarty->assign("s_name","All");
	
	if( $service_in <> "" ) $str_service = "AND appid in ( ".$service_in." )";
	else $str_service = "AND appid = '0000'";
	
}
$smarty->assign("service",$_GET['service']);

$smarty->assign("bulan_traffic",$bulan_traffic);
$smarty->assign("tahun_traffic",$tahun_traffic);

$mo_tot = 0; $mt_ok_tot = 0; $mt_nok_tot = 0; $total_mt_tot = 0; $gross_tot = 0; $net_tot = 0; $persentage_tot = 0; $mo_reg_tot = 0; $mo_unreg_tot = 0;

for($i=$h;$i<=$tgl_now;$i++) {
	if($h == 1) $k = $i - 1;
	else $k = $i;
	$hasil = $tgl_now - $i;
	if( $hasil > 0 ) {
		if( strlen($hasil) == 1) $tgl_traffic = "0".$hasil;
		else $tgl_traffic = $hasil;
		
		$tgl_print[$k] = $hasil;
		
		//
		$mo_reg[$k] = 0;
		$sql_reg = "SELECT jumlah
									FROM mp_system.summary_reg_".$tahun_traffic.$bulan_traffic." 
									WHERE 1 $str_sql $str_service $fix_where
									AND mo_type = 'REG'
									AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		/*							
		$sql_reg = "SELECT count( 1 ) AS jml
					FROM app_sms.msisdn_subscriber
					WHERE 1 $str_sql $str_service AND date_format( date_reg, '%Y-%m-%d' ) = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		*/
		//echo $sql_reg."<BR>";
		$rs_reg = $db_app->Execute($sql_reg) or die( $db_app->ErrorMsg() );
		if ( $rs_reg->RecordCount() > 0 ) {
			while ( !$rs_reg->EOF ) {
				$mo_reg[$k] += $rs_reg->fields[0];
				$rs_reg->MoveNext();
			}
		}
		
		$mo_unreg[$k] = 0;
		$sql_unreg = "SELECT jumlah
									FROM mp_system.summary_reg_".$tahun_traffic.$bulan_traffic." 
									WHERE 1 $str_sql $str_service $fix_where
									AND mo_type = 'UNREG'
									AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		/*
		$sql_unreg = "SELECT count( 1 ) AS jml
					FROM app_sms.msisdn_subscriber
					WHERE 1 $str_sql $str_service AND date_format( date_unreg, '%Y-%m-%d' ) = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."'";
		*/
		//echo $sql_unreg."<BR>";
		$rs_unreg = $db_app->Execute($sql_unreg) or die( $db_app->ErrorMsg() );
		if ( $rs_unreg->RecordCount() > 0 ) {
			while ( !$rs_unreg->EOF ) {
				$mo_unreg[$k] += $rs_unreg->fields[0];
				$rs_unreg->MoveNext();
			}
		}
		//
		
		$mo[$k] = 0;
		$sql_mo = "SELECT sum( jumlah )
								FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic." 
								WHERE datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' $str_sql $str_service $fix_where 
								AND subject NOT LIKE 'push%'";
		$rs_mo = $db->Execute($sql_mo) or die( $db->ErrorMsg() );
		if ( $rs_mo->RecordCount() > 0 ) {
			while ( !$rs_mo->EOF ) {
				$mo[$k] += $rs_mo->fields[0];
				$rs_mo->MoveNext();
			}
		}
		
		$mt_ok[$k] = 0;
		$mt_c_ok[$k] = 0;
		$mt_nok[$k] = 0;
		$total_mt_c[$k] = 0;
		$total_mt[$k] = 0;
		$gross[$k] = 0;
		$net[$k] = 0;
		
		$sql = "SELECT telco, charge, dn_status, sum( jumlah )
							FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic." 
							WHERE datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' $str_sql $str_service $fix_where 
							GROUP BY telco,charge,dn_status";
		//echo $sql."<br>";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if ( $rs->RecordCount() > 0 ) {
			
			while ( !$rs->EOF ) {
				
				$ar_price_c = explode("-",$rs->fields[1]);
				$price_c = $ar_price_c[1];
				
				if( $rs->fields[2] == "DELIVERED" ) {
					
					if( $bulan_traffic.$tahun_traffic == "032011" ) $telco_share = 0.5;
					elseif( $bulan_traffic.$tahun_traffic == "042011" ) $telco_share = 0.5;
					elseif( $bulan_traffic.$tahun_traffic == "052011" ) $telco_share = 0.5;
					else $telco_share = $config['share'][$rs->fields[0]];
					
					$ar_price = explode("-",$rs->fields[1]);
					$price = $ar_price[1];
					
					$mt_ok[$k] += $rs->fields[3];
					if( $price == 250 ) $price = 0;
					
					if( $price > 400 ) $mt_c_ok[$k] += $rs->fields[3];
					
					$gross[$k] += $rs->fields[3] * $price;
					$net[$k] += $rs->fields[3] * ($price * $telco_share );
					
				}
				else {
					$mt_nok[$k] += $rs->fields[3];
				}
				
				$total_mt[$k] += $rs->fields[3];
				if( $price_c > 400 ) $total_mt_c[$k] += $rs->fields[3];
				
				$rs->MoveNext();
			}
		}
		
		//Detail SUBJECT
		if( $j_check == 1 ) {
			$smarty->assign("subject_detail","ya");
			$rd = 0; $rr = 0;
			$appid_selected = $_GET['check_list'.$j_check_r];
			
			$sql_subject = "SELECT appid, subject, dn_status, sum( jumlah )
											FROM mp_system.mt_summary_daily_".$tahun_traffic.$bulan_traffic." 
											WHERE 1
											AND datetime = '".$tahun_traffic."-".$bulan_traffic."-".$tgl_traffic."' $str_sql $str_service $fix_where 
											GROUP BY subject, dn_status
											ORDER BY dn_status, subject";
			echo $sql_subject;
			$rs_subject = $db->Execute($sql_subject) or die( $db->ErrorMsg() );
			if ( $rs_subject->RecordCount() > 0 ) {
				while ( !$rs_subject->EOF ) {
					
					if( $rs_subject->fields[2] == "DELIVERED" ) {
						$subject_dlv[$rs_subject->fields[1]][$hasil] = $rs_subject->fields[3];
					}
					else {
						$subject_re[$rs_subject->fields[1]][$hasil] = $rs_subject->fields[3];
					}
					
					$subject_tot[$rs_subject->fields[1]][$hasil] += $rs_subject->fields[3];
					
					$rs_subject->MoveNext();
				}
			}
			else {
				$subject_dlv[$rs_subject->fields[1]][$hasil] = 0;
				$subject_re[$rs_subject->fields[1]][$hasil] = 0;
				$subject_tot[$rs_subject->fields[1]][$hasil] = 0;
			}
			
		}
		//Detail SUBJECT
		
		//$persentage[$k] = ($mt_ok[$k] / $total_mt[$k]) * 100;
		$persentage[$k] = ($mt_c_ok[$k] / $total_mt_c[$k]) * 100;
		
		$mo_tot += $mo[$k];
		$mt_ok_tot += $mt_ok[$k];
		$mt_nok_tot += $mt_nok[$k];
		$total_mt_tot += $total_mt[$k];
		$gross_tot += $gross[$k];
		$net_tot += $net[$k];
		
		$mo_reg_tot += $mo_reg[$k];
		$mo_unreg_tot += $mo_unreg[$k];
		
	}
}

foreach($subject_dlv as $service => $key) {
	$num[$service] = 0;
	foreach($key as $tgl => $item) {
		if( $item <> 0) {
			$num[$service] += 1;
		}
	}
}

foreach($subject_dlv as $service => $key) {
	if( $num[$service] <> 0) {
		foreach($key as $tgl => $item) {
			$subject_dlvf[$service][$tgl] = $item;
		}
	}
}

foreach($subject_re as $servicere => $keyre) {
	$numre[$servicere] = 0;
	foreach($keyre as $tglre => $itemre) {
		if( $itemre <> 0) {
			$numre[$servicere] += 1;
		}
	}
}

foreach($subject_re as $servicere => $keyre) {
	if( $numre[$servicere] <> 0) {
		foreach($keyre as $tglre => $itemre) {
			$subject_ref[$servicere][$tglre] = $itemre;
		}
	}
}

foreach($subject_tot as $servicetot => $keytot) {
	$numtot[$servicetot] = 0;
	foreach($keytot as $tgltot => $itemtot) {
		if( $itemtot <> 0) {
			$numtot[$servicetot] += 1;
		}
	}
}

foreach($subject_tot as $servicetot => $keytot) {
	if( $numtot[$servicetot] <> 0) {
		foreach($keytot as $tgltot => $itemtot) {
			$subject_totf[$servicetot][$tgltot] = $itemtot;
		}
	}
}

$smarty->assign("subject_dlvf",$subject_dlvf);
$smarty->assign("subject_ref",$subject_ref);
$smarty->assign("subject_totf",$subject_totf);

//$persentage[$k] = ($mt_c_ok[$k] / $total_mt_c[$k]) * 100;

///
if( $_GET['bln'] == date("m") || !isset($_GET['bln']) ) {
	//
	$smarty->assign("is_today","ya");
	$tgl_printt = date("j");
	
	//
	$mo_regt = 0;
	$sql_regt = "SELECT count( 1 ) AS jml
				FROM app_sms.msisdn_subscriber
				WHERE 1 $str_sql $str_service $fix_where AND date_format( date_reg, '%Y-%m-%d' ) = '".date("Y-m-d")."'";
	//echo $sql_reg."<BR>";
	$rs_regt = $db->Execute($sql_regt) or die( $db->ErrorMsg() );
	if ( $rs_regt->RecordCount() > 0 ) {
		while ( !$rs_regt->EOF ) {
			$mo_regt += $rs_regt->fields[0];
			$rs_regt->MoveNext();
		}
	}
	
	$mo_unregt = 0;
	$sql_unregt = "SELECT count( 1 ) AS jml
				FROM app_sms.msisdn_subscriber
				WHERE 1 $str_sql $str_service $fix_where AND date_format( date_unreg, '%Y-%m-%d' ) = '".date("Y-m-d")."'";
	//echo $sql_unreg."<BR>";
	$rs_unregt = $db->Execute($sql_unregt) or die( $db->ErrorMsg() );
	if ( $rs_unregt->RecordCount() > 0 ) {
		while ( !$rs_unregt->EOF ) {
			$mo_unregt += $rs_unregt->fields[0];
			$rs_unregt->MoveNext();
		}
	}
	//
	
	$mot = 0;
	$sql_mot = "SELECT sum( jumlah )
							FROM mp_system.mt_summary_hourly 
							WHERE 1 $str_sql $str_service $fix_where 
							AND subject NOT LIKE 'push%'";
	$rs_mot = $db->Execute($sql_mot) or die( $db->ErrorMsg() );
	if ( $rs_mot->RecordCount() > 0 ) {
		while ( !$rs_mot->EOF ) {
			$mot += $rs_mot->fields[0];
			$rs_mot->MoveNext();
		}
	}
	
	$mt_okt = 0;
	$mt_c_okt = 0;
	$mt_nokt = 0;
	$total_mt_ct = 0;
	$total_mtt = 0;
	$grosst = 0;
	$nett = 0;
	
	$sqlt = "SELECT telco, charge, dn_status, sum( jumlah )
						FROM mp_system.mt_summary_hourly 
						WHERE 1 $str_sql $str_service $fix_where 
						GROUP BY telco,charge,dn_status";
	//echo $sqlt."<br>";
	$rst = $db->Execute($sqlt) or die( $db->ErrorMsg() );
	
	if ( $rst->RecordCount() > 0 ) {
		
		while ( !$rst->EOF ) {
			
			$ar_price_ct = explode("-",$rst->fields[1]);
			$price_ct = $ar_price_ct[1];
			
			if( $rst->fields[2] == "DELIVERED" ) {
				
				if( $bulan_traffic.$tahun_traffic == "032011" ) $telco_share = 0.5;
				elseif( $bulan_traffic.$tahun_traffic == "042011" ) $telco_share = 0.5;
				elseif( $bulan_traffic.$tahun_traffic == "052011" ) $telco_share = 0.5;
				else $telco_share = $config['share'][$rst->fields[0]];
				
				$ar_pricet = explode("-",$rst->fields[1]);
				$pricet = $ar_pricet[1];
				
				$mt_okt += $rst->fields[3];
				if( $pricet == 250 ) $pricet = 0;
				
				if( $pricet > 400 ) $mt_c_okt += $rst->fields[3];
				
				$grosst += $rst->fields[3] * $pricet;
				$nett += $rst->fields[3] * ($pricet * $telco_share );
				
			}
			else {
				$mt_nokt += $rst->fields[3];
			}
			
			$total_mtt += $rst->fields[3];
			if( $price_ct > 400 ) $total_mt_ct += $rst->fields[3];
			
			$rst->MoveNext();
		}
	}
	
	//Detail SUBJECT today
		if( $j_check == 1 ) {
			$smarty->assign("subject_detail","ya");
			$rdt = 0; $rrt = 0;
			
			$sql_subjectt = "SELECT appid, subject, dn_status, sum( jumlah )
											FROM mp_system.mt_summary_hourly 
											WHERE 1
											$str_sql $str_service $fix_where 
											GROUP BY subject, dn_status
											ORDER BY dn_status, subject";
			//echo $sql_subject;
			$rs_subjectt = $db->Execute($sql_subjectt) or die( $db->ErrorMsg() );
			if ( $rs_subjectt->RecordCount() > 0 ) {
				while ( !$rs_subjectt->EOF ) {
					
					if( $rs_subjectt->fields[2] == "DELIVERED" ) {
						$subject_dlvt[$rs_subjectt->fields[1]] = $rs_subjectt->fields[3];
					}
					else {
						$subject_ret[$rs_subjectt->fields[1]] = $rs_subjectt->fields[3];
					}
					
					$subject_tott[$rs_subjectt->fields[1]] += $rs_subjectt->fields[3];
					
					$rs_subjectt->MoveNext();
				}
			}
			/*
			foreach($subject_dlvt as $servicet => $keyt) {
				$numt[$servicet] = 0;
				foreach($keyt as $tglt => $itemt) {
					if( $itemt <> 0) {
						$numt[$servicet] += 1;
					}
				}
			}
			
			foreach($subject_dlvt as $servicet => $keyt) {
				if( $numt[$servicet] <> 0) {
					foreach($keyt as $tglt => $itemt) {
						$subject_dlvft[$servicet][$tglt] = $itemt;
					}
				}
			}
			
			foreach($subject_ret as $serviceret => $keyret) {
				$numret[$serviceret] = 0;
				foreach($keyret as $tglret => $itemret) {
					if( $itemret <> 0) {
						$numret[$serviceret] += 1;
					}
				}
			}
			
			foreach($subject_ret as $serviceret => $keyret) {
				if( $numret[$serviceret] <> 0) {
					foreach($keyret as $tglret => $itemret) {
						$subject_reft[$serviceret][$tglret] = $itemret;
					}
				}
			}
			
			foreach($subject_tott as $servicetott => $keytott) {
				$numtott[$servicetott] = 0;
				foreach($keytott as $tgltott => $itemtott) {
					if( $itemtott <> 0) {
						$numtott[$servicetott] += 1;
					}
				}
			}
			
			foreach($subject_tott as $servicetott => $keytott) {
				if( $numtott[$servicetott] <> 0) {
					foreach($keytott as $tgltott => $itemtott) {
						$subject_totft[$servicetott][$tgltott] = $itemtott;
					}
				}
			}
			*/
			$smarty->assign("subject_dlvft",$subject_dlvt);
			$smarty->assign("subject_reft",$subject_ret);
			$smarty->assign("subject_totft",$subject_tott);
			
		}
		//Detail SUBJECT today
	
	//$persentage[$k] = ($mt_ok[$k] / $total_mt[$k]) * 100;
	$persentaget = ($mt_c_okt / $total_mt_ct) * 100;
	
	$mo_tot += $mot;
	$mt_ok_tot += $mt_okt;
	$mt_nok_tot += $mt_nokt;
	$total_mt_tot += $total_mtt;
	$gross_tot += $grosst;
	$net_tot += $nett;
	//
	
	$mo_reg_tot += $mo_regt;
	$mo_unreg_tot += $mo_unregt;
	
}
///

$persentage_tot = ($mt_ok_tot / $total_mt_tot) * 100;

$smarty->assign("mo",$mo);
$smarty->assign("mt_ok",$mt_ok);
$smarty->assign("mt_nok",$mt_nok);
$smarty->assign("total_mt",$total_mt);
$smarty->assign("gross",$gross);
$smarty->assign("net",$net);
$smarty->assign("tgl_print",$tgl_print);
$smarty->assign("mo_tot",$mo_tot);
$smarty->assign("mt_ok_tot",$mt_ok_tot);
$smarty->assign("mt_nok_tot",$mt_nok_tot);
$smarty->assign("total_mt_tot",$total_mt_tot);
$smarty->assign("gross_tot",$gross_tot);
$smarty->assign("net_tot",$net_tot);
$smarty->assign("persentage",$persentage);
$smarty->assign("persentage_tot",$persentage_tot);


$smarty->assign("mo_reg",$mo_reg);
$smarty->assign("mo_regt",$mo_regt);
$smarty->assign("mo_reg_tot",$mo_reg_tot);
$smarty->assign("mo_unreg",$mo_unreg);
$smarty->assign("mo_unregt",$mo_unregt);
$smarty->assign("mo_unreg_tot",$mo_unreg_tot);

$smarty->assign("mot",$mot);
$smarty->assign("mt_okt",$mt_okt);
$smarty->assign("mt_nokt",$mt_nokt);
$smarty->assign("total_mtt",$total_mtt);
$smarty->assign("grosst",$grosst);
$smarty->assign("nett",$nett);
$smarty->assign("tgl_printt",$tgl_printt);
$smarty->assign("persentaget",$persentaget);

$rata_gross = $gross_tot / $tgl_now;
$rata_net = $net_tot / $tgl_now;

$gross_projection = $rata_gross * $j_hari;
$net_projection = $rata_net * $j_hari;

$smarty->assign("gross_projection",$gross_projection);
$smarty->assign("net_projection",$net_projection);

/*
$smarty->assign("subject_dlv",$subject_dlv);
$smarty->assign("subject_re",$subject_re);

$smarty->assign("j_subject_dlv",$j_subject_dlv);
$smarty->assign("j_subject_re",$j_subject_re);
*/
$smarty->display(TEMPLATES_DIR.'daily_traffic.html');

?>