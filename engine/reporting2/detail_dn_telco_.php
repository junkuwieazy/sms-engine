<?php
include_once("/var/www/sms/engine/include/config.php");
include_once('/var/www/sms/engine/include/smarty/Smarty.class.php');
include_once('/var/www/sms/engine/include/adodb/adodb.inc.php');

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Helvetica, sans-serif;
	font-weight: bold;
}
.style2 {
	font-family: Arial, Arial, Helvetica, sans-serif;
	font-weight: bold;
}
.style3 {
	font-family: Arial, Arial, Helvetica, sans-serif;
	size:10px;
	font-weight: normal;
}
-->
</style>
</head>

<body>

<div align="center">
  <p class="style1">Detail DN TELCO : <?php echo $_GET['tgl'];?></p>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td bgcolor="#0099FF"><table width="100%"  border="0" cellspacing="1" cellpadding="3">
        <tr bgcolor="#FFFFFF">
          <td><div align="center" class="style2">NO</div></td>
          <td><div align="center" class="style2">SUBJECT </div></td>
	   <td><div align="center" class="style2">DN TELCO </div></td>
          <td><div align="center" class="style2">JUMLAH</div></td>
        </tr>
<?php
$total_dn = 0;
$i = 1;

$ar_tgl = explode("-",$_GET['tgl']); // 2016-08-12

$str = "";
if( $_GET['telco'] <> "All" ) {
	$str = " AND telco='".$_GET['telco']."'";
}

if( $_GET['tgl'] == date("Y-m-d") ) {
	$sql = "SELECT subject, dn_telco, sum( jumlah ) AS jml FROM mp_system.mt_summary_hourly WHERE 1 $str GROUP BY subject, dn_telco";
}
else {
	$sql = "SELECT subject, dn_telco, sum( jumlah ) AS jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str AND datetime = '".$_GET['tgl']."' GROUP BY subject, dn_telco";
}

//echo $sql."<br/>";
$rs = $db_app->Execute($sql) or die( $db_app->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {

?>
		<tr bgcolor="#FFFFFF">
          <td><div align="center" class="style3"><?php echo $i;?></div></td>
          <td><div align="center" class="style3"><?php echo $rs->fields[0];?></div></td>
	   <td><div align="center" class="style3"><?php echo $rs->fields[1];?></div></td>
          <td><div align="right" class="style3"><?php echo number_format($rs->fields[2],0,".",".");?></div></td>
        </tr>
<?php

		$total_dn += $rs->fields[2];

		$i++;
		$rs->MoveNext();
	}
}
?>
        <tr bgcolor="#FFFFFF">
          <td><span class="style3"></span></td>
	   <td><span class="style3"></span></td>
          <td><div align="center" class="style3">TOTAL</div></td>
          <td><div align="right" class="style3"><strong><?php echo number_format($total_dn,0,".",".");?></strong></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</div>
</body>
</html>
