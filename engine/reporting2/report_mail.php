<?php
// ini_set('display_errors', 'On');
// error_reporting(0);


include_once('lib/include.php');

include_once('lib/libphpmailer/PHPMailerAutoload.php');

$datetime = date('Y-m-d', strtotime(' -1 day'));


//APPDATA
$appList = "SELECT * FROM app_sms.appid_sms WHERE appid NOT IN('10010','99999')";
$rsApp = $db_app->Execute($appList) or die( $db_app->ErrorMsg() );
if ( $rsApp->RecordCount() > 0 ) {
	while ( !$rsApp->EOF ) {

		//REG
		$reg = "SELECT telco,sum(jumlah) jml FROM app_sms.msisdn_hourly_reg WHERE appid = '".$rsApp->fields[1]."' AND tanggal = '$datetime' GROUP BY tanggal";
		$rsReg = $db_app->Execute($reg) or die( $db_app->ErrorMsg() );
		if ( $rsReg->RecordCount() > 0 ) {

			while ( !$rsReg->EOF ) {

				$data[$rsApp->fields[2]]['telco'] = $rsReg->fields[0];
				$data[$rsApp->fields[2]]['reg'] = $rsReg->fields[1];

				// $telcos[] = $rsReg->fields[0];
				// $tReg[] = $rsReg->fields[1];

				$rsReg->MoveNext();
			}
		} else {
			$data[$rsApp->fields[2]]['telco'] = '-';
			$data[$rsApp->fields[2]]['reg'] = 0;
		}

		//UNREG
		$unreg = "SELECT telco,sum(jumlah) jml FROM app_sms.msisdn_hourly_unreg WHERE appid = '".$rsApp->fields[1]."' AND tanggal = '$datetime' GROUP BY tanggal";
		$rsUnreg = $db_app->Execute($unreg) or die( $db_app->ErrorMsg() );
		if ( $rsUnreg->RecordCount() > 0 ) {

			while ( !$rsUnreg->EOF ) {

				$data[$rsApp->fields[2]]['telco'] = $rsUnreg->fields[0];
				$data[$rsApp->fields[2]]['unreg'] = $rsUnreg->fields[1];

				// $telcos[] = $rsUnreg->fields[0];
				// $tUnreg[] = $rsUnreg->fields[1];

				$rsUnreg->MoveNext();
			}
		} else {
			$data[$rsApp->fields[2]]['telco'] = '--';
			$data[$rsApp->fields[2]]['unreg'] = 00;
		}

		$data[$rsApp->fields[2]]['datetime'] = $datetime;

		$rsApp->MoveNext();
	}
}

///////
// REVENUE
///////

$isi = "<h2 style=color:red>Auto Report for Inzpire ".date("Y-m-d")."</h2>";
$isi .= "<hr>";
$isi .= "<br>";

$isi .= '<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
.tg .tg-tdu6{font-weight:bold;background-color:#036400;color:#ffffff;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
<tr>
<th class="tg-tdu6">DATETIME</th>
<th class="tg-tdu6">OPERATOR</th>
<th class="tg-tdu6">PROGRAM</th>
<th class="tg-tdu6">TOTAL REG</th>
<th class="tg-tdu6">TOTAL UNREG</th>
</tr>';

foreach ($data as $key => $value) {

	if($value['reg'] != 0 && $value['unreg'] != 0) {
		$isi .= "<tr>";

		$isi .= "<td class='tg-yw4l' style='text-align:left'>".$value['datetime']."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:left'>".$value['telco']."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:left'>".$key."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:right'>".$value['reg']."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:right'>".$value['unreg']."</td>";

		$isi .= "</tr>";
	}
}
$isi .= "</table>";


$isi .= "<br>";


$isi .= "<h2 style=color:red>REPORT FOR ".strtoupper(date('F', strtotime(' -1 day')))." </h2>";
$isi .= "<hr>";
$isi .= "<br>";

$isi .= '<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aaa;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aaa;color:#fff;background-color:#f38630;}
.tg .tg-tdu6{font-weight:bold;background-color:#036400;color:#ffffff;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
<tr>
<th class="tg-tdu6">DATETIME</th>
<th class="tg-tdu6">TOTAL</th>
<th class="tg-tdu6">OPERATOR</th>
<th class="tg-tdu6">'.strtoupper(date('F', strtotime(' -1 day'))).' REVENUE</th>
</tr>';



//////////////////////////////////////////////////////////////////////////////////////

$tahun_traffic = date('Y');
$fix_where = '';

foreach ($config['telco'] as $keyTelco => $valueTelco) {
	$tl[] = $keyTelco;


	$Telco = join("','",$tl);

	$str_sql = " WHERE telco IN('".$keyTelco."')";

	$mo_tot = 0; $mt_ok_tot = 0; $mt_nok_tot = 0; $total_mt_tot = 0; $gross_tot = 0; $net_tot = 0;
	$hh = 0;
	$gross[$hh] = 0;
	$total_mt[$hh] = 0;

	$tahun_traffic == date("Y");
	$blnn = date("m");

	$sql = "SELECT telco, charge, dn_status, sum( jumlah ), sid
	FROM mp_system.mt_summary_daily_".$tahun_traffic.$blnn." $str_sql
	GROUP BY telco,charge,sid,dn_status";

	if( $tahun_traffic == "2015" ) {
		$rs = $dblocal->Execute($sql) or die( $dblocal->ErrorMsg() );
	}
	else {
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
	}

	if ( $rs->RecordCount() > 0 ) {

		while ( !$rs->EOF ) {

			if( $rs->fields[2] == "DELIVERED" ) {

				$sqlbearer = "SELECT bearer_cost FROM mp_system.list_sid WHERE sid='".$rs->fields[4]."'";
				$rsbearer = $db->Execute($sqlbearer) or die( $db->ErrorMsg() );
				if ( $rsbearer->RecordCount() > 0 ) {
					while ( !$rsbearer->EOF ) {

						$bearer_cost = $rsbearer->fields[0];

						$rsbearer->MoveNext();
					}
				}
				else {
					$bearer_cost = 0;
				}


				if( $blnn.$tahun_traffic == "032011" ) $telco_share = 0.5;
				elseif( $blnn.$tahun_traffic == "042011" ) $telco_share = 0.5;
				elseif( $blnn.$tahun_traffic == "052011" ) $telco_share = 0.5;
				else $telco_share = $config['share'][$rs->fields[0]];

				$ar_price = explode("-",$rs->fields[1]);
				$price = $ar_price[1] - $bearer_cost;
				if( $price == 250 ) $price = 0;

				$gross[$hh] += $rs->fields[3] * $price;
				$net[$hh] += $rs->fields[3] * ($price * $telco_share );

			}
			else {
				$mt_nok[$hh] += $rs->fields[3];
			}

			$total_mt[$hh] += $rs->fields[3];

			$rs->MoveNext();
		}
	}


	$sql2 = "SELECT telco, charge, dn_status, sum( jumlah ),sid
	FROM mp_system.mt_summary_hourly $str_sql
	GROUP BY telco,charge,sid,dn_status";

	$rs2 = $db->Execute($sql2) or die( $db->ErrorMsg() );

	if ( $rs2->RecordCount() > 0 ) {

		while ( !$rs2->EOF ) {

			if( $rs2->fields[2] == "DELIVERED" ) {

				$sqlbearer2 = "SELECT bearer_cost FROM mp_system.list_sid WHERE sid='".$rs2->fields[4]."'";
				$rsbearer2 = $db->Execute($sqlbearer2) or die( $db->ErrorMsg() );
				if ( $rsbearer2->RecordCount() > 0 ) {
					while ( !$rsbearer2->EOF ) {

						$bearer_cost2 = $rsbearer2->fields[0];

						$rsbearer2->MoveNext();
					}
				}
				else {
					$bearer_cost2 = 0;
				}

				if( $blnn.$tahun_traffic == "032011" ) $telco_share = 0.5;
				elseif( $blnn.$tahun_traffic == "042011" ) $telco_share = 0.5;
				elseif( $blnn.$tahun_traffic == "052011" ) $telco_share = 0.5;
				else $telco_share = $config['share'][$rs2->fields[0]];

				$ar_price = explode("-",$rs2->fields[1]);
				$price = $ar_price[1] - $bearer_cost2;

				$mt_ok[$hh] += $rs2->fields[3];
				if( $price == 250 ) $price = 0;

				$gross[$hh] += $rs2->fields[3] * $price;
				$net[$hh] += $rs2->fields[3] * ($price * $telco_share );

			}
			else {
				$mt_nok[$hh] += $rs2->fields[3];
			}

			$total_mt[$hh] += $rs2->fields[3];

			$rs2->MoveNext();
		}
	}


	$tel += $tel_ok[$hh];

	$mo_tot += $mo[$hh];
	$mt_ok_tot += $mt_ok[$hh];
	$mt_nok_tot += $mt_nok[$hh];
	$total_mt_tot += $total_mt[$hh];
	$gross_tot += $gross[$hh];
	$net_tot += $net[$hh];


	if( $total_mt[0] > 0 || !empty( $total_mt[0])) {
		// echo $total_mt[0].'-'.$keyTelco.'-'.$gross[0];

		$totals[] = $gross[0];
		$isi .= "<tr>";

		$isi .= "<td class='tg-yw4l' style='text-align:left'>".date('Y-m-d', strtotime(' -1 day'))."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:left'>".number_format($total_mt[0])."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:left'>".$keyTelco."</td>";
		$isi .= "<td class='tg-yw4l' style='text-align:right'>".number_format($gross[0])."</td>";

		$isi .= "</tr>";
	}
}

$print_total = array_sum($totals);

$isi .= "<tr><td colspan=3 style='text-align:right'><b>TOTAL</b></td><td style='text-align:right'><b>".number_format($print_total)."</b></td></tr>";

$isi .= "</table>";


//Create a new PHPMailer instance
$mail = new PHPMailer();

      //Tell PHPMailer to use SMTP
$mail->isSMTP();

      //Enable SMTP debugging
      // 0 = off (for production use)
      // 1 = client messages
      // 2 = client and server messages
$mail->SMTPDebug = 0;

      //Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

      //Set the hostname of the mail server
      //$mail->Host = 'smtp.gmail.com';
$mail->Host = 'mail.mobiwin.co.id';

      //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

      //Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

      //Whether to use SMTP authentication
$mail->SMTPAuth = true;

      //Username to use for SMTP authentication - use full email address for gmail
      //$mail->Username = "email.ironmask@gmail.com";
$mail->Username = "juri.pebrianto@mobiwin.co.id";

      //Password to use for SMTP authentication
      //$mail->Password = "topengsaja";
$mail->Password = "Juri2017";

      //Set who the message is to be sent from
$mail->setFrom('no-reply@pragosa-indonesia.com', 'Inzpire Auto Report');

//Set an alternative reply-to address
//$mail->addReplyTo('replyto@example.com', 'First Last');
/*
'novan@persadasolution.com',
'fery@persadasolution.com',
'ikaroseliyana29@gmail.com',
*/
//Set who the message is to be sent to
$mail->addAddress('roy_yardin@yahoo.com', 'Pak Roy');
// $mail->addAddress('jati.prasetyo@mobiwin.co.id', 'Jati Mobiwin');
// $mail->addCC("hudysetiyatmoko@gmail.com", 'Pak Hudy');
// $mail->addCC("adi.noegroho@gmail.com", 'Pak Adi');
$mail->addCC("muji@mobiwin.co.id", 'Pak Muji');
// $mail->addCC("filadj@gmail.com", 'Mba Fila');

//Set the subject line
$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = "Auto Report for Inzpire ".date("Y-m-d");
$mail->Body    = $isi;

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML($msg);
//$mail->isHTML(true);

//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';

//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
	echo "Mailer Error: " . $mail->ErrorInfo;
} else {
	echo "Message has been sent at $datetime \n";
}

// echo $isi;

?>
