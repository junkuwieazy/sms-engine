<?php

include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if(!empty($_GET['bln'])) {
	$bulan = $_GET['bln'];
	$bulan_name = get_bulan_name($_GET['bln']);
}
else {
	$bulan = date("m");
	$bulan_name = date("F");
}
$smarty->assign("bulan",$bulan);
$smarty->assign("bulan_name",$bulan_name);

$tahun = (!empty($_GET['thn'])) ? $_GET['thn'] : date("Y");
$smarty->assign("tahun",$tahun);

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

$log = $tahun.$bulan;

$where = " WHERE date_format(datetime,'%Y-%m') = '".$tahun."-".$bulan."'";

if(!empty($_GET['mo_origin2'])) {
	$where .= " AND mo_origin = '".$_GET['mo_origin2']."'";
	$smarty->assign("mo_origin2",$_GET['mo_origin2']);
}
if(!empty($_GET['msisdn2'])) {
	$where .= " AND msisdn = '".$_GET['msisdn2']."'"; 
	$smarty->assign("msisdn2",$_GET['msisdn2']);
}
if(!empty($_GET['appid2'])) {
	$where .= " AND appid = '".$_GET['appid2']."'"; 
	$smarty->assign("appid2",$_GET['appid2']);
}
if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco2', $_GET['telco2'], 'All', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(!empty($_GET['telco2'])) {
	if($_GET['telco2'] <> "--") {
		$where .= " AND telco like '".$_GET['telco2']."%'";
	}
	$telco_name = $mFunc->get_telco_name($_GET['telco2']);
	$smarty->assign("telco2",$_GET['telco2']);
	$smarty->assign("telco_name",$telco_name);
}
if(!empty($_GET['shortcode2'])) {
	$where .= " AND shortcode = '".$_GET['shortcode2']."'"; 
	$smarty->assign("shortcode2",$_GET['shortcode2']);
}
if(!empty($_GET['trx_id2'])) {
	$where .= " AND trx_id = '".$_GET['trx_id2']."'"; 
	$smarty->assign("trx_id2",$_GET['trx_id2']);
}
if(!empty($_GET['session_id2'])) {
	$where .= " AND session_id = '".$_GET['session_id2']."'"; 
	$smarty->assign("session_id2",$_GET['session_id2']);
}

if( $_SESSION["ses_partner_id"] == 0 ) $str_sql = "";
else $str_sql = " AND partner_id=".$_SESSION["ses_partner_id"];

$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : 20;
$sql = "SELECT * FROM mp_system.mo_incoming_partner_log".$log." $where $str_sql ORDER BY datetime DESC LIMIT $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$mo_origin[] = $rs->fields[1];
		$appid[] = $rs->fields[2];
		$msisdn[] = $rs->fields[3];
		$prepaid[] = $rs->fields[4];
		$sms[] = $rs->fields[5];
		$telco[] = $rs->fields[6];
		$shortcode[] = $rs->fields[7];
		$trx_id[] = $rs->fields[8];
		$trx_date[] = $rs->fields[9];
		$session_id[] = $rs->fields[10];
		$datetime[] = $rs->fields[12];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}
	
	$smarty->assign("mo_origin",$mo_origin);
	$smarty->assign("msisdn",$msisdn);
	$smarty->assign("appid",$appid);
	$smarty->assign("prepaid",$prepaid);
	$smarty->assign("sms",$sms);
	$smarty->assign("telco",$telco);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("trx_id",$trx_id);
	$smarty->assign("trx_date",$trx_date);
	$smarty->assign("session_id",$session_id);
	$smarty->assign("datetime",$datetime);
	$smarty->assign("warna",$warna);
	
}

$smarty->display(TEMPLATES_DIR.'mo_log_partner.html');

function get_bulan_name($bln) {
	switch($bln) {
		case "01" : $bulan_name = "January"; break;
		case "02" : $bulan_name = "February"; break;
		case "03" : $bulan_name = "March"; break;
		case "04" : $bulan_name = "April"; break;
		case "05" : $bulan_name = "May"; break;
		case "06" : $bulan_name = "June"; break;
		case "07" : $bulan_name = "July"; break;
		case "08" : $bulan_name = "August"; break;
		case "09" : $bulan_name = "September"; break;
		case "10" : $bulan_name = "October"; break;
		case "11" : $bulan_name = "November"; break;
		case "12" : $bulan_name = "December"; break;
	}
	return $bulan_name;
}

?>