<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}
$str_sql = '';
$bln_now = date("m");
$smarty->assign("bln_now",$bln_now);

$thn_now = date("Y");
$smarty->assign("thn_now",$thn_now);

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application ORDER BY appid ASC";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where ORDER BY appid ASC";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], '', 'class="text"');
$smarty->assign("dd_service",$dd_service);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], '', 'class="text"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], '', 'class="text"');
}
//$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], '', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}
$smarty->assign("telco",$_GET['telco']);

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], '', 'class="text"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}
$smarty->assign("shortcode",$_GET['shortcode']);

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if(isset($_GET['submit'])) {
	
	$bln = $_GET['bln'];
	$thn = $_GET['thn'];
	
	$log = $thn.$bln;
	
	$smarty->assign("bln",$_GET['bln']);
	$smarty->assign("thn",$_GET['thn']);
	
	$smarty->assign("service",$_GET['service']);
	

		
		$s_name = $mFunc->get_description($db, $_GET['service']);
		$smarty->assign("s_name",$s_name);
		
		if( $_GET['bln'] == date("m") ) $j_hari = date("j");
		else $j_hari = $mFunc->get_jumlah_hari($_GET['bln']);
		
		$tot_mt_1 = 0; $tot_mt_0 = 0; $tot_mt_3101 = 0; $tot_mt_3105 = 0; $tot_mt_3321 = 0; $tot_mt_3109 = 0; $tot_mt_4 = 0; $tot_mt_lain = 0;
		$total_reg = 0; $total_unreg = 0;
		
		for($i=1;$i<=$j_hari;$i++) {
			if( strlen($i) == 1) $tgl = "0".$i;
			else $tgl = $i;
			
			// REG
			$sql_reg = "SELECT jumlah
									FROM mp_system.summary_reg_".$log."
									WHERE 1
									AND appid = '".$_GET['service']."'
									AND mo_type = 'REG'
									AND datetime = '".$thn."-".$bln."-".$tgl."'";
			//echo $sql_reg;
			$reg[$i] = 0;
			$rs_reg = $db_app->Execute($sql_reg) or die( $db_app->ErrorMsg() );
			if ( $rs_reg->RecordCount() > 0 ) {
				$reg[$i] = $rs_reg->fields[0];
			}
			
			// UNREG
			$sql_unreg = "SELECT jumlah
									FROM mp_system.summary_reg_".$log."
									WHERE 1
									AND appid = '".$_GET['service']."'
									AND mo_type = 'UNREG'
									AND datetime = '".$thn."-".$bln."-".$tgl."'";
			$unreg[$i] = 0;
			$rs_unreg = $db_app->Execute($sql_unreg) or die( $db_app->ErrorMsg() );
			if ( $rs_unreg->RecordCount() > 0 ) {
				$unreg[$i] = $rs_unreg->fields[0];
			}
			
			// REG, UNREG TODAY
			if( $_GET['bln'] == date("m") && date("j") == $i ) {
				// UNREG
				$sql_regn = "SELECT count( 1 ) AS jml
							FROM app_sms.msisdn_subscriber
							WHERE 1 AND appid = '".$_GET['service']."' $str_sql AND date_format( date_reg, '%Y-%m-%d' ) = '".$thn."-".$bln."-".$tgl."'";
				$reg[$i] = 0;
				$rs_regn = $db_app->Execute($sql_regn) or die( $db_app->ErrorMsg() );
				if ( $rs_regn->RecordCount() > 0 ) {
					$reg[$i] = $rs_regn->fields[0];
				}
				
				// UNREG
				$sql_unregn = "SELECT count( 1 ) AS jml
							FROM app_sms.msisdn_subscriber
							WHERE 1 AND appid = '".$_GET['service']."' $str_sql AND date_format( date_unreg, '%Y-%m-%d' ) = '".$thn."-".$bln."-".$tgl."'";
				$unreg[$i] = 0;
				$rs_unregn = $db_app->Execute($sql_unregn) or die( $db_app->ErrorMsg() );
				if ( $rs_unregn->RecordCount() > 0 ) {
					$unreg[$i] = $rs_unregn->fields[0];
				}
			}
			// REG, UNREG TODAY

			// Traffic MT
			$sql_mt = "SELECT dn_telco, sum( jumlah )
									FROM mp_system.dn_telco_summary_".$log."
									WHERE 1
									AND appid = '".$_GET['service']."' $str_sql
									AND datetime = '".$thn."-".$bln."-".$tgl."'
									GROUP BY dn_telco";
			$rs_mt = $db->Execute($sql_mt) or die( $db->ErrorMsg() );
			$mt_1[$i] = 0;
			$mt_0[$i] = 0;
			$mt_3101[$i] = 0;
			$mt_3105[$i] = 0;
			$mt_3321[$i] = 0;
			$mt_3109[$i] = 0;
			$mt_4[$i] = 0;
			$mt_lain[$i] = 0;
			$total_mt[$i] = 0;
			
			if ( $rs_mt->RecordCount() > 0 ) {
				while ( !$rs_mt->EOF ) {
					switch($rs_mt->fields[0]) {
						case "1" : 
							$mt_1[$i] += $rs_mt->fields[1];
						break;
						case "0" : 
							$mt_0[$i] += $rs_mt->fields[1];
						break;
						case "3:101" : 
							$mt_3101[$i] += $rs_mt->fields[1];
						break;
						case "3:105" : 
							$mt_3105[$i] += $rs_mt->fields[1];
						break;
						case "3:3:21" : 
							$mt_3321[$i] += $rs_mt->fields[1];
						break;
						case "3:109" : 
							$mt_3109[$i] += $rs_mt->fields[1];
						break;
						case "4" : 
							$mt_4[$i] += $rs_mt->fields[1];
						break;
						default : 
							$mt_lain[$i] += $rs_mt->fields[1];
						break;
					}
					$rs_mt->MoveNext();
				}
				$total_mt[$i] = $mt_1[$i] + $mt_0[$i] + $mt_3101[$i] + $mt_3105[$i] + $mt_3321[$i] + $mt_3109[$i] + $mt_4[$i] + $mt_lain[$i];
			}
			
		}
		
		for($k=1;$k< $i;$k++) {
			$t_tgl1[] = $k."-".$bln."-".$thn;
			$t_reg1[] = $reg[$k];
			$total_reg += $reg[$k];
			$t_unreg1[] = $unreg[$k];
			$total_unreg += $unreg[$k];
			$t_mt_1[] = $mt_1[$k];
			$total_mt_1 += $mt_1[$k];
			$t_mt_0[] = $mt_0[$k];
			$total_mt_0 += $mt_0[$k];
			$t_mt_3101[] = $mt_3101[$k];
			$total_mt_3101 += $mt_3101[$k];
			$t_mt_3105[] = $mt_3105[$k];
			$total_mt_3105 += $mt_3105[$k];
			$t_mt_3321[] = $mt_3321[$k];
			$total_mt_3321 += $mt_3321[$k];
			$t_mt_3109[] = $mt_3109[$k];
			$total_mt_3109 += $mt_3109[$k];
			$t_mt_4[] = $mt_4[$k];
			$total_mt_4 += $mt_4[$k];
			$t_mt_lain[] = $mt_lain[$k];
			$total_mt_lain += $mt_lain[$k];
			$total_mt1[] = $total_mt[$k];
			$total_mt_all += $total_mt[$k];
		}
		
		$smarty->assign("t_tgl1",$t_tgl1);
		$smarty->assign("t_reg1",$t_reg1);
		$smarty->assign("t_unreg1",$t_unreg1);
		$smarty->assign("t_mt_1",$t_mt_1);
		$smarty->assign("t_mt_0",$t_mt_0);
		$smarty->assign("t_mt_3101",$t_mt_3101);
		$smarty->assign("t_mt_3105",$t_mt_3105);
		$smarty->assign("t_mt_3321",$t_mt_3321);
		$smarty->assign("t_mt_3109",$t_mt_3109);
		$smarty->assign("t_mt_4",$t_mt_4);
		$smarty->assign("t_mt_lain",$t_mt_lain);
		$smarty->assign("total_mt1",$total_mt1);
		//print_r($total_mt);
		$smarty->assign("total_reg",$total_reg);
		$smarty->assign("total_unreg",$total_unreg);
		$smarty->assign("total_mt_1",$total_mt_1);
		$smarty->assign("total_mt_0",$total_mt_0);
		$smarty->assign("total_mt_3101",$total_mt_3101);
		$smarty->assign("total_mt_3105",$total_mt_3105);
		$smarty->assign("total_mt_3321",$total_mt_3321);
		$smarty->assign("total_mt_3109",$total_mt_3109);
		$smarty->assign("total_mt_4",$total_mt_4);
		$smarty->assign("total_mt_lain",$total_mt_lain);
		$smarty->assign("total_mt_all",$total_mt_all);
	
	
}

if(!empty($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
}

$smarty->display(TEMPLATES_DIR.'tsel_traffic.html');

?>
