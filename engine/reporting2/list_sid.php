<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$filter = '';


if(isset($_GET['msg'])) {
	$msg = $_GET['msg'];
} else {
	$msg = '';
}

if(!empty($_GET['filter'])) {
	$filter = " WHERE sid like '%".$_GET['filter']."%'";
	$smarty->assign("filter",$_GET['filter']);
}
if(!empty($_POST['sid_search'])) {
	$filter = " WHERE sid like '%".$_POST['sid_search']."%'";
	$smarty->assign("filter",$_POST['sid_search']);
}

if(isset($_POST['sid_add'])) {
	
	if(!empty($_POST['sid']) && !empty($_POST['telco']) && !empty($_POST['shortcode']) && !empty($_POST['charge']) ) {
		if(!empty($_POST['action'])) {
			if($_POST['action'] == "edit") {
				$sql_add = "UPDATE mp_system.list_sid SET sid = '".$_POST['sid']."',telco = '".$_POST['telco']."',shortcode = '".$_POST['shortcode']."', charge = '".$_POST['charge']."', bearer_cost = '".$_POST['bearer']."' WHERE id_sid= ".$_POST['id'];
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				header("Location: list_sid.php?active=report&menu=List SID&msg=Data berhasil diedit");
			
			}
		}
		else {
			$sqlcek = "SELECT sid FROM mp_system.list_sid WHERE sid = '".$_POST['sid']."' AND telco = '".$_POST['telco']."' AND shortcode = '".$_POST['shortcode']."' AND charge = '".$_POST['charge']."'";
			$rs = $db->Execute($sqlcek);
			if ( $rs->RecordCount() > 0 ) {
				$msg = "SID ".$_POST['sid']." sudah ada";
			}
			else {
				$sql_add = "INSERT INTO mp_system.list_sid (sid,telco,shortcode,charge)
								VALUES ('".$_POST['sid']."','".$_POST['telco']."','".$_POST['shortcode']."','".$_POST['charge']."')";
				$db->Execute($sql_add) or die( $db->ErrorMsg() );
				$msg = "SID berhasil ditambah";
			}
		}
		
	}
	else {
		$msg = "Kolom yang bertanda * harus diisi";
	}
	
}

if(!empty($_GET['action'])) {
	if($_GET['action'] == "edit") {
		$sql = "SELECT * FROM mp_system.list_sid WHERE id_sid = ".$_GET['id']." ORDER BY sid";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
				$id_edit = $rs->fields[0];
				$sid_edit = $rs->fields[1];
				$telco_edit = $rs->fields[2];
				$shortcode_edit = $rs->fields[3];
				$charge_edit = $rs->fields[4];
				$bearer_edit = $rs->fields[5];

				$rs->MoveNext();
			}

			$smarty->assign("id_edit",$id_edit);
			$smarty->assign("sid_edit",$sid_edit);
			$smarty->assign("shortcode_edit",$shortcode_edit);
			$smarty->assign("telco_edit",$telco_edit);
			$smarty->assign("charge_edit",$charge_edit);
			$smarty->assign("bearer_edit",$bearer_edit);
			$smarty->assign("action","edit");
		}
	}
	elseif($_GET['action'] == "delete") {
		$sql_add = "DELETE FROM mp_system.list_sid WHERE id_sid=".$_GET['id'];
		$db->Execute($sql_add) or die( $db->ErrorMsg() );
		header("Location: list_sid.php?active=report&menu=List SID&msg=Data berhasil dihapus");
	}
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 20;
$sql = "SELECT * FROM mp_system.list_sid $filter ORDER BY sid LIMIT $begin, $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	$j = 1;
	while ( !$rs->EOF ) {
		$id[] = $rs->fields[0];
		$sid[] = $rs->fields[1];
		$telco[] = $rs->fields[2];
		$shortcode[] = $rs->fields[3];
		$charge[] = $rs->fields[4];
		$bearer_cost[] = $rs->fields[5];
		if($j%2 == 0) $warna[] = "#CCCCCC";
		else $warna[] = "#FFFFFF";
		$j++;
		$rs->MoveNext();
	}

	$smarty->assign("sid",$sid);
	$smarty->assign("id",$id);
	$smarty->assign("telco",$telco);
	$smarty->assign("shortcode",$shortcode);
	$smarty->assign("charge",$charge);
	$smarty->assign("bearer_cost",$bearer_cost);

	$sql_count = "SELECT sid FROM mp_system.list_sid $filter ORDER BY sid";
	$numresult = $db->Execute($sql_count);

	$numrows = $numresult->RecordCount();

	$smarty->assign("numrows",$numrows);

	/*** Manage movement ***/
	$first = 0;

	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;

		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;

		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;

	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}

	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";

	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );

	/*** End of manage movement ***/
}


$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_POST['shortcode'], '', 'class="form-control"');
$smarty->assign("dd_shortcode",$dd_shortcode);

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_POST['telco'], '', 'class="form-control"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_POST['telco'], '', 'class="form-control"');
}
$smarty->assign("dd_telco",$dd_telco);

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'list_sid.html');

?>
