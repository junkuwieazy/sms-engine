<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}
$filter = "";
if(!empty($_GET['msisdns'])) {
	$filter = " AND msisdn like '%".$_GET['msisdns']."%'";
	$smarty->assign("msisdns",$_GET['msisdns']);
}

$bulan = ""; $tahun = "";

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$smarty->assign("bln",$bulan);
$smarty->assign("thn",$tahun);

$smarty->assign("bln_now",date("m"));
$smarty->assign("thn_now",date("Y"));

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if( empty($_GET['submit']) ) {
	$bulan_traffic = date("m");
	$tahun_traffic = date("Y");
}
else {
	$bulan_traffic = $_GET['bln'];
	$tahun_traffic = $_GET['thn'];
	
}

$smarty->assign("bulan_traffic",$bulan_traffic);
$smarty->assign("tahun_traffic",$tahun_traffic);

$str_sql = "";
$i = 1;
$sqlss = "SELECT msisdn, count( 1 ) AS jml FROM mp_system.mt_outgoing_log".$tahun_traffic.$bulan_traffic." WHERE 1 $filter AND dn_telco = '2' AND msisdn not in (SELECT msisdn FROM app_sms.msisdn_blacklist) GROUP BY msisdn ORDER BY jml DESC LIMIT 30";
//echo $sqlss;
$rsss = $db->Execute($sqlss) or die( $db->ErrorMsg() );
if ( $rsss->RecordCount() > 0 ) {
	while ( !$rsss->EOF ) {
		
		$total_nett = 0;
		
		$sql = "SELECT charge FROM mp_system.mt_outgoing_log".$tahun_traffic.$bulan_traffic." WHERE 1 AND msisdn='".$rsss->fields[0]."' AND dn_telco='2'";
		$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			
			while ( !$rs->EOF ) {
				
				$ar_price = explode("-",$rs->fields[0]);
				$price = $ar_price[1];
				if( $price == 250 ) $price = 0;
				
				$total_nett += $price;
				
				$rs->MoveNext();
			}
			
		}
		
		$sqlb = "SELECT msisdn FROM app_sms.msisdn_blacklist WHERE msisdn='".$rsss->fields[0]."'";
		$rsb = $db->Execute($sqlb) or die( $db->ErrorMsg() );
		if ( $rsb->RecordCount() > 0 ) {
			$black[] = "<strong>BLACKLIST</strong>";
		}
		else {
			$black[] = '<a href="blacklist_msisdn.php?msisdn='.$rsss->fields[0].'" class="link1">Not Yet</a>';
		}
		
		$no[] = $i;
		$msisdn[] = $rsss->fields[0];
		$cost[] = $total_nett;
		
		$i++;
		$rsss->MoveNext();
	}
}

$smarty->assign("no", $no );
$smarty->assign("msisdn", $msisdn );
$smarty->assign("cost", $cost );
$smarty->assign("black", $black );

$smarty->display(TEMPLATES_DIR.'cost_per_msisdn.html');

?>