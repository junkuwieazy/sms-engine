<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$msg = "";
$strsql = "";

if(isset($_POST['AddBL']) && $_POST['AddBL'] == "Add") {
	if(empty($_POST['msisdn']) ) {
	    $msg = "MSISDN harus diisi"; 
	}
	else {
		$sql = "INSERT INTO app_sms.msisdn_blacklist (msisdn,telco,entry_date) 
				VALUES ('".$_POST['msisdn']."','".$_POST['telco']."',NOW())";
		$result = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if($result) {
			$msg = "Input Blacklist MSISDN berhasil";
		}
		else {
			$msg = "Input gagal, silahkan ulangi lagi!";
		}
		
		$strsql = " AND msisdn='".$_POST['msisdn']."'";
		$smarty->assign("msisdnS",$_POST['msisdn']);
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "delete") {
	$sql = "DELETE FROM app_sms.msisdn_blacklist WHERE id=".$_GET['id'];
	$result = $db->Execute($sql) or die( $db->ErrorMsg() );
}

if(isset($_GET['Scari']) && $_GET['Scari'] == "Cari") {
	$strsql = " AND msisdn LIKE '%".$_GET['msisdnS']."%'";
	$smarty->assign("msisdnS",$_GET['msisdnS']);
}

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 50;
$sql = "SELECT * FROM app_sms.msisdn_blacklist WHERE 1 $strsql ORDER BY entry_date DESC LIMIT $begin, $limit";


$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		 $id[] = $rs->fields[0];
		 $msisdn[] = $rs->fields[1];
		 $telco[] = $rs->fields[2];
		 $entry_date[] = $rs->fields[3];
		 
		 $rs->MoveNext();
	}
	
	$smarty->assign("msisdn",$msisdn);
	$smarty->assign("telco",$telco);
	$smarty->assign("id",$id);
	$smarty->assign("entry_date",$entry_date);
	
	$sql_count = "SELECT msisdn FROM app_sms.msisdn_blacklist WHERE 1 $strsql";
	$numresult = $db->Execute($sql_count);

	$numrows = $numresult->RecordCount();
	
	$smarty->assign("numrows",$numrows);
	
	/*** Manage movement ***/
	$first = 0;
	
	if ( empty($_GET['begin']) ) {
		$prev = 0;
		$next = $limit;
		
		$counter = 1;
		$dispfrom = 1;
	} else {
		$prev = $_GET['begin'] - $limit;
		$next = $_GET['begin'] + $limit;
		
		$dispfrom = $counter = $_GET['begin'] + 1;
	}
	$dispto = $dispfrom + ($limit-1);
	if ($prev < 0) $prev = 0;

	$lastremainder = $numrows % $limit;
	
	if ( $lastremainder == 0) {
		$last = $numrows - $limit;
	} else {
		$last = $numrows - $lastremainder;
	}
	
	if ( $next > $last ) $next = $last;
	if ( $dispto > $numrows ) $dispto = $numrows;
	// if ( $dispto == $dispfrom ) $dispto = "";
	
	$smarty->assign( 'first', $first );
	$smarty->assign( 'prev', $prev );
	$smarty->assign( 'next', $next );
	$smarty->assign( 'last', $last );
	$smarty->assign( 'dispfrom', $dispfrom );
	$smarty->assign( 'dispto', $dispto );
	
	/*** End of manage movement ***/
	
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'blacklist.html');

?>