<?php
session_start();

//ini_set('display_errors', 'On'); 

include_once(dirname(__FILE__) . "/../../include/config.php");
include_once(dirname(__FILE__) . "/../../reporting2/lib/config_report.php");
include_once(dirname(__FILE__) . '/../../include/smarty/Smarty.class.php');
include_once(dirname(__FILE__) . '/../../include/adodb/adodb.inc.php');
define("TEMPLATES_DIR",dirname(__FILE__) . "/../../reporting2/templates/");
define("IMG_DIR","images/");
define("CSS_FILE","css/style.css");
define("JAVA_FILE","js/java.js");

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

//Create Template Object
$smarty = new smarty;

$smarty->template_dir = dirname(__FILE__) . "/../templates/";
$smarty->compile_dir = dirname(__FILE__) . '/../templates_c/';
$smarty->config_dir = 'configs/';
$smarty->cache_dir = 'cache/';

$smarty->assign("img_dir",IMG_DIR);
$smarty->assign("css_file",CSS_FILE);
$smarty->assign("java_file",JAVA_FILE);

$smarty->assign("title_header",$title_header);
$smarty->assign("title",$title);
$smarty->assign("copyright",$copyright);
if(isset($_SESSION["ses_username"])) {
	$smarty->assign("ses_username",$_SESSION["ses_username"]);
}
if(isset($_SESSION["ses_operator"])) {
	$smarty->assign("ses_operator",$_SESSION["ses_operator"]);
}
if(isset($_SESSION["ses_service"])) {
	$smarty->assign("ses_service",$_SESSION["ses_service"]);
}
if(isset($_SESSION["ses_menu_module"])) {
	$smarty->assign("ses_menu_module",$_SESSION["ses_menu_module"]);
}

// Access Level

$levels = array("U","R","C","T");
$menu_html ='';


$array_menu = explode(",",$_SESSION["ses_menu_module"]);

foreach($array_menu as $m) {
	$a = explode("_",$m);
	if(strlen($a[0]) == 1) {
		$level[] = $a[0];
	}
}

rsort($level);


foreach(array_unique($level) as $lvl) {

	if($lvl == 'U') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'user') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-book"></i><span> User</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> User</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> User</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=user&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li class="a"><a href="'.$config['menu'][$menu_name].'?active=user&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="b"><a href="'.$config['menu'][$menu_name].'?active=user&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}
	

	if($lvl == 'T') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'cs') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-book"></i><span> CS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> CS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> CS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=cs&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li class="c"><a href="'.$config['menu'][$menu_name].'?active=cs&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="d"><a href="'.$config['menu'][$menu_name].'?active=cs&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	if($lvl == 'C') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'cms') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-book"></i><span> Content Manage</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> Content Manage</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> Content Manage</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=cms&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li class="e"><a href="'.$config['menu'][$menu_name].'?active=cms&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="f"><a href="'.$config['menu'][$menu_name].'?active=cms&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	if($lvl == 'R') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'report') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-book"></i><span> Report</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> Report</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-book"></i><span> Report</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=report&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li class="g"><a href="'.$config['menu'][$menu_name].'?active=report&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="h"><a href="'.$config['menu'][$menu_name].'?active=report&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	$menu_html .= '</ul></li>';
}


$smarty->assign("menu_html",$menu_html);

include_once('functions.php');

$mFunc =& new myFunctions();

?>