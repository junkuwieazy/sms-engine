<?php

$config['menu'] = array(
					'Application' => "application.php" ,
					'Keyword' => "keyword.php" ,
					'Reg Keyword' => "reg_keyword.php" ,
					'Partner Name' => "partner.php" ,
					'Media Name' => "media.php" ,
					'MO LOG' => "mo_log.php" ,
					'MT LOG' => "mt_log.php" ,
					'DN LOG' => "dn_log.php" ,
					'Daily MT SMS' => "mt_sms.php" ,
					'SMS Subscriber' => "subscriber.php" ,
					'Daily Traffic' => "daily_traffic.php" ,
					'Monthly Traffic' => "monthly_traffic.php" ,
					'Top Service' => "top_service.php" ,
					'Push SMS' => "push_sms.php" ,
					'Cost Service' => "cost_service.php" ,
					'Profit Loss' => "profit_loss.php" ,
					'Daily DN Telco' => "daily_dn_telco.php" ,
					'MO REG' => "mo_reg.php" ,
					'Check MSISDN' => "check_msisdn.php" ,
					'Blacklist' => "blacklist.php" ,
					'Transaction History' => "transaction_history.php",
					'List SID' => "list_sid.php",
					'Report SID' => "report_sid.php",

					'Add User' => "add_user.php" ,
					'Manage User' => "manage_user.php" ,

					'Content Text' => "contents_text.php",
					
					
					'Content Download' => "content_download.php",
					'Category Download' => "category_download.php",
					'Market Code' => "market_code.php"
					);

$title_header = ":::- Reporting SMS -:::";
$title = "::: SMS REPORTING :::";
$copyright = "copyright &copy; ".date("Y")." - Macrokiosk. All Right Reserved";

?>
