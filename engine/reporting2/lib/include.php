<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

session_start();

include_once("/var/www/sms/engine/include/config.php");
include_once("/var/www/sms/engine/reporting2/lib/config_report.php");
include_once('../include/smarty/Smarty.class.php');
include_once('../include/adodb/adodb.inc.php');
define("TEMPLATES_DIR","/var/www/sms/engine/reporting2/templates/");
define("IMG_DIR","images/");
define("CSS_FILE","css/style.css");
define("JAVA_FILE","js/java.js");

$burl = "http://103.29.214.124:8319/sms/engine/reporting2/";

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

// Connection Content Download
global $config_cd;
$db_cd = &ADONewConnection( $config_cd['DBType'] );
$db_cd->connect( $config_cd['DBHost'].":".$config_cd['DBPort'],$config_cd['DBUser'],$config_cd['DBPassword'],$config_cd['DBName']) or die ( $db->ErrorMsg() );

//Create Template Object
$smarty = new smarty;

$smarty->template_dir = 'templates/';
$smarty->compile_dir = '/var/www/sms/engine/reporting2/templates_c/';
$smarty->config_dir = 'configs/';
$smarty->cache_dir = 'cache/';

$smarty->assign("img_dir",IMG_DIR);
$smarty->assign("css_file",CSS_FILE);
$smarty->assign("java_file",JAVA_FILE);

$smarty->assign("title_header",$title_header);
$smarty->assign("title",$title);
$smarty->assign("copyright",$copyright);
if(isset($_SESSION["ses_username"])) {
	$smarty->assign("ses_username",$_SESSION["ses_username"]);
}
if(isset($_SESSION["ses_operator"])) {
	$smarty->assign("ses_operator",$_SESSION["ses_operator"]);
}
if(isset($_SESSION["ses_service"])) {
	$smarty->assign("ses_service",$_SESSION["ses_service"]);
}
if(isset($_SESSION["ses_menu_module"])) {
	$smarty->assign("ses_menu_module",$_SESSION["ses_menu_module"]);
}
if(isset($_SESSION["appidses"])) {
	$ses_appid = $_SESSION["appidses"];
}
else $ses_appid = "all";

// Access Level

$levels = array("U","R","C","T","D");
$menu_html ='';


$array_menu = explode(",",$_SESSION["ses_menu_module"]);

foreach($array_menu as $m) {
	$a = explode("_",$m);
	if(strlen($a[0]) == 1) {
		$level[] = $a[0];
	}
}

rsort($level);


foreach(array_unique($level) as $lvl) {

	if($lvl == 'U') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'user') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-user"></i><span> User</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-user"></i><span> User</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-user"></i><span> User</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=user&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li><a href="'.$config['menu'][$menu_name].'?active=user&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="a"><a href="'.$config['menu'][$menu_name].'?active=user&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}
	

	if($lvl == 'T') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'cs') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-phone-square"></i><span> CS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-phone-square"></i><span> CS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-phone-square"></i><span> CS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=cs&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li><a href="'.$config['menu'][$menu_name].'?active=cs&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="a"><a href="'.$config['menu'][$menu_name].'?active=cs&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	if($lvl == 'C') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'cms') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-cogs"></i><span> Content Manage</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-cogs"></i><span> Content Manage</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-cogs"></i><span> Content Manage</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=cms&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li><a href="'.$config['menu'][$menu_name].'?active=cms&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="a"><a href="'.$config['menu'][$menu_name].'?active=cms&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	if($lvl == 'R') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'report') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-archive"></i><span> Report</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-archive"></i><span> Report</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-archive"></i><span> Report</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=report&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li><a href="'.$config['menu'][$menu_name].'?active=report&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="a"><a href="'.$config['menu'][$menu_name].'?active=report&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	if($lvl == 'D') {
		if(isset($_GET['active'])) {
			if($_GET['active'] == 'download') {
				$menu_html .= '<li class="treeview menu-open"><a href="#"><i class="fa fa-cloud-download"></i><span> Rich Content Download</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu" style="display: block;">';
			} else {
				$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-cloud-download"></i><span> Rich Content Download</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
			}
		} else {
			$menu_html .= '<li class="treeview"><a href="#"><i class="fa fa-cloud-download"></i><span> Rich Content Download</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
		}	

		foreach($array_menu as $menu_order => $menuArr) {

			if(substr($menuArr,0,2) == $lvl.'_') {
				$menu_name = substr($menuArr,2);
				if(isset($_GET['menu'])) {
					if($_GET['menu'] == $menu_name) {
						$menu_html .= '<li class="active"><a href="'.$config['menu'][$menu_name].'?active=download&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					} else {
						$menu_html .= '<li><a href="'.$config['menu'][$menu_name].'?active=download&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
					}
				} else {
					$menu_html .= '<li class="a"><a href="'.$config['menu'][$menu_name].'?active=download&menu='.$menu_name.'"><i class="fa fa-bars"></i> <span> '.$menu_name.'</span></a></li>';
				}
			}

		}
		
	}

	$menu_html .= '</ul></li>';
}


$smarty->assign("menu_html",$menu_html);

include_once('functions.php');

$mFunc =& new myFunctions();

?>