<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$tgl_now = date("Y-m-d");
$smarty->assign("tgl_now",$tgl_now);

if(empty($_GET['type'])) {
	$check = 1;
}

if( empty($_GET['bln']) ) $bulan = date("m");
else $bulan = $_GET['bln'];

if( empty($_GET['thn']) ) $tahun = date("Y");
else $tahun = $_GET['thn'];

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco2', $_GET['telco2'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);

if(isset($_GET['submit'])) {
	$where = ""; $lists = ""; $stat_tot = ''; $stat_d = ""; $stat_u = "";
	$appids = ""; $descs = ''; $dlvs = ''; $undlvs = ''; $total_sms = ''; $c_sms = ''; $c_wap = ''; $c_web = ''; $c_stk = '';
	$c_umb = ''; $c_menu = ''; $c_other = ''; $gross_rev = ''; $net_rev = ''; $stat_d = ''; $stat_u = ''; $stat_gr = '';
	$stat_nr = ''; $tot_sms = ''; $tot_wap = ''; $tot_web = ''; $tot_stk = ''; $tot_umb = ''; $tot_menu = ''; $tot_other = ''; $stat_tot = '';
	
	switch($_GET['type']) {
		case "bln" :
			$bln = $_GET['bln'];
			$thn = $_GET['thn'];
			$check = 3;
			$where .= " AND date_format(m.datetime,'%Y-%m') = '$thn-$bln'";
			$log = $thn.$bln;
			$smarty->assign("bln",$_GET['bln']);
			$smarty->assign("thn",$_GET['thn']);
		break;
		case "other" :
			$tgl = $_GET['tgl'];
			$check = 2;
			$where .= " AND date_format(m.datetime,'%Y-%m-%d') = '$tgl'";
			$arrTgl = explode("-",$tgl);
			$log = $arrTgl[0].$arrTgl[1];
			$smarty->assign("tgl",$_GET['tgl']);
		break;
		default :
			$tgl = $tgl_now;
			$where .= " AND date_format(m.datetime,'%Y-%m-%d') = '$tgl'";
			$arrTgl = explode("-",$tgl);
			$log = $arrTgl[0].$arrTgl[1];
			$check = 1;
		break;
	}
	
	$smarty->assign("type",$_GET['type']);
	
	if(!empty($_GET['telco2'])) {
		if($_GET['telco2'] <> "all") {
			$where .= " AND telco like '".$_GET['telco2']."%'";
		}
		
		$telco_name = $mFunc->get_telco_name($_GET['telco2']);
		$smarty->assign("telco2",$_GET['telco2']);
		$smarty->assign("telco_name",$telco_name);
	}
	
	if(!empty($_GET['shortcode2'])) {
		
		$where .= " AND shortcode = '".$_GET['shortcode2']."'";
		$smarty->assign("shortcode2",$_GET['shortcode2']);
	}
	
	if(!empty($_GET['media2'])) {
		if($_GET['media2'] <> "all") {
			$where .= " AND media_id = '".$_GET['media2']."'";
			
			$sql_media2 = "SELECT media_id,media_name FROM app_sms.media WHERE media_id=".$_GET['media2'];
			$rs_media2 = $db_app->Execute($sql_media2) or die( $db_app->ErrorMsg() );
			if ( $rs_media2->RecordCount() > 0 ) {
				while ( !$rs_media2->EOF ) {
					$media2_id = $rs_media2->fields[0];
					$media2_name = $rs_media2->fields[1];
					
					$rs_media2->MoveNext();
				}
				$smarty->assign("media2_id",$media2_id);
				$smarty->assign("media2_name",$media2_name);
			}
		}
		else {
			$smarty->assign("media2_name","All");
		}
	}
	
	if(!empty($_GET['partner2'])) {
		if($_GET['partner2'] <> "all") {
			$where .= " AND partner_id = '".$_GET['partner2']."'";
			
			$sql_partner2 = "SELECT partner_id,partner_name FROM app_sms.partner WHERE partner_id=".$_GET['partner2'];
			$rs_partner2 = $db_app->Execute($sql_partner2) or die( $db_app->ErrorMsg() );
			if ( $rs_partner2->RecordCount() > 0 ) {
				while ( !$rs_partner2->EOF ) {
					$partner2_id = $rs_partner2->fields[0];
					$partner2_name = $rs_partner2->fields[1];
					
					$rs_partner2->MoveNext();
				}
				$smarty->assign("partner2_id",$partner2_id);
				$smarty->assign("partner2_name",$partner2_name);
			}
		}
		else {
			$smarty->assign("partner2_name","All");
		}
	}
	
	$sql = "SELECT mo_origin, m.appid, description, dn_status, telco, charge,count(*) as jumlah 
				FROM mp_system.mt_outgoing_log".$log." m,mp_system.application a
				WHERE m.appid=a.appid $where 
				GROUP BY m.appid, mo_origin, dn_status, telco, charge";
	//echo $sql."<BR/>";
	
	$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
	if ( $rs->RecordCount() > 0 ) {
		while ( !$rs->EOF ) {
			$mo_origin = strtolower($rs->fields[0]);
			$appid = $rs->fields[1];
			$description = $rs->fields[2];
			$dn_status = $rs->fields[3];
			$telco = $rs->fields[4];
			$mt_charge = $rs->fields[5];
			$jumlah = $rs->fields[6];
			if($mo_origin == "") $mo_origin = "sms";
			
			$charge = getChargeT($mt_charge,$telco,$appid);
			$charge = $charge + $charge * 0.1;
			//echo $charge."<br/>";
			$dlv_status = get_dlv_status($dn_status, $jumlah);
			
			$lists[$appid]['delivrd'] += $dlv_status;
			$lists[$appid]['hit'] += get_undlv_status($dn_status, $jumlah);
			$lists[$appid]['desc'] = $description;
			
			$lists[$appid]['rev'] += countRevenue($dlv_status, $charge);
			
			$lists[$appid]['cprev'] += countRevenueCP($dlv_status, $telco, $charge);
			
			$lists[$appid][$mo_origin] += $dlv_status;
			
			$rs->MoveNext();
		}
		$smarty->assign("media_id",$media_id);
		$smarty->assign("media_name",$media_name);
	}
	
	if(is_array($lists)) {
		foreach ($lists as $apid => $val) {

			$appids[] = $apid;
			$descs[] = $val['desc'];
			$dlvs[] += ($val['delivrd']);
			$undlvs[] += ($val['hit']);
			$total_sms[] += ($val['delivrd'] + $val['hit']);
			$c_sms[] += ($val['sms']);
			$c_wap[] += ($val['wap']);
			$c_web[] += ($val['web']);
			$c_stk[] += ($val['stk']);
			$c_umb[] += ($val['umb']);
			$c_menu[] += ($val['menu']);
			$c_other[] += ($val['other']);
			$gross_rev[] = ($val['rev']);
			$net_rev[] = (ceil($val['cprev']));
			
			$stat_d += ($val['delivrd']);
			$stat_u += ($val['hit']);
			$stat_gr += ($val['rev']);
			$stat_nr += (ceil($val['cprev']));
			$tot_sms += ($val['sms']);
			$tot_wap += ($val['wap']);
			$tot_web += ($val['web']);
			$tot_stk += ($val['stk']);
			$tot_umb += ($val['umb']);
			$tot_menu += ($val['menu']);
			$tot_other += ($val['other']);
			
		}
	}
	
	$stat_tot += ($stat_d + $stat_u);
	
	$smarty->assign("appids",$appids);
	$smarty->assign("descs",$descs);
	$smarty->assign("dlvs",($dlvs));
	$smarty->assign("undlvs",$undlvs);
	$smarty->assign("total_sms",$total_sms);
	$smarty->assign("c_sms",$c_sms);
	$smarty->assign("c_wap",$c_wap);
	$smarty->assign("c_web",$c_web);
	$smarty->assign("c_stk",$c_stk);
	$smarty->assign("c_umb",$c_umb);
	$smarty->assign("c_menu",$c_menu);
	$smarty->assign("c_other",$c_other);
	$smarty->assign("gross_rev",$gross_rev);
	$smarty->assign("net_rev",$net_rev);
	$smarty->assign("stat_d",$stat_d);
	$smarty->assign("stat_u",$stat_u);
	$smarty->assign("stat_gr",$stat_gr);
	$smarty->assign("stat_nr",$stat_nr);
	$smarty->assign("tot_sms",$tot_sms);
	$smarty->assign("tot_wap",$tot_wap);
	$smarty->assign("tot_web",$tot_web);
	$smarty->assign("tot_stk",$tot_stk);
	$smarty->assign("tot_umb",$tot_umb);
	$smarty->assign("tot_menu",$tot_menu);
	$smarty->assign("tot_other",$tot_other);
	$smarty->assign("stat_tot",$stat_tot);
	
}     
      
$sql_media = "SELECT media_id,media_name FROM app_sms.media ORDER BY media_id ASC";
$rs_media = $db_app->Execute($sql_media) or die( $db_app->ErrorMsg() );
if ( $rs_media->RecordCount() > 0 ) {
	while ( !$rs_media->EOF ) {
		$media_id[] = $rs_media->fields[0];
		$media_name[] = $rs_media->fields[1];
		
		$rs_media->MoveNext();
	}
	$smarty->assign("media_id",$media_id);
	$smarty->assign("media_name",$media_name);
}

function getChargeT($charge,$telco,$appid) {
	if($appid == 99999) {
		$charge = 0;
	}
	else {
		if($charge) {
			switch(strtolower($telco)) {
				case "esia" :
					if(strtoupper($charge{1}) == "S") $charge = str_replace("PS", "", $charge);
					else {
						$charge = str_replace("PL", "", $charge);
						if($charge <> 0) $charge = $charge - 250; 
					}
					break;
				default :
					if(strtoupper($charge{1}) == "S") $charge = str_replace("PS", "", $charge);
					else {
						$charge = str_replace("PL", "", $charge); 
						if($charge <> 0) $charge = $charge - 350; 
					}
					break;
			}
		}
		else {
			$charge = 0;
		}
	}
	
	return $charge;
}

$sql_partner = "SELECT partner_id,partner_name FROM app_sms.partner ORDER BY partner_id ASC";
$rs_partner = $db_app->Execute($sql_partner) or die( $db_app->ErrorMsg() );
if ( $rs_partner->RecordCount() > 0 ) {
	while ( !$rs_partner->EOF ) {
		$partner_id[] = $rs_partner->fields[0];
		$partner_name[] = $rs_partner->fields[1];
		
		$rs_partner->MoveNext();
	}
	$smarty->assign("partner_id",$partner_id);
	$smarty->assign("partner_name",$partner_name);
}

$smarty->assign("check",$check);

if(!empty($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
}

$smarty->display(TEMPLATES_DIR.'report.html');

function get_dlv_status($dn_status, $jumlah){
	if($dn_status == "3700") {
		return $jumlah;
	}
	else {
		return 0;
	}
}

function get_undlv_status($dn_status, $jumlah){
	if($dn_status == "3700") {
		return 0;
	}
	else {
		return $jumlah;
	}
}

function countRevenue($Dstatus, $charge){
	return $Dstatus * $charge;
}

function countRevenueCP($Dstatus, $telco, $charge){
	$telco = strtolower($telco);
	
	switch($telco) {
		case "tsel" : $percent = 0.5;
			break;
		case "isat" : $percent = 0.5;
			break;
		case "xl" : $percent = 0.5;
			break;
		case "flexi" : $percent = 0.5;
			break;
		case "esia" : $percent = 0.5;
			break;
		case "mobile8" : $percent = 0.5;
			break;
		case "hutch" : $percent = 0.5;
			break;
		case "axis" : $percent = 0.5;
			break;
		case "nts" : $percent = 0.5;
			break;
	}

	return ($Dstatus * $charge) * $percent;
}

?>
