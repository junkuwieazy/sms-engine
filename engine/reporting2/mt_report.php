<?php

include_once('lib/include.php');

$config['prefix'] = array('62814' => '62814', '62815' => '62815', '62816' => '62816', '62855' => '62855', '62856' => '62856', '62857' => '62857', '62858' => '62858');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$dd_prefix = $mFunc->crtDropDownTelco($config['prefix'],$config['prefix'], 'prefix2', $_GET['prefix2'], 'All', 'class="text"');
$smarty->assign("dd_prefix",$dd_prefix);

if(!empty($_GET['bln'])) {
	$bulan = $_GET['bln'];
	$bulan_name = get_bulan_name($_GET['bln']);
}
else {
	$bulan = date("m");
	$bulan_name = date("F");
}
$smarty->assign("bulan",$bulan);
$smarty->assign("bulan_name",$bulan_name);

$tahun = (!empty($_GET['thn'])) ? $_GET['thn'] : date("Y");
$smarty->assign("tahun",$tahun);

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="text"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="text"');
$smarty->assign("dd_year",$dd_year);

if(isset($_GET['submit'])) {
	$log = $tahun.$bulan;
	$smarty->assign("submit", $_GET['submit'] );
	if(!empty($_GET['prefix2'])) {
		if($_GET['prefix2'] <> "--") {
			$where .= " AND msisdn LIKE '".$_GET['prefix2']."%'";
		}
		$smarty->assign("prefix2",$_GET['prefix2']);
	}

	if(!empty($_GET['tgl2'])) {
		$tgl_start = $_GET['tgl2'];
		$tgl_log = str_replace("-","",$_GET['tgl2']);
		$smarty->assign("tgl2",$_GET['tgl2']);
	}
	else {
		$tgl_start = date("Y-m-d");
		$smarty->assign("tgl2",$_GET['tgl2']);
	}

	if(!empty($_GET['tgl3'])) {
		$tgl_end = $_GET['tgl3'];
		$smarty->assign("tgl3",$_GET['tgl3']);
	}
	else {
		$tgl_end = date("Y-m-d");
		$smarty->assign("tgl3",$_GET['tgl3']);
	}
	$total_gross = 0;
	$limit = (!empty($_GET['limit'])) ? $_GET['limit'] : 20;
	$sql = "SELECT sid, charge, dn_telco, count( 1 ) AS jml FROM mp_system.mt_outgoing_log_".$tgl_log." WHERE 1 AND telco = 'isat' and dn_telco='2' and datetime between '".$tgl_start." 00:00:00' and '".$tgl_end." 23:59:59' $where GROUP BY sid, charge, dn_telco";
	//echo $sql;
	$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
	if ( $rs->RecordCount() > 0 ) {
		$j = 1;
		while ( !$rs->EOF ) {
			$prefix[] = $_GET['prefix2'];
			$sid[] = $rs->fields[0];
			$trf[] = $rs->fields[1];
			$tarif = split("-",$rs->fields[1]);

			if( in_array( $rs->fields[0] , array("95899331015004","95899331015005","95899331015006","95899331015007","95899331015008","95899331015009") ) ) {
				$charge[] = $tarif[1] - 350;
				$harga = $tarif[1] - 350;
			}
			else {
				$charge[] = $tarif[1];
				$harga = $tarif[1];
			}

			$dn_telco[] = $rs->fields[2];
			$jml[] = $rs->fields[3];
			$jumlah_final[] = round( ($rs->fields[3] * 0.2) + $rs->fields[3]);
			$gross[] = $harga * (  round(($rs->fields[3] * 0.2) + $rs->fields[3]) );
			$total_gross += $harga * ( round(($rs->fields[3] * 0.2) + $rs->fields[3]) );
			if($j%2 == 0) $warna[] = "#CCCCCC";
			else $warna[] = "#FFFFFF";
			$j++;
			$rs->MoveNext();
		}

		$smarty->assign("prefix",$prefix);
		$smarty->assign("sid",$sid);
		$smarty->assign("trf",$trf);
		$smarty->assign("charge",$charge);
		$smarty->assign("dn_telco",$dn_telco);
		$smarty->assign("jumlah_final",$jumlah_final);
		$smarty->assign("gross",$gross);
		$smarty->assign("total_gross",$total_gross);
		$smarty->assign("warna",$warna);

	}
}

$smarty->display(TEMPLATES_DIR.'mt_report.html');

function get_bulan_name($bln) {
	switch($bln) {
		case "01" : $bulan_name = "January"; break;
		case "02" : $bulan_name = "February"; break;
		case "03" : $bulan_name = "March"; break;
		case "04" : $bulan_name = "April"; break;
		case "05" : $bulan_name = "May"; break;
		case "06" : $bulan_name = "June"; break;
		case "07" : $bulan_name = "July"; break;
		case "08" : $bulan_name = "August"; break;
		case "09" : $bulan_name = "September"; break;
		case "10" : $bulan_name = "October"; break;
		case "11" : $bulan_name = "November"; break;
		case "12" : $bulan_name = "December"; break;
	}
	return $bulan_name;
}

?>
