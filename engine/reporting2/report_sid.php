<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$bln_now = date("m");
$smarty->assign("bln_now",$bln_now);

$thn_now = date("Y");
$smarty->assign("thn_now",$thn_now);

$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="form-control"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND s.shortcode IN('".$_GET['shortcode']."')";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else {
		$b = "'".join("','",array_keys($config['shortcode']))."'";
		$str_sql .= " AND s.shortcode IN(".$b.")";
		$smarty->assign("sc","All");
	}
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="form-control"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="form-control"');
}
//$dd_telco = $mFunc->crtDropDown($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="text"');
$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND s.telco IN('".$_GET['telco']."')";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else {
		$a = "'".join("','",array_keys($config['telco']))."'";
		$str_sql .= " AND s.telco IN(".$a.")";
		$smarty->assign("tlc", "All" );
	}
}

if( isset($_GET['bln']) ) $bulan = $_GET['bln'];
else $bulan = date("m");
if( isset($_GET['thn']) ) $tahun = $_GET['thn'];
else $tahun = date("Y");

$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="form-control"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="form-control"');
$smarty->assign("dd_year",$dd_year);

if(isset($_GET['submit'])) {

	$smarty->assign("bln",$_GET['bln']);
	$smarty->assign("thn",$_GET['thn']);

	$sql = "SELECT s.telco,s.shortcode,s.charge,l.sid,SUM(s.jumlah) as jml,l.bearer_cost FROM mp_system.mt_summary_daily_".$_GET['thn'].$_GET['bln']." as s, mp_system.list_sid as l WHERE 1 AND s.telco = l.telco AND s.shortcode = l.shortcode AND s.charge = l.charge $str_sql AND s.dn_status = 'delivered' GROUP BY s.charge";

	//$sql = "SELECT s.telco,s.shortcode,s.charge,l.sid,SUM(jumlah) as jml FROM mp_system.mt_summary_daily_".$_GET['thn'].$_GET['bln']." as s, mp_system.list_sid as l WHERE 1 AND s.telco = l.telco AND s.shortcode = l.shortcode $str_sql AND s.dn_status = 'delivered' GROUP BY l.sid,s.charge";
	$tot_gross = 0;
	$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
	if ( $rs->RecordCount() > 0 ) {

		while ( !$rs->EOF ) {

			$ar_price = explode("-",$rs->fields[2]);

			if($ar_price[1] == 250 || $ar_price[1] == 'REG' || $ar_price[1] == 'UNREG') {
				$price_ = 0;
			} else {
				$price_ = $ar_price[1];
			}

			$price[] = $price_ - $rs->fields[5];
			$gross[] = ($price_ - $rs->fields[5]) * $rs->fields[4];
			$tot_gross += ($price_ - $rs->fields[5]) * $rs->fields[4];
			$telcoZ[] = $rs->fields[0];
			$shortcodeZ[] = $rs->fields[1];
			$chargeZ[] = $rs->fields[2];
			$sidZ[] = $rs->fields[3];
			$jmlZ[] = $rs->fields[4];

			$rs->MoveNext();
		}

		$smarty->assign("price",$price);
		$smarty->assign("telco",$telcoZ);
		$smarty->assign("shortcode",$shortcodeZ);
		$smarty->assign("charge",$chargeZ);
		$smarty->assign("sid",$sidZ);
		$smarty->assign("jml",$jmlZ);
		$smarty->assign("gross",$gross);
		$smarty->assign("tot_gross",$tot_gross);
	}

}

if(!empty($_GET['submit'])) {
	$smarty->assign("submit",$_GET['submit']);
}

$smarty->display(TEMPLATES_DIR.'report_sid.html');

?>
