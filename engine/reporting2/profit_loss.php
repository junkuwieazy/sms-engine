<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

if( $_SESSION["ses_service"] == "all" ) {
	$sql = "SELECT appid,description FROM mp_system.application";
}
else {
	$arr = explode(",",$_SESSION['ses_service']);
	$where = " AND appid in (";
	for($i=0;$i< count($arr);$i++) {
		$where .= "'".$arr[$i]."',";
	}
	$where1 = ereg_replace(',$',"",$where);
	$where2 = ")";
	$fix_where = $where1.$where2;
	$sql = "SELECT appid,description FROM mp_system.application WHERE 1 $fix_where";
}

$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		$arr_service[$rs->fields[0]] = $rs->fields[1];
		$rs->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDown($arr_service,$arr_service, 'service', $_GET['service'], '', 'class="form-control"');
$smarty->assign("dd_service",$dd_service);



if(isset($_GET['thn'])){
	$tahun = $_GET['thn'];
} else{
	$tahun = date("Y");
}

if(isset($_GET['bln'])){
	$bulan = $_GET['bln'];
} else{
	$bulan = date("m");
}
$dd_month = $mFunc->crtDropDownMonth($config['month'],$config['month'], 'bln', $bulan, '', 'class="form-control"');
$smarty->assign("dd_month",$dd_month);
$dd_year = $mFunc->crtDropDownYear($config['year'],$config['year'], 'thn', $tahun, '', 'class="form-control"');
$smarty->assign("dd_year",$dd_year);


$str_sql = "";

$dd_shortcode = $mFunc->crtDropDown($config['shortcode'],$config['shortcode'], 'shortcode', $_GET['shortcode'], 'All', 'class="form-control"');
$smarty->assign("dd_shortcode",$dd_shortcode);
if(empty($_GET['shortcode'])) $smarty->assign("sc","All");
else {
	if($_GET['shortcode'] <> "--") {
		$str_sql .= " AND shortcode = '".$_GET['shortcode']."'";
		$smarty->assign("sc",$_GET['shortcode']);
	}
	else $smarty->assign("sc","All");
}

if( $_SESSION["ses_operator"] <> "all" ) {
	$arr = explode(",",$_SESSION['ses_operator']);
	for($i=0;$i<count($arr);$i++) {
		$arr_telco[$arr[$i]] = $config['telco'][$arr[$i]];
	}
	$dd_telco = $mFunc->crtDropDownTelco($arr_telco,$arr_telco, 'telco', $_GET['telco'], 'All', 'class="form-control"');
}
else {
	$dd_telco = $mFunc->crtDropDownTelco($config['telco'],$config['telco'], 'telco', $_GET['telco'], 'All', 'class="form-control"');
}

$smarty->assign("dd_telco",$dd_telco);
if(empty($_GET['telco'])) $smarty->assign("tlc","All");
else {
	if($_GET['telco'] <> "--") {
		$str_sql .= " AND telco = '".$_GET['telco']."'";
		$smarty->assign("tlc", $_GET['telco'] );
	}
	else $smarty->assign("tlc", "All" );
}

if(isset($_GET['submit'])) {
	
	$smarty->assign("submit", $_GET['submit'] );
	
	$total_nett = 0;
	
	$sql = "SELECT charge,jumlah,bulan,telco FROM mp_system.nett_monthly_service WHERE 1 AND appid='".$_GET['service']."' AND bulan = '".$tahun."-".$bulan."' $str_sql ";
	$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
	if ( $rs->RecordCount() > 0 ) {
		
		while ( !$rs->EOF ) {
			
			$ar_price = explode("-",$rs->fields[0]);
			$price = $ar_price[1];
			if( $price == 250 ) $price = 0;
			
			if( $rs->fields[2] == "2011-03" ) $telco_share = 0.5;
			elseif( $rs->fields[2] == "2011-04" ) $telco_share = 0.5;
			elseif( $rs->fields[2] == "2011-05" ) $telco_share = 0.5;
			else $telco_share = $config['share'][$rs->fields[3]];
			
			$total_nett += $rs->fields[1] * ($price * $telco_share );
			
			$rs->MoveNext();
		}
		
	}

	$sqlbn = "SELECT telco, charge, SUM( jumlah ) AS jml FROM mp_system.mt_summary_daily_$tahun$bulan WHERE 1 AND appid = '".$_GET['service']."' AND dn_status = 'delivered' AND datetime LIKE '".$tahun."-".$bulan."%' $str_sql  GROUP BY telco,charge";
	$rsbn = $db->Execute($sqlbn) or die( $db->ErrorMsg() );
	if ( $rsbn->RecordCount() > 0 ) {
		
		while ( !$rsbn->EOF ) {
			
			$ar_pricebn = explode("-",$rsbn->fields[1]);
			$pricebn = $ar_pricebn[1];
			if( $pricebn == 250 ) $pricebn = 0;
			
			$telco_sharebn = $config['share'][$rsbn->fields[0]];
			
			$total_nett += $rsbn->fields[2] * ($pricebn * $telco_sharebn );
			
			$rsbn->MoveNext();
		}
		
	}
	
	$sqlbnh = "SELECT telco, charge, SUM( jumlah ) AS jml FROM mp_system.mt_summary_hourly WHERE 1 AND appid = '".$_GET['service']."' AND dn_status = 'delivered' AND entry_date LIKE '%".$tahun."-".$bulan."%' $str_sql GROUP BY telco,charge";
	$rsbnh = $db->Execute($sqlbnh) or die( $db->ErrorMsg() );
	if ( $rsbnh->RecordCount() > 0 ) {
		
		while ( !$rsbnh->EOF ) {
			
			$ar_pricebnh = explode("-",$rsbnh->fields[1]);
			$pricebnh = $ar_pricebnh[1];
			if( $pricebnh == 250 ) $pricebnh = 0;
			
			$telco_sharebnh = $config['share'][$rsbnh->fields[0]];
			
			$total_nett += $rsbnh->fields[2] * ($pricebnh * $telco_sharebnh );
			
			$rsbnh->MoveNext();
		}
		
	}
	
	$total_cost = 0;


	$sqlc = "SELECT SUM(cost_amount) as cost FROM mp_system.cost_service WHERE 1 AND appid='".$_GET['service']."' AND month_year = '".$tahun."-".$bulan."' $str_sql";
	$rsc = $db->Execute($sqlc) or die( $db->ErrorMsg() );
	if ( $rsc->RecordCount() > 0 ) {
			$total_cost = $rsc->fields[0];
	}
	
	$profit_loss = $total_nett - $total_cost;
	
	$smarty->assign("s_name", $mFunc->get_description($db, $_GET['service']) );
	$smarty->assign("appid", $_GET['service'] );
	$smarty->assign("total_nett", $total_nett );
	$smarty->assign("total_cost", $total_cost );
	$smarty->assign("profit_loss", $profit_loss );
	
}

$smarty->display(TEMPLATES_DIR.'profit_loss.html');

?>