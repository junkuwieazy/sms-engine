<?
include_once('lib/include.php');

if(!isset($_SESSION["ses_username"])) {
	header("Location:index.php");
}

$msg = "";

$smarty->assign("smt", "Submit");

if( isset($_GET['appid']) && !isset($_POST['appid']) ) $_POST['appid'] = $_GET['appid'];

$smarty->assign("len_content", $len_content);

if(isset($_POST['Submit'])) {
	if(empty($_POST['content']) ) {
	    $msg = "Content harus diisi"; 
	}
	elseif( $_POST['appid'] == "all" ) {
	    $msg = "Pilih content yang akan di isi"; 
	}
	else {
		$sql = "INSERT INTO app_sms.content_text (appid,category,content,publish_date,publish_seq,datetime) 
				VALUES ('".$_POST['appid']."','".$_POST['category']."','".$_POST['content']."','".$_POST['publish_date']."',".$_POST['publish_seq'].",NOW())";
		$result = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if($result) {
			$msg = "Input content berhasil";
		}
		else {
			$msg = "Input gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_POST['SubmitEdit'])) {
	if(empty($_POST['content']) ) {
	    $msg = "Content harus diisi"; 
	}
	elseif( $_POST['appid'] == "all" ) {
	    $msg = "Pilih content yang akan di isi"; 
	}
	else {
		$sql = "UPDATE app_sms.content_text SET appid = '".$_POST['appid']."', category='".$_POST['category']."', content='".$_POST['content']."', publish_date='".$_POST['publish_date']."', publish_seq=".$_POST['publish_seq']." WHERE id=".$_POST['id'];
		$result = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if($result) {
			$msg = "Edit content berhasil";
		}
		else {
			$msg = "Edit gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "edit") {
	$smarty->assign("smt", "SubmitEdit");
	$sqle = "SELECT category,content,publish_date,publish_seq,id FROM app_sms.content_text WHERE id=".$_GET['id'];
	$rse = $db->Execute($sqle) or die( $db->ErrorMsg() );
	if ( $rse->RecordCount() > 0 ) {
		while ( !$rse->EOF ) {
			 $categorye = $rse->fields[0];
			 $contente = $rse->fields[1];
			 $publish_datee = $rse->fields[2];
			 $publish_seqe = $rse->fields[3];
			 $ide = $rse->fields[4];
			 
			 $rse->MoveNext();
		}
		
		$smarty->assign("ide",$ide);
		$smarty->assign("categorye",$categorye);
		$smarty->assign("contente",$contente);
		$smarty->assign("publish_datee",$publish_datee);
		$smarty->assign("publish_seqe",$publish_seqe);
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "delete") {
	$sql = "DELETE FROM app_sms.content_text WHERE id=".$_GET['id'];
	$result = $db->Execute($sql) or die( $db->ErrorMsg() );
}

$fix_where_appid = "";
if( $ses_appid <> "all" ) {
	$array_appid = explode(",",$ses_appid);
	
	$where_appid = " AND c.appid in (";
	for($ip=0;$ip< count($array_appid);$ip++) {
		$where_appid .= "'".$array_appid[$ip]."',";
	}
	$where_appid1 = ereg_replace(',$',"",$where_appid);
	$where_appid2 = ")";
	$fix_where_appid = $where_appid1.$where_appid2;
}

$sql_app = "SELECT appid,description FROM app_sms.appid_sms c WHERE 1 $fix_where_appid ORDER BY appid ASC";
$rs_app = $db->Execute($sql_app) or die( $db->ErrorMsg() );
if ( $rs_app->RecordCount() > 0 ) {
	while ( !$rs_app->EOF ) {
		$arr_service[$rs_app->fields[0]] = $rs_app->fields[0]." - ".$rs_app->fields[1];
		$rs_app->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDownContent($arr_service,$arr_service, 'appid', $_POST['appid'], 'All', 'class="form-control"');
$smarty->assign("dd_service",$dd_service);

if( isset($_POST['appid']) && $_POST['appid'] <> "all" ) $str_sql = " AND c.appid='".$_POST['appid']."'";

if ( $_POST['appid'] == "10015" ) $order_by = "order by publish_seq DESC";
else $order_by = "order by publish_date DESC, publish_seq DESC";

$begin = (!empty($_GET['begin'])) ? $_GET['begin'] : 0;
$limit = 10; 

$sql = "SELECT c.appid,description,category,content,publish_date,publish_seq,c.datetime,c.id FROM app_sms.content_text c, app_sms.appid_sms a WHERE c.appid=a.appid $str_sql $fix_where_appid $order_by LIMIT $begin, $limit";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		 $appid[] = $rs->fields[0];
		 $desc[] = $rs->fields[1];
		 $category[] = $rs->fields[2];
		 $content[] = $rs->fields[3];
		 $publish_date[] = $rs->fields[4];
		 $publish_seq[] = $rs->fields[5];
		 $entry_date[] = $rs->fields[6];
		 $id[] = $rs->fields[7];
		 
		 $rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("desc",$desc);
	$smarty->assign("id",$id);
	$smarty->assign("category",$category);
	$smarty->assign("content",$content);
	$smarty->assign("publish_date",$publish_date);
	$smarty->assign("publish_seq",$publish_seq);
	$smarty->assign("entry_date",$entry_date);
	
	if( isset($_POST['appid']) && $_POST['appid'] <> "all") {
		$sql_count = "SELECT c.appid FROM app_sms.content_text c, app_sms.appid_sms a WHERE c.appid=a.appid $str_sql $fix_where_appid";
		$numresult = $db->Execute($sql_count);
	
		$numrows = $numresult->RecordCount();
		
		$smarty->assign("numrows",$numrows);
		$smarty->assign("app_i",$_POST['appid']);
		
		/*** Manage movement ***/
		$first = 0;
		
		if ( empty($_GET['begin']) ) {
			$prev = 0;
			$next = $limit;
			
			$counter = 1;
			$dispfrom = 1;
		} else {
			$prev = $_GET['begin'] - $limit;
			$next = $_GET['begin'] + $limit;
			
			$dispfrom = $counter = $_GET['begin'] + 1;
		}
		$dispto = $dispfrom + ($limit-1);
		if ($prev < 0) $prev = 0;
	
		$lastremainder = $numrows % $limit;
		
		if ( $lastremainder == 0) {
			$last = $numrows - $limit;
		} else {
			$last = $numrows - $lastremainder;
		}
		
		if ( $next > $last ) $next = $last;
		if ( $dispto > $numrows ) $dispto = $numrows;
		// if ( $dispto == $dispfrom ) $dispto = "";
		
		$smarty->assign( 'first', $first );
		$smarty->assign( 'prev', $prev );
		$smarty->assign( 'next', $next );
		$smarty->assign( 'last', $last );
		$smarty->assign( 'dispfrom', $dispfrom );
		$smarty->assign( 'dispto', $dispto );
		
		/*** End of manage movement ***/
	}
	
	
	
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'content_text.html');

?>