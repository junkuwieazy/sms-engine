<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

// include_once("/home/web/orion/engine/include/config.php");
// include_once("/home/web/orion/engine/include/smarty/Smarty.class.php");
// include_once("/home/web/orion/engine/include/adodb/adodb.inc.php");

include_once("/var/www/sms/engine/include/config.php");
include_once("/var/www/sms/engine/include/smarty/Smarty.class.php");
include_once("/var/www/sms/engine/include/adodb/adodb.inc.php");

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$dblocal = &ADONewConnection( $config['dbTypelocal'] );
$dblocal->connect( $config['dbHostlocal'].":".$config['dbPortlocal'],$config['dbUserlocal'],$config['dbPasslocal'],$config['dbNamelocal']) or die ( $db->ErrorMsg() );

//Create DB Object
global $config_app;
$db_app = &ADONewConnection( $config_app['DBType'] );
$db_app->connect( $config_app['DBHost'].":".$config_app['DBPort'],$config_app['DBUser'],$config_app['DBPassword'],$config_app['DBName']) or die ( $db->ErrorMsg() );

?>


<!-- Modal content-->
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" id="clear">&times;</button>
		<h4 class="modal-title">Detail MT Persentage : <?php echo $_GET['tgl'];?></h4>
	</div>
	<div class="modal-body">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>NO</th>
					<th>SUBJECT</th>
					<th>TELCO</th>
					<th>SHORTCODE</th>
					<th>CHARGE</th>
					<th>DELIVERED</th>
					<th>TOTAL MT</th>
					<th>PERSENTAGE</th>
				</tr>
			</thead>

			<tbody>
				<?php
				$total_dn = 0;
				$i = 1;

        $ar_tgl = explode("-",$_GET['tgl']); // 2016-08-12

        if(strlen($ar_tgl[2]) == 1) {
        	$tanggals = $ar_tgl[0].'-'.$ar_tgl[1].'-0'.$ar_tgl[2];
        } else {
        	$tanggals = $_GET['tgl'];
        }

        $str = "";
        if( $_GET['telco'] <> "All" ) {
        	$str = " AND telco='".$_GET['telco']."'";
        } else {
        	$str = '';
        }

        if(!empty($_GET['s'])) {
        	$b = explode('-',$_GET['s']);
        	foreach($b as $bb) {
        		if(!empty($bb)) {
        			$c = "'".join("','",$b)."'";
        		}
        	}

        	$idApp = " AND appid IN(".$c.")";
        } else {
        	$idApp = '';
        }

        if( $_GET['sc'] <> "All" ) {
        	$sCode = " AND shortcode = '".$_GET['sc']."'";
        } else {
        	$sCode = '';
        }
        
        if( $tanggals == date("Y-m-d") ) {

        	$sqlFP = "SELECT appid,subject,telco,shortcode,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND dn_status = 'DELIVERED' AND subject LIKE '%FIRSTPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

        	$sqlDP = "SELECT appid,subject,telco,shortcode,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND dn_status = 'DELIVERED' AND subject LIKE '%DAILYPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

        	$sqlTotalFP = "SELECT sum(jumlah) as t_jml,subject,telco,shortcode,charge,dn_status FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND subject LIKE '%FIRSTPUSH%' AND charge != 'PULL-UNREG' AND charge != 'PULL-REG' AND subject NOT LIKE '%ERROR%' GROUP BY appid,subject,telco,shortcode,charge";

        	$sqlTotalDP = "SELECT sum(jumlah) as t_jml,subject,telco,shortcode,charge,dn_status FROM mp_system.mt_summary_hourly WHERE 1 $str $idApp $sCode AND subject LIKE '%DAILYPUSH%' AND charge != 'PULL-UNREG' AND charge != 'PULL-REG' AND subject NOT LIKE '%ERROR%' GROUP BY appid,subject,telco,shortcode,charge";

        }
        else {

        	$sqlFP = "SELECT appid,subject,telco,shortcode,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND dn_status = 'DELIVERED' AND subject LIKE '%FIRSTPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

        	$sqlDP = "SELECT appid,subject,telco,shortcode,charge,dn_status,sum(jumlah) as jml FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND dn_status = 'DELIVERED' AND subject LIKE '%DAILYPUSH%' GROUP BY appid,subject,telco,shortcode,charge";

        	$sqlTotalFP = "SELECT sum(jumlah) as t_jml,subject,telco,shortcode,charge,dn_status FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND subject LIKE '%FIRSTPUSH%' AND charge != 'PULL-UNREG' AND charge != 'PULL-REG' AND subject NOT LIKE '%ERROR%' GROUP BY appid,subject,telco,shortcode,charge";

        	$sqlTotalDP = "SELECT sum(jumlah) as t_jml,subject,telco,shortcode,charge,dn_status FROM mp_system.mt_summary_daily_".$ar_tgl[0].$ar_tgl[1]." WHERE 1 $str $idApp $sCode AND datetime = '".$_GET['tgl']."' AND subject LIKE '%DAILYPUSH%' AND charge != 'PULL-UNREG' AND charge != 'PULL-REG' AND subject NOT LIKE '%ERROR%' GROUP BY appid,subject,telco,shortcode,charge";

        }

        // echo "<pre>";


        //FIRSTPUSH DATA
        $rsFp = $db->Execute($sqlFP) or die( $db->ErrorMsg() );
        if ( $rsFp->RecordCount() > 0 ) {
        	while ( !$rsFp->EOF ) {
        		$firstData[] = $rsFp->fields;
        		$rsFp->MoveNext();
        	}
        }

        // DAILYPUSH DATA
        $rsDP = $db->Execute($sqlDP) or die( $db->ErrorMsg() );
        if ( $rsDP->RecordCount() > 0 ) {
        	while ( !$rsDP->EOF ) {
        		$dailyData[] = $rsDP->fields;
        		$rsDP->MoveNext();
        	}
        }

        // --------------------------------

        //FIRSTPUSH TOTAL
        $rsTFp = $db->Execute($sqlTotalFP) or die( $db->ErrorMsg() );
        if ( $rsTFp->RecordCount() > 0 ) {
        	while ( !$rsTFp->EOF ) {
        		$firstTotal[] = $rsTFp->fields;
        		$rsTFp->MoveNext();
        	}
        } 

        // DAILYPUSH TOTAL
        $rsTDp = $db->Execute($sqlTotalDP) or die( $db->ErrorMsg() );
        if ( $rsTDp->RecordCount() > 0 ) {
        	while ( !$rsTDp->EOF ) {            
        		$dailyTotal[] = $rsTDp->fields;
        		$rsTDp->MoveNext();
        	}
        }

        //FIRSTPUSH FETCH DATA
        foreach ($firstData as $k => $v) {
        	foreach ($firstTotal as $key => $value) {   
        		if($v[1] == $value[1] && $v[4] == $value[4]) { 
        			$d[$value[1]]['subject'] = $value[1];
        			$d[$value[1]]['telco'] = $value[2];
        			$d[$value[1]]['shortcode'] = $value[3];
        			$d[$value[1]]['charge'] = $value[4];
        			$d[$value[1]]['deliv'] = $v[6];
        			$d[$value[1]]['total'] = $value[0];
        		} else {
        			$d[$value[1]]['subject'] = $value[1];
        			$d[$value[1]]['telco'] = $value[2];
        			$d[$value[1]]['shortcode'] = $value[3];
        			$d[$value[1]]['charge'] = $value[4];
        			$d[$value[1]]['deliv'] = 0;
        			$d[$value[1]]['total'] = $value[0];                 
        		}
        	}
        }

        //FIRSTPUSH FETCH DATA
        foreach ($dailyData as $kF => $vF) {
        	foreach ($dailyTotal as $keyF => $valueF) {   
        		if($vF[1] == $valueF[1] && $vF[4] == $valueF[4]) { 
        			$d[$valueF[1]]['subject'] = $valueF[1];
        			$d[$valueF[1]]['telco'] = $valueF[2];
        			$d[$valueF[1]]['shortcode'] = $valueF[3];
        			$d[$valueF[1]]['charge'] = $valueF[4];
        			$d[$valueF[1]]['deliv'] = $vF[6];
        			$d[$valueF[1]]['total'] = $valueF[0];
        		} else {
        			$d[$valueF[1]]['subject'] = $valueF[1];
        			$d[$valueF[1]]['telco'] = $valueF[2];
        			$d[$valueF[1]]['shortcode'] = $valueF[3];
        			$d[$valueF[1]]['charge'] = $valueF[4];
        			$d[$valueF[1]]['deliv'] = 0;
        			$d[$valueF[1]]['total'] = $valueF[0];                 
        		}
        	}
        } 

        // print_r($d);

        foreach ($d as $ky => $u) {

        	?>
        	<tr>
        		<td style='text-align: right;'><?php echo $i;?></td>
        		<td style='text-align: right;'><?php echo $u['subject'];?></td>
        		<td style='text-align: left;'><?php echo $u['telco'];?></td>
        		<td style='text-align: right;'><?php echo $u['shortcode'];?></td>
        		<td style='text-align: left;'><?php echo $u['charge'];?></td>
        		<td style='text-align: right;'><?php echo number_format($u['deliv']);?></td>

        		<?php
        		$persen = ($u['deliv'] / $u['total']) * 100;
        		echo "<td style='text-align: right;'>".number_format($u['total'])."</td>";
        		echo "<td style='text-align: right;'>".number_format($persen,2,".",".")."%</td>";
        		?>
        	</tr>
        	<?php
        	$i++;
        }

        ?>
    </tbody>
</table>
</div>
</div>