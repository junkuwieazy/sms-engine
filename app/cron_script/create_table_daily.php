<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Create table -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], "mp_system");

$thnn = date("Y");
//$blnn = "10";

$date	= mktime(0,0,0,date("m")+1,date("d"),date("Y"));
$blnn = date("m",$date);

for( $i=1;$i<=31;$i++ ) {

	if( strlen($i) == 1 ) $tgl = "0".$i;
	else $tgl = $i;

	$sqldn = "CREATE TABLE IF NOT EXISTS mt_outgoing_log_".$thnn.$blnn.$tgl." (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(20) NOT NULL DEFAULT '',
  `mo_origin` varchar(10) DEFAULT NULL,
  `msisdn` varchar(30) NOT NULL DEFAULT '',
  `prepaid` int(2) NOT NULL DEFAULT '0',
  `sms` text,
  `subject` varchar(100) DEFAULT NULL,
  `message` text,
  `url` text,
  `telco` varchar(20) NOT NULL DEFAULT '',
  `shortcode` varchar(20) NOT NULL DEFAULT '',
  `charge` varchar(20) NOT NULL DEFAULT '0',
  `mt_type` varchar(20) DEFAULT NULL,
  `trx_id` varchar(50) NOT NULL DEFAULT '0',
  `trx_date` varchar(50) NOT NULL DEFAULT '0',
  `session_id` varchar(50) NOT NULL DEFAULT '0',
  `media_id` smallint(6) NOT NULL DEFAULT '0',
  `partner_id` int(3) NOT NULL DEFAULT '0',
  `ack_status` varchar(200) DEFAULT NULL,
  `sid` varchar(30) DEFAULT NULL,
  `dn_telco` varchar(30) DEFAULT NULL,
  `dn_status` varchar(20) DEFAULT NULL,
  `url_dr` text,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `priority` varchar(10) DEFAULT NULL,
  `shortname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ack_status` (`ack_status`),
  KEY `media_id` (`media_id`),
  KEY `shortcode` (`shortcode`),
  KEY `dn_status` (`dn_status`),
  KEY `mt_charge` (`charge`),
  KEY `datetime` (`datetime`),
  KEY `appid` (`appid`),
  KEY `dn_shortcode` (`sid`),
  KEY `msisdn` (`msisdn`),
  KEY `session_id` (`session_id`),
  KEY `trx_id` (`trx_id`),
  KEY `telco` (`telco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqldn);

	print($sqldn."\n");

}

print("Create table -- ".date("Y-m-d H:i:s")." -- End\n");

?>
