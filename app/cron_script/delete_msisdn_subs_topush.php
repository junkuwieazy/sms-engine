<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Delete subs -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$sql = "TRUNCATE TABLE app_sms.msisdn_subscriber_topush";
$db->query($sql);

print($sql."\n");

print("Delete subs -- ".date("Y-m-d H:i:s")." -- End\n");

?>