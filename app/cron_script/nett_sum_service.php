<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("NETT Summary Monthly Service -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$date	= mktime(0,0,0,date("m")-1,date("d"),date("Y"));
$yesterday	= date("Y-m",$date);
$log	= date("Ym",$date);

$sqld = "SELECT appid FROM mp_system.application WHERE appid not in ('10011','99999')";
$rsd = $db->query($sqld);
while ($rowd = $db->fetch($rsd)){
	
	$sql = "SELECT telco, shortcode, charge, SUM( jumlah ) as jml
					FROM mp_system.mt_summary_daily_".$log."
					WHERE 1
					AND dn_status = 'delivered' AND appid='".$rowd['appid']."'
					GROUP BY telco, shortcode, charge";
	$rs = $db->query($sql);
	while ($row = $db->fetch($rs)){
		
		$ar_price = explode("-",$row['charge']);
		$price = $ar_price[1];
		
		if( !in_array( $price, array("0", "250") ) ) {
			$sqli = "INSERT INTO mp_system.nett_monthly_service (appid,telco,shortcode,charge,jumlah,bulan,entry_date) VALUES ('".$rowd['appid']."','".$row['telco']."','".$row['shortcode']."','".$row['charge']."','".$row['jml']."','$yesterday',NOW())";
			$db->query($sqli);
			print($sql."\n");
		}
		
	}
	
}

print("NETT Summary Monthly Service -- ".date("Y-m-d H:i:s")." -- End\n");

?>