<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Create table -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$thnn = date("Y") + 1;

for( $i=1;$i<=12;$i++ ) {

	if( strlen($i) == 1 ) $bln = "0".$i;
	else $bln = $i;

	$sqldn = "CREATE TABLE IF NOT EXISTS `dn_telco_summary_".$thnn.$bln."` (
		  `id` bigint(10) NOT NULL AUTO_INCREMENT,
		  `appid` varchar(20) NOT NULL,
		  `telco` varchar(20) NOT NULL,
		  `shortcode` varchar(20) NOT NULL,
		  `charge` varchar(20) NOT NULL,
		  `dn_telco` text,
		  `dn_status` varchar(20) NOT NULL,
		  `jumlah` int(20) NOT NULL DEFAULT '0',
		  `datetime` date NOT NULL,
		  `entry_date` datetime NOT NULL,
		  PRIMARY KEY (`id`),
		  KEY `idx_appid` (`appid`),
		  KEY `idx_telco` (`telco`),
		  KEY `idx_shortcode` (`shortcode`),
		  KEY `idx_dn_status` (`dn_status`),
		  KEY `idx_datetime` (`datetime`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqldn);

	$sqldr = "CREATE TABLE IF NOT EXISTS `dr_incoming_log".$thnn.$bln."` (
		  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
		  `telco` varchar(20) NOT NULL DEFAULT '',
		  `shortcode` varchar(20) NOT NULL DEFAULT '',
		  `msisdn` varchar(30) NOT NULL DEFAULT '',
		  `trx_id` varchar(50) NOT NULL,
		  `sid` varchar(20) NOT NULL,
		  `status` varchar(10) NOT NULL,
		  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`id`),
		  KEY `status` (`status`),
		  KEY `transid` (`trx_id`),
		  KEY `datetime` (`datetime`),
		  KEY `shortcode` (`shortcode`),
		  KEY `msisdn` (`msisdn`),
		  KEY `telco` (`telco`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqldr);

	$sqlmo = "CREATE TABLE IF NOT EXISTS `mo_incoming_log".$thnn.$bln."` (
		  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
		  `mo_origin` varchar(9) NOT NULL DEFAULT '',
		  `msisdn` varchar(30) NOT NULL DEFAULT '',
		  `prepaid` int(2) NOT NULL DEFAULT '0',
		  `sms` text NOT NULL,
		  `telco` varchar(20) NOT NULL DEFAULT '',
		  `shortcode` varchar(13) NOT NULL DEFAULT '',
		  `trx_id` varchar(50) NOT NULL DEFAULT '0',
		  `trx_date` varchar(50) NOT NULL DEFAULT '0',
		  `session_id` varchar(50) NOT NULL DEFAULT '0',
		  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		  PRIMARY KEY (`id`),
		  KEY `telco` (`telco`),
		  KEY `datetime` (`datetime`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqlmo);

	$sqlmop = "CFREATE TABLE IF NOT EXISTS `mo_incoming_partner_log".$thnn.$bln."` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `mo_origin` varchar(10) NOT NULL,
  `appid` varchar(20) NOT NULL,
  `msisdn` varchar(30) NOT NULL,
  `prepaid` int(2) NOT NULL DEAULT '0',
  `sms` text NOT NULL,
  `telco` varchar(20) NOT NULL,
  `shortcode` varchar(10) NOT NULL,
  `trx_id` varchar(50) NOT NULL DEFAULT '0',
  `trx_date` varchar(50) NOT NULL DEFAULT '0',
  `session_id` varchar(50) NOT NULL DEFAULT '0',
  `partner_id` varchar(3) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_msisdn` (`msisdn`),
  KEY `idx_telco` (`telco`),
  KEY `idx_shortcode` (`shortcode`),
  KEY `idx_trx_id` (`trx_id`),
  KEY `idx_session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqlmop);

	$sqlmt = "CREATE TABLE IF NOT EXISTS `mt_outgoing_log".$thnn.$bln."` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `appid` varchar(20) NOT NULL DEFAULT '',
  `mo_origin` varchar(10) DEFAULT NULL,
  `msisdn` varchar(30) NOT NULL DEFAULT '',
  `prepaid` int(2) NOT NULL DEFAULT '0',
  `sms` text,
  `subject` varchar(100) DEFAULT NULL,
  `message` text,
  `url` text,
  `telco` varchar(20) NOT NULL DEFAULT '',
  `shortcode` varchar(20) NOT NULL DEFAULT '',
  `charge` varchar(9) NOT NULL DEFAULT '0',
  `mt_type` varchar(20) DEFAULT NULL,
  `trx_id` varchar(50) NOT NULL DEFAULT '0',
  `trx_date` varchar(50) NOT NULL DEFAULT '0',
  `session_id` varchar(50) NOT NULL DEFAULT '0',
  `media_id` smallint(6) NOT NULL DEFAULT '0',
  `partner_id` int(3) NOT NULL DEFAULT '0',
  `ack_status` varchar(200) DEFAULT NULL,
  `sid` varchar(30) DEFAULT NULL,
  `dn_telco` varchar(30) DEFAULT NULL,
  `dn_status` varchar(20) DEFAULT NULL,
  `url_dr` text,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `priority` varchar(10) DEFAULT NULL,
  `shortname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ack_status` (`ack_status`),
  KEY `media_id` (`media_id`),
  KEY `shortcode` (`shortcode`),
  KEY `dn_status` (`dn_status`),
  KEY `mt_charge` (`charge`),
  KEY `datetime` (`datetime`),
  KEY `appid` (`appid`),
  KEY `dn_shortcode` (`sid`),
  KEY `msisdn` (`msisdn`),
  KEY `session_id` (`session_id`),
  KEY `trx_id` (`trx_id`),
  KEY `telco` (`telco`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqlmt);

	$sqlmtp = "CREATE TABLE IF NOT EXISTS `mt_outgoing_partner_log".$thnn.$bln."` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `appid` varchar(20) NOT NULL,
  `mo_origin` varchar(10) DEFAULT NULL,
  `msisdn` varchar(30) NOT NULL,
  `prepaid` int(2) NOT NULL,
  `sms` text,
  `subject` varchar(100) DEFAULT NULL,
  `message` text,
  `url` text,
  `telco` varchar(20) NOT NULL,
  `shortcode` varchar(20) NOT NULL,
  `charge` varchar(20) NOT NULL DEFAULT '0',
  `mt_type` varchar(5) DEFAULT NULL,
  `trx_id` varchar(50) NOT NULL DEFAULT '0',
  `trx_date` varchar(50) NOT NULL DEFAULT '0',
  `session_id` varchar(50) NOT NULL DEFAULT '0',
  `media_id` int(3) NOT NULL DEFAULT '0',
  `partner_id` int(3) NOT NULL DEFAULT '0',
  `ack_status` text,
  `sid` varchar(30) DEFAULT NULL,
  `dn_telco` text,
  `dn_status` varchar(20) DEFAULT NULL,
  `url_dr` text,
  `datetime` datetime NOT NULL,
  `priority` varchar(10) DEFAULT NULL,
  `shortname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_appid` (`appid`),
  KEY `idx_msisdn` (`msisdn`),
  KEY `idx_telco` (`telco`),
  KEY `idx_shortcode` (`shortcode`),
  KEY `idx_charge` (`charge`),
  KEY `idx_mt_type` (`mt_type`),
  KEY `idx_trx_id` (`trx_id`),
  KEY `idx_session_id` (`session_id`),
  KEY `idx_dn_status` (`dn_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqlmtp);

	$sqlmsd = "CREATE TABLE IF NOT EXISTS `mt_summary_daily_".$thnn.$bln."` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `appid` varchar(20) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `telco` varchar(20) NOT NULL,
  `shortcode` varchar(20) NOT NULL,
  `charge` varchar(20) NOT NULL,
  `dn_status` varchar(20) NOT NULL,
  `jumlah` int(5) NOT NULL DEFAULT '0',
  `datetime` date NOT NULL,
  `entry_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqlmsd);

	$sqlsr = "CREATE TABLE IF NOT EXISTS `summary_reg_".$thnn.$bln."` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `appid` varchar(20) NOT NULL,
  `telco` varchar(20) NOT NULL,
  `shortcode` varchar(20) NOT NULL,
  `mo_type` varchar(20) NOT NULL,
  `jumlah` int(20) NOT NULL DEFAULT '0',
  `datetime` date NOT NULL,
  `entry_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_appid` (`appid`),
  KEY `idx_telco` (`telco`),
  KEY `idx_shortcode` (`shortcode`),
  KEY `idx_mo_type` (`mo_type`),
  KEY `idx_datetime` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
	$db->query($sqlsr);

	print($sqlmt."\n");

}

print("Create table -- ".date("Y-m-d H:i:s")." -- End\n");

?>
