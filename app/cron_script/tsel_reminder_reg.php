<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $path."include/functions.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("REMINDER REG TSEL -- ".date("Y-m-d H:i:s")." -- Start\n");

$datenow = date("Ymd");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$sql = "SELECT id,appid,msisdn,telco,shortcode,keyword,DATE_FORMAT(date_reg, '%Y%m%d') AS sdate FROM app_sms.msisdn_subscriber WHERE telco='tsel' AND status = 1";
$rs = $db->query($sql);
while ($row = $db->fetch($rs)){
	
	$date_diff = datediff___( "d", $row['sdate'] , $datenow );
	
	if( $date_diff % 7 == 0 ) {
		
		$last_keyword = $row['keyword'];
		
		$sqlk = "SELECT keyword FROM mp_system.keyword_reg WHERE telco='tsel' and appid='".$row['appid']."' LIMIT 1";
		$rsk = $db->query($sqlk);
		while ($rowk = $db->fetch($rsk)){
			$last_keyword = $rowk['keyword'];
		}
		$keyword = "REG ".$last_keyword;
		
		$sqli = "INSERT INTO app_sms.msisdn_session (appid,msisdn,shortcode,telco,keyword,entry_date) VALUES ('".$row['appid']."','".$row['msisdn']."','".$row['shortcode']."','".$row['telco']."','$keyword',NOW())";
		$db->query($sqli);
		
		$sqlu = "UPDATE app_sms.msisdn_subscriber SET status = 3 WHERE id=".$row['id'];
		$db->query($sqlu);
		
		$trx_date = date("YmdHis");
		$reply_sms = "Masa berlangganan layanan ".strtoupper($last_keyword)." sudah habis, ketik YA kirim ke 9477 utk melanjutkan layanan ini.";
		$subject = "PUSH;IOD;".strtoupper($last_keyword).";REMINDER";
		$push_price = "PUSH-0";
		$session_id = date("YmdHis").$row['msisdn'].rand(10000,99999);
		create_mt($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$media_id=0,$partner_id=0,$row['appid'],$subject,$priority=99,$url="",$mt_type=1,$url_dr="",$shortname="",$reply_sms);
		
		print($row['msisdn']." - ".$reply_sms."\n");
		
	}
	
}

print("REMINDER REG TSEL -- ".date("Y-m-d H:i:s")." -- End\n");


function datediff___($interval, $datefrom, $dateto, $using_timestamps = false) 
{
	if (!$using_timestamps) {
		$datefrom = strtotime($datefrom, 0);
		$dateto = strtotime($dateto, 0);
	}
	$difference = $dateto - $datefrom; // Difference in seconds	   
	
	switch($interval) {
	
	case 'yyyy': // Number of full years

		$years_difference = floor($difference / 31536000);
		if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
		  $years_difference--;
		}
		if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
		  $years_difference++;
		}
		$datediff = $years_difference;
		break;

	case "q": // Number of full quarters

		$quarters_difference = floor($difference / 8035200);
		while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
		  $months_difference++;
		}
		$quarters_difference--;
		$datediff = $quarters_difference;
		break;

	case "m": // Number of full months

		$months_difference = floor($difference / 2678400);
		while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
		  $months_difference++;
		}
		$months_difference--;
		$datediff = $months_difference;
		break;

	case 'y': // Difference between day numbers

		$datediff = date("z", $dateto) - date("z", $datefrom);
		break;

	case "d": // Number of full days

		$datediff = floor($difference / 86400);
		break;

	case "w": // Number of full weekdays

		$days_difference = floor($difference / 86400);
		$weeks_difference = floor($days_difference / 7); // Complete weeks
		$first_day = date("w", $datefrom);
		$days_remainder = floor($days_difference % 7);
		$odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
		if ($odd_days > 7) { // Sunday
		  $days_remainder--;
		}
		if ($odd_days > 6) { // Saturday
		  $days_remainder--;
		}
		$datediff = ($weeks_difference * 5) + $days_remainder;
		break;

	case "ww": // Number of full weeks

		$datediff = floor($difference / 604800);
		break;

	case "h": // Number of full hours

		$datediff = floor($difference / 3600);
		break;

	case "n": // Number of full minutes

		$datediff = floor($difference / 60);
		break;

	default: // Number of full seconds (default)

		$datediff = $difference;
		break;
	}	 

	return $datediff;
}

?>