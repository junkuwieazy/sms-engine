<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Clear Summary Hourly -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
$yesterday	= date("Y-m-d",$date); 

$sql = "DELETE FROM mp_system.mt_summary_hourly WHERE date_format(entry_date,'%Y-%m-%d') = '$yesterday'";
$db->query($sql);

print($sql."\n");

print("Clear Summary Hourly -- ".date("Y-m-d H:i:s")." -- End\n");

?>