<?php

$path = "/var/www/sms/app/logs/mo/";

$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
$yesterday	= date("Ymd",$date);

if(!is_dir( $path."archive/".$yesterday )) {
	if(!mkdir( $path."archive/".$yesterday )) {
		die("Create direktori gagal");
	}
}

$cekFile = opendir( $path );
while (false !== ($mFilename = readdir($cekFile))) {
	if ($mFilename != "." && $mFilename != ".."  && $mFilename != "archive" ) {
	
		if( $mFilename <> "mo_sms_1_".date("Ymd").".txt" && $mFilename <> "mo_sms_2_".date("Ymd").".txt" && $mFilename <> "mo_sms_3_".date("Ymd").".txt" && $mFilename <> "mo_sms_4_".date("Ymd").".txt" && $mFilename <> "mo_sms_5_".date("Ymd").".txt" ) {
			print( $mFilename ."\n");
			if(copy($path.$mFilename, $path."archive/".$yesterday."/".$mFilename)) {
				$mHapus = Unlink($path.$mFilename);
			}
		}
		
	}
}

closedir($cekFile);
	
?>