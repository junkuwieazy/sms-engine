<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Summary MO -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
$yesterday	= date("Y-m-d",$date); 

if( date("d") == "01" ) {
	$bln	= mktime(0,0,0,date("m")-1,date("d"),date("Y"));
	$log	= date("Ym",$bln); 
}
else {
	$log = date("Ym");
}

$sql = "SELECT mo_origin, telco, shortcode, count( 1 ) AS jml
				FROM mp_system.mo_incoming_log".$log." WHERE date_format(datetime,'%Y-%m-%d') = '$yesterday' 
				GROUP BY mo_origin, telco, shortcode
				ORDER BY tgl ASC";
$rs = $db->query($sql);
while ($row = $db->fetch($rs)){
	$sqli = "INSERT INTO mp_system.mo_summary_daily_".$log." (mo_origin,telco,shortcode,jumlah,datetime,entry_date)
						VALUES ('".$row['mo_origin']."','".$row['telco']."','".$row['shortcode']."',".$row['jml'].",'$yesterday',NOW())";
	$db->query($sqli);
	print($sql."\n");
}

print("Summary MO -- ".date("Y-m-d H:i:s")." -- End\n");

?>