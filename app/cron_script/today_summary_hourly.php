<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Today Summary Hourly -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

if( date("H") <> "00" ) {

	$jam_now = date("H");
	$jam_1 = $jam_now - 1;

	$sqld = "TRUNCATE TABLE mp_system.mt_summary_hourly";
	$db->query($sqld);

	$log = date("Ym");

	$sql = "SELECT appid, subject, telco, shortcode, charge, dn_telco,dn_status, count( 1 ) AS jml
					FROM mp_system.mt_outgoing_log_".date("Ymd")." WHERE date_format(datetime,'%Y-%m-%d') = '".date("Y-m-d")."' 
					GROUP BY appid, subject, telco, shortcode, charge, dn_telco, dn_status";
	//echo $sql;
	$rs = $db->query($sql);
	while ($row = $db->fetch($rs)) {
		$sqli = "INSERT INTO mp_system.mt_summary_hourly (appid,subject,telco,shortcode,charge,dn_telco, dn_status,jumlah,jam,entry_date)
							VALUES ('".$row['appid']."','".$row['subject']."','".$row['telco']."','".$row['shortcode']."','".$row['charge']."','".$row['dn_telco']."','".$row['dn_status']."',".$row['jml'].",'$jam_now',NOW())";
		$db->query($sqli);
		print($sqli."\n");
	}

}

print("Today Summary Hourly -- ".date("Y-m-d H:i:s")." -- End\n");

?>
