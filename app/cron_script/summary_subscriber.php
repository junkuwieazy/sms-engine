<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Summary REG -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
$yesterday	= date("Y-m-d",$date); 

if( date("d") == "01" ) {
	$bln	= mktime(0,0,0,date("m")-1,date("d"),date("Y"));
	$log	= date("Ym",$bln); 
}
else {
	$log = date("Ym");
}

$sql = "SELECT appid, shortcode, telco, count( 1 ) AS jml
				FROM app_sms.msisdn_subscriber
				WHERE 1 AND status = 1
				GROUP BY appid, shortcode, telco";
$rs = $db->query($sql);
while ($row = $db->fetch($rs)){
	$sqli = "INSERT INTO app_sms.msisdn_subscriber_daily (appid,shortcode,telco,member_status,jumlah,subs_date,entry_date)
						VALUES ('".$row['appid']."','".$row['shortcode']."','".$row['telco']."',1,".$row['jml'].",'$yesterday',NOW())";
	$db->query($sqli);
	print($sqli."\n");
}

$sqlu = "SELECT appid, shortcode, telco, count( 1 ) AS jml
				FROM app_sms.msisdn_subscriber
				WHERE 1 AND status <> 1
				GROUP BY appid, shortcode, telco";
$rsu = $db->query($sqlu);
while ($rowu = $db->fetch($rsu)){
	$sqliu = "INSERT INTO app_sms.msisdn_subscriber_daily (appid,shortcode,telco,member_status,jumlah,subs_date,entry_date)
						VALUES ('".$rowu['appid']."','".$rowu['shortcode']."','".$rowu['telco']."',0,".$rowu['jml'].",'$yesterday',NOW())";
	$db->query($sqliu);
	print($sqliu."\n");
}

print("Summary REG -- ".date("Y-m-d H:i:s")." -- End\n");

?>