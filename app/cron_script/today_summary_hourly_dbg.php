<?php

set_time_limit(0);

$path = "/var/www/sms/app/";

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

print("Today Summary Hourly -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

if( date("H") <> "00" ) {
	
	$jam_now = "15";
	$jam_1 = $jam_now - 1;
	
	if( strlen($jam_1) == 1 ) $jam_1_fix = "0".$jam_1;
	else $jam_1_fix = $jam_1;
	
	$log = date("Ym");
	
	$sql = "SELECT appid, subject, telco, shortcode, charge, dn_status, count( 1 ) AS jml 
					FROM mp_system.mt_outgoing_log".$log." WHERE date_format(datetime,'%Y-%m-%d %H') = '".date("Y-m-d")." ".$jam_1_fix."' AND telco <> 'xl' 
					GROUP BY appid, subject, telco, shortcode, charge, dn_status";
	//echo $sql;
	$rs = $db->query($sql);
	while ($row = $db->fetch($rs)) {
		$sqli = "INSERT INTO mp_system.mt_summary_hourly (appid,subject,telco,shortcode,charge,dn_status,jumlah,jam,entry_date)
							VALUES ('".$row['appid']."','".$row['subject']."','".$row['telco']."','".$row['shortcode']."','".$row['charge']."','".$row['dn_status']."',".$row['jml'].",'$jam_1_fix',NOW())";
		$db->query($sqli);
		print($sqli."\n");
	}
	
}

print("Today Summary Hourly -- ".date("Y-m-d H:i:s")." -- End\n");

?>