#!/bin/sh

allprocess="
/mt_driver.php 1
/mt_driver.php 2
/mt_driver.php 3
/mt_driver.php 4
/mt_driver.php 5
/mt_driver.php 6
/mt_driver.php 7
/mt_driver.php 8
/mt_driver.php 9
/mt_driver.php 10
/mt_driver.php 11
/mt_driver.php 12
/mt_driver.php 13
/mt_driver.php 14
/mt_driver.php 15
/mt_driver.php 16
/mt_driver.php 17
/mt_driver.php 18
/mt_driver.php 19
/mt_driver.php 20
/mt_driver.php 21
/mt_driver.php 22
/mt_driver.php 23
/mt_driver.php 24
/mt_driver.php 25
/mt_driver.php 26
/mt_driver.php 27
/mt_driver.php 28
/mt_driver.php 29
/mt_driver.php 30
/mt_driver.php 31
/mt_driver.php 32
/mt_driver.php 33
/mt_driver.php 34
/mt_driver.php 35
/mt_driver.php 36
/mt_driver.php 37
/mt_driver.php 38
/mt_driver.php 39
/mt_driver.php 40
/mt_driver.php 41
/mt_driver.php 42
/mt_driver.php 43
/mt_driver.php 44
/mt_driver.php 45
/mt_driver.php 46
/mt_driver.php 47
/mt_driver.php 48
/mt_driver.php 49
/mt_driver.php 50
/mt_driver.php 51
/mt_driver.php 52
/mt_driver.php 53
/mt_driver.php 54
/mt_driver.php 55
/mt_driver.php 56
/mt_driver.php 57
/mt_driver.php 58
/mt_driver.php 59
/mt_driver.php 60
/mt_driver.php 61
/mt_driver.php 62
/mt_driver.php 63
/mt_driver.php 64
/mt_driver.php 65
/mt_driver.php 66
/mt_driver.php 67
/mt_driver.php 68
/mt_driver.php 69
/mt_driver.php 70
/mt_driver.php 71
/mt_driver.php 72
/mt_driver.php 73
/mt_driver.php 74
/mt_driver.php 75
/mt_driver.php 76
/mt_driver.php 77
/mt_driver.php 78
/mt_driver.php 79
/mt_driver.php 80
/mt_driver.php 81
/mt_driver.php 82
/mt_driver.php 83
/mt_driver.php 84
/mt_driver.php 85
/mt_driver.php 86
/mt_driver.php 87
/mt_driver.php 88
/mt_driver.php 89
/mt_driver.php 90
/mt_driver.php 91
/mt_driver.php 92
/mt_driver.php 93
/mt_driver.php 94
/mt_driver.php 95
/mt_driver.php 96
/mt_driver.php 97
/mt_driver.php 98
/mt_driver.php 99
/mt_driver.php 100
/mt_driver.php 101
/mt_driver.php 102
/mt_driver.php 103
/mt_driver.php 104
/mt_driver.php 105
/mt_driver.php 106
/mt_driver.php 107
/mt_driver.php 108
/mt_driver.php 109
/mt_driver.php 110
/mt_driver.php 111
/mt_driver.php 112
/mt_driver.php 113
/mt_driver.php 114
/mt_driver.php 115
/mt_driver.php 116
/mt_driver.php 117
/mt_driver.php 118
/mt_driver.php 119
/mt_driver.php 120
/mt_driver.php 121
/mt_driver.php 122
/mt_driver.php 123
/mt_driver.php 124
/mt_driver.php 125
/mt_driver.php 126
/mt_driver.php 127
/mt_driver.php 128
/mt_driver.php 129
/mt_driver.php 130
/mt_driver.php 131
/mt_driver.php 132
/mt_driver.php 133
/mt_driver.php 134
/mt_driver.php 135
/mt_driver.php 136
/mt_driver.php 137
/mt_driver.php 138
/mt_driver.php 139
/mt_driver.php 140
/mt_driver.php 141
/mt_driver.php 142
/mt_driver.php 143
/mt_driver.php 144
/mt_driver.php 145
/mt_driver.php 146
/mt_driver.php 147
/mt_driver.php 148
/mt_driver.php 149
/mt_driver.php 150
"

# Redefine IFS with >
IFS=">
"

counter=1
apa=""

for i in $allprocess
do

	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`

	if [[ $apa != ""  ]]; then
		echo "Stoppping " $i
		kill $apa
	fi

counter=`expr $counter + 1`
done

counterstart=1
apastart=""

for j in $allprocess
do

	apastart=`ps -eo "%p %a" | grep -P "$j$" | grep -v grep | cut -c1-5`

	if [[ $apastart == ""  ]]; then
		echo "starting" $j
		cd /var/www/sms/app/mt_driver/
		/usr/bin/php /var/www/sms/app/mt_driver/mt_driver.php $counterstart &
	fi

counterstart=`expr $counterstart + 1`
done
