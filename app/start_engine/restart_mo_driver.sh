#!/bin/sh

allprocess="
/mo_driver.php 1
/mo_driver.php 2
/mo_driver.php 3
/mo_driver.php 4
/mo_driver.php 5
/mo_driver.php 6
/mo_driver.php 7
/mo_driver.php 8
/mo_driver.php 9
/mo_driver.php 10
"

# Redefine IFS with >
IFS=">
"

counter=1
apa=""

for i in $allprocess
do
	
	apa=`ps -eo "%p %a" | grep -P "$i$" | grep -v grep | cut -c1-5`
	
	if [[ $apa != ""  ]]; then
		echo "Stoppping " $i
		kill $apa
	fi
	
counter=`expr $counter + 1`
done

counterstart=1
apastart=""

for j in $allprocess
do
	
	apastart=`ps -eo "%p %a" | grep -P "$j$" | grep -v grep | cut -c1-5`
	
	if [[ $apastart == ""  ]]; then
		echo "starting" $j
		cd /var/www/sms/app/mo_driver/
		/usr/bin/php /var/www/sms/app/mo_driver/mo_driver.php $counterstart &
	fi
	
counterstart=`expr $counterstart + 1`
done
