<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR		: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME		: mo_driver.php
#			FUNCTION		: Read SMS from File, process to application
#			MODIFIED		: October, 22 2008
#
#################################################################################

$file_path = "";

require($file_path."include/config_app.php");
require($file_path."include/mysql.class.php");
require($file_path."include/functions.php");

if(empty($_GET['debug'])) $debug = false;
else $debug = true;

// Restrict Access
if(!$debug) if(!in_array($_SERVER['REMOTE_ADDR'], $config['allow_ip'])) die ("You are not authorized to view this page");

if(empty($_GET['mo_origin'])) $mo_origin = "";
else $mo_origin = $_GET['mo_origin'];

if(empty($_GET['msisdn'])) die("Empty Parameter2");
else $msisdn = $_GET['msisdn'];

if(empty($_GET['prepaid'])) {
	$prepaid = 0;
	$shortname = "";
}
else { //0-apa
	$prepaid = $_GET['prepaid'];
	$ar_prepaid = explode("-",$prepaid);
	if( count($ar_prepaid > 1) ) $shortname = $ar_prepaid[1];
	else $shortname = "";
}

if(empty($_GET['sms'])) $sms = "";
else $sms = $_GET['sms'];

if(empty($_GET['telco'])) die("Empty Parameter4");
else $telco = $_GET['telco'];

if(empty($_GET['shortcode'])) die("Empty Parameter5");
else $shortcode = $_GET['shortcode'];

if(empty($_GET['trx_id'])) $trx_id = "";
else $trx_id = $_GET['trx_id'];

if(empty($_GET['trx_date'])) $trx_date = "";
else $trx_date = $_GET['trx_date'];

if(empty($_GET['session_id'])) die("Empty Parameter8");
else $session_id = $_GET['session_id'];

if(empty($_GET['mo_direct'])) $mo_direct = false;
else $mo_direct = true;

if(empty($_GET['is_3rdpartner'])) $is_3rdpartner = false;
else $is_3rdpartner = $_GET['is_3rdpartner'];

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

define("MO_ACCESS", 1);

$arMT_idx = 0;

switch($telco) {
	case "tsel" : $arMT[$arMT_idx]['reply_sms']	= "Maaf keyword tdk ditemukan."; break;
	case "isat" : $arMT[$arMT_idx]['reply_sms']	= "Maaf keyword tidak ditemukan."; break;
	case "xl" : $arMT[$arMT_idx]['reply_sms']	= "Maaf keyword tidak ditemukan."; break;
	case "modem" : $arMT[$arMT_idx]['reply_sms']	= "Maaf format sms Anda salah."; break;
	default : $arMT[$arMT_idx]['reply_sms']	= "Maaf keyword tidak ditemukan."; break;
}
$arMT[$arMT_idx]['mo_origin']	= $mo_origin;
$arMT[$arMT_idx]['msisdn']	= $msisdn;
$arMT[$arMT_idx]['prepaid']	= $prepaid;
$arMT[$arMT_idx]['sms']	= $sms;
$arMT[$arMT_idx]['telco']	= $telco;
$arMT[$arMT_idx]['shortcode']	= $shortcode;
$arMT[$arMT_idx]['trx_id']	= $trx_id;
$arMT[$arMT_idx]['trx_date']	= $trx_date;
$arMT[$arMT_idx]['session_id']	= $session_id;
$arMT[$arMT_idx]['charge']	= "PULL-0";
$arMT[$arMT_idx]['media_id']	= "0";
$arMT[$arMT_idx]['partner_id']	= "0";
$arMT[$arMT_idx]['appid']	= "99999";
$arMT[$arMT_idx]['subject']	= "PULL;IOD;ERROR_SMS";
$arMT[$arMT_idx]['priority']	= "99";
$arMT[$arMT_idx]['url']	= "";
$arMT[$arMT_idx]['mt_type']	= 1; // 1 = SMS Text , 2 = WAPPUSH , 3 = Smart Messaging
$arMT[$arMT_idx]['url_dr']	= "";
$arMT[$arMT_idx]['shortname']	= $shortname;

$response = "OK";

$iod = split(" ",$sms);

if(strtolower($iod[0]) == "reg" || strtolower($iod[0]) == "unreg" || strtolower($iod[0]) == "unrek" || strtolower($iod[0]) == "stop" || strtolower($iod[0]) == "berhenti") {
	$table_keyword = "keyword_reg";
	if(count($iod) > 1) $keyword = strtolower($iod[1]);
	else $keyword = "unreg";

	if( isset($iod[1]) ) $keyword = strtolower($iod[1]);
	else $keyword = strtolower($iod[0]);

}
else {
	$table_keyword = "keyword";

	$keyword = strtolower($iod[0]);

}

$include_processor = "iod_".$keyword.".php";

$arrTelco = explode("-",$telco);
if( count($arrTelco) > 1 ) $table_telco = $arrTelco[1];
else $table_telco = $telco;

// Cek Blacklist
$sqlb = "SELECT msisdn FROM app_sms.msisdn_blacklist WHERE msisdn='".$msisdn."'";
$rsb = $db->query($sqlb);
if ($rowb = $db->fetch($rsb)) {
	$include_processor = "iod_blacklist.php";
	$debug = true;
}

$sql = "SELECT keyword FROM mp_system.".$table_keyword." WHERE keyword ='".$keyword."' AND telco='$table_telco' AND shortcode='$shortcode'";
$rs = $db->query($sql);
if ($row = $db->fetch($rs)) {
	if( file_exists($config['base_path']."/_iod_cmd/".$include_processor) ) {

		include $config['base_path']."/_iod_cmd/".$include_processor;
	}
	else $arMT[$arMT_idx]['reply_sms']	= "Maaf IOD tidak ditemukan.";
}

if(SizeOf($arMT) > 0) {
	foreach($arMT as $k => $arMT_data) {
		if($arMT_data['mt_type'] == 1) $arMT_data['reply_sms'] = substr($arMT_data['reply_sms'],0,160);
		$arrSubject = explode(";",$arMT_data['subject']);

		$arMT_data['trx_date'] = date("Y-m-d H:i:s");

		if( !$is_3rdpartner ) {
			if($debug) create_mt_debug($arMT_data['mo_origin'],$arMT_data['msisdn'],$arMT_data['prepaid'],$arMT_data['sms'],$arMT_data['telco'],$arMT_data['shortcode'],$arMT_data['trx_id'],$arMT_data['trx_date'],$arMT_data['session_id'],$arMT_data['charge'],$arMT_data['media_id'],$arMT_data['partner_id'],$arMT_data['appid'],$arMT_data['subject'],$arMT_data['priority'],$arMT_data['url'],$arMT_data['mt_type'],$arMT_data['url_dr'],$arMT_data['shortname'],$arMT_data['reply_sms']);
			else create_mt($arMT_data['mo_origin'],$arMT_data['msisdn'],$arMT_data['prepaid'],$arMT_data['sms'],$arMT_data['telco'],$arMT_data['shortcode'],$arMT_data['trx_id'],$arMT_data['trx_date'],$arMT_data['session_id'],$arMT_data['charge'],$arMT_data['media_id'],$arMT_data['partner_id'],$arMT_data['appid'],$arMT_data['subject'],$arMT_data['priority'],$arMT_data['url'],$arMT_data['mt_type'],$arMT_data['url_dr'],$arMT_data['shortname'],$arMT_data['reply_sms']);
		}

	}
}

echo $response;

?>
