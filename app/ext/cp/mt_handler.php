<?php

include "config.cp.auth.php";
include "/var/www/sms/app/include/config_app.php";
require("/var/www/sms/app/include/mysql.class.php");
include "/var/www/sms/app/include/functions.php";

// Restrict IP
if( !in_array($_SERVER['REMOTE_ADDR'], $allow_ip ) ) die ("500");
// Restrict user
if( empty($_GET['username']) ) die ("501");
if( !in_array($_GET['username'], $allow_user ) ) die ("501");
// Restrict pwd
if( empty($_GET['password']) ) die ("502");
if( !in_array($_GET['password'], $allow_pwd ) ) die ("502");
// Check MSISDN
if( empty($_GET['msisdn']) ) die ("503");
else $msisdn = $_GET['msisdn'];
// Check Telco
if( empty($_GET['telco']) ) die ("504");
else $telco = $_GET['telco'];
if( !in_array($_GET['telco'], $allow_telco ) ) die ("504");
// Check SHortcode
if( empty($_GET['shortcode']) ) die ("505");
else $shortcode = $_GET['shortcode'];
if( !in_array($_GET['shortcode'], $allow_shortcode ) ) die ("505");
// Check Session ID
if( empty($_GET['session_id']) ) die ("506");
else $session_id = $_GET['session_id'];
// Check APPID
if( empty($_GET['appid']) ) die ("507");
else $appid = $_GET['appid'];
// Check PRICE
if( empty($_GET['price']) ) die ("508");
else $price = $_GET['price'];
if( empty($_GET['mt_type']) ) $mt_type = 1;
else $mt_type = $_GET['mt_type'];
if( empty($_GET['mo_origin']) ) $mo_origin = "";
else $mo_origin = $_GET['mo_origin'];
if( empty($_GET['partner_id']) ) die ("511");
else $partner_id = $_GET['partner_id'];
if( empty($_GET['url']) ) $url = "";
else $url = $_GET['url'];
if( empty($_GET['trx_id']) ) $trx_id = "";
else $trx_id = $_GET['trx_id'];
if( empty($_GET['trx_date']) ) $trx_date = "";
else $trx_date = $_GET['trx_date'];
if( empty($_GET['shortname']) ) $shortname = "";
else $shortname = $_GET['shortname'];
// Check SUBJECT
if( empty($_GET['subject']) ) die ("509");
else $subject = $_GET['subject'];
$url_dr = $allow_urldr[ $_GET['username'] ];
if( $url_dr == "" ) die ("510");
$reply_sms = $_GET['reply_sms'];

if( $partner_id <> $allow_partnerid[ $_GET['username'] ] ) die ("512");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

insert_outgoing_log_partner($appid,$mo_origin,$msisdn,$prepaid=0,$subject,$reply_sms,$url,$telco,$shortcode,$price,$mt_type,$trx_id,$trx_date,$session_id,$media_id=0,$partner_id,$ack_status=200,$url_dr,$shortname);

create_mt($mo_origin,$msisdn,$prepaid=0,$sms="",$telco,$shortcode,$trx_id,$trx_date,$session_id,$price,$media_id=0,$partner_id,$appid,$subject,99,$url,$mt_type=1,$url_dr,$shortname,$reply_sms);

echo "200";

/*
List Error Code
200 - Success
500 - IP Not Allowed
501 - Invalid Username
502 - Invalid Password
503 - Invalid MSISDN
504 - Invalid Telco
505 - Invalid Shortcode
506 - Invalid Session ID
507 - Invalid APPID
508 - Invalid Price
509 - Invalid Subject
510 - Invalid URL DR
511 - Empty Partner ID
512 - Invalid Partner ID
*/
?>