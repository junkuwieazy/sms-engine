<?php

$allow_ip = array (
"localhost", // IP Local
"127.0.0.1", // IP Local
"10.10.0.1", // IP Bima
"10.10.0.2", // IP Arjuna
"10.10.0.3", // IP Durga
"10.10.0.4", // IP Citra
"202.75.28.18", // IP Publik Bima
"202.75.28.88", // IP Publik Web
"202.43.73.202", // IP Mobiwin
"110.4.44.38", // IP Malaysia
"110.5.109.155" // IP MUI
);

$allow_user = array (
"mobiwin", // Username Mobiwin
"partnermui" // Username Partner MUI
);

$allow_pwd = array (
"pwdm0b1w1n", // Password Galaksi
"pwdNY4MUI" // Password Partner MUI
);

$allow_partnerid = array (
"mobiwin" => "0", //Partner ID Mobiwin
"partnermui" => "20" //Partner ID MUI
);

$allow_urldr = array (
"mobiwin" => "http://202.43.73.202/sms/tes_dr.php", // URL DR Mobiwin
"partnermui" => "http://202.149.67.83/" //URL DR MUI
);

$allow_telco = array (
"tsel",
"isat",
"xl",
"flexi",
"esia",
"hutch",
"axis",
"mob8",
"nts",
);

$allow_shortcode = array (
"98555",
"97555",
"9855",
"9755"
);

?>
