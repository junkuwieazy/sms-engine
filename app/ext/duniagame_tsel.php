<?php

$file_path = "/var/www/sms/app/";

require($file_path."include/config_app.php");

if( empty($_GET['msisdn']) ) die("FAIL");
if( empty($_GET['trx_id']) ) die("FAIL");
if( empty($_GET['operator']) ) die("FAIL");
if( empty($_GET['username']) ) die("FAIL");
if( empty($_GET['password']) ) die("FAIL");
if( empty($_GET['sms']) ) die("FAIL");

//if( $_GET['username'] <> "gmcgame" ) die("FAIL");
//if( $_GET['password'] <> "g4m3@gmc" ) die("FAIL");

$msisdn = $_GET['msisdn'];
$trx_id = $_GET['trx_id'];
$trx_date = $_GET['trx_date'];
$operator = "tsel-dg";
$username = $_GET['username'];
$password = $_GET['password'];
$shortcode = "9000";
$channel = $_GET['channel'];
$sms = $_GET['sms'];
$session_id = $msisdn.date("YmdHis").rand(0,10000);

$params = "mo_origin=sms&msisdn=".$msisdn."&sms=".urlencode($sms)."&telco=".$operator."&shortcode=".$shortcode."&trx_id=".$trx_id."&trx_date=".$trx_date."&session_id=".$session_id."&mo_direct=1";
$url_app = sprintf('http://localhost/sms/app/mo_processor.php?%s',$params);
$HTTP_Response = file_get_contents( $url_app );
list($subject,$reply_sms) = explode("||", $HTTP_Response);

$logdata = "==============================\n";
$logdata .= "MO:\n";
$logdata .= "DATE:".date("Y-m-d H:i:s")."\n";
$logdata .= "MSISDN:".$msisdn."\n";
$logdata .= "TRX ID:".$trx_id."\n";
$logdata .= "SMS:".$sms."\n";
$logdata .= "TELCO:".$operator."\n";
$logdata .= "SHORTCODE:".$shortcode."\n";
$logdata .= "SESSION ID:".$session_id."\n";
$logdata .= "RESULT:".$HTTP_Response."\n";
$logdata .= "==============================\n";

// log sms in file
$pathLOG = $config['base_path']."/logs/duniagame/";

$m_FileLOG = fopen($pathLOG."mo_sms_".date("Ymd").".txt","a");

fwrite($m_FileLOG, $logdata);
fclose($m_FileLOG);

echo $reply_sms;

?>