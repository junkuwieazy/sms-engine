<?php
error_reporting(true);
$file_path = "/var/www/sms/app/";

require($file_path."include/config_app.php");
require($file_path."include/adodb/adodb.inc.php");

$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],"dreambox") or die ( $db->ErrorMsg() );

$xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>";
$xml .= "<rss version=\"2.0\">";
$xml .= "<channel>";
$xml .= "<title>".htmlentities("Dreambox-media SMS")."</title>";
$xml .= "<link>".htmlentities("http://www.dreambox-media.tv")."</link>";
$xml .= "<description>".htmlentities("SMS Gateway")."</description>";

$sql = "SELECT * FROM sms_inbox WHERE 1 AND DATE_FORMAT(datetime,'%Y-%m-%d') = '".date("Y-m-d")."'";
if( $rs = $db->Execute($sql) ) {
	if( $rs->RecordCount() > 0 ) {
		while ( !$rs->EOF ) {
			
			$xml .= "<item><description>".htmlentities($rs->fields[4]." : ".$rs->fields[2]." : ".$rs->fields[3])."</description></item>";
			
			$rs->MoveNext();
		}
	}
}

$xml .= "</channel>";
$xml .= "</rss>";

header("Content-Type:text/xml");
echo $xml;

?>
