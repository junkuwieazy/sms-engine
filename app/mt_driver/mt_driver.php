<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR		: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME		: mt_driver.php
#			FUNCTION		: Read SMS from File, process to application
#			MODIFIED		: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for mt driver!\n");

$file_path = "/var/www/sms/app/";

require($file_path."include/config_app.php");
include($file_path."include/class_aiom.php");
require($file_path."include/functions.php");

$i = 1;
$folder = trim($argv[1]);

$path = $config['spool_mt_driver_path'].$folder."/";

$file_monitoring = $config['mt_driver_monitoring_path']."mt_driver_".$folder.".txt";

while(true){

	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);
	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
}

exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $folder;

	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);

		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}
		// mo_origin||msisdn||prepaid||sms||telco||shortcode||trx_id||trx_date||session_id||charge||media_id||partner_id||appid||subject||priority||url||mt_type||url_dr||shortname||reply_sms
		$mo_origin = $mText[1];
		$msisdn = $mText[2];
		$prepaid = $mText[3];
		$sms = $mText[4];
		$telco = $mText[5];
		$shortcode = $mText[6];
		$trx_id = $mText[7];
		$trx_date = str_replace("-","",$mText[8]);
		$trx_date = str_replace(" ","",$trx_date);
		$trx_date = str_replace(":","",$trx_date);
		$session_id = $mText[9];

		$charge = $mText[10];
		$media_id = $mText[11];
		$partner_id = $mText[12];
		$appid = $mText[13];
		$subject = $mText[14];
		$priority = $mText[15];
		$url = $mText[16];
		$mt_type = $mText[17];
		$url_dr = $mText[18];
		$shortname = $mText[19];
		$reply_sms = $mText[20];

		if( eregi("^PUSH",strtoupper($subject)) ) $url_gateway = $config['url_gw']['push'][$telco][$shortcode];
		else $url_gateway = $config['url_gw']['pull'][$telco][$shortcode];

		$param = "?serverkey=s1st3m&mo_origin=".urlencode($mo_origin)."&msisdn=".$msisdn."&prepaid=".urlencode($prepaid)."&sms=".urlencode($sms)."&telco=$telco&shortcode=$shortcode&trx_id=".$trx_id."&trx_date=".$trx_date."&session_id=".$session_id."&charge=".$charge."&media_id=".$media_id."&partner_id=".$partner_id."&appid=".$appid."&subject=".$subject."&priority=$priority&url=".urlencode($url)."&mt_type=$mt_type&url_dr=".$url_dr."&shortname=".urlencode($shortname)."&reply_sms=".urlencode($reply_sms);

		$url_send = $url_gateway.$param;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_send);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

		$data = curl_exec($ch);

		curl_close($ch);

		$logdata = "==============================\n";
		$logdata .= "MT:\n";
		$logdata .= "DATE:".date("Y-m-d H:i:s")."\n";
		$logdata .= "MSISDN:".$msisdn."\n";
		$logdata .= "TRX ID:".$trx_id."\n";
		$logdata .= "SMS:".$sms."\n";
		$logdata .= "TELCO:".$telco."\n";
		$logdata .= "APPID:".$appid."\n";
		$logdata .= "SHORTCODE:".$shortcode."\n";
		$logdata .= "TRX DATE:".$trx_date."\n";
		$logdata .= "SESSION ID:".$session_id."\n";
		$logdata .= "SUBJECT:".$subject."\n";
		$logdata .= "MT TYPE:".$mt_type."\n";
		$logdata .= "CHARGE:".$charge."\n";
		$logdata .= "URL DR:".$url_dr."\n";
		$logdata .= "SHORTNAME:".$shortname."\n";
		$logdata .= "REPLY SMS:".$reply_sms."\n";
		$logdata .= "URL SEND:".$url_send."\n";
		$logdata .= "RESULT:".$data."\n";
		$logdata .= "==============================\n";

		// log sms in file
		$pathLOG = $config['base_path']."/logs/mt/";

		$m_FileLOG = fopen($pathLOG."mt_sms_".$folder."_".date("Ymd").".txt","a");

		fwrite($m_FileLOG, $logdata);
		fclose($m_FileLOG);

		return true;

	}
	else {
		return false;
	}
}

?>
