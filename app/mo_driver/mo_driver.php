<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR		: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME		: mo_driver.php
#			FUNCTION		: Read SMS from File, process to application
#			MODIFIED		: October, 22 2008
#
#################################################################################

if(empty($argv[1])) die("Please specify spool id for mo driver!\n");

$file_path = "/var/www/sms/app/";

require($file_path."include/config_app.php");
include($file_path."include/class_aiom.php");

$i = 1;
$folder = trim($argv[1]);

$path = $config['spool_mo_driver_path'].$folder."/";

$file_monitoring = $config['mo_driver_monitoring_path']."mo_driver_".$folder.".txt";

while(true){

	if($i++ % 10 == 0) {
		AIOM::app_refresh($file_monitoring);
		$i = 1;
	}

	$cekFile = opendir($path);

	while (false !== ($mFilename = readdir($cekFile)))
	{
		if ($mFilename != "." && $mFilename != "..")
		{
			$result = Readfiletext($mFilename);
			if($result) {
				$mHapus = Unlink($path."/".$mFilename);
			}
		}
	}
	closedir($cekFile);
	sleep(1);
}

exit;

////////////////////////////////////// List of Function /////////////////////////////////

function Readfiletext($mFile) {
	global $path, $config, $folder;

	$SMSRow = "";
	$fp = fopen($path."/".$mFile, "r");
	if($fp) {
		while (!feof($fp)) {
			$SMSRow .= fread($fp, 1024);
		}
		fclose($fp);

		$SMSExploded = explode("@#@",$SMSRow);
		$Brs = 0;
		Foreach ($SMSExploded as $mField)
		{
			$Brs = $Brs + 1;
			$mText[$Brs] = $mField;
		}

		$mo_origin = $mText[1];
		$msisdn = $mText[2];
		$prepaid = $mText[3];
		$sms = $mText[4];
		$telco = $mText[5];
		$shortcode = $mText[6];
		$trx_id = $mText[7];
		$trx_date = $mText[8];
		$session_id = $mText[9];

		$url = $config['url_app'];

		$param = "?mo_origin=".urlencode($mo_origin)."&msisdn=".$msisdn."&prepaid=".urlencode($prepaid)."&sms=".urlencode($sms)."&telco=$telco&shortcode=$shortcode&trx_id=".$trx_id."&trx_date=".$trx_date."&session_id=".$session_id;
		$url_logic = $url.$param;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url_logic);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);

		$data = curl_exec($ch);

		curl_close($ch);

		$logdata = "==============================\n";
		$logdata .= "MO:\n";
		$logdata .= "DATE:".date("Y-m-d H:i:s")."\n";
		$logdata .= "MSISDN:".$msisdn."\n";
		$logdata .= "TRX ID:".$trx_id."\n";
		$logdata .= "SMS:".$sms."\n";
		$logdata .= "TELCO:".$telco."\n";
		$logdata .= "SHORTCODE:".$shortcode."\n";
		$logdata .= "TRX DATE:".$trx_date."\n";
		$logdata .= "SESSION ID:".$session_id."\n";
		$logdata .= "URL:".$url_logic."\n";
		$logdata .= "RESULT:".$data."\n";
		$logdata .= "==============================\n";

		// log sms in file
		$pathLOG = $config['base_path']."/logs/mo/";

		$m_FileLOG = fopen($pathLOG."mo_sms_".$folder."_".date("Ymd").".txt","a");

		fwrite($m_FileLOG, $logdata);
		fclose($m_FileLOG);

		return true;
		
	}
	else {
		return false;
	}
}

?>
