<?php
#################################################################################
#
#			PROJECT			: Messaging Platform
#			OPERATOR		: All
#			AUTHOR			: Mujiburrohman
#			EMAIL				: burrohman@gmail.com
#			FILENAME		: app_incoming_2.php
#			FUNCTION		: Receive parameter SMS from Telco
#			MODIFIED		: October, 20 2008
#
#################################################################################

$file_path = "../";

require($file_path."include/config_app.php");

// Restrict Access
if(!in_array($_SERVER['REMOTE_ADDR'], $config['allow_ip'])) die ("You are not authorized to view this page");

if(empty($_GET['mo_origin'])) $mo_origin = "";
else $mo_origin = $_GET['mo_origin'];

if(empty($_GET['msisdn'])) die("Empty Parameter");
else $msisdn = $_GET['msisdn'];

if(empty($_GET['prepaid'])) $prepaid = 0;
else $prepaid = $_GET['prepaid'];

if(empty($_GET['sms'])) $sms = "";
else $sms = $_GET['sms'];

if(empty($_GET['telco'])) die("Empty Parameter");
else $telco = $_GET['telco'];

if(empty($_GET['shortcode'])) die("Empty Parameter");
else $shortcode = $_GET['shortcode'];

if(empty($_GET['trx_id'])) $trx_id = "";
else $trx_id = $_GET['trx_id'];

if(empty($_GET['trx_date'])) $trx_date = "";
else $trx_date = $_GET['trx_date'];

if(empty($_GET['session_id'])) die("Empty Parameter");
else $session_id = $_GET['session_id'];

$full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".date("YmdHis");

$spool = range (1,SizeOf($config['spool_mo_driver']));
srand ((double)microtime()*1000000);
shuffle ($spool);

$fp = fopen ($config['base_path'].$config['spool_mo_driver'][$spool[0]]."/mo-".$session_id.".sms", "w+");
fputs($fp, $full_sms );
fclose($fp);

echo "OK";

?>