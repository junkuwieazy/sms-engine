<?php
session_start();

include_once("/var/www/sms/app/include/config_app.php");
include_once('/var/www/sms/app/include/smarty/Smarty.class.php');
include_once('/var/www/sms/app/include/adodb/adodb.inc.php');
define("TEMPLATES_DIR","/var/www/sms/app/cms_text/templates/");
define("IMG_DIR","images/");
define("CSS_FILE","css/style.css");
define("JAVA_FILE","js/java.js");
define("JAVA_FILE_WYSWYG","js/nicEdit.js");

//Create DB Object
global $config;
$db = &ADONewConnection( $config['dbType'] );
$db->connect( $config['dbHost'].":".$config['dbPort'],$config['dbUser'],$config['dbPass'],$config['dbName']) or die ( $db->ErrorMsg() );

$config['menu'] = array(
					'Content Info' => "list_content.php" , 
					'Content Quiz' => "list_quiz.php" , 
					'Content Wap' => "list_content_wap.php" , 
					'Push Schedule' => "push_schedule.php" , 
					);

//Create Template Object
$smarty = new smarty;

$smarty->template_dir = 'templates/';
$smarty->compile_dir = '/var/www/sms/app/cms_text/templates_c/';
$smarty->config_dir = 'configs/';
$smarty->cache_dir = 'cache/';

$smarty->assign("img_dir",IMG_DIR);
$smarty->assign("css_file",CSS_FILE);
$smarty->assign("java_file",JAVA_FILE);
$smarty->assign("java_file_wyswyg",JAVA_FILE_WYSWYG);

$smarty->assign("URL_PATH",$url_path);

$smarty->assign("title_header",$title_header);
$smarty->assign("title",$title);
$smarty->assign("copyright",$copyright);

$smarty->assign("tgl",date("F, d Y"));

if(isset($_SESSION["levelcms"])) {
	$smarty->assign("level",$_SESSION["levelcms"]);
}

if(isset($_SESSION["appidses"])) {
	$ses_appid = $_SESSION["appidses"];
}
else $ses_appid = "all";

if(isset($_SESSION["menu_module_cms"])) {
	$smarty->assign("menu_module_cms",$_SESSION["menu_module_cms"]);
}

$array_menu = explode(",",$_SESSION["menu_module_cms"]);
$menu_html = '';
foreach($array_menu as $menu_order => $menu_name) {
	$menu_html .= '
				  | <a href="'.$config['menu'][$menu_name].'" class="link-page">'.$menu_name.'</a>
				  ';
}
$smarty->assign("menu_html",$menu_html);

include_once('functions.php');

$mFunc =& new myFunctions();

?>