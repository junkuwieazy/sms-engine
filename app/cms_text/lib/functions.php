<?php
class myFunctions {
	
	// ===================================== NON DATABASE FUNCTION ================================= //
	
	function makeArray ( $start, $end ) {
		for ( $i = $start; $i <= $end; $i++ ) {
			$myarray[] = $i;
		}
		return $myarray;
	}
	
	function get_telco_name($telco) {
		switch($telco) {
			case "tsel" 	: $telco_name = "Telkomsel"; break;
			case "isat" 	: $telco_name = "Indosat"; break;
			case "xl" 		: $telco_name = "XL"; break;
			case "flexi" 	: $telco_name = "Flexi"; break;
			case "esia" 	: $telco_name = "Esia"; break;
			case "mob8" 	: $telco_name = "Mob8"; break;
			case "hutch" 	: $telco_name = "Hutch"; break;
			default 			: $telco_name = "All"; break;
		}
		return $telco_name;
	}
	
	function crtDropDown($objA,$objB,$objC,$objD="",$objE="",$objF="") {
		// $objA = Option Name
		// $objB = Option Value
		// $objC = Field Name
		// $objD = Selected Value
		// $objE = Default Value
		$html_result = "<select name='$objC' $objF>\n";
		($objE <> "") ? $html_result .= "<option value='all'>$objE</option>" : $html_result .= "";
		foreach($objA as $k=>$v) {
			if($objD == $k) $selected = "selected";
			else $selected = "";
			$html_result .= "<option value='$k' $selected>$v</option>\n";
		}
		$html_result .= "</select>\n";
		return $html_result;
	}
	
	function crtDropDownContent($objA,$objB,$objC,$objD="",$objE="",$objF="") {
		// $objA = Option Name
		// $objB = Option Value
		// $objC = Field Name
		// $objD = Selected Value
		// $objE = Default Value
		$html_result = "<select name='$objC' $objF onchange='this.form.submit()'>\n";
		($objE <> "") ? $html_result .= "<option value='all'>$objE</option>" : $html_result .= "";
		foreach($objA as $k=>$v) {
			if($objD == $k) $selected = "selected";
			else $selected = "";
			$html_result .= "<option value='$k' $selected>$v</option>\n";
		}
		$html_result .= "</select>\n";
		return $html_result;
	}
	
	function get_status_name($status) {
		switch($status) {
			case "1" 	: $status_name = "Aktif"; break;
			case "0" 	: $status_name = "Non Aktif"; break;
			default 		: $status_name = "All"; break;
		}
		return $status_name;
	}
	
	function monthName ( $num ) {
		switch( (int)$num ) {
			case 1: $monthname = 'Januari'; break;
			case 2: $monthname = 'Februari'; break;
			case 3: $monthname = 'Maret'; break;
			case 4: $monthname = 'April'; break;
			case 5: $monthname = 'Mei'; break;
			case 6: $monthname = 'Juni'; break;
			case 7: $monthname = 'Juli'; break;
			case 8: $monthname = 'Agustus'; break;
			case 9: $monthname = 'September'; break;
			case 10: $monthname = 'Oktober'; break;
			case 11: $monthname = 'November'; break;
			case 12: $monthname = 'Desember'; break;
		}
		return $monthname;
	}
	
	function getMonths ( $which = 1 ) {
		// 1 for id, 2 for name.
		if ( $which == 1 ) {
			$months = array ( '01', '02', '03',
									'04', '05', '06',
									'07', '08', '09',
									'10', '11', '12' );
		} else if ( $which == 2 ) {
			$months = array ( 'Januari', 'Februari', 'Maret',
									'April', 'Mei', 'Juni',
									'Juli', 'Agustus', 'September',
									'Oktober', 'November', 'Desember' );
		}
		return $months;
	}
	
	function getOprs ( $which = 1 ) {
		$num = 8;
	
		// 1 to get the id and 2 to get the name.
		if ( $which == 1 ) {
			for ( $i = 1; $i <= $num; $i++ ) {
				switch($i) {
					case 1 : $nama = "tsel";break;
					case 2 : $nama = "isat";break;
					case 3 : $nama = "xl";break;
					case 4 : $nama = "flexi";break;
					case 5 : $nama = "fren";break;
					case 6 : $nama = "esia";break;
					case 7 : $nama = "3id";break;
					case 8 : $nama = "nts";break;
				}
				$results[] = $nama;
			}
		} else if ( $which == 2 ) {
			for ( $i = 1; $i <= $num; $i++ ) {
				switch($i) {
					case 1 : $nama = "tsel";break;
					case 2 : $nama = "isat";break;
					case 3 : $nama = "xl";break;
					case 4 : $nama = "flexi";break;
					case 5 : $nama = "fren";break;
					case 6 : $nama = "esia";break;
					case 7 : $nama = "3id";break;
					case 8 : $nama = "nts";break;
				}
				switch( $nama ) {
					case "tsel":
						$results[] = 'Telkomsel';
						break;
					case "isat":
						$results[] = 'Indosat';
						break;
					case "xl":
						$results[] = 'XL';
						break;
					case "flexi":
						$results[] = 'Flexi';
						break;
					case "fren":
						$results[] = 'Mob8';
						break;
					case "esia":
						$results[] = 'Esia';
						break;
					case "3id":
						$results[] = 'Tri';
						break;
					case "nts":
						$results[] = 'NTS';
						break;
				}
			}
		}
		
		return $results;
	}
	
	function get_description($db_82,$appid) {
		$appid = 10000 + $appid;
		$desc = '';
		$sql = "SELECT * FROM mo_system.application WHERE appid=".$appid." LIMIT 1";
		$rs = $db_82->Execute($sql) or die( $db_82->ErrorMsg() );
		if ( $rs->RecordCount() > 0 ) {
			while ( !$rs->EOF ) {
		  	 $desc = $rs->fields[2];
		  	 
		  	 $rs->MoveNext();
			}
		}
		return $desc;
	}

}
?>