<?
include_once('lib/include.php');

if(!isset($_SESSION["levelcms"])) {
	header("Location:index.php");
}

$msg = "";

$smarty->assign("smt", "Submit");

if( isset($_GET['appid']) && !isset($_POST['appid']) ) $_POST['appid'] = $_GET['appid'];

$smarty->assign("len_content", $len_content);

if(isset($_POST['Submit'])) {
	if(empty($_POST['content']) ) {
	    $msg = "Content harus diisi"; 
	}
	elseif( $_POST['appid'] == "all" ) {
	    $msg = "Pilih content yang akan di isi"; 
	}
	else {
		$sql = "INSERT INTO app_sms.push_hourly_schedule (appid,keyword,content,push_date,push_hour,charge,is_pushed,entry_date) 
				VALUES ('".$_POST['appid']."','".$_POST['keyword']."','".$_POST['content']."','".$_POST['publish_date']."','".$_POST['push_hour']."','".$_POST['push_charge']."',0,NOW())";
		$result = $db->Execute($sql) or die( $db->ErrorMsg() );
		
		if($result) {
			$msg = "Input content berhasil";
		}
		else {
			$msg = "Input gagal, silahkan ulangi lagi!";
		}
	}
}

if(isset($_GET['id']) && isset($_GET['action']) && $_GET['action'] == "delete") {
	$sql = "DELETE FROM app_sms.push_hourly_schedule WHERE id=".$_GET['id'];
	$result = $db->Execute($sql) or die( $db->ErrorMsg() );
}

$fix_where_appid = "";
if( $ses_appid <> "all" ) {
	$array_appid = explode(",",$ses_appid);
	
	$where_appid = " AND c.appid in (";
	for($ip=0;$ip< count($array_appid);$ip++) {
		$where_appid .= "'".$array_appid[$ip]."',";
	}
	$where_appid1 = ereg_replace(',$',"",$where_appid);
	$where_appid2 = ")";
	$fix_where_appid = $where_appid1.$where_appid2;
}

$sql_app = "SELECT appid,description FROM app_sms.appid_sms c WHERE 1 $fix_where_appid ORDER BY appid ASC";
$rs_app = $db->Execute($sql_app) or die( $db->ErrorMsg() );
if ( $rs_app->RecordCount() > 0 ) {
	while ( !$rs_app->EOF ) {
		$arr_service[$rs_app->fields[0]] = $rs_app->fields[0]." - ".$rs_app->fields[1];
		$rs_app->MoveNext();
	}
}

$dd_service = $mFunc->crtDropDownContent($arr_service,$arr_service, 'appid', $_POST['appid'], 'All', 'class="text"');
$smarty->assign("dd_service",$dd_service);

if( isset($_POST['appid']) && $_POST['appid'] <> "all" ) $str_sql = " AND c.appid='".$_POST['appid']."'";

$sql = "SELECT c.appid,description,content,push_date,push_hour,c.entry_date,c.id FROM app_sms.push_hourly_schedule c, app_sms.appid_sms a WHERE c.appid=a.appid $str_sql $fix_where_appid ORDER BY push_date DESC LIMIT 30";
$rs = $db->Execute($sql) or die( $db->ErrorMsg() );
if ( $rs->RecordCount() > 0 ) {
	while ( !$rs->EOF ) {
		 $appid[] = $rs->fields[0];
		 $desc[] = $rs->fields[1];
		 $content[] = $rs->fields[2];
		 $publish_date[] = $rs->fields[3];
		 $publish_seq[] = $rs->fields[4];
		 $entry_date[] = $rs->fields[5];
		 $id[] = $rs->fields[6];
		 
		 $rs->MoveNext();
	}
	
	$smarty->assign("appid",$appid);
	$smarty->assign("desc",$desc);
	$smarty->assign("id",$id);
	$smarty->assign("content",$content);
	$smarty->assign("publish_date",$publish_date);
	$smarty->assign("publish_seq",$publish_seq);
	$smarty->assign("entry_date",$entry_date);
}

$smarty->assign("msg",$msg);

$smarty->display(TEMPLATES_DIR.'push_schedule.html');

?>