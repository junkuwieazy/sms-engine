<?php

// /usr/bin/php /var/www/sms/app/push_config/daily_push_info.php 06 info >> /var/www/sms/app/logs/push/daily_push_info_6.log
// /usr/bin/php /var/www/sms/app/push_config/daily_push_info.php 07 info >> /var/www/sms/app/logs/push/daily_push_info_7.log
// /usr/bin/php /var/www/sms/app/push_config/daily_push_info.php 08 info >> /var/www/sms/app/logs/push/daily_push_info_8.log

set_time_limit(0);

$path = "/var/www/sms/app/";

$jum = 0;

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/functions.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

$push_time = $_SERVER['argv'][1];

$push_title = "Info";

$is_debug = false;
if( !empty($_SERVER['argv'][3]) && $_SERVER['argv'][3] == "debug" ) $is_debug = true;

print("Push Tagging -- ".$push_title." -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$today_day = strtolower(date("D"));

$is_available_appid = false;
$sqla = "SELECT appid FROM mp_system.application WHERE is_push = 1 AND push_time = '".$push_time."' AND status = 1 AND push_day like '%".$today_day."%'";
print("Push Tagging : Push Time ".$push_time." , Push Day : ".$today_day." , sql : ".$sqla." \n");
$rsa = $db->query($sqla);
while ($rowa = $db->fetch($rsa)){
	$appid = $rowa["appid"];
	$is_available_appid = true;
	$include_processor = "iod_".$appid.".php";
	if( file_exists($config['base_path']."/push_config/iod/".$include_processor) ) {
		include $config['base_path']."/push_config/iod/".$include_processor;

		$info_sid = ( !empty($iid) ) ? $iid : $appid;

		if( empty($seq) ) { // Content is Not Sequential
			print("Content : NOT Sequential \n");
			if( empty($icid) ) { // Content is not category

				$content_empty = true;
				$sqlc = "SELECT * FROM app_sms.content_text
								WHERE appid = '".$info_sid."' AND status = 1
								AND DATE_FORMAT(publish_date, '%Y-%m-%d') = '".$publish_date."'
								ORDER BY publish_seq ASC";
				$rsc = $db->query($sqlc);
				while ($rowc = $db->fetch($rsc)){
					$content_empty = false;
					$pseq = ( $rowc["publish_seq"] > 0 )? $rowc["publish_seq"] : 1;
					$def_info[ $pseq ] = trim($rowc["content"]);
				}

				if( $content_empty ) {

					$sqlc = "SELECT * FROM app_sms.content_text
								WHERE appid = '".$info_sid."' AND status = 1
								ORDER BY RAND() LIMIT 1";
					$rsc = $db->query($sqlc);
					while ($rowc = $db->fetch($rsc)){
						$content_empty = false;
						$pseq = ( $rowc["publish_seq"] > 0 )? $rowc["publish_seq"] : 1;
						$def_info[ $pseq ] = trim($rowc["content"]);
					}

					if( $content_empty ) {
						print("Push Tagging : CONTENT ".$appid." EMPTY \n");
						continue;
					}

				}

			}
			else {
				$content_empty = true;
				$arr_temp = array();
				$ar_reply_msg = "";
				$sqlc = "SELECT * FROM app_sms.content_text
								WHERE appid = '".$info_sid."' AND status = 1
								AND DATE_FORMAT(publish_date, '%Y-%m-%d') = '".$publish_date."'
								ORDER BY publish_seq ASC";
				$rsc = $db->query($sqlc);
				while ($rowc = $db->fetch($rsc)){
					$content_empty = false;
					$pseq = ( $rowc["publish_seq"] > 0 )? $rowc["publish_seq"] : 1;
	        $arr_temp[ $pseq ][ $rowc["category"] ] = trim($rowc["content"]);

				}

				if( $content_empty ) {
					print("Push Tagging : CONTENT ".$appid." EMPTY \n");
					continue;
				}

				$def_info = $arr_temp;
			}

		}
		else {
			print("Content : Sequential \n");
			$def_info = "";
		}

		$is_member_available = false;
		$sql = "SELECT msisdn,telco,shortcode,category, DATE_FORMAT(date_reg, '%Y%m%d') AS sdate , media_id, partner_id, shortname FROM app_sms.msisdn_subscriber WHERE appid = '".$appid."' AND status = 1";
		print("Query Member : ".$sql." \n");
		$rs = $db->query($sql);
		while ($row = $db->fetch($rs)) {
			if( (date("Ymd") - $row["sdate"]) < 1 ) continue;

			$is_member_available = true;

			if( is_array($def_info) && count($def_info) > 0 ) { // Push based on publish date
				foreach($def_info as $sms_info) {

					if( is_array($sms_info) ) {
						$reply_sms = $sms_info[$row['category']].$add_reply.$msg_footer[$row['telco']];
					}
					else {
						$reply_sms = $sms_info.$add_reply.$msg_footer[$row['telco']];
					}
					/*
					$tag_info = get_push_flag($appid, $row['msisdn'], $row['shortcode'], $sms_type="info");
					if( !$tag_info ) {
						$push_seq = 1;
					} else {
						$push_seq = $tag_info + 1;
					}

					$push_info_flag = cek_push_flag($appid, $row['msisdn'], $row['shortcode'], $push_title, $push_seq, $row['telco']);
					if($push_info_flag) continue;
					*/
					if( $opr_conf[ $row["telco"] ]["i"] >= $push_seq ) {
						$push_price = $opr_conf[ $row["telco"] ]["pi"];

						//insert_push_flag($appid,$row['msisdn'],$row['shortcode'],strtolower($push_title),$push_seq);

						$trx_date = date("YmdHis");
						$subject = "PUSH;IOD;".strtoupper($iod).";DAILYPUSH";
						$session_id = date("YmdHis").$row['msisdn'].rand(10000,99999);

						print( date("Y-m-d H:i:s")." | ".$jum." | ".$subject." | ".$session_id." | ".$row['msisdn']." | ".$push_price." | ".substr($reply_sms, 0, 160)."\n" );

						$jum ++;

						if(!$is_debug) create_mt($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$row['media_id'],$row['partner_id'],$appid,$subject,$priority="99",$url="",$mt_type=1,$url_dr,$row['shortname'],$reply_sms);
						else create_mt_debug($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$row['media_id'],$row['partner_id'],$appid,$subject,$priority="99",$url="",$mt_type=1,$url_dr,$row['shortname'],$reply_sms);

					}

				}

			}
			else { // Push Sequential
				$info_id = get_info_item($row['msisdn'],$q,$row['shortcode']);
				$content = get_next_info($info_sid, $info_id['info_id']);

				if( $content['info'] == "" ) continue;

				$reply_sms = $content['info'].$add_reply.$msg_footer[$row['telco']];

				insert_info_user($row['msisdn'], $appid, $row['shortcode'], $content['info_id']);

				$tag_info = get_push_flag($appid, $row['msisdn'], $row['shortcode'], $_SERVER["argv"][2]);
				if( !$tag_info ) {
					$push_seq = 1;
				} else {
					$push_seq = $tag_info + 1;
				}

				$push_info_flag = cek_push_flag($appid, $row['msisdn'], $row['shortcode'], $push_title, $push_seq, $row['telco']);
				if($push_info_flag) continue;

				if( $opr_conf[ $row["telco"] ]["i"] >= $push_seq ) {
					$push_price = $opr_conf[ $row["telco"] ]["pi"];

					insert_push_flag($appid,$row['msisdn'],$row['shortcode'],strtolower($push_title),$push_seq);

					$trx_date = date("YmdHis");
					$subject = "PUSH;IOD;".strtoupper($iod).";DAILYPUSH";
					$session_id = date("YmdHis").$row['msisdn'].rand(10000,99999);

					print( date("Y-m-d H:i:s")." | ".$subject." | ".$session_id." | ".$row['msisdn']." | ".$push_price." | ".substr($reply_sms, 0, 160)."\n" );

					if(!$is_debug) create_mt($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$row['media_id'],$row['partner_id'],$appid,$subject,$priority="99",$url="",$mt_type=1,$url_dr,$row['shortname'],$reply_sms);
					else create_mt_debug($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$row['media_id'],$row['partner_id'],$appid,$subject,$priority="99",$url="",$mt_type=1,$url_dr,$row['shortname'],$reply_sms);

				}

			}

		}

		if( !$is_member_available ) print("Push Tagging : SERVICE ".$appid." NO MEMBER \n");

	}
	else print("Push Tagging : NO FILE iod_".$appid.".php \n");

}

if( !$is_available_appid ) print("Push Tagging : NO APPID \n");

print("Push Tagging -- ".$push_title." -- ".date("Y-m-d H:i:s")." -- End\n");

?>
