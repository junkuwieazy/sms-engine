<?php

// /usr/bin/php /var/www/sms/app/push_config/daily_push_info.php 06 info >> /var/www/sms/app/logs/push/daily_push_info_6.log
// /usr/bin/php /var/www/sms/app/push_config/daily_push_info.php 07 info >> /var/www/sms/app/logs/push/daily_push_info_7.log
// /usr/bin/php /var/www/sms/app/push_config/daily_push_info.php 08 info >> /var/www/sms/app/logs/push/daily_push_info_8.log

set_time_limit(0);

$path = "/var/www/sms/app/";

$jum = 0;

require_once( $path."include/config_app.php" );
require_once( $config['base_path']."/include/functions.php" );
require_once( $config['base_path']."/include/mysql.class.php" );

$push_time = $_SERVER['argv'][1];

$push_title = "Info";

$is_debug = false;
if( !empty($_SERVER['argv'][3]) && $_SERVER['argv'][3] == "debug" ) $is_debug = true;

print("Push Tagging RETRY -- ".$push_title." -- ".date("Y-m-d H:i:s")." -- Start\n");

$db = new DB($config['dbHost'], $config['dbUser'], $config['dbPass'], $config['dbName']);

$today_day = strtolower(date("j"));

$is_available_appid = false;
$sqla = "SELECT appid,retry_type FROM mp_system.application WHERE is_retry = 1 AND retry_time = '".$push_time."' AND status = 1 AND retry_day like '%,".$today_day.",%'";
print("Push Tagging : Push Time ".$push_time." , Push Day : ".$today_day." , sql : ".$sqla." \n");
$rsa = $db->query($sqla);
while ($rowa = $db->fetch($rsa)){

  $appid = $rowa["appid"];
	$is_available_appid = true;

  if( strtolower($rowa['retry_type']) == "today" ) { // retry same day
    $sql = "SELECT msisdn,subject,message,telco,shortcode,charge,media_id,partner_id,shortname FROM mp_system.mt_outgoing_log_".date("Ymd")." WHERE appid='$appid' AND subject like '%DAILYPUSH' AND dn_telco='101@#@0000000000003101'";
    $subj = "RETRY";
  }
  else { // retry the day after
    $date	= mktime(0,0,0,date("m"),date("d")-1,date("Y"));
    $yesterday	= date("Ymd",$date);

    $sql = "SELECT msisdn,subject,message,telco,shortcode,charge,media_id,partner_id,shortname,url_dr FROM mp_system.mt_outgoing_log_".$yesterday." WHERE appid='$appid' AND subject like '%DAILYPUSH' AND dn_telco='101@#@0000000000003101'";
    $subj = "RETRY;H-1";
  }

  $rs = mysql_query($sql);
  $num = mysql_num_rows($rs);
  if( $num > 0 ) {
    while( $row = mysql_fetch_assoc($rs) ) {

      $push_price = $row['charge'];
      $trx_date = date("YmdHis");
      $subject = $row['subject'].";".$subj;
      $session_id = date("YmdHis").$row['msisdn'].rand(10000,99999);
      $reply_sms = $row['message'];

      print( date("Y-m-d H:i:s")." | ".$jum." | ".$subject." | ".$session_id." | ".$row['msisdn']." | ".$push_price." | ".substr($reply_sms, 0, 160)."\n" );

      $jum ++;

      if(!$is_debug) create_mt($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$row['media_id'],$row['partner_id'],$appid,$subject,$priority="99",$url="",$mt_type=1,$row['url_dr'],$row['shortname'],$reply_sms);
      else create_mt_debug($mo_origin="",$row['msisdn'],$prepaid=0,$sms="",$row['telco'],$row['shortcode'],$trx_id="",$trx_date,$session_id,$push_price,$row['media_id'],$row['partner_id'],$appid,$subject,$priority="99",$url="",$mt_type=1,$row['url_dr'],$row['shortname'],$reply_sms);

    }
  }

}

if( !$is_available_appid ) print("Push Tagging : NO APPID \n");

print("Push Tagging RETRY -- ".$push_title." -- ".date("Y-m-d H:i:s")." -- End\n");

?>
