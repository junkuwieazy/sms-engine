<?php

if(!defined('MO_ACCESS')) die("Direct access is not permitted");

// CUTE2 Processor
// June 2013, Muji

$keyword = "CUTE2";
$appid = 10011;
$info_sid = 10011;
$media_id = 0;
$partner_id = 0;
$priority = 99;
$url = "";
$mt_type = 1; // 1 = SMS Text , 2 = WAPPUSH , 3 = Smart Messaging
$url_dr = "";
$sub_iod = "IOD";
$sub_keyword = "";

switch(strtolower($iod[0])) {
	case "reg" :

	$arMT[$arMT_idx]['charge']	= "PULL-REG";
	$arMT[$arMT_idx]['media_id']	= $media_id;
	$arMT[$arMT_idx]['partner_id']	= $partner_id;
	$arMT[$arMT_idx]['appid']	= $appid;
	$arMT[$arMT_idx]['subject']	= "REG;".strtoupper($sub_iod).";".strtoupper($keyword);
	$arMT[$arMT_idx]['priority']	= $priority;
	$arMT[$arMT_idx]['url']	= $url;
	$arMT[$arMT_idx]['mt_type']	= $mt_type;
	$arMT[$arMT_idx]['url_dr']	= $url_dr;
	if($telco == "xl") {
		switch(strtolower($mo_origin)) {
			case "umb" : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
			case "wap" : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
			default : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
		}
	}
	else $arMT[$arMT_idx]['shortname']	= "cute2_95799";

	$already_subs = cek_subscriber($msisdn,$appid,$shortcode);
	if($already_subs == "ok") {
		$arMT[$arMT_idx]['reply_sms'] = "Terima kasih anda masih berlangganan layanan ".strtoupper($keyword)." dari PT Permata Cipta Rejeki.";
	}
	else {

		insert_reg_player($msisdn,$appid,$prepaid,$telco,$shortcode,$keyword,$sub_keyword,$arMT[$arMT_idx]['shortname'],$session_id,$media_id,$partner_id);

		$sqli = "INSERT INTO app_sms.msisdn_hourly_reg (appid,tanggal,jam,jumlah,entry_date) VALUES ('".$appid."','".date("Y-m-d")."','".date("H")."',1,NOW()) ON DUPLICATE KEY UPDATE jumlah = jumlah+1";
		$db->query($sqli);

			if( count($iod) > 2 ) { // reg masak 32432432

				$url_mo = "http://clickmobile.id/callback/trace.php?transaction_id=".$iod[2];

				$dataair = file_get_contents( $url_mo );

			}

			$arMT[$arMT_idx]['reply_sms'] = "Terima kasih anda telah berlangganan layanan ".strtoupper($keyword)." dari PT Permata Cipta Rejeki. 3sms/mgg,2rb/sms. Stop unreg $keyword ke $shortcode.".$config['cs_number'];

			$push_info_flag = cek_push_flag($appid, $msisdn, $shortcode, "info", $seq="1", $telco);
			if(!$push_info_flag) {


				// GET PIN
				$a = file_get_contents("http://video.sukaklik.id/index.php/incoming?msisdn=".$msisdn);

				$b = explode('|', $a);

				$validData = explode('-', $b[2]);
				$tgl = $validData[2];
				$bln = $validData[1];
				$thn = $validData[0];


				$content = "Ini adalah PIN kamu : ".$b[0]." berlaku sampai ".$tgl."-".$bln."-".$thn.". Lihat video nya di http://video.sukaklik.id  Stop:UNREG VGOKIL ke 99350. ";


				// $content = "Ini adalah PIN kamu : 8642830 berlaku sampai 01/11/2017. Lihat video nya di http://video.sukaklik.id  Stop:UNREG VGOKIL ke 99350. ";

				if($content <> '') {
					$arMT_idx++;
					$arMT[$arMT_idx]['mo_origin']	= "";
					$arMT[$arMT_idx]['msisdn']	= $msisdn;
					$arMT[$arMT_idx]['prepaid']	= $prepaid;
					$arMT[$arMT_idx]['sms']	= "";
					$arMT[$arMT_idx]['telco']	= $telco;
					$arMT[$arMT_idx]['shortcode']	= $shortcode;
					$arMT[$arMT_idx]['trx_id']	= "";
					$arMT[$arMT_idx]['trx_date']	= "";
					$session_id_push_info = get_session_id($msisdn);
					$arMT[$arMT_idx]['session_id']	= $session_id_push_info;
					$arMT[$arMT_idx]['charge']	= "PUSH-2000";
					$arMT[$arMT_idx]['media_id']	= $media_id;
					$arMT[$arMT_idx]['partner_id']	= $partner_id;
					$arMT[$arMT_idx]['appid']	= $appid;
					$arMT[$arMT_idx]['subject']	= "PUSH;".strtoupper($sub_iod).";".strtoupper($keyword).";FIRSTPUSH";
					$arMT[$arMT_idx]['priority']	= $priority;
					$arMT[$arMT_idx]['url']	= $url;
					$arMT[$arMT_idx]['mt_type']	= $mt_type;
					$arMT[$arMT_idx]['url_dr']	= $url_dr;
					if($telco == "xl") {
						switch(strtolower($mo_origin)) {
							case "umb" : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
							case "wap" : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
							default : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
						}
					}
					else $arMT[$arMT_idx]['shortname']	= "cute2_95799";

					insert_push_flag($appid,$msisdn,$shortcode,"info",1);

					$arMT[$arMT_idx]['reply_sms'] = $content;

				}
			}

		}
		break;
		case "unreg" :
		case "unrek" :
		case "stop" :
		case "berhenti" :

		$arMT[$arMT_idx]['charge']	= "PULL-UNREG";
		$arMT[$arMT_idx]['media_id']	= $media_id;
		$arMT[$arMT_idx]['partner_id']	= $partner_id;
		$arMT[$arMT_idx]['appid']	= $appid;
		$arMT[$arMT_idx]['subject']	= "UNREG;".strtoupper($sub_iod).";".strtoupper($keyword);
		$arMT[$arMT_idx]['priority']	= $priority;
		$arMT[$arMT_idx]['url']	= $url;
		$arMT[$arMT_idx]['mt_type']	= $mt_type;
		$arMT[$arMT_idx]['url_dr']	= $url_dr;
		if($telco == "xl") {
			switch(strtolower($mo_origin)) {
				case "umb" : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
				case "wap" : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
				default : $arMT[$arMT_idx]['shortname']	= "0031492000009791"; break;
			}
		}
		else $arMT[$arMT_idx]['shortname']	= "cute2_95799";

		unreg_player($msisdn,$appid,$shortcode,$session_id);
		clear_push_flag($appid,$msisdn,$shortcode);

		$sqli = "INSERT INTO app_sms.msisdn_hourly_unreg (appid,tanggal,jam,jumlah,entry_date) VALUES ('".$appid."','".date("Y-m-d")."','".date("H")."',1,NOW()) ON DUPLICATE KEY UPDATE jumlah = jumlah+1";
		$db->query($sqli);

		$arMT[$arMT_idx]['reply_sms'] = "Anda sudah tidak terdaftar dalam layanan ".$keyword.".Terima kasih.";

		break;
	}
	?>
