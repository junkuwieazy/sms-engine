<?php

if(!defined('MO_ACCESS')) die("Direct access is not permitted");

// STOP Processor
// November 2008, Muji

$keyword = "STOP";
$appid = 10011;
$info_sid = 10011;
$media_id = 0;
$partner_id = 0;
$priority = 99;
$url = "";
$mt_type = 1; // 1 = SMS Text , 2 = WAPPUSH , 3 = Smart Messaging
$url_dr = "";
$sub_iod = "IOD";
$sub_keyword = "";

if($shortcode == "9117") {
	switch(strtolower($iod[0])) {
		case "stop" :
		case "unreg" :
		case "batal" :
		case "berhenti" :
		case "unsub" :
			$arMT[$arMT_idx]['charge']	= "PL-0";
			$arMT[$arMT_idx]['media_id']	= $media_id;
			$arMT[$arMT_idx]['partner_id']	= $partner_id;
			$arMT[$arMT_idx]['appid']	= $appid;
			$arMT[$arMT_idx]['subject']	= "PULL;".strtoupper($sub_iod).";".strtoupper($keyword);
			$arMT[$arMT_idx]['priority']	= $priority;
			$arMT[$arMT_idx]['url']	= $url;
			$arMT[$arMT_idx]['mt_type']	= $mt_type;
			$arMT[$arMT_idx]['url_dr']	= $url_dr;
			if($telco == "xl") {
				switch(strtolower($mo_origin)) {
					case "umb" : $arMT[$arMT_idx]['shortname']	= strtolower($keyword)."2"; break;
					case "wap" : $arMT[$arMT_idx]['shortname']	= strtolower($keyword)."3"; break;
					default : $arMT[$arMT_idx]['shortname']	= strtolower($keyword)."1"; break;
				}
			}
			else $arMT[$arMT_idx]['shortname']	= "";
			
			unreg_all_service($msisdn,$shortcode,$session_id);
			
			$content = "Anda sudah berhenti dari semua layanan 9117. Terima kasih";
			$arMT[$arMT_idx]['reply_sms'] = $content;
			
		break;
		case "off" :
			//$arMT[$arMT_idx]['charge']	= "PL-0";
			if( substr( $msisdn ,0,5 ) == "62811" ) $arMT[$arMT_idx]['charge']	= "PL-0";
			else $arMT[$arMT_idx]['charge']	= "PL-0";
			$arMT[$arMT_idx]['media_id']	= $media_id;
			$arMT[$arMT_idx]['partner_id']	= $partner_id;
			$arMT[$arMT_idx]['appid']	= $appid;
			$arMT[$arMT_idx]['subject']	= "PULL;".strtoupper($sub_iod).";".strtoupper($keyword);
			$arMT[$arMT_idx]['priority']	= $priority;
			$arMT[$arMT_idx]['url']	= $url;
			$arMT[$arMT_idx]['mt_type']	= $mt_type;
			$arMT[$arMT_idx]['url_dr']	= $url_dr;
			if($telco == "xl") {
				switch(strtolower($mo_origin)) {
					case "umb" : $arMT[$arMT_idx]['shortname']	= strtolower($keyword)."2"; break;
					case "wap" : $arMT[$arMT_idx]['shortname']	= strtolower($keyword)."3"; break;
					default : $arMT[$arMT_idx]['shortname']	= strtolower($keyword)."1"; break;
				}
			}
			else $arMT[$arMT_idx]['shortname']	= "";
			
			unreg_all_service($msisdn,$shortcode,$session_id);
			
			$content = "Anda sudah berhenti dari semua layanan 9117. Terima kasih";
			$arMT[$arMT_idx]['reply_sms'] = $content;
			
		break;
	}
}

?>