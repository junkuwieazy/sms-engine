<?php

$config['dbHost']		= 'localhost';
$config['dbUser']		= 'smsgw';
$config['dbPass']		= 'DBt3rb41k';
$config['dbName']		= 'app_sms';
$config['dbPort']		= '3306';
$config['dbType']		= 'mysql';

$title_header = ":::- CMS TEXT ADMIN -:::";
$title = "::: CMS TEXT - ADMIN :::";
$title_header_content = ":::- CMS CONTENT ADMIN -:::";
$title_content = "::: CMS CONTENT - ADMIN :::";
$copyright = "copyright &copy; ".date("Y")." - Mobiwin. Some Right Reserved";

$len_content = 160;

$url_path = "http://103.29.214.124:8319/sms/app/cms_text/";

$config['base_path']	= "/var/www/sms/app";

$config['cs_number']	= "CS:087889303034";

$config['allow_ip'] = array(
						"localhost", "127.0.0.1" , "::1" , "192.168.1.54", "192.168.1.222" ,"202.43.73.202", "202.43.73.203", "202.43.73.204", "202.43.73.205", "202.43.73.206",
						"202.43.73.250", "202.43.73.251", "202.43.73.252", "202.43.73.253", "202.43.73.254","122.248.32.26", "103.29.214.183", "103.29.214.133", "103.29.214.195",
						"103.29.214.114","103.29.214.115","103.29.214.116","103.29.214.117","103.29.214.118", "202.149.67.82", "202.149.67.83", "103.29.214.205"
						);

$config['cross_sell']['tsel']	= "";
$config['cross_sell']['xl']	= "";
$config['cross_sell']['isat']	= "";

$config['share'] = array('tsel' => 0.3 , 'isat' => 0.5 , 'xl' => 0.4 , 'flexi' => 0.5 , 'esia' => 0.5 , 'mob8' => 0.5 , 'hutch' => 0.5 , 'axis' => 0.5 , 'galaksi' => 0.3);

$config['url_gw']['pull']['xl']['95799']	= "http://localhost:8319/sms/engine/momt/mt_spooler/sms_gateway_pull_xl.php";
$config['url_gw']['push']['xl']['95799']	= "http://localhost:8319/sms/engine/momt/mt_spooler/sms_gateway_push_xl.php";
$config['url_gw']['pull']['xl']['95899']	= "http://localhost:8319/sms/engine/momt/mt_spooler/sms_gateway_pull_xl.php";
$config['url_gw']['push']['xl']['95899']	= "http://localhost:8319/sms/engine/momt/mt_spooler/sms_gateway_push_xl.php";

$config['spool_mo_driver']	= array ( 1=>"/spool/mo_driver/1",2=>"/spool/mo_driver/2",3=>"/spool/mo_driver/3",4=>"/spool/mo_driver/4",5=>"/spool/mo_driver/5",6=>"/spool/mo_driver/6",7=>"/spool/mo_driver/7",8=>"/spool/mo_driver/8",9=>"/spool/mo_driver/9",10=>"/spool/mo_driver/10" );

$config['spool_mo_driver_path']	= $config['base_path']."/spool/mo_driver/";
$config['spool_mt_driver_path']	= $config['base_path']."/spool/mt_driver/";

$config['url_app']	= "http://localhost:8319/sms/app/mo_processor.php";

$config['mo_driver_monitoring_path']	= $config['base_path']."/monitoring/mo_driver/";
$config['mt_driver_monitoring_path']	= $config['base_path']."/monitoring/mt_driver/";

?>
