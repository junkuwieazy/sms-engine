<?php

function get_media_id($media_id) {
	global $db;

	$media = 0;

	$sql = "SELECT media_id FROM app_sms.media WHERE media_code='".$media_id."'";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)){
		return $row['media_id'];
	}
	else return $media;

}

function get_keyword($msisdn,$shortcode,$telco) {
	global $db;

	$keyword = "";

	$sql = "SELECT id,keyword,sub_keyword FROM app_sms.msisdn_session WHERE msisdn='$msisdn' AND shortcode='$shortcode' AND telco='$telco' ORDER BY entry_date ASC LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)){

		$balikan = array("keyword" => $row['keyword'] , "sub_keyword" => $row['sub_keyword'] );

		$sql = "UPDATE app_sms.msisdn_session SET entry_date = NOW() WHERE msisdn = '".$msisdn."' AND telco = '".$telco."' AND shortcode = '".$shortcode."'";
		$db->query($sql);

	}
	else $balikan = "";

	return $balikan;
}

function cek_push_flag($appid, $msisdn, $shortcode, $type, $seq="1", $telco="tsel") {
	global $db;

	if ( $telco == "tsel" ) {
		if ( $telco == "tsel" && $appid == "10016" ) {
			$sql = "SELECT push_seq FROM app_sms.push_flag WHERE appid='$appid' AND msisdn='$msisdn' AND shortcode='$shortcode' AND type='$type' AND push_date='".date("Y-m-d")."' AND push_seq=$seq";
			$rs = $db->query($sql);
			if ($row = $db->fetch($rs)){
				return $row['push_seq'];
			}
			else return false;
		}
		else {
			$sql = "SELECT count(1) as jml FROM app_sms.push_flag WHERE msisdn='$msisdn' AND shortcode='$shortcode' AND push_date='".date("Y-m-d")."'";
			$rs = $db->query($sql);
			if ($row = $db->fetch($rs)){
				if ( $row['jml'] >= 2 ) return true;
				else return false;
			}
			else return false;
		}

	}
	else {
		$sql = "SELECT push_seq FROM app_sms.push_flag WHERE appid='$appid' AND msisdn='$msisdn' AND shortcode='$shortcode' AND type='$type' AND push_date='".date("Y-m-d")."' AND push_seq=$seq";
		$rs = $db->query($sql);
		if ($row = $db->fetch($rs)){
			return $row['push_seq'];
		}
		else return false;
	}

}
/*
function cek_push_flag($appid, $msisdn, $shortcode, $type, $seq="1") {
	global $db;

	$sql = "SELECT push_seq FROM app_sms.push_flag WHERE appid='$appid' AND msisdn='$msisdn' AND shortcode='$shortcode' AND type='$type' AND push_date='".date("Y-m-d")."' AND push_seq=$seq";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)){
		return $row['push_seq'];
	}
	else return false;

}
*/

function date_different($interval, $datefrom, $dateto, $using_timestamps = false) {
	if (!$using_timestamps) {
		$datefrom = strtotime($datefrom, 0);
		$dateto = strtotime($dateto, 0);
	}
	$difference = $dateto - $datefrom; // Difference in seconds

	switch($interval) {

	case 'yyyy': // Number of full years

		$years_difference = floor($difference / 31536000);
		if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
		  $years_difference--;
		}
		if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
		  $years_difference++;
		}
		$datediff = $years_difference;
		break;

	case "q": // Number of full quarters

		$quarters_difference = floor($difference / 8035200);
		while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
		  $months_difference++;
		}
		$quarters_difference--;
		$datediff = $quarters_difference;
		break;

	case "m": // Number of full months

		$months_difference = floor($difference / 2678400);
		while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
		  $months_difference++;
		}
		$months_difference--;
		$datediff = $months_difference;
		break;

	case 'y': // Difference between day numbers

		$datediff = date("z", $dateto) - date("z", $datefrom);
		break;

	case "d": // Number of full days

		$datediff = floor($difference / 86400);
		break;

	case "w": // Number of full weekdays

		$days_difference = floor($difference / 86400);
		$weeks_difference = floor($days_difference / 7); // Complete weeks
		$first_day = date("w", $datefrom);
		$days_remainder = floor($days_difference % 7);
		$odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
		if ($odd_days > 7) { // Sunday
		  $days_remainder--;
		}
		if ($odd_days > 6) { // Saturday
		  $days_remainder--;
		}
		$datediff = ($weeks_difference * 5) + $days_remainder;
		break;

	case "ww": // Number of full weeks

		$datediff = floor($difference / 604800);
		break;

	case "h": // Number of full hours

		$datediff = floor($difference / 3600);
		break;

	case "n": // Number of full minutes

		$datediff = floor($difference / 60);
		break;

	default: // Number of full seconds (default)

		$datediff = $difference;
		break;
	}

	return $datediff;
}

function get_push_flag($appid, $msisdn, $shortcode, $type) {
	global $db;

	$sql = "SELECT push_seq FROM app_sms.push_flag WHERE appid='$appid' AND msisdn='$msisdn' AND shortcode='$shortcode' AND type='$type' AND push_date='".date("Y-m-d")."' ORDER BY push_seq DESC LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)){
		return $row['push_seq'];
	}
	else return false;

}

function insert_push_flag($appid,$msisdn,$shortcode,$type="info",$push_seq=1) {
	global $db;

	$sql2 = "INSERT INTO app_sms.push_flag (appid,msisdn,shortcode,type,push_date,push_seq,datetime)
					VALUES ('$appid','$msisdn','$shortcode','$type',NOW(),$push_seq,NOW())";
	$db->query($sql2);
}

function insert_session($appid,$msisdn,$shortcode,$telco,$keyword,$sub_keyword) {
	global $db;

	$sqlcek = "SELECT msisdn FROM app_sms.msisdn_session WHERE msisdn = '".$msisdn."' AND telco = '".$telco."' AND shortcode = '".$shortcode."'";
	$rscek = $db->query($sqlcek);
	if ($rowcek = $db->fetch($rscek)) {

		$sql = "UPDATE app_sms.msisdn_session SET appid='$appid', keyword='".$keyword."', sub_keyword='".$sub_keyword."', entry_date = NOW() WHERE msisdn = '".$msisdn."' AND telco = '".$telco."' AND shortcode = '".$shortcode."'";
		$db->query($sql);
	}
	else {
		$sql2 = "INSERT INTO app_sms.msisdn_session (appid,msisdn,shortcode,telco,keyword,sub_keyword,entry_date)
				VALUES ('$appid','$msisdn','$shortcode','$telco','$keyword','$sub_keyword',NOW())";
		$db->query($sql2);
	}
}

function clear_push_flag($appid,$msisdn,$shortcode) {
	global $db;

	$sql2 = "DELETE FROM app_sms.push_flag WHERE msisdn='$msisdn' AND appid = '$appid' AND shortcode='$shortcode'";
	$db->query($sql2);
}

function clear_push_flag_all($msisdn,$shortcode) {
	global $db;

	$sql2 = "DELETE FROM app_sms.push_flag WHERE msisdn='$msisdn' AND shortcode='$shortcode'";
	$db->query($sql2);
}

function get_session_id($msisdn) {
	return $msisdn.date("YmdHis").rand(1000,9999).rand(1000,9999);
}

function get_content_info($appid, $cat, $seq){
	global $db;

	if($cat <> "") $w = " AND category='".$cat."'";
	else $w = "";

	$sql = "SELECT content FROM app_sms.content_text WHERE appid='$appid' $w and status=1 and publish_date='".date("Y-m-d")."' AND publish_seq = $seq LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)){
		return $row['content'];
	}
	else {
		return get_content_info_random($appid, $cat, $seq);
	}

}

function get_content_info_random($appid, $cat, $seq){
	global $db;

	if($cat <> "") $w = " AND category='".$cat."'";
	else $w = "";

	$sql = "SELECT content FROM app_sms.content_text WHERE appid='$appid' $w and status=1 ORDER BY RAND() LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)){
		return $row['content'];
	}
	else {
		return "";
	}

}

function push_alert($iod, $appid, $type, $push_seq="") {
	global $db;

	$sql2 = "INSERT INTO app_sms.push_content_alert (appid,keyword,alert_type,datetime)
					VALUES ('$appid','$iod','$type',NOW())";
	$db->query($sql2);
}

function cek_subscriber($msisdn,$appid,$shortcode) {
	global $db;

	$sql = "SELECT * FROM app_sms.msisdn_subscriber WHERE msisdn = '".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		if($row['status'] == 1) return "ok";
		else return "nok";
	}
	else return "nok";

}

function get_point($msisdn,$appid,$shortcode) {
	global $db;

	$sql = "SELECT total_point FROM app_sms.quiz_user WHERE msisdn='".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return $row['total_point'];
	}
	else return 0;
}

function get_pin($msisdn,$appid,$shortcode) {
	global $db;

	$sql = "SELECT count(1) as jml FROM app_sms.member_pin WHERE msisdn='".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return $row['jml'];
	}
	else return 0;
}

function GenRandom($len = 8, $type = 'alnum') {
	// define ASCII ranges...
	// See http://www.keller.com/html-quickref/latin1.html
	$ranges	  = array
	(
		'alpha' => array (
					1 => array(97, 122), // a-z (lowercase)
					2 => array(65, 90)	// A-Z (uppercase)
				 ),
		'num'	=> array (
					1 => array(48, 57)	// 0-9 (numeral)
				 ),
		'alnum' => array (
					1 => array(97, 122), // a-z (lowercase)
					2 => array(65, 90),	 // A-Z (uppercase)
					3 => array(48, 57)	// 0-9 (numeral)
				 )
	);

	$rand_key = "";
	for ($i=0; $i<$len; $i++)
	{
		$r = mt_rand(1,count($ranges[$type]));
		$rand_key .= chr(mt_rand($ranges[$type][$r][0], $ranges[$type][$r][1]));
	}
	return $rand_key;
}

function insert_pin($msisdn,$appid,$telco,$shortcode,$pin) {
	global $db;

	$sql2 = "INSERT INTO app_sms.member_pin (appid,msisdn,telco,shortcode,pin,datetime)
					VALUES ('$appid','$msisdn','$telco','$shortcode','$pin',NOW())";
	$db->query($sql2);
}

function get_quiz_item($msisdn,$appid,$shortcode) {
	global $db;

	$sql = "SELECT * FROM app_sms.quiz_user WHERE msisdn='".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("quiz_item" => $row['quiz_item'] , "total_point" => $row['total_point'] , "j_answer" => $row['j_answer'] );
	}
	else array("quiz_item" => 0 , "total_point" => 0 , "j_answer" => 0 );
}

function get_info_item($msisdn,$appid,$shortcode) {
	global $db;

	$sql = "SELECT * FROM app_sms.info_user WHERE msisdn='".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("info_id" => $row['info_id'] );
	}
	else array("info_id" => 0 );
}

function get_answer($quiz_id) {
	global $db;

	$sql = "SELECT * FROM app_sms.content_quiz WHERE id=".$quiz_id;
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("answer" => $row['answer'] , "point_true" => $row['point_true'] , "point_false" => $row['point_false'] );
	}
}

function get_next_quiz($quiz_sid, $quiz_item) {
	global $db;

	$quiz_id_next = $quiz_item + 1;

	$sql = "SELECT id,question FROM app_sms.content_quiz WHERE status = 1 AND appid='".$quiz_sid."' AND publish_seq = ".$quiz_id_next." LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("quiz_id" => $quiz_id_next , "question" => $row['question'] );
	}
	else return array("quiz_id" => "" , "question" => "" );
}

function get_today_quiz($quiz_sid, $group = "") {
	global $db;

	if( $group <> "0000-00-00" ) $str_sql = " AND publish_date = '".$group."'";
	else $str_sql = "";

	$sql = "SELECT id,question FROM app_sms.content_quiz WHERE status = 1 AND appid='".$quiz_sid."' $str_sql ORDER BY RAND() LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("quiz_id" => $row['id'] , "question" => $row['question'] );
	}
	else return array("quiz_id" => "" , "question" => "" );
}

function get_content_info_seq($info_sid, $msisdn, $shortcode) {
	global $db;

	$sqlc = "SELECT info_id FROM app_sms.info_user WHERE msisdn='".$msisdn."' AND appid='".$info_sid."' AND shortcode='".$shortcode."'";
	$rsc = $db->query($sqlc);
	if ($rowc = $db->fetch($rsc)) {

		$info = get_next_info($info_sid, $rowc['info_id'] - 1);

		return array("info_id" => $info['info_id'] , "info" => $info['info'] );
	}
	else {

		$first_info = get_first_info($info_sid);

		return array("info_id" => $first_info['info_id'] , "info" => $first_info['info'] );
	}

}

function get_first_info($info_sid) {
	global $db;

	$sql = "SELECT id,content FROM app_sms.content_text WHERE status = 1 AND appid='".$info_sid."' AND publish_seq = 1 LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("info_id" => "1" , "info" => $row['content'] );
	}
	else return array("info_id" => "1" , "info" => "" );
}

function get_next_info($info_sid, $info_item) {
	global $db;

	$info_id_next = $info_item + 1;

	$sql = "SELECT id,content FROM app_sms.content_text WHERE status = 1 AND appid='".$info_sid."' AND publish_seq = ".$info_id_next." LIMIT 1";
	$rs = $db->query($sql);
	if ($row = $db->fetch($rs)) {
		return array("info_id" => $info_id_next , "info" => $row['content'] );
	}
	else return array("info_id" => "1" , "info" => "" );
}

function get_today_quiz_seq($quiz_sid, $msisdn, $shortcode) {
	global $db;

	$sqlc = "SELECT quiz_item FROM app_sms.quiz_user WHERE msisdn='".$msisdn."' AND appid='".$quiz_sid."' AND shortcode='".$shortcode."'";
	$rsc = $db->query($sqlc);
	if ($rowc = $db->fetch($rsc)) {

		$quiz = get_next_quiz($quiz_sid, $rowc['quiz_item'] - 1);

		return array("quiz_id" => $quiz['quiz_id'] , "question" => $quiz['question'] );
	}
	else {
		return array("quiz_id" => "1" , "question" => "Mau MOTOR VARIO, BB, TV LCD dan PSP? Isi ya survey berikut ini. Ktk: 1.Nama 2.Usia 3.L/P Cth:R(spasi)Muji#25#L krm ke 9117" );
	}

}

function insert_quiz_user($msisdn, $appid, $shortcode, $point, $quiz_id) {
	global $db;

	$sqlc = "SELECT msisdn FROM app_sms.quiz_user WHERE msisdn='".$msisdn."' AND appid='".$appid."' AND shortcode='".$shortcode."'";
	$rsc = $db->query($sqlc);
	if ($rowc = $db->fetch($rsc)) {
		$sql = "UPDATE app_sms.quiz_user SET quiz_item='$quiz_id', entry_date=NOW(), j_answer = 0 WHERE msisdn='".$msisdn."' AND appid='".$appid."' AND shortcode='".$shortcode."'";
		$db->query($sql);
	}
	else {
		$sql = "INSERT INTO app_sms.quiz_user (appid,msisdn,shortcode,total_point,quiz_item,entry_date,j_answer)
				VALUES ('".$appid."','".$msisdn."','".$shortcode."',".$point.",'".$quiz_id."',NOW(),0)";
		$db->query($sql);
	}

}

function insert_info_user($msisdn, $appid, $shortcode, $info_id) {
	global $db;

	$sqlc = "SELECT msisdn FROM app_sms.info_user WHERE msisdn='".$msisdn."' AND appid='".$appid."' AND shortcode='".$shortcode."'";
	$rsc = $db->query($sqlc);
	if ($rowc = $db->fetch($rsc)) {
		$sql = "UPDATE app_sms.info_user SET info_id='$info_id', entry_date=NOW() WHERE msisdn='".$msisdn."' AND appid='".$appid."' AND shortcode='".$shortcode."'";
		$db->query($sql);
	}
	else {
		$sql = "INSERT INTO app_sms.info_user (appid,msisdn,shortcode,info_id,entry_date)
				VALUES ('".$appid."','".$msisdn."','".$shortcode."','".$info_id."',NOW())";
		$db->query($sql);
	}

}

function update_quiz_user($msisdn, $appid, $shortcode, $point, $quiz_id) {
	global $db;

	$sql = "UPDATE app_sms.quiz_user SET total_point = ".$point." , quiz_item='$quiz_id', entry_date=NOW(), j_answer = j_answer + 1 WHERE msisdn='".$msisdn."' AND appid='".$appid."' AND shortcode='".$shortcode."'";
	$db->query($sql);

}

function insert_reg_player($msisdn,$appid,$prepaid,$telco,$shortcode,$keyword,$sub_keyword,$shortname,$session_id,$media_id,$partner_id) {
	global $db;

	$sqlcek = "SELECT status FROM app_sms.msisdn_subscriber WHERE msisdn = '".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rscek = $db->query($sqlcek);
	if ($rowcek = $db->fetch($rscek)) {
		if( $shortname <> "" ) {
			$sql__ = "shortname = '".$shortname."',";
			$sql___ = "date_reg = NOW(),";
		}
		else {
			$sql__ = "";
			$sql___ = "";
		}
		$sql = "UPDATE app_sms.msisdn_subscriber SET $sql__ $sql___ category = '".$sub_keyword."', session_id_reg = '".$session_id."', status = 1 WHERE msisdn = '".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
		$db->query($sql);
	}
	else {
		$sql = "INSERT INTO app_sms.msisdn_subscriber (appid,msisdn,telco,shortcode,keyword,shortname,category,session_id_reg,date_reg,media_id,partner_id,status)
						VALUES ('".$appid."','".$msisdn."','".$telco."','".$shortcode."','".$keyword."','".$shortname."','".$sub_keyword."','".$session_id."',NOW(),'".$media_id."','".$partner_id."',1)";
		$db->query($sql);
	}

	//$sql = "UPDATE app_sms.msisdn_subscriber SET status = 1 WHERE msisdn = '".$msisdn."' AND shortcode = '".$shortcode."' AND status = 2";
	//$db->query($sql);

}

function insert_data_player($msisdn,$appid,$shortcode) {
	global $db;

	$sqlcek = "SELECT msisdn FROM app_sms.msisdn_profile WHERE msisdn = '".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$rscek = $db->query($sqlcek);
	if ($rowcek = $db->fetch($rscek)) {

	}
	else {
		$sql = "INSERT INTO app_sms.msisdn_profile (appid,msisdn,shortcode,entry_date)
						VALUES ('".$appid."','".$msisdn."','".$shortcode."',NOW())";
		$db->query($sql);
	}
}

function unreg_player($msisdn,$appid,$shortcode,$session_id) {
	global $db;

	$sql = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id."', date_unreg = NOW(), status = 0, unreg_reason = '0' WHERE msisdn = '".$msisdn."' AND appid = '".$appid."' AND shortcode = '".$shortcode."'";
	$db->query($sql);
}

function unreg_all_service($msisdn,$shortcode,$session_id) {
	global $db;

	$sql = "UPDATE app_sms.msisdn_subscriber SET session_id_unreg = '".$session_id."', date_unreg = NOW(), status = 2, unreg_reason = '0' WHERE msisdn = '".$msisdn."' AND shortcode = '".$shortcode."'";
	$db->query($sql);
}

function insert_incoming_log_partner($appid,$mo_origin,$msisdn,$prepaid,$sms,$telco,$shortcode,$trx_id,$trx_date,$session_id,$partner_id) {
	global $db;

	$sql = "INSERT INTO mp_system.mo_incoming_partner_log".date("Ym")." (mo_origin,appid,msisdn,prepaid,sms,telco,shortcode,trx_id,trx_date,session_id,partner_id,datetime)
					VALUES ('".$mo_origin."','".$appid."','".$msisdn."','".$prepaid."','".$sms."','".$telco."','".$shortcode."','".$trx_id."','".$trx_date."','".$session_id."',".$partner_id.",NOW())";

	$db->query($sql);
}

function insert_outgoing_log_partner($appid,$mo_origin,$msisdn,$prepaid=0,$subject,$reply_sms,$url,$telco,$shortcode,$price,$mt_type,$trx_id,$trx_date,$session_id,$media_id=0,$partner_id,$ack_status=200,$url_dr,$shortname) {
	global $db;

	$sql = "INSERT INTO mp_system.mt_outgoing_partner_log".date("Ym")." (appid,mo_origin,msisdn,prepaid,subject,message,url,telco,shortcode,charge,mt_type,trx_id,trx_date,session_id,media_id,partner_id,ack_status,url_dr,datetime,shortname)
					VALUES ('".$appid."','".$mo_origin."','".$msisdn."','".$prepaid."','".$subject."','".$reply_sms."','".$url."','".$telco."','".$shortcode."','".$price."','".$mt_type."','".$trx_id."','".$trx_date."','".$session_id."','".$media_id."','".$partner_id."','".$ack_status."','".$url_dr."',NOW(),'".$shortname."')";
	$db->query($sql);
}

function create_mt_debug($mo_origin,$msisdn,$prepaid,$sms,$telco,$shortcode,$trx_id,$trx_date,$session_id,$charge,$media_id,$partner_id,$appid,$subject,$priority,$url,$mt_type,$url_dr,$shortname,$reply_sms) {
  global $config, $db;

  $sql = "INSERT INTO app_sms.debug_app (appid,msisdn,sms,subject,message,telco,shortcode,charge,trx_id,session_id,datetime)
  				VALUES ('$appid','$msisdn','$sms','$subject','".$reply_sms."','$telco','$shortcode','$charge','$trx_id','$session_id',NOW())";
	$db->query($sql);

}

function create_mt($mo_origin,$msisdn,$prepaid,$sms,$telco,$shortcode,$trx_id,$trx_date,$session_id,$charge,$media_id,$partner_id,$appid,$subject,$priority,$url,$mt_type,$url_dr,$shortname,$reply_sms) {
	  global $config, $db;

	  if( eregi("^PUSH",strtoupper($subject)) ) $spool = range (6,150); // PUSH
	  else $spool = range (1,5); // PULL
	  srand ((double)microtime()*1000000);
	  shuffle ($spool);

	  $sms = str_replace("'","",$sms);
	  $reply_sms = str_replace("'","",$reply_sms);
	  $reply_sms = substr($reply_sms,0,160);

	  $full_sms = $mo_origin."@#@".$msisdn."@#@".$prepaid."@#@".$sms."@#@".$telco."@#@".$shortcode."@#@".$trx_id."@#@".$trx_date."@#@".$session_id."@#@".$charge."@#@".$media_id."@#@".$partner_id."@#@".$appid."@#@".$subject."@#@".$priority."@#@".$url."@#@".$mt_type."@#@".$url_dr."@#@".$shortname."@#@".$reply_sms;

	$fp = fopen ($config['spool_mt_driver_path'].$spool[0]."/mt-".$session_id.".sms", "w+");
	fputs($fp, $full_sms );
	fclose($fp);

}

?>
