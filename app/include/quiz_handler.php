<?php
if(!defined('MO_ACCESS')) die("Direct access is not permitted");

$sql = "SELECT * FROM app_sms.quiz WHERE appid='$quiz_sid'";
$rs = $db->query($sql);
if ($row = $db->fetch($rs)) {
	if($row['status'] == 0) $reply_msg = "Maaf kuis sudah berakhir";
	else {
		
		$sms_footer = str_replace("[[SHORTCODE]]", $shortcode, $row['footer']);
    $sms_footer = str_replace("[[IOD]]", strtoupper($iod[0]), $sms_footer);
		
		switch(strtolower($iod)) {
			case "a" : case "b" : case "c" : case "d" :
				$played_items = get_played_item($msisdn,$quiz_sid,$shortcode);
				if(!in_array('',$played_items)) {
					$last_played_item = $played_items['items'][SizeOf($played_items['items'])-1];
					$_str_item = ($last_played_item) ? "id=".$last_played_item : 1;
					$quiz_item = get_quiz($quiz_sid,$_str_item);
					
				}
				else {
					$reply_msg = "Terima kasih. Jawaban sudah kami terima";
				}
			break;
		}
	}
}
else {
	$reply_msg = "Maaf kode kuis tersebut tidak ada";
}

function get_played_item($msisdn, $q_sid, $shortcode) {
  global $db;

  $sql = "SELECT total_point,quiz_item FROM app_sms.quiz_user WHERE appid=".$q_sid." AND msisdn='".$msisdn."' AND shortcode = '$shortcode' ORDER BY id DESC LIMIT 1";
  $rs = $db->query($sql);
  if ($row = $db->fetch($rs)) {
  	$played_item['point'] = $row['total_point'];
  	$played_item['items'] = unserialize($row['quiz_item']);
  }
  else {
  	$played_item['point'] = '';
    $played_item['items'] = '';
  }
  
  return $played_item;
}

function get_quiz($quiz_sid,$str_item='1') {
  global $db, $group;

  $groupSql = "";
  if(!empty($group)) $groupSql = "AND publish_date = '$group'";

  $sql = "SELECT id FROM content_quiz WHERE $str_item AND appid='$quiz_sid' AND status='1' $groupSql ORDER BY id ASC";
	
	$rs = $db->query($sql);
  if ($row = $db->fetch($rs)) {
  	$quiz_item['question'] = $row['total_point'];
  	$quiz_item['point'] = $row['total_point'];
  	$quiz_item['answer'] = $row['total_point'];
  	$quiz_item['answer'] = $row['total_point'];
  }
  else {
  	$quiz_item['point'] = '';
    $quiz_item['items'] = '';
  }
  
  return $played_item;
	
  $results = $db->get_results($sql, OBJECT);
  array_push($log_data,"GET_QUIZ: $sql","RESULT: ".$db->num_rows);
  if($db->num_rows > 0) {
    $item_id = array();
    foreach($results as $row) {
      array_push($item_id,$row->item_id);
    }

    $numbers = range (0,SizeOf($item_id)-1);
    srand ((double)microtime()*1000000);
    shuffle ($numbers);

    $sql = "SELECT * FROM quiz_item WHERE status='1' AND item_id=".$item_id[$numbers[0]];
	  // if ($quiz_id == 96) error_log("THR::".$sql);
    $quiz_item = $db->get_row($sql, OBJECT);
    array_push($log_data,"GET_QUIZ_ITEM: $sql","RESULT: ".$db->num_rows);

    $sql = "UPDATE quiz_item SET hits = hits + 1 WHERE item_id=".$quiz_item->item_id." AND quiz_id=$quiz_id";
    $db->query($sql);
    array_push($log_data,"UPD_QUIZ_ITEM: $sql","RESULT: ".$db->rows_affected);

    return $quiz_item;
  }
  else return FALSE;
}

?>